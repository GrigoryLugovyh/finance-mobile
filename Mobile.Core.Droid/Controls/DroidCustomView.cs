﻿using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Mobile.Core.BL.Core.Platform.Display;
using Mobile.Core.BL.IoCContainer;
using Mobile.Core.Droid.Binding;
using Mobile.Core.Droid.Extensions;
using Mobile.Core.Droid.Views;

namespace Mobile.Core.Droid.Controls
{
    public abstract class DroidCustomView<TValue> : LinearLayout, IDroidCustomView
    {
        protected readonly object sync = new object();
        private Activity activity;
        private IDroidBindingContext bindingContext;
        protected Random randomId;

        protected DroidCustomView(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
        {
            Init();
        }

        protected DroidCustomView(Context context) : base(context)
        {
            Init();
        }

        protected DroidCustomView(Context context, IAttributeSet attrs)
            : base(context, attrs)
        {
            Init();
        }

        protected DroidCustomView(Context context, IAttributeSet attrs, int defStyle)
            : base(context, attrs, defStyle)
        {
            Init();
        }

        protected bool Disposed { get; private set; }

        protected Activity Activity => activity ?? (activity = Context as Activity);

        protected IDroidBindingContext BindingContext
            =>
                bindingContext ??
                (bindingContext = Container.Resolve<IDroidBindingContextStack<IDroidView>>().Current.BindingContext());

        protected TValue DataValue { get; set; }

        protected int InDpUnits(int size)
        {
            return Container.Resolve<IMViewDimensions>().InDpUnits(size);
        }

        private void Init()
        {
            randomId = new Random();

            СonfigureLayout();
        }

        protected virtual void СonfigureLayout()
        {
            СonfigurePadding();
        }

        protected virtual void СonfigurePadding()
        {
            SetPadding(15, 15, 15, 15);
        }

        public virtual void Set(TValue value)
        {
            if (Disposed)
            {
                return;
            }

            lock (sync)
            {
                ClearViews();
                SetValue(value);
            }
        }

        protected abstract void SetValue(TValue value);

        protected virtual void ClearViews()
        {
            if (!Disposed)
            {
                RemoveAllViews();
            }
        }

        public virtual TValue Get()
        {
            return DataValue;
        }

        protected virtual void AddViewSafe(View view)
        {
            lock (sync)
            {
                if (view == null || Context == null)
                {
                    return;
                }

                var viewActivity = Context as Activity;

                if (viewActivity == null)
                {
                    AddView(view);
                    return;
                }

                viewActivity.RunOnUiThread(() => AddView(view));
            }
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            if (disposing)
            {
                ClearViews();
                Disposed = true;
            }
        }
    }
}