﻿using System;
using Android.Content;
using Android.Runtime;
using Android.Util;
using Android.Widget;
using Mobile.Core.Droid.Binding;
using Mobile.Core.Droid.Extensions;

namespace Mobile.Core.Droid.Controls
{
    public class DroidListView : ListView, IDroidSourceTemplate
    {
        public DroidListView(IntPtr javaReference, JniHandleOwnership transfer)
            : base(javaReference, transfer)
        {
            ItemTemplateId = DroidWidgetExtension.InitItemTemplateId(null, null);
        }

        public DroidListView(Context context)
            : base(context)
        {
            ItemTemplateId = DroidWidgetExtension.InitItemTemplateId(null, null);
        }

        public DroidListView(Context context, IAttributeSet attrs)
            : base(context, attrs)
        {
            ItemTemplateId = attrs.InitItemTemplateId(context);
        }

        public DroidListView(Context context, IAttributeSet attrs, int defStyle)
            : base(context, attrs, defStyle)
        {
            ItemTemplateId = attrs.InitItemTemplateId(context);
        }

        public int ItemTemplateId { get; }
    }
}