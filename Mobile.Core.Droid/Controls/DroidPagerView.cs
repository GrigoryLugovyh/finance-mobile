﻿using System;
using Android.Content;
using Android.Runtime;
using Android.Support.V4.View;
using Android.Util;
using Mobile.Core.Droid.Binding;
using Mobile.Core.Droid.Extensions;

namespace Mobile.Core.Droid.Controls
{
    public class DroidPagerView : ViewPager, IDroidSourceTemplate
    {
        public DroidPagerView(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
        {
            ItemTemplateId = DroidWidgetExtension.InitItemTemplateId(null, null);
        }

        public DroidPagerView(Context context) : base(context)
        {
            ItemTemplateId = DroidWidgetExtension.InitItemTemplateId(null, context);
        }

        public DroidPagerView(Context context, IAttributeSet attrs) : base(context, attrs)
        {
            ItemTemplateId = attrs.InitItemTemplateId(context);
        }

        public int ItemTemplateId { get; }
    }
}