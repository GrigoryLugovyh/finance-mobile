﻿using Android.OS;
using Android.Views;
using Mobile.Core.BL.Core.Model;
using Mobile.Core.BL.Core.ViewModels;
using Mobile.Core.BL.IoCContainer;
using Mobile.Core.Droid.Platform;

namespace Mobile.Core.Droid.Views
{
    public class DroidSplashScreenActivity : DroidActivity, IDroidSplashScreenActivity
    {
        protected const int NoContext = MConstants.NoContext;
        protected readonly int resourceId;

        public DroidSplashScreenActivity(int resourceId = NoContext)
        {
            this.resourceId = resourceId;
        }

        public override bool EmptyViewModel => true;

        public new MEmptyViewModel ViewModel
        {
            get { return base.ViewModel as MEmptyViewModel; }
            set { base.ViewModel = value; }
        }

        public virtual void InitCompleted()
        {
            if (!IsActive)
            {
                return;
            }

            Container.Resolve<IMViewStart>().Start();
        }

        protected virtual void ScreenNoTitle()
        {
            RequestWindowFeature(WindowFeatures.NoTitle);
        }

        protected virtual void ScreenFull()
        {
            Window.SetFlags(WindowManagerFlags.Fullscreen, WindowManagerFlags.Fullscreen);
        }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            ScreenNoTitle();

            ScreenFull();

            DroidStarter.Instance.Create(ApplicationContext);

            DroidStarter.Instance.Start(this);

            base.OnCreate(savedInstanceState);

            if (!NoContext.Equals(resourceId))
            {
                var context = LayoutInflater.Inflate(resourceId, null);
                SetContentView(context);
            }
        }

        protected override void OnResume()
        {
            base.OnResume();

            DroidStarter.Instance.Start(this);
        }

        protected override void OnPause()
        {
            DroidStarter.Instance.Remove();

            base.OnPause();
        }
    }
}