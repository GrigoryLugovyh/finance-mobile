﻿using Android.Content;
using Mobile.Core.BL.Core;
using Mobile.Core.BL.Core.Enums;
using Mobile.Core.BL.Core.Messages;
using Mobile.Core.BL.Core.ViewModels;
using Mobile.Core.BL.Core.Views;
using Mobile.Core.BL.IoCContainer;
using Mobile.Core.Droid.Platform;

namespace Mobile.Core.Droid.Views
{
    public class DroidViewPresenter : IDroidViewPresenter
    {
        public virtual void Show(MViewModelRequest viewModelRequest)
        {
            var intent = CreateIntent(viewModelRequest);

            if (intent == null)
            {
                Tracer.Warning("Intent not resolved");
                return;
            }

            Show(intent, viewModelRequest.Transition);
        }

        public virtual void Finish(MViewModelRequest viewModelRequest)
        {
            var requestDispatcher = Container.Resolve<IDroidViewModelRequestDispatcher>();

            var viewType = requestDispatcher.GetViewType(viewModelRequest);

            var activitiesMonitorOwner = Container.Resolve<IDroidActivitiesMonitorOwner>();

            var activity = activitiesMonitorOwner.GetActivity(viewType) as SourceActivity;

            activity?.Finish();
        }

        public virtual void Finish()
        {
            var activity = Container.Resolve<IDroidCurrentActivity>().Activity;

            if (activity == null)
            {
                Tracer.Warning("Current activity not resolved");
                return;
            }

            if (activity.IsFinishing)
            {
                Tracer.Warning("Current activity  already finishing");
                return;
            }

            activity.Finish();
        }

        public virtual void Show(Intent intent, MTransition transition)
        {
           if (transition == MTransition.None)
            {
                Container.Resolve<IDroidCurrentContext>().Context.StartActivity(intent);
                return;
            }

            var activity = Container.Resolve<IDroidCurrentActivity>().Activity;

            if (activity == null)
            {
                Tracer.Warning("Current activity not resolved");
                return;
            }

            activity.RunOnUiThread(() =>
            {
                activity.StartActivity(intent);

                var view = activity as IMTransitionView;

                view?.OnTransition(transition);
            });
        }

        public virtual Intent CreateIntent(MViewModelRequest request)
        {
            var requestDispatcher = Container.Resolve<IDroidViewModelRequestDispatcher>();

            var intent = requestDispatcher.GetIntent(request);

            return intent;
        }

        public void Show(Intent intent, MDialogRequest dialogRequest)
        {
            var activity = Container.Resolve<IDroidCurrentActivity>().Activity;

            if (activity == null)
            {
                Tracer.Warning("Current activity not resolved");
                return;
            }

            activity.StartActivityForResult(intent, 0);
        }
    }
}