﻿using System.Collections.Generic;
using System.Linq;

namespace Mobile.Core.Droid.Views
{
    public class DroidBindingContextStack : IDroidBindingContextStack<IDroidView>
    {
        private readonly object sync = new object();
        private readonly List<IDroidView> views = new List<IDroidView>();

        public IDroidView Current
        {
            get
            {
                lock (sync)
                {
                    if (views.Any())
                    {
                        return views.Last();
                    }

                    return null;
                }
            }
        }

        public void Push(IDroidView view)
        {
            lock (sync)
            {
                views.Add(view);
            }
        }

        public void Clear(IDroidView view)
        {
            lock (sync)
            {
                if (views.Contains(view))
                {
                    views.Remove(view);
                }
            }
        }
    }
}