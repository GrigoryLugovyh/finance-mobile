﻿using System;
using Android.Bluetooth;
using Android.OS;
using Mobile.Core.BL.Core.Binding;
using Mobile.Core.BL.Core.Events;
using Mobile.Core.BL.Core.Extensions;
using Mobile.Core.BL.Core.ViewModels;
using Mobile.Core.BL.IoCContainer;
using Mobile.Core.Droid.Binding;
using Mobile.Core.Droid.Extensions;
using Mobile.Core.Droid.Platform;
namespace Mobile.Core.Droid.Views
{
    public abstract class DroidActivity : SourceActivity, IDroidView
    {
        protected DroidActivity()
        {
            BindingContext = new DroidBindingContext(this, this);
        }

        public IMBindingContext BindingContext { get; set; }

        public object DataContext
        {
            get { return BindingContext.DataContext; }
            set
            {
                BindingContext.DataContext = value;
                DataContextSet.Rise(this, value);
            }
        }

        public IMViewModel ViewModel
        {
            get { return BindingContext.ViewModel; }
            set
            {
                BindingContext.ViewModel = value;
                ViewModelSet.Rise(this, value);
            }
        }

        public event EventHandler<MValueEventArgs<IMViewModel>> ViewModelSet;

        public event EventHandler<MValueEventArgs<object>> DataContextSet;

        public BluetoothAdapter BluetoothAdapter => BluetoothAdapter.DefaultAdapter;

        public virtual int ListTemplateLayout(object source)
        {
            return -1;
        }

        protected virtual bool DroidStarterCheck()
        {
            if (this is DroidSplashScreenActivity)
            {
                return false;
            }

            if (!DroidStarter.Instance.Started)
            {
                Finish();

                if (StartActivityType != null)
                {
                    StartActivity(StartActivityType);
                }

                return false;
            }

            return true;
        }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            if (DroidStarterCheck())
            {
                Container.Resolve<IDroidBindingContextStack<IDroidView>>().Push(DataContext as IDroidView);
            }

            base.OnCreate(savedInstanceState);
        }

        protected override void OnDestroy()
        {
            if (DroidStarterCheck())
            {
                Container.Resolve<IDroidBindingContextStack<IDroidView>>().Clear(this);
            }

            base.OnDestroy();
        }

        public override void SetContentView(int layoutResId)
        {
            var view = this.BindingInflate(layoutResId);

            base.SetContentView(view);
        }

        protected override void OnTransitionFade()
        {
            OverridePendingTransition(Android.Resource.Animation.FadeIn, Android.Resource.Animation.FadeOut);
        }

        protected override void OnTransitionToCenter()
        {
            OverridePendingTransition(Resource.Animation.ToCenter, Resource.Animation.ToCenter);
        }

        protected override void OnTransitionFromCenter()
        {
            OverridePendingTransition(Resource.Animation.ToCenter, Resource.Animation.FromCenter);
        }

        protected override void OnTransitionScale()
        {
            OverridePendingTransition(Resource.Animation.ScaleFromCorner, Resource.Animation.ScaleToCorner);
        }

        protected override void OnTransitionToLeft()
        {
            OverridePendingTransition(Resource.Animation.SlideInLeft, Resource.Animation.SlideInRight);
        }

        protected override void OnTransitionToRight()
        {
            OverridePendingTransition(Resource.Animation.SlideInRight, Resource.Animation.SlideInLeft);
        }

        protected override void OnTransitionOutLeft()
        {
            OverridePendingTransition(Resource.Animation.SlideOutLeft, Resource.Animation.SlideOutRight);
        }

        protected override void OnTransitionOutRight()
        {
            OverridePendingTransition(Resource.Animation.SlideOutRight, Resource.Animation.SlideOutLeft);
        }

        public override string ToString()
        {
            return GetType().Name;
        }
    }
}