﻿using System;
using Android.Content;
using Android.OS;
using Mobile.Core.BL.Core.Events;
using Mobile.Core.BL.Core.Platform.Keyboard;
using Mobile.Core.BL.Core.Platform.Media;
using Mobile.Core.BL.IoCContainer;

namespace Mobile.Core.Droid.Views
{
    public class DroidSourceListener : IDroidSourceListener
    {
        private IDroidActivityListener activityListener;

        protected ISourceActivity source;

        public DroidSourceListener(ISourceActivity source)
        {
            this.source = source;

            this.source.CreateCalled += SourceCreateCalled;
            this.source.CreatePostCalled += SourceCreatePostCalled;
            this.source.CreatePreCalled += SourceCreatePreCalled;
            this.source.DestroyCalled += SourceDestroyCalled;
            this.source.NewIntentCalled += SourceNewIntentCalled;
            this.source.PauseCalled += SourcePauseCalled;
            this.source.RestartCalled += SourceRestartCalled;
            this.source.ResumeCalled += SourceResumeCalled;
            this.source.StartCalled += SourceStartCalled;
            this.source.StopCalled += SourceStopCalled;
            this.source.BackPressCalled += SourceOnBackPressCalled;
            this.source.KeyDownCalled += SourceOnKeyDownCalled;
            this.source.OnActivityResultCalled += SourceOnOnActivityResultCalled;
            this.source.OnGestureCalled += SourceOnOnGestureCalled;
            this.source.OnUncaughtExceptionCalled += SourceOnOnUncaughtExceptionCalled;
            this.source.OnMenuCalled += SourceOnOnMenuCalled;
        }

        protected IDroidActivityListener Listener
            => activityListener ?? (activityListener = Container.Resolve<IDroidActivityListener>());

        public void Dispose()
        {
            source.CreateCalled -= SourceCreateCalled;
            source.CreatePostCalled -= SourceCreatePostCalled;
            source.CreatePreCalled -= SourceCreatePreCalled;
            source.DestroyCalled -= SourceDestroyCalled;
            source.NewIntentCalled -= SourceNewIntentCalled;
            source.PauseCalled -= SourcePauseCalled;
            source.RestartCalled -= SourceRestartCalled;
            source.ResumeCalled -= SourceResumeCalled;
            source.StartCalled -= SourceStartCalled;
            source.StopCalled -= SourceStopCalled;
            source.BackPressCalled -= SourceOnBackPressCalled;
            source.KeyDownCalled -= SourceOnKeyDownCalled;
            source.OnActivityResultCalled -= SourceOnOnActivityResultCalled;
            source.OnGestureCalled -= SourceOnOnGestureCalled;
            source.OnUncaughtExceptionCalled -= SourceOnOnUncaughtExceptionCalled;
            source.OnMenuCalled -= SourceOnOnMenuCalled;
            activityListener = null;
            source = null;
        }

        public virtual void Report(Action<IDroidActivityListener> call)
        {
            call.Invoke(Listener);
        }

        protected virtual void SourceCreateCalled(object sender, EventArgs e)
        {
            Report(listener => listener.OnCreate(source));
        }

        protected virtual void SourceStartCalled(object sender, EventArgs e)
        {
            Report(listener => listener.OnStart(source));
        }

        protected virtual void SourceRestartCalled(object sender, EventArgs e)
        {
            Report(listener => listener.OnRestart(source));
        }

        protected virtual void SourceResumeCalled(object sender, EventArgs e)
        {
            Report(listener => listener.OnResume(source));
        }

        protected virtual void SourceDestroyCalled(object sender, EventArgs e)
        {
            Report(listener => listener.OnDestroy(source));
        }

        protected virtual void SourceOnBackPressCalled(object sender, MValueEventArgs<KeyEventArgs> mValueEventArgs)
        {
            Report(listener => listener.OnKey(source, mValueEventArgs.Value));
        }

        protected virtual void SourceOnKeyDownCalled(object sender, MValueEventArgs<KeyEventArgs> mValueEventArgs)
        {
            Report(listener => listener.OnKey(source, mValueEventArgs.Value));
        }

        protected virtual void SourceOnOnActivityResultCalled(object sender,
            MValueEventArgs<ResultEventArgs> mValueEventArgs)
        {
            Report(listener => listener.OnResult(source, mValueEventArgs.Value));
        }

        private void SourceOnOnGestureCalled(object sender, MValueEventArgs<GestureEventArgs> mValueEventArgs)
        {
            Report(listener => listener.OnGesture(source, mValueEventArgs.Value));
        }

        private void SourceOnOnUncaughtExceptionCalled(object sender,
            MValueEventArgs<UncaughtExceptionEventArgs> mValueEventArgs)
        {
            Report(listener => listener.OnUncaughtException(source, mValueEventArgs.Value));
        }

        private void SourceOnOnMenuCalled(object sender, MValueEventArgs<int> mValueEventArgs)
        {
            Report(listener => listener.OnMenu(source, mValueEventArgs.Value));
        }

        protected virtual void SourcePauseCalled(object sender, EventArgs e)
        {
            Report(listener => listener.OnPause(source));
        }

        protected virtual void SourceStopCalled(object sender, EventArgs e)
        {
        }

        protected virtual void SourceNewIntentCalled(object sender, MValueEventArgs<Intent> e)
        {
        }

        protected virtual void SourceCreatePreCalled(object sender, MValueEventArgs<Bundle> e)
        {
        }

        protected virtual void SourceCreatePostCalled(object sender, MValueEventArgs<Bundle> e)
        {
        }
    }
}