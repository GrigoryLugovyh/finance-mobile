﻿using System;
using Mobile.Core.BL.Core.Enums;
using Mobile.Core.BL.Core.Model;

namespace Mobile.Core.Droid.Views
{
    public class DroidSimpleMainThreadDispatcher : DroidSyncMainThreadDispatcher
    {
        public override ActionResult RequestAction(Action action,
            MExceptionHandlingAppearance appearance = MExceptionHandlingAppearance.None)
        {
            return InvokeAction(action, appearance);
        }
    }
}