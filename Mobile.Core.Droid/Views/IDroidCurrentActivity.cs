﻿using Android.App;

namespace Mobile.Core.Droid.Views
{
    public interface IDroidCurrentActivity
    {
        Activity Activity { get; }
    }
}