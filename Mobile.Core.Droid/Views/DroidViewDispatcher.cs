﻿using System;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Widget;
using Mobile.Core.BL.Core;
using Mobile.Core.BL.Core.Binding.Source;
using Mobile.Core.BL.Core.Enums;
using Mobile.Core.BL.Core.Events;
using Mobile.Core.BL.Core.Exceptions;
using Mobile.Core.BL.Core.Messages;
using Mobile.Core.BL.Core.Model;
using Mobile.Core.BL.Core.Platform.Keyboard;
using Mobile.Core.BL.Core.Services;
using Mobile.Core.BL.Core.ViewModels;
using Mobile.Core.BL.Core.Views;
using Mobile.Core.BL.IoCContainer;

namespace Mobile.Core.Droid.Views
{
    public class DroidViewDispatcher : DroidSimpleMainThreadDispatcher, IMViewDispatcher
    {
        private readonly IMErrorMessagesService errorMessagesService;
        private readonly IMKeyboardHandler keyboard;
        protected readonly IDroidViewPresenter presenter;
        private readonly object sync = new object();

        private IMMethodSourceBindingStack sourceBindingStack;

        public DroidViewDispatcher(IDroidViewPresenter presenter)
        {
            this.presenter = presenter;

            ExceptionCall += DroidViewDispatcherExceptionCall;

            keyboard = Container.Resolve<IMKeyboardHandler>();
            errorMessagesService = Container.Resolve<IMErrorMessagesService>();
        }

        private IMMethodSourceBindingStack SourceBindingStack
            => sourceBindingStack ?? (sourceBindingStack = Container.Resolve<IMMethodSourceBindingStack>());

        public ActionResult ShowViewModel(MViewModelRequest viewModelRequest)
        {
            return RequestAction(() => presenter.Show(viewModelRequest));
        }

        public ActionResult FinishViewModel(MViewModelRequest viewModelRequest)
        {
            return RequestAction(() => presenter.Finish(viewModelRequest));
        }

        public ActionResult FinishViewModel()
        {
            return RequestAction(presenter.Finish);
        }

        public async Task RequestActionAsync(Action action,
            MExceptionHandlingAppearance appearance = MExceptionHandlingAppearance.None)
        {
            await Task.Factory.StartNew(() => RequestAction(action, appearance));
        }

        public void RequestActionOnUI(Action<object> action)
        {
            var activity = Container.Resolve<IDroidCurrentActivity>().Activity;

            if (activity == null)
            {
                Tracer.Warning("RequestActionOnUI: current activity is null");
                return;
            }

            if (action == null)
            {
                Tracer.Warning("RequestActionOnUI: action is null");
                return;
            }

            activity.RunOnUiThread(() => RequestAction(() => action(activity)));
        }

        public async Task RequestActionOnUIAsync(Action<object> action)
        {
            await Task.Factory.StartNew(() => RequestActionOnUI(action));
        }

        public void ShowMessageQuick(string message, params object[] args)
        {
            RequestActionOnUI(
                context => Toast.MakeText(context as Context, string.Format(message, args), ToastLength.Long).Show());
        }

        public async Task ShowMessageQuickAsync(string message, params object[] args)
        {
            await
                RequestActionOnUIAsync(
                    context => Toast.MakeText(context as Context, string.Format(message, args), ToastLength.Long).Show());
        }

        public void ShowMessage(MDialogRequest request)
        {
            RequestActionOnUI(context => ShowDialog(context as Context, request).Show());
        }

        public async Task ShowMessageAsync(MDialogRequest request)
        {
            await RequestActionOnUIAsync(context => ShowDialog(context as Context, request).Show());
        }

        public Task<ActionResult> RequestActionInWaitShell(Action action, MWaitShellRequest request,
            MExceptionHandlingAppearance appearance = MExceptionHandlingAppearance.None)
        {
            lock (sync)
            {
                var context = Container.Resolve<IDroidCurrentActivity>().Activity;

                if (context == null)
                {
                    Tracer.Warning("RequestActionInWaitShell: context is null");
                    return null;
                }

                if (action == null || request == null)
                {
                    Tracer.Warning("RequestActionInWaitShell: arguments was bad format");
                    return null;
                }

                if (context.IsFinishing)
                {
                    return null;
                }

                ProgressDialog dialog = null;

                context.RunOnUiThread(() =>
                {
                    dialog = ProgressDialog.Show(context, request.Title, request.Message, true);
                });

                EventHandler dialogDismissDelegate = delegate
                {
                    if (dialog != null && dialog.IsShowing)
                    {
                        context.RunOnUiThread(() =>
                        {
                            dialog.Dismiss();
                        });
                    }
                };

                return Task.Run(() =>
                {
                    try
                    {
                        ((ISourceActivity) context).DestroyCalled += dialogDismissDelegate;
                        var actionResult = RequestAction(action, appearance);
                        return actionResult;
                    }
                    finally
                    {
                        dialogDismissDelegate(null, EventArgs.Empty);
                        ((ISourceActivity) context).DestroyCalled -= dialogDismissDelegate;
                    }
                });
            }
        }

        public void HideKeyboard()
        {
            keyboard?.HideKeyboard();
        }

        protected virtual Dialog ShowDialog(Context context, MDialogRequest request)
        {
            if (request == null)
            {
                Tracer.Warning("ShowDialog: request is null");
                return null;
            }

            if (context == null)
            {
                Tracer.Warning("ShowDialog: context is null");
                return null;
            }

            if (!(context is Activity) || ((Activity) context).IsFinishing)
            {
                return null;
            }

            var dialog = new AlertDialog.Builder(context);

            dialog.SetCancelable(!request.Modal);

            if (request.Icon != null)
            {
                dialog.SetIcon((int) request.Icon);
            }

            dialog.SetTitle(request.Title);

            dialog.SetMessage(request.Message);

            if (request.PositiveButton?.Action != null)
            {
                dialog.SetPositiveButton(request.PositiveButton.ButtonText,
                    (sender, args) => RequestAction(request.PositiveButton.Action));
            }

            if (request.NegativeButton?.Action != null)
            {
                dialog.SetNegativeButton(request.NegativeButton.ButtonText,
                    (sender, args) => RequestAction(request.NegativeButton.Action));
            }

            if (request.NeutralButton?.Action != null)
            {
                dialog.SetNeutralButton(request.NeutralButton.ButtonText,
                    (sender, args) => RequestAction(request.NeutralButton.Action));
            }

            return dialog.Create();
        }

        protected virtual void DroidViewDispatcherExceptionCall(object sender, MValueEventArgs<MExceptionRequest> e)
        {
            if (e.Value.Appearance == MExceptionHandlingAppearance.None)
            {
                return;
            }

            var message = errorMessagesService.ProvideForExceptionMessage(e.Value.Exception);

            if (e.Value.Appearance == MExceptionHandlingAppearance.Toast)
            {
                ShowMessageQuick(message);
            }
            else if (e.Value.Appearance == MExceptionHandlingAppearance.Dialog)
            {
                var title = SourceBindingStack.Current;

                ShowMessage(new MDialogRequest
                {
                    PositiveButton = new MDialogRequest.DialogRequestButton
                    {
                        Action = () => { },
                        ButtonText = "OK"
                    },
                    Title = string.IsNullOrEmpty(title) ? "Внимание!" : title,
                    Message = message
                });
            }
        }
    }
}