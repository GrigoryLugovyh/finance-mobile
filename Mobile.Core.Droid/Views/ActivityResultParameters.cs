﻿using Android.App;
using Android.Content;

namespace Mobile.Core.Droid.Views
{
    public class ActivityResultParameters : ActivityResultParametersStart
    {
        public ActivityResultParameters(int request, Intent intent, Result result)
            : base(request, intent)
        {
            Result = result;
        }

        public Result Result { get; protected set; }
    }
}