﻿using Mobile.Core.BL.Core.Platform.Keyboard;
using Mobile.Core.BL.Core.Platform.Media;

namespace Mobile.Core.Droid.Views
{
    public interface IDroidActivityListener
    {
        int ActivityCount { get; }

        void OnCreate(ISourceActivity activity);

        void OnStart(ISourceActivity activity);

        void OnRestart(ISourceActivity activity);

        void OnResume(ISourceActivity activity);

        void OnPause(ISourceActivity activity);

        void OnDestroy(ISourceActivity activity);

        void OnKey(ISourceActivity activity, KeyEventArgs args);

        void OnResult(ISourceActivity activity, ResultEventArgs args);

        void OnGesture(ISourceActivity activity, GestureEventArgs gesture);

        void OnUncaughtException(ISourceActivity activity, UncaughtExceptionEventArgs uncaught);

        void OnMenu(ISourceActivity activity, int id);
    }
}