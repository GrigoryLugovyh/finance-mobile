﻿namespace Mobile.Core.Droid.Views
{
    public interface IDroidViewListable
    {
        /// <summary>
        /// Resource.Layout
        /// </summary>
        int ListTemplateLayout(object source);
    }
}