﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Java.Lang;
using Mobile.Core.BL.Core.Enums;
using Mobile.Core.BL.Core.Events;
using Mobile.Core.BL.Core.Extensions;
using Mobile.Core.BL.Core.Platform.Keyboard;
using Mobile.Core.BL.Core.Platform.Media;
using Mobile.Core.BL.Core.Views;
using Mobile.Core.Droid.Extensions;
using Mobile.Core.Droid.Listeners;
using Mobile.Core.Droid.Listeners.Gestures;
using Exception = System.Exception;

namespace Mobile.Core.Droid.Views
{
    public class SourceActivity : Activity, ISourceActivity, IMGestureView, IMTransitionView, IMThrowableView,
        IMLiveView
    {
        protected readonly Dictionary<MTransition, Action> transitions = new Dictionary<MTransition, Action>();

        protected GestureDetector gestureDetector;

        protected DroidKeyUpHandler keyUpHandler;

        protected virtual bool NeedInitTransitions => true;

        protected virtual bool NeedInitUncaughtException => true;

        protected virtual bool NeedInitSwipeGestureDetector => true;

        protected virtual bool HandleKeyDown => false;

        protected virtual Type StartActivityType => null;

        protected bool IsSplashActivity => this is DroidSplashScreenActivity;

        public virtual void OnGestureResult(MGesture gesture)
        {
            OnGestureCalled.Rise(this, new GestureEventArgs(gesture));
        }

        public void Kill()
        {
            JavaSystem.Exit(0);
        }

        public virtual void OnUncaughtExceptionThrow(Exception exception)
        {
            OnUncaughtExceptionCalled.Rise(this,
                new UncaughtExceptionEventArgs(exception.Message, exception.StackTrace, exception));
        }

        public virtual void OnTransition(MTransition transition)
        {
            if (transition == MTransition.None)
            {
                return;
            }

            Action actionTransition;

            if (!transitions.TryGetValue(transition, out actionTransition))
            {
                return;
            }

            actionTransition();
        }

        public bool IsActive { get; protected set; }

        public virtual bool EmptyViewModel => false;

        public event EventHandler DisposeCalled;
        public event EventHandler CreateCalled;
        public event EventHandler DestroyCalled;
        public event EventHandler RestartCalled;
        public event EventHandler ResumeCalled;
        public event EventHandler PauseCalled;
        public event EventHandler StartCalled;
        public event EventHandler StopCalled;

        public event EventHandler<MValueEventArgs<Bundle>> CreatePostCalled;
        public event EventHandler<MValueEventArgs<Bundle>> CreatePreCalled;
        public event EventHandler<MValueEventArgs<Intent>> NewIntentCalled;

        public event EventHandler<MValueEventArgs<KeyEventArgs>> BackPressCalled;
        public event EventHandler<MValueEventArgs<KeyEventArgs>> KeyDownCalled;
        public event EventHandler<MValueEventArgs<ResultEventArgs>> OnActivityResultCalled;
        public event EventHandler<MValueEventArgs<GestureEventArgs>> OnGestureCalled;
        public event EventHandler<MValueEventArgs<UncaughtExceptionEventArgs>> OnUncaughtExceptionCalled;
        public event EventHandler<MValueEventArgs<int>> OnMenuCalled;

        public string ActionBarTitle { get; set; }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            InitSourceActivity();
            this.AddActivityListener();

            CreatePreCalled.Rise(this, savedInstanceState);
            base.OnCreate(savedInstanceState);
            CreatePostCalled.Rise(this, savedInstanceState);
            IsActive = true;
            CreateCalled.Rise(this);
        }

        protected override void OnNewIntent(Intent intent)
        {
            base.OnNewIntent(intent);
            NewIntentCalled.Rise(this, intent);
            IsActive = true;
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            IsActive = false;

            DestroyCalled.Rise(this);
            this.RemoveActivityListener();
        }

        protected override void OnResume()
        {
            base.OnResume();
            ResumeCalled.Rise(this);
            IsActive = true;
        }

        protected override void OnPause()
        {
            PauseCalled.Rise(this);
            base.OnPause();
            IsActive = false;
        }

        protected override void OnStart()
        {
            base.OnStart();
            StartCalled.Rise(this);
            IsActive = true;
        }

        protected override void OnRestart()
        {
            base.OnRestart();
            RestartCalled.Rise(this);
            IsActive = true;
        }

        protected override void OnStop()
        {
            StopCalled.Rise(this);
            base.OnStop();
            IsActive = false;
        }

        public override bool OnKeyDown(Keycode keyCode, KeyEvent e)
        {
            if (keyCode == Keycode.Back)
            {
                if (Back())
                {
                    return true;
                }
            }

            return base.OnKeyDown(keyCode, e);
        }

        public override bool OnKeyUp(Keycode keyCode, KeyEvent e)
        {
            return KeyUp(keyCode, e);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                DisposeCalled.Rise(this);
            }

            base.Dispose(disposing);
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            OnActivityResultCalled.Rise(this, new ResultEventArgs(requestCode, (MResult) resultCode));
        }

        public override bool OnTouchEvent(MotionEvent e)
        {
            if (gestureDetector == null || e == null)
            {
                return false;
            }

            if (gestureDetector.OnTouchEvent(e))
            {
                return true;
            }

            return base.OnTouchEvent(e);
        }

        protected virtual void InitSourceActivity()
        {
            InitTransitions();
            InitUncaughtExceptionHandler();
            InitSwipeGestureDetectorHandler();
        }

        protected virtual void InitTransitions()
        {
            if (IsSplashActivity) return;

            if (!NeedInitTransitions) return;

            transitions.Add(MTransition.Fade, OnTransitionFade);
            transitions.Add(MTransition.ToCenter, OnTransitionToCenter);
            transitions.Add(MTransition.FromCenter, OnTransitionFromCenter);
            transitions.Add(MTransition.Scale, OnTransitionScale);
            transitions.Add(MTransition.ToLeft, OnTransitionToLeft);
            transitions.Add(MTransition.ToRight, OnTransitionToRight);
            transitions.Add(MTransition.OutLeft, OnTransitionOutLeft);
            transitions.Add(MTransition.OutRight, OnTransitionOutRight);
        }

        protected virtual void InitUncaughtExceptionHandler()
        {
            if (IsSplashActivity) return;

            if (!NeedInitUncaughtException) return;

            if (!(Thread.DefaultUncaughtExceptionHandler is DroidUncaughtExceptionListener))
            {
                Thread.DefaultUncaughtExceptionHandler = new DroidUncaughtExceptionListener(StartActivityType);
            }
        }

        protected virtual void InitSwipeGestureDetectorHandler()
        {
            if (IsSplashActivity) return;

            if (!NeedInitSwipeGestureDetector) return;

            if (gestureDetector == null)
            {
                gestureDetector = new DroidSwipeGestureDetector(this, new DroidSwipeGestureListener(this));
            }
        }

        protected virtual bool Back()
        {
            return BackPressCalled.Rise(this, new KeyEventArgs(MKeyCode.Back)).Handle;
        }

        protected virtual bool KeyUp(Keycode keyCode, KeyEvent keyEvent)
        {
            if (keyUpHandler == null)
            {
                keyUpHandler = new DroidKeyUpHandler();
            }

            var keyEventArgs = keyUpHandler.GetEventArgs(keyCode, keyEvent);

            if (HandleKeyDown)
            {
                KeyDownCalled.Rise(this, keyEventArgs);
                return true;
            }

            return base.OnKeyUp(keyCode, keyEvent);
        }
        
        protected virtual void Menu(int id)
        {
            OnMenuCalled.Rise(this, id);
        }

        protected virtual void OnTransitionFade()
        {
        }

        protected virtual void OnTransitionToCenter()
        {
        }

        protected virtual void OnTransitionFromCenter()
        {
        }

        protected virtual void OnTransitionScale()
        {
        }

        protected virtual void OnTransitionToLeft()
        {
        }

        protected virtual void OnTransitionOutLeft()
        {
        }

        protected virtual void OnTransitionToRight()
        {
        }

        protected virtual void OnTransitionOutRight()
        {
        }
    }
}