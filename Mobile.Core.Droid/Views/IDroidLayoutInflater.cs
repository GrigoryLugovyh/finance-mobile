﻿using Android.Views;

namespace Mobile.Core.Droid.Views
{
    public interface IDroidLayoutInflater
    {
        LayoutInflater LayoutInflater { get; }
    }
}