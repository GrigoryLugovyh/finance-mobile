﻿namespace Mobile.Core.Droid.Views
{
    public class DroidSourceListenerFactory : IDroidSourceListenerFactory
    {
        public IDroidSourceListener Create(ISourceActivity source)
        {
            return new DroidSourceListener(source);
        }
    }
}