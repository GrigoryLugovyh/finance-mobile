﻿using System;

namespace Mobile.Core.Droid.Views
{
    public interface IDroidActivitiesMonitorOwner
    {
        event EventHandler ListenerAdded;

        event EventHandler ListenerRemoved;

        ISourceActivity GetActivity(Type activityType);

        IDroidSourceListener GetListener(Type activityType);

        void AddListener(ISourceActivity activity);

        void RemoveListener(ISourceActivity activity);
    }
}