﻿using System.Collections.Generic;
using Android.Views;
using Mobile.Core.BL.Core.Platform.Keyboard;

namespace Mobile.Core.Droid.Views
{
    public class DroidKeyUpHandler
    {
        private readonly Dictionary<Keycode, MKeyCode> keyCodeMapping = new Dictionary<Keycode, MKeyCode>
        {
            {Keycode.Num0, MKeyCode.D0},
            {Keycode.Num1, MKeyCode.D1},
            {Keycode.Num2, MKeyCode.D2},
            {Keycode.Num3, MKeyCode.D3},
            {Keycode.Num4, MKeyCode.D4},
            {Keycode.Num5, MKeyCode.D5},
            {Keycode.Num6, MKeyCode.D6},
            {Keycode.Num7, MKeyCode.D7},
            {Keycode.Num8, MKeyCode.D8},
            {Keycode.Num9, MKeyCode.D9},
            {Keycode.Enter, MKeyCode.Enter},
            {Keycode.A, MKeyCode.A},
            {Keycode.B, MKeyCode.B},
            {Keycode.C, MKeyCode.C},
            {Keycode.D, MKeyCode.D},
            {Keycode.E, MKeyCode.E},
            {Keycode.F, MKeyCode.F},
            {Keycode.G, MKeyCode.G},
            {Keycode.H, MKeyCode.H},
            {Keycode.I, MKeyCode.I},
            {Keycode.J, MKeyCode.J},
            {Keycode.K, MKeyCode.K},
            {Keycode.L, MKeyCode.L},
            {Keycode.M, MKeyCode.M},
            {Keycode.N, MKeyCode.N},
            {Keycode.O, MKeyCode.O},
            {Keycode.P, MKeyCode.P},
            {Keycode.Q, MKeyCode.Q},
            {Keycode.R, MKeyCode.R},
            {Keycode.S, MKeyCode.S},
            {Keycode.T, MKeyCode.T},
            {Keycode.U, MKeyCode.U},
            {Keycode.V, MKeyCode.V},
            {Keycode.W, MKeyCode.W},
            {Keycode.X, MKeyCode.X},
            {Keycode.Y, MKeyCode.Y},
            {Keycode.Z, MKeyCode.Z},
            {Keycode.F1, MKeyCode.F1},
            {Keycode.F2, MKeyCode.F2},
            {Keycode.F3, MKeyCode.F3},
            {Keycode.F4, MKeyCode.F4},
            {Keycode.F5, MKeyCode.F5},
            {Keycode.F6, MKeyCode.F6},
            {Keycode.F7, MKeyCode.F7},
            {Keycode.F8, MKeyCode.F8},
            {Keycode.F9, MKeyCode.F9},
            {Keycode.F10, MKeyCode.F10},
            {Keycode.F11, MKeyCode.F11},
            {Keycode.F12, MKeyCode.F12},
            {Keycode.ShiftLeft, MKeyCode.LeftShift},
            {Keycode.ShiftRight, MKeyCode.RightShift},
            {Keycode.AltRight, MKeyCode.RightAlt},
            {Keycode.AltLeft, MKeyCode.LeftAlt},
            {Keycode.CtrlLeft, MKeyCode.LeftCtrl},
            {Keycode.CtrlRight, MKeyCode.RightCtrl},
            {Keycode.Escape, MKeyCode.Escape},
            {Keycode.Back, MKeyCode.Back},
            {Keycode.Tab, MKeyCode.Tab},
            {Keycode.DpadUp, MKeyCode.Up},
            {Keycode.DpadDown, MKeyCode.Down},
            {Keycode.DpadLeft, MKeyCode.Left},
            {Keycode.DpadRight, MKeyCode.Right}
        };

        public KeyEventArgs GetEventArgs(Keycode keyCode, KeyEvent keyEvent)
        {
            var mKeycode = MKeyCode.None;

            if (keyCodeMapping.ContainsKey(keyCode))
            {
                mKeycode = keyCodeMapping[keyCode];
            }

            var mKeyCodes = new List<MKeyCode> {mKeycode};

            if (keyEvent.IsAltPressed)
            {
                mKeyCodes.Add(MKeyCode.LeftAlt);
                mKeyCodes.Add(MKeyCode.RightAlt);
            }

            if (keyEvent.IsCtrlPressed)
            {
                mKeyCodes.Add(MKeyCode.LeftCtrl);
                mKeyCodes.Add(MKeyCode.RightCtrl);
            }

            if (keyEvent.IsShiftPressed)
            {
                mKeyCodes.Add(MKeyCode.LeftShift);
                mKeyCodes.Add(MKeyCode.RightShift);
            }

            return new KeyEventArgs(mKeyCodes);
        }
    }
}