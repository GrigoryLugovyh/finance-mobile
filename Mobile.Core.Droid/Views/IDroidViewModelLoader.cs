﻿using System;

namespace Mobile.Core.Droid.Views
{
    public interface IDroidViewModelLoader
    {
        Type LoadViewModelType(IDroidView view);
    }
}