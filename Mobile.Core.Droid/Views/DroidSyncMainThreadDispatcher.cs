﻿using System;
using System.Threading;
using Android.App;
using Mobile.Core.BL.Core.Enums;
using Mobile.Core.BL.Core.Model;

namespace Mobile.Core.Droid.Views
{
    public class DroidSyncMainThreadDispatcher : MainThreadDispatcher
    {
        public virtual ActionResult RequestAction(Action action,
            MExceptionHandlingAppearance appearance = MExceptionHandlingAppearance.None)
        {
            ActionResult result = new ActionResult();

            if (Application.SynchronizationContext == SynchronizationContext.Current)
            {
                result = InvokeAction(action, appearance);
            }
            else
            {
                Application.SynchronizationContext.Post(d => result = InvokeAction(action, appearance), null);
            }

            return result;
        }
    }
}