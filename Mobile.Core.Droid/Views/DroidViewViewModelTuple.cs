﻿using System;
using Mobile.Core.BL.Core.ViewModels;

namespace Mobile.Core.Droid.Views
{
    public class DroidViewViewModelTuple : Tuple<IDroidView, IMViewModel>
    {
        public DroidViewViewModelTuple(IDroidView item1, IMViewModel item2) : base(item1, item2)
        {
            View = item1;
            ViewModel = item2;
        }

        public IDroidView View { get; private set; }

        public IMViewModel ViewModel { get; private set; }
    }
}