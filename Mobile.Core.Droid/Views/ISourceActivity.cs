﻿using System;
using Android.Content;
using Android.OS;
using Mobile.Core.BL.Core.Events;
using Mobile.Core.BL.Core.Platform.Keyboard;
using Mobile.Core.BL.Core.Platform.Media;
using Mobile.Core.BL.Core.Views;

namespace Mobile.Core.Droid.Views
{
    public interface ISourceActivity : IMSource, IMViewEditable
    {
        bool IsActive { get; }

        bool EmptyViewModel { get; }

        event EventHandler<MValueEventArgs<Bundle>> CreatePreCalled;

        event EventHandler<MValueEventArgs<Bundle>> CreatePostCalled;

        event EventHandler<MValueEventArgs<Intent>> NewIntentCalled;

        event EventHandler CreateCalled;

        event EventHandler DestroyCalled;

        event EventHandler ResumeCalled;

        event EventHandler PauseCalled;

        event EventHandler StartCalled;

        event EventHandler RestartCalled;

        event EventHandler StopCalled;

        event EventHandler<MValueEventArgs<KeyEventArgs>> BackPressCalled;

        event EventHandler<MValueEventArgs<KeyEventArgs>> KeyDownCalled;

        event EventHandler<MValueEventArgs<ResultEventArgs>> OnActivityResultCalled;

        event EventHandler<MValueEventArgs<GestureEventArgs>> OnGestureCalled;

        event EventHandler<MValueEventArgs<UncaughtExceptionEventArgs>> OnUncaughtExceptionCalled;

        event EventHandler<MValueEventArgs<int>> OnMenuCalled;
    }
}