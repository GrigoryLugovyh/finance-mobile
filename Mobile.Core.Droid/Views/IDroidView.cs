﻿using System;
using Android.Bluetooth;
using Mobile.Core.BL.Core.Binding;
using Mobile.Core.BL.Core.Events;
using Mobile.Core.BL.Core.ViewModels;
using Mobile.Core.BL.Core.Views;

namespace Mobile.Core.Droid.Views
{
    public interface IDroidView : IMView, IMBindingContextOwner, IDroidLayoutInflater, IDroidViewListable
    {
        event EventHandler<MValueEventArgs<IMViewModel>> ViewModelSet;

        event EventHandler<MValueEventArgs<object>> DataContextSet;

        BluetoothAdapter BluetoothAdapter { get; }
    }
}