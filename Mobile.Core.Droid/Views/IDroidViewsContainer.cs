﻿namespace Mobile.Core.Droid.Views
{
    public interface IDroidViewsContainer : IDroidViewModelLoader, IDroidViewModelRequestDispatcher
    {
    }
}