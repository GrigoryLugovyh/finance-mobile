﻿using Android.Content;
using Mobile.Core.BL.Core.Enums;
using Mobile.Core.BL.Core.Messages;
using Mobile.Core.BL.Core.ViewModels;
using Mobile.Core.BL.Core.Views;

namespace Mobile.Core.Droid.Views
{
    public interface IDroidViewPresenter : IMViewPresenter
    {
        void Show(Intent intent, MTransition transition);
        Intent CreateIntent(MViewModelRequest request);
        void Show(Intent intent, MDialogRequest dialogRequest);
    }
}