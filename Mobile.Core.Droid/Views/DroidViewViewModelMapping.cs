﻿using Mobile.Core.BL.Core.ViewModels;

namespace Mobile.Core.Droid.Views
{
    public class DroidViewViewModelMapping : MViewViewModelMapping
    {
        protected readonly string[] postfixes;

        public DroidViewViewModelMapping(params string[] postfixes)
        {
            this.postfixes = postfixes;
        }

        public override string Map(string name)
        {
            foreach (var postfix in postfixes)
            {
                if (!name.EndsWith(postfix) || name.Length <= postfix.Length) continue;

                name = name.Substring(0, name.Length - postfix.Length);
                break;
            }

            return base.Map(name);
        }
    }
}