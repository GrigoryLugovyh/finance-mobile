﻿using System;
using Android.App;
using Mobile.Core.BL.Core.Enums;
using Mobile.Core.BL.Core.Extensions;
using Mobile.Core.BL.Core.Model;
using Mobile.Core.BL.Core.Platform.Keyboard;
using Mobile.Core.BL.Core.Platform.Media;
using Mobile.Core.BL.Core.ViewModels;
using Mobile.Core.BL.IoCContainer;
using Mobile.Core.Droid.Extensions;

namespace Mobile.Core.Droid.Views
{
    public class DroidActivityMonitor : MRealtimeMonitor, IDroidActivityListener, IDroidCurrentActivity
    {
        public int ActivityCount { get; protected set; }

        public void OnCreate(ISourceActivity activity)
        {
            if (activity.EmptyViewModel)
            {
                return;
            }

            var view = activity as IDroidView;

            if (view == null)
            {
                return;
            }

            if (view.ViewModel != null)
            {
                return;
            }

            var cash = Container.Resolve<IMViewModelsCache>();

            var container = Container.Resolve<IDroidViewsContainer>();

            Type viewModelType = container.LoadViewModelType(view);

            if (viewModelType == null)
            {
                return;
            }

            IMViewModel viewModel = cash.GetViewModel(viewModelType);

            if (viewModel == null)
            {
                return;
            }

            viewModel.View = activity;

            viewModel.InitViewModel();

            view.ViewModel = viewModel;
        }

        public void OnStart(ISourceActivity activity)
        {
            ActivityCount++;

            Activity = activity.ToActivity();

            OnRealtimeChanged(ActivityCount == 1 ? MRealtimeState.Activate : MRealtimeState.Start);
        }

        public void OnRestart(ISourceActivity activity)
        {
            Activity = activity.ToActivity();

            OnRealtimeChanged(MRealtimeState.Restart);
        }

        public void OnResume(ISourceActivity activity)
        {
            Activity = activity.ToActivity();

            OnRealtimeChanged(MRealtimeState.Resume);

            if (activity.EmptyViewModel)
            {
                return;
            }

            var view = activity as IDroidView;

            if (view?.ViewModel == null)
            {
                return;
            }

            view.ViewModel.CheckViewModel();

            view.ViewModel.ResumeViewModel();
        }

        public void OnPause(ISourceActivity activity)
        {
            Activity = activity.ToActivity();

            OnRealtimeChanged(MRealtimeState.Pause);

            if (activity.EmptyViewModel)
            {
                return;
            }

            var view = activity as IDroidView;

            view?.ViewModel?.PauseViewModel();
        }

        public void OnDestroy(ISourceActivity activity)
        {
            if (Activity == activity.ToActivity())
            {
                Activity = null;
            }

            var view = activity as IDroidView;

            view?.ViewModel?.DestroyViewModel();

            ActivityCount--;

            if (ActivityCount == 0)
            {
                OnRealtimeChanged(MRealtimeState.Deactivate);
            }
            else if (ActivityCount > 0)
            {
                OnRealtimeChanged(MRealtimeState.Destroy);
            }

            BindingExtension.UnregisterBindings(activity.UniqueId());

            if (view != null)
            {
                view.ViewModel = null;
            }
        }

        public void OnKey(ISourceActivity activity, KeyEventArgs args)
        {
            IMViewModel viewModel = activity.ToViewModel();

            if (viewModel == null)
            {
                return;
            }

            if (args.Keycode == MKeyCode.Back)
            {
                viewModel.BackViewModel(args);
            }
            else
            {
                viewModel.OnKeyDown(args);
            }
        }

        public void OnResult(ISourceActivity activity, ResultEventArgs args)
        {
            OnRealtimeChanged(MRealtimeState.Result, args);

            IMViewModel viewModel = activity.ToViewModel();

            viewModel?.ResultViewModel(args);
        }

        public void OnGesture(ISourceActivity activity, GestureEventArgs gesture)
        {
            IMViewModel viewModel = activity.ToViewModel();

            viewModel?.GestureViewModel(gesture);
        }

        public void OnUncaughtException(ISourceActivity activity, UncaughtExceptionEventArgs uncaught)
        {
            IMViewModel viewModel = activity.ToViewModel();

            viewModel?.UncaughtExceptionViewModel(uncaught);
        }

        public void OnMenu(ISourceActivity activity, int id)
        {
            IMViewModel viewModel = activity.ToViewModel();

            viewModel?.MenuViewModel(id);
        }

        public Activity Activity { get; protected set; }
    }
}