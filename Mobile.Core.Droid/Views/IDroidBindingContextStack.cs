﻿using Mobile.Core.BL.Core.Infrastructure;

namespace Mobile.Core.Droid.Views
{
    public interface IDroidBindingContextStack<TContext> : IMContextStack<TContext>
    {
        void Clear(IDroidView view);
    }
}