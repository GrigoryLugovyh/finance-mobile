﻿using System;
using Android.Content;
using Mobile.Core.BL.Core.Exceptions;
using Mobile.Core.BL.Core.ViewModels;
using Mobile.Core.BL.Core.Views;
using Mobile.Core.BL.IoCContainer;

namespace Mobile.Core.Droid.Views
{
    public class DroidViewViewModelContainer : MViewViewModelContainer, IDroidViewsContainer
    {
        protected readonly Context appContext;

        public DroidViewViewModelContainer(Context appContext)
        {
            this.appContext = appContext;
        }

        public Intent GetIntent(MViewModelRequest request)
        {
            Type viewType = GetViewType(request);

            if (viewType == null)
            {
                throw new MException("Not found type for {0}", request.ViewModelType);
            }

            var intent = new Intent(appContext, viewType);

            intent.AddFlags(ActivityFlags.NewTask);

            return intent;
        }

        public Type GetViewType(MViewModelRequest request)
        {
            if (appContext == null)
            {
                throw new MException("Application context is null");
            }

            Type viewType = GetViewType(request.ViewModelType);

            if (viewType == null)
            {
                throw new MException("Not found type for {0}", request.ViewModelType);
            }

            return viewType;
        }

        public Type LoadViewModelType(IDroidView view)
        {
            var detector = Container.Resolve<IMViewModelTypeDetector>();

            Type viewModelType = detector.DetectType(view.GetType());

            return viewModelType;
        }
    }
}