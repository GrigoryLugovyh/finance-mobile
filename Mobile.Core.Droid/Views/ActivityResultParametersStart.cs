﻿using Android.Content;

namespace Mobile.Core.Droid.Views
{
    public class ActivityResultParametersStart
    {
        public ActivityResultParametersStart(int request, Intent intent)
        {
            Request = request;
            Intent = intent;
        }

        public int Request { get; protected set; }

        public Intent Intent { get; protected set; }
    }
}