﻿using System;
using System.Collections.Generic;
using System.Linq;
using Mobile.Core.BL.Core;
using Mobile.Core.BL.Core.Extensions;
using Mobile.Core.BL.IoCContainer;

namespace Mobile.Core.Droid.Views
{
    public class DroidActivitiesMonitorOwner : IDroidActivitiesMonitorOwner
    {
        protected readonly Dictionary<ISourceActivity, IDroidSourceListener> activitiesMonitorOwner =
            new Dictionary<ISourceActivity, IDroidSourceListener>();

        private readonly object sync = new object();

        public event EventHandler ListenerAdded;

        public event EventHandler ListenerRemoved;

        public ISourceActivity GetActivity(Type activityType)
        {
            lock (sync)
            {
                return
                    activitiesMonitorOwner.Keys.FirstOrDefault(activity => activity.GetType() == activityType);
            }
        }

        public IDroidSourceListener GetListener(Type activityType)
        {
            lock (sync)
            {
                IDroidSourceListener listener;

                if (activitiesMonitorOwner.TryGetValue(GetActivity(activityType), out listener))
                {
                    return listener;
                }

                return null;
            }
        }

        public void AddListener(ISourceActivity activity)
        {
            lock (sync)
            {
                if (activitiesMonitorOwner.ContainsKey(activity))
                {
                    Tracer.Warning("Activity already tapped");
                    return;
                }

                activitiesMonitorOwner.Add(activity, Container.Resolve<IDroidSourceListenerFactory>().Create(activity));

                ListenerAdded.Rise();
            }
        }

        public void RemoveListener(ISourceActivity activity)
        {
            lock (sync)
            {
                if (!activitiesMonitorOwner.ContainsKey(activity))
                {
                    Tracer.Warning("Activity did not tapped");
                    return;
                }

                activitiesMonitorOwner[activity].Dispose();
                activitiesMonitorOwner.Remove(activity);

                ListenerRemoved.Rise();
            }
        }
    }
}