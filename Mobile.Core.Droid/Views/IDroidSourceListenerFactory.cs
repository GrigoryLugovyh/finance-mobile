﻿namespace Mobile.Core.Droid.Views
{
    public interface IDroidSourceListenerFactory
    {
        IDroidSourceListener Create(ISourceActivity source);
    }
}