﻿using System;

namespace Mobile.Core.Droid.Views
{
    public interface IDroidSourceListener : IDisposable
    {
        void Report(Action<IDroidActivityListener> call);
    }
}