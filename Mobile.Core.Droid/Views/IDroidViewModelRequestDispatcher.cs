﻿using System;
using Android.Content;
using Mobile.Core.BL.Core.ViewModels;

namespace Mobile.Core.Droid.Views
{
    public interface IDroidViewModelRequestDispatcher
    {
        Intent GetIntent(MViewModelRequest request);

        Type GetViewType(MViewModelRequest request);
    }
}