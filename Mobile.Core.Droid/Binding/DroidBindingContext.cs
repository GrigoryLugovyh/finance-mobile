﻿using Android.Content;
using Android.Views;
using Mobile.Core.BL.Core.Binding;
using Mobile.Core.BL.Core.Extensions;
using Mobile.Core.BL.IoCContainer;
using Mobile.Core.Droid.Controls;
using Mobile.Core.Droid.Extensions;
using Mobile.Core.Droid.Views;

namespace Mobile.Core.Droid.Binding
{
    public class DroidBindingContext : MBindingContext, IDroidBindingContext
    {
        public DroidBindingContext(Context context, IDroidLayoutInflater layout)
        {
            DataContext = context;
            Layout = layout;
        }
        
        public IDroidLayoutInflater Layout { get; set; }

        public View BindingInflate(View view, object source)
        {
            BindingInflateImpl(view, source);
            return view;
        }

        public View BindingInflate(int layoutResId, ViewGroup root = null, object template = null, string scope = "")
        {
            return BindingInflateImpl(layoutResId, root, template, scope);
        }

        protected virtual void BindingInflateImpl(View view, object source)
        {
            if (!(view is IDroidCustomView) && view is ViewGroup)
            {
                var @group = (ViewGroup)view;

                for (int i = 0; i <= @group.ChildCount; ++i)
                {
                    BindingInflateImpl(@group.GetChildAt(i), source);
                }
            }

            BindingExtension.BindingContainer.RefreshBindings((DataContext as ISourceActivity).UniqueId(), view, source);
        }

        protected virtual View BindingInflateImpl(int layoutResId, ViewGroup root = null, object template = null, string scope = "")
        {
            var layoutInflater = Layout.LayoutInflater;

            using (var clone = layoutInflater.CloneInContext(DataContext.ToDroidContext()))
            {
                using (var inflateFactory = Container.Resolve<IDroidBindingInflateFactoryHandler>().Create(template, scope))
                {
                    if (inflateFactory != null)
                    {
                        clone.Factory = inflateFactory;
                    }

                    var view = root == null
                        ? clone.Inflate(layoutResId, null)
                        : clone.Inflate(layoutResId, root, false);

                    return view;
                }
            }
        }
    }
}