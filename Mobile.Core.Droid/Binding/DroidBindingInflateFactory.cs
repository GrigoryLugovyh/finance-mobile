﻿using Android.Content;
using Android.Content.Res;
using Android.Util;
using Android.Views;
using Java.Lang;
using Mobile.Core.BL.Core;
using Mobile.Core.BL.Core.Binding;
using Mobile.Core.BL.Core.Extensions;
using Mobile.Core.BL.Core.ViewModels;
using Mobile.Core.BL.Core.Views;
using Mobile.Core.BL.IoCContainer;
using Mobile.Core.Droid.Extensions;
using Mobile.Core.Droid.Views;

namespace Mobile.Core.Droid.Binding
{
    public class DroidBindingInflateFactory : Object, IDroidBindingInflateFactory
    {
        private readonly string scope;

        public DroidBindingInflateFactory()
        {
        }

        public DroidBindingInflateFactory(object template) : this(template, string.Empty)
        {
        }

        public DroidBindingInflateFactory(object template, string scope)
        {
            this.scope = scope;
            Template = template;
        }

        public object Template { get; set; }

        public virtual View OnCreateView(string name, Context context, IAttributeSet attrs)
        {
            var factory = Container.Resolve<IDroidBindingViewFactory>();

            View view = factory.CreateView(name, context, attrs);

            if (view != null)
            {
                using (
                    TypedArray typedArray =
                        context.ObtainStyledAttributes(attrs,
                            DroidBindingResourceFactory.Instance.BindingStylableGroupId))
                {
                    for (int i = 0; i < typedArray.IndexCount; ++i)
                    {
                        int attributeId = typedArray.GetIndex(i);

                        BindViewFromAttribute(attributeId, typedArray, view, context);
                    }

                    typedArray.Recycle();
                }
            }

            return view;
        }

        protected virtual void BindViewFromAttribute(int attributeId, TypedArray typedArray, View view, Context context)
        {
            if (attributeId == DroidBindingResourceFactory.Instance.BindingBindId)
            {
                string bindingText = typedArray.GetString(attributeId);

                var binder = Container.Resolve<IMBinder>();

                var source = context as IMView;

                if (source == null)
                {
                    return;
                }

                var bindings = Template is IMViewModel
                    ? binder.Bind((IMViewModel) Template, view, bindingText, null as object)
                    : binder.Bind(source.ViewModel, view, bindingText, Template);

                bindings?.RegisterBindings(view, (context as ISourceActivity).UniqueId(), scope);
            }
            else
            {
                Tracer.Warning("Was unable to detect the attribute, check BindingAttributes.xml");
            }
        }
    }
}