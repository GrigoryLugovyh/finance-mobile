﻿using System;
using Mobile.Core.Droid.Platform;

namespace Mobile.Core.Droid.Binding
{
    public interface IDroidBindingResourceHandler
    {
        void InitResource(out Type styleable, out int[] bindingStylableGroupId, out int bindingBindId);

        object GetFieldValue(Type @type, string fieldName, int @default = DroidConstants.ResourceBindingDefaultValue);
    }
}