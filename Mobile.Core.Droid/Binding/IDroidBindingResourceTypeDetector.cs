﻿using System;

namespace Mobile.Core.Droid.Binding
{
    public interface IDroidBindingResourceTypeDetector
    {
        Type Detect();
    }
}