﻿using Android.Views;
using Mobile.Core.BL.Core.Binding;

namespace Mobile.Core.Droid.Binding
{
    public interface IDroidBindingInflateFactory : LayoutInflater.IFactory, IBindingFactory
    {
    }
}