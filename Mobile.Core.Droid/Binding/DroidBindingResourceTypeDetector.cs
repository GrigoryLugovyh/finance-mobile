﻿using System;
using Mobile.Core.BL.Core.Exceptions;
using Mobile.Core.BL.IoCContainer;
using Mobile.Core.Droid.Platform;

namespace Mobile.Core.Droid.Binding
{
    public class DroidBindingResourceTypeDetector : IDroidBindingResourceTypeDetector
    {
        public Type Detect()
        {
            var assembly = Container.Resolve<IDroidCurrentAssembly>();
            var resourceTypeName = string.Format("{0}.{1}", assembly.CurrentNamespace, DroidConstants.ResourceName);
            var resourceType = assembly.CurrentAssembly.GetType(resourceTypeName);
            if (resourceType == null)
            {
                throw new MException("Сould not detect the type of resource in the executable assembly");
            }

            return resourceType;
        }
    }
}