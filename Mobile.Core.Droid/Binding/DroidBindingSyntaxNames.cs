﻿namespace Mobile.Core.Droid.Binding
{
    public class DroidBindingSyntaxNames
    {
        /// <summary>
        ///     Нажание на widget
        ///     Синтаксис: "click: Method"
        ///     Method - метод из ViewModel
        /// </summary>
        public const string Click = "click";

        public const string ImageClick = "imageClick";

        /// <summary>
        ///     Аналогично Click, только Long :)
        /// </summary>
        public const string LongClick = "longClick";

        public const string ImageLongClick = "imageLongClick";

        /// <summary>
        ///     Запись/чтение TextView
        ///     Синтаксис: "label: Property"
        ///     Property - field или property из ViewModel
        /// </summary>
        public const string Label = "label";

        /// <summary>
        ///     Чекбокс
        ///     Синтаксис: "check: Property"
        ///     Property - свойство из ViewModel
        /// </summary>
        public const string Check = "check";

        /// <summary>
        ///     EditText
        /// </summary>
        public const string Text = "text";

        /// <summary>
        ///     Прогресс бар
        /// </summary>
        public const string Progress = "progress";

        /// <summary>
        ///     Биндинг коллекции в ListView, поддерживает аргументы в виде click: ListViewItem
        ///     Синтаксис: "list: TradePoints, { click: TradePointItem }"
        ///     TradePoints - коллекция из ViewModel реализующая IList
        ///     TradePointItem - метод из ViewModel, срабатывает при нажатии на элемент списка и принимает аргумент
        ///     MTargetBindingItemArgs
        /// </summary>
        public const string List = "list";

        /// <summary>
        ///     Биндинг коллекции в GridView, поддерживает аргументы в виде click: ListViewItem
        ///     Синтаксис: "list: TradePoints, { click: TradePointItem }"
        ///     TradePoints - коллекция из ViewModel реализующая IList
        ///     TradePointItem - метод из ViewModel, срабатывает при нажатии на элемент списка и принимает аргумент
        ///     MTargetBindingItemArgs
        /// </summary>
        public const string GridView = "grid";

        public const string Local = "local";
        public const string TextColor = "textColor";
        public const string BackgroundColor = "backgroundColor";
        public const string BackgroundDrawable = "backgroundDrawable";
        public const string Visible = "visible";
        public const string Invisible = "invisible";
        public const string ImageViewDrawable = "imageDrawable";
        public const string ButtonText = "textButton";
        public const string ImageDataArray = "imageDataArray";
        public const string ImagePath = "imagePath";
        public const string ImageBitmap = "imageBitmap";
        public const string DroidListSource = "listSource";
        public const string DroidGrigSource = "grigSource";
        public const string DroidPager = "pager";
        public const string DroidPagerIndex = "pagerIndex";
        public const string Shown = "shown";
        public const string Enable = "enable";
        public const string Disable = "disable";
        public const string ScrollViewUp = "scrollUp";
    }
}