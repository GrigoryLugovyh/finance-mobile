﻿namespace Mobile.Core.Droid.Binding
{
    public class DroidBindingInflateFactoryHandler : IDroidBindingInflateFactoryHandler
    {
        public IDroidBindingInflateFactory Create(object template = null, string scope = "")
        {
            // создание экземпляра именно этого типа реализовано в обход Container в виду того,
            // что при повторном Resolve при обращении к типу Java.Lang.Object падает Java.Lang.NullPointerException 
            return new DroidBindingInflateFactory(template, scope);
        }
    }
}