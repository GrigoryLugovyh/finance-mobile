﻿using Android.Content;
using Android.Util;
using Android.Views;

namespace Mobile.Core.Droid.Binding
{
    public interface IDroidBindingViewFactory
    {
        View CreateView(string name, Context context, IAttributeSet attrs);
    }
}