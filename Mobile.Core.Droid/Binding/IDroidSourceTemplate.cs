﻿namespace Mobile.Core.Droid.Binding
{
    public interface IDroidSourceTemplate
    {
        int ItemTemplateId { get; }
    }
}