﻿using Android.Views;
using Mobile.Core.Droid.Views;

namespace Mobile.Core.Droid.Binding
{
    public interface IDroidBindingContext
    {
        IDroidLayoutInflater Layout { get; }

        View BindingInflate(View view, object source);

        View BindingInflate(int layoutResId, ViewGroup root = null, object template = null, string scope = "");
    }
}