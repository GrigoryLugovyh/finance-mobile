﻿using System;
using Android.Content;
using Android.Util;
using Android.Views;
using Mobile.Core.BL.Core;
using Mobile.Core.BL.Core.Extensions;
using Mobile.Core.BL.IoCContainer;
using Mobile.Core.Droid.Platform;

namespace Mobile.Core.Droid.Binding
{
    public class DroidBindingViewFactory : IDroidBindingViewFactory
    {
        public View CreateView(string name, Context context, IAttributeSet attrs)
        {
            try
            {
                var cash = Container.Resolve<IDroidCacheTypes>();

                var type = cash.Resolve(name);

                if (type == null)
                {
                    Tracer.Warning("To create a view could not get the type name {0}", name);
                    return null;
                }

                var view = Activator.CreateInstance(type, context, attrs) as View;

                if (view == null)
                {
                    Tracer.Warning("Failed to create a presentation lead to the desired type View");
                }

                return view;
            }
            catch (Exception ex)
            {
                Tracer.Error(
                    "While trying to get the type of representation of the name {0} of the error occurred: {1}", name,
                    ex.ToLong());
                return null;
            }
        }
    }
}