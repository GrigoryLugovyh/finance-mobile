﻿using System;
using System.Collections;
using Android.Widget;
using Mobile.Core.BL.Core.Extensions;
using Mobile.Core.BL.IoCContainer;
using Mobile.Core.Droid.Extensions;
using Mobile.Core.Droid.Views;

namespace Mobile.Core.Droid.Binding.Target
{
    public class DroidListViewBinding<T> : DroidTargetBinding where T : AbsListView
    {
        protected IList source = new ArrayList();

        private readonly string currentContextUniqueId;

        public DroidListViewBinding(T listView)
            : base(listView)
        {
            currentContextUniqueId =
                ((ISourceActivity)Container.Resolve<IDroidBindingContextStack<IDroidView>>().Current).UniqueId();
        }

        private T View => Target as T;

        public override Type TargetType => typeof (T);

        public override void Subscribe()
        {
            var view = View;

            if (view == null)
            {
                return;
            }

            view.ItemClick += ListViewOnItemClick;
        }

        private void ListViewOnItemClick(object sender, AdapterView.ItemClickEventArgs itemClickEventArgs)
        {
            var view = View;

            var item = view?.Adapter.GetItem(itemClickEventArgs.Position).FromJava<object>();

            if (item != null)
            {
                RiseItemValueChanged(item, itemClickEventArgs.Position);
            }
        }

        protected virtual IListAdapter GetAdapter()
        {
            return new DroidListAdapter(source);
        }

        protected virtual void RefreshSource(IList sourceCandidate)
        {
            source.Clear();

            foreach (var item in sourceCandidate)
            {
                source.Add(item);
            }
        }

        protected override void SetValueTarget(object value)
        {
            var view = View;

            if (view == null)
            {
                return;
            }

            if (!(value is IList))
            {
                return;
            }

            RefreshSource((IList) value);

            if (view.Adapter == null)
            {
                view.Adapter = GetAdapter();
            }
            else
            {
                ((DroidListAdapter) view.Adapter).NotifyDataSetChanged();
            }
        }

        protected override object GetValueTarget()
        {
            return null;
        }

        protected override bool ValuesEquals(object newValue, object currentValue)
        {
            return false;
        }

        protected override void Dispose(bool isDisposing)
        {
            base.Dispose(isDisposing);

            if (isDisposing)
            {
                var view = View;

                if (view == null)
                {
                    return;
                }

                foreach (var item in source)
                {
                    BindingExtension.UnregisterBindingsBySource(currentContextUniqueId, item);
                }

                source = null;
                view.Adapter = null;

                view.ItemClick -= ListViewOnItemClick;
            }
        }
    }
}