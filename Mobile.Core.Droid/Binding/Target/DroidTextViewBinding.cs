﻿using System;
using Android.Widget;

namespace Mobile.Core.Droid.Binding.Target
{
    public class DroidTextViewBinding : DroidTargetBinding
    {
        public DroidTextViewBinding(TextView textView)
            : base(textView)
        {
        }

        protected TextView TextView => Target as TextView;

        public override Type TargetType => typeof (string);

        protected override void SetValueTarget(object value)
        {
            TextView view = TextView;

            if (view == null)
            {
                return;
            }

            if (value == null)
            {
                value = string.Empty;
            }

            view.Text = value.ToString();
        }

        protected override object GetValueTarget()
        {
            TextView view = TextView;

            return view?.Text;
        }

        protected override bool ValuesEquals(object newValue, object currentValue)
        {
            return Equals(newValue, currentValue);
        }
    }
}