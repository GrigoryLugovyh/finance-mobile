﻿using System;
using System.Windows.Input;
using Android.Views;

namespace Mobile.Core.Droid.Binding.Target
{
    public class DroidViewLongClickBinding : DroidTargetBinding
    {
        protected ICommand command;

        public DroidViewLongClickBinding(View view)
            : base(view)
        {
        }

        protected View View => Target as View;

        public override Type TargetType => typeof (ICommand);

        public override void Subscribe()
        {
            var view = View;

            if (view == null)
            {
                return;
            }

            view.LongClick += ViewLongClick;
        }

        public void ViewLongClick(object sender, View.LongClickEventArgs e)
        {
            if (command == null || !command.CanExecute(null))
            {
                return;
            }

            command.Execute(null);
        }

        protected override void SetValueTarget(object value)
        {
            command = value as ICommand;
        }

        protected override object GetValueTarget()
        {
            return command;
        }

        protected override bool ValuesEquals(object newValue, object currentValue)
        {
            return false;
        }

        protected override void Dispose(bool isDisposing)
        {
            base.Dispose(isDisposing);

            if (isDisposing)
            {
                var view = View;

                if (view != null)
                {
                    view.LongClick -= ViewLongClick;
                }
            }
        }
    }
}