﻿using System;
using Android.Views;

namespace Mobile.Core.Droid.Binding.Target
{
    public class DroidVisibleBinding : DroidTargetBinding
    {
        private readonly bool gone;
        private readonly bool requestFocus;
        private readonly bool inverted;

        public DroidVisibleBinding(View target, bool inverted = false, bool gone = true, bool requestFocus = false) : base(target)
        {
            this.inverted = inverted;
            this.gone = gone;
            this.requestFocus = requestFocus;
        }

        protected View View => Target as View;

        public override Type TargetType => typeof (bool);

        protected override void SetValueTarget(object value)
        {
            if (View == null)
            {
                return;
            }

            var valueIsEmpty = CheckValueIsEmpty(value);
            var stateGone = gone ? ViewStates.Gone : ViewStates.Invisible;

            View.Visibility = inverted
                ? (valueIsEmpty ? ViewStates.Visible : stateGone)
                : (valueIsEmpty ? stateGone : ViewStates.Visible);

            if (View.Visibility == ViewStates.Visible && requestFocus)
            {
                View.RequestFocus();
            }
        }

        private bool CheckValueIsEmpty(object value)
        {
            if (value == null)
                return true;

            if (value is decimal)
                return (decimal) value == 0;
            if (value is double)
                return Math.Abs((double) value) < 0.0001;
            if (value is int)
                return (int) value == 0;
            if (value is bool)
                return !(bool) value;
            if (value is string)
                return string.IsNullOrEmpty((string) value);

            return false;
        }

        protected override object GetValueTarget()
        {
            return inverted ? View.Visibility == ViewStates.Gone : View.Visibility == ViewStates.Visible;
        }

        protected override bool ValuesEquals(object newValue, object currentValue)
        {
            return Equals(newValue, currentValue);
        }
    }
}