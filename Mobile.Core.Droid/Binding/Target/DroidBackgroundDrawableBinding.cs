﻿using System;
using Android.Views;

namespace Mobile.Core.Droid.Binding.Target
{
    public class DroidBackgroundDrawableBinding : DroidTargetBinding
    {
        public DroidBackgroundDrawableBinding(View target) : base(target)
        {
        }

        private View View => Target as View;

        public override Type TargetType => typeof (int);

        protected override void SetValueTarget(object value)
        {
            View?.SetBackgroundResource((int)value);
        }

        protected override object GetValueTarget()
        {
            return null;
        }

        protected override bool ValuesEquals(object newValue, object currentValue)
        {
            return false;
        }
    }
}