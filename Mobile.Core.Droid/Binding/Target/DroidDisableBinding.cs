﻿namespace Mobile.Core.Droid.Binding.Target
{
    public class DroidDisableBinding : DroidEnableBinding
    {
        public DroidDisableBinding(object target) : base(target)
        {
        }

        protected override bool Prepare(bool value)
        {
            return !base.Prepare(value);
        }
    }
}