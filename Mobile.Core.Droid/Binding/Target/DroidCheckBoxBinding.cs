﻿using System;
using Android.Widget;

namespace Mobile.Core.Droid.Binding.Target
{
    public class DroidCheckBoxBinding : DroidTargetBinding
    {
        public DroidCheckBoxBinding(CheckBox target) : base(target)
        {
        }

        protected CheckBox CheckBox => Target as CheckBox;

        public override Type TargetType => typeof (bool);

        protected override void SetValueTarget(object value)
        {
            CheckBox view = CheckBox;

            if (view == null || value == null)
            {
                return;
            }

            view.Checked = (bool) value;
        }

        protected override object GetValueTarget()
        {
            CheckBox view = CheckBox;

            return view != null && view.Checked;
        }

        protected override bool ValuesEquals(object newValue, object currentValue)
        {
            return Equals(newValue, currentValue);
        }

        public override void Subscribe()
        {
            CheckBox view = CheckBox;

            if (view == null)
            {
                return;
            }

            view.CheckedChange += ViewOnCheckedChange;
        }

        private void ViewOnCheckedChange(object sender, CompoundButton.CheckedChangeEventArgs checkedChangeEventArgs)
        {
            CheckBox view = CheckBox;

            if (view == null)
            {
                return;
            }

            RiseValueChanged(view.Checked);
        }

        protected override void Dispose(bool isDisposing)
        {
            base.Dispose(isDisposing);

            if (isDisposing)
            {
                var view = CheckBox;

                if (view == null)
                {
                    return;
                }

                view.CheckedChange -= ViewOnCheckedChange;
            }
        }
    }
}