﻿using System;
using System.Collections;
using System.Collections.Generic;
using Android.Support.V4.View;
using Android.Views;
using Android.Widget;
using Mobile.Core.BL.Core.Extensions;
using Mobile.Core.BL.IoCContainer;
using Mobile.Core.Droid.Extensions;
using Mobile.Core.Droid.Views;
using Object = Java.Lang.Object;

namespace Mobile.Core.Droid.Binding.Target
{
    public class DroidPagerAdapter : PagerAdapter
    {
        public enum AdapterTemplateMode
        {
            Single = 0,
            Multi
        }

        private readonly IList items;
        private readonly int templateId;
        private readonly int[] templateIds;
        private readonly string currentContextUniqueId;
        private readonly AdapterTemplateMode templateMode;
        private readonly IDroidBindingContext bindingContext;

        private IDroidBindingContextStack<IDroidView> contextStack;

        private readonly Dictionary<AdapterTemplateMode, Func<int, int>> templateOwner;

        public DroidPagerAdapter(IList items, int templateId) : this(items)
        {
            this.templateId = templateId;
            templateMode = AdapterTemplateMode.Single;
        }

        public DroidPagerAdapter(IList items, int[] templateIds) : this(items)
        {
            this.templateIds = templateIds;
            templateMode = AdapterTemplateMode.Multi;
        }

        private DroidPagerAdapter(IList items)
        {
            this.items = items;

            bindingContext = Context.BindingContext();
            currentContextUniqueId = ((ISourceActivity) Context).UniqueId();

            templateOwner = new Dictionary
                <AdapterTemplateMode, Func<int, int>>
            {
                {
                    AdapterTemplateMode.Single, i => templateId
                },
                {
                    AdapterTemplateMode.Multi, portion => templateIds[portion]
                }
            };
        }

        private IDroidBindingContextStack<IDroidView> ContextStack
            => contextStack ?? (contextStack = Container.Resolve<IDroidBindingContextStack<IDroidView>>());

        private IDroidView Context => ContextStack.Current;

        public override int Count => items.Count;

        public override bool IsViewFromObject(View view, Object @object)
        {
            return view == (LinearLayout) @object;
        }

        public override Object InstantiateItem(ViewGroup container, int position)
        {
            var view = bindingContext.BindingInflate(templateOwner[templateMode](position), container, items[position]);
            container.AddView(view);
            return view;
        }

        public override void DestroyItem(ViewGroup container, int position, Object @object)
        {
            BindingExtension.UnregisterBindingsBySource(currentContextUniqueId, items[position]);
            container.RemoveView((LinearLayout) @object);
        }
    }
}