﻿using System.Collections;

namespace Mobile.Core.Droid.Binding.Target
{
    public class DroidSourceAdapter : DroidListAdapter
    {
        private readonly int itemTemplate;

        public DroidSourceAdapter(IList items, int itemTemplate) : base(items)
        {
            this.itemTemplate = itemTemplate;
        }

        protected override int TemplateId(object source)
        {
            return itemTemplate;
        }
    }
}