﻿using System;
using Android.Text;
using Android.Widget;

namespace Mobile.Core.Droid.Binding.Target
{
    public class DroidEditTextBinding : DroidTargetBinding
    {
        public DroidEditTextBinding(EditText editText)
            : base(editText)
        {
        }

        protected EditText EditText => Target as EditText;

        public override Type TargetType => typeof (string);

        protected override void SetValueTarget(object value)
        {
            var view = EditText;
            
            if (view == null)
            {
                return;
            }

            if (value == null)
            {
                value = string.Empty;
            }

            view.Text = value.ToString();
        }

        protected override object GetValueTarget()
        {
            var view = EditText;

            return view?.Text;
        }

        protected override bool ValuesEquals(object newValue, object currentValue)
        {
            return Equals(newValue, currentValue);
        }

        public override void Subscribe()
        {
            var view = EditText;

            if (view == null)
            {
                return;
            }

            view.AfterTextChanged += ViewOnAfterTextChanged;
        }

        private void ViewOnAfterTextChanged(object sender, AfterTextChangedEventArgs e)
        {
            var view = EditText;

            if (view == null)
            {
                return;
            }

            RiseValueChanged(view.Text);
        }

        protected override void Dispose(bool isDisposing)
        {
            base.Dispose(isDisposing);

            if (isDisposing)
            {
                var view = EditText;

                if (view == null)
                {
                    return;
                }

                view.AfterTextChanged -= ViewOnAfterTextChanged;
            }
        }
    }
}