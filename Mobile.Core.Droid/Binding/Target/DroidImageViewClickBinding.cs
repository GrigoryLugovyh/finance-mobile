﻿using Android.Views;

namespace Mobile.Core.Droid.Binding.Target
{
    public class DroidImageViewClickBinding : DroidViewClickBinding
    {
        public DroidImageViewClickBinding(View view) : base(view)
        {
        }
    }
}