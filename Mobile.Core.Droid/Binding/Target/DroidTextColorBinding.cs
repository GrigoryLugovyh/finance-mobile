﻿using System;
using Android.Graphics;
using Android.Widget;

namespace Mobile.Core.Droid.Binding.Target
{
    public class DroidTextColorBinding : DroidTargetBinding
    {
        public DroidTextColorBinding(TextView target) : base(target)
        {
        }

        protected TextView View => Target as TextView;

        public override Type TargetType => typeof (bool);

        protected override void SetValueTarget(object value)
        {
            if (value is Color)
            {
                View?.SetTextColor((Color) value);
            }
        }

        protected override object GetValueTarget()
        {
            return null;
        }

        protected override bool ValuesEquals(object newValue, object currentValue)
        {
            return false;
        }
    }
}