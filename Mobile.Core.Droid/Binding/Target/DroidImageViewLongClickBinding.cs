﻿using Android.Views;

namespace Mobile.Core.Droid.Binding.Target
{
    public class DroidImageViewLongClickBinding : DroidViewLongClickBinding
    {
        public DroidImageViewLongClickBinding(View view) : base(view)
        {
        }
    }
}