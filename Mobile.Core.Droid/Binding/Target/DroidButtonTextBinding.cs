﻿using System;
using Android.Widget;

namespace Mobile.Core.Droid.Binding.Target
{
    public class DroidButtonTextBinding : DroidTargetBinding
    {
        public DroidButtonTextBinding(object target) : base(target)
        {
        }

        private Button View => Target as Button;

        public override Type TargetType => typeof (string);

        protected override void SetValueTarget(object value)
        {
            var button = View;

            if (button == null)
            {
                return;
            }

            button.Text = (string) value;
        }

        protected override object GetValueTarget()
        {
            var button = View;

            return button?.Text;
        }

        protected override bool ValuesEquals(object newValue, object currentValue)
        {
            return newValue == currentValue;
        }
    }
}