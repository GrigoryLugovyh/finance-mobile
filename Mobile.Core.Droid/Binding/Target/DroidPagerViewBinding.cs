﻿using System;
using System.Collections;
using Android.Support.V4.View;
using Mobile.Core.BL.Core.Binding.Target.Values;
using Mobile.Core.Droid.Controls;

namespace Mobile.Core.Droid.Binding.Target
{
    public class DroidPagerViewBinding : DroidTargetBinding
    {
        protected IList source = new ArrayList();

        public DroidPagerViewBinding(object target) : base(target)
        {
        }

        private DroidPagerView View => Target as DroidPagerView;

        protected virtual int[] ItemsTemplateId
        {
            get
            {
                var view = View;
                return view == null ? new int[0] : new[] {view.ItemTemplateId};
            }
        }

        public override Type TargetType => typeof (IList);

        protected virtual void RefreshSource(IList sourceCandidate)
        {
            if (sourceCandidate == null) return;

            source.Clear();

            foreach (var item in sourceCandidate)
            {
                source.Add(item);
            }
        }

        protected virtual void ViewOnPageSelected(object sender, ViewPager.PageSelectedEventArgs a)
        {
            var view = View;

            if (view == null)
            {
                return;
            }

            var item = source[a.Position];

            if (item != null)
            {
                RiseItemValueChanged(item, a.Position);
            }
        }

        protected virtual PagerAdapter CreatePagerAdapter()
        {
            return ItemsTemplateId.Length == 1
                ? new DroidPagerAdapter(source, ItemsTemplateId[0])
                : new DroidPagerAdapter(source, ItemsTemplateId);
        }

        public override void Subscribe()
        {
            base.Subscribe();

            var view = View;

            if (view == null)
            {
                return;
            }

            view.PageSelected += ViewOnPageSelected;
        }

        protected override void SetValueTarget(object value)
        {
            var view = View;

            if (view == null)
            {
                return;
            }

            var pagerValue = value as IPagerValue;

            if (!(pagerValue?.Value is IList) && !(value is IList))
            {
                return;
            }

            RefreshSource((pagerValue != null ? pagerValue.Value : value) as IList);

            if (view.Adapter == null)
            {
                view.Adapter = CreatePagerAdapter();
            }
            else
            {
                view.Adapter.NotifyDataSetChanged();
            }

            if (pagerValue != null && pagerValue.CurrentItem >= 0)
            {
                var currentItem = pagerValue.CurrentItem;

                if (currentItem <= source.Count)
                {
                    view.SetCurrentItem(currentItem, true);
                }
            }
        }

        protected override object GetValueTarget()
        {
            return null;
        }

        protected override bool ValuesEquals(object newValue, object currentValue)
        {
            return false;
        }

        protected override void Dispose(bool isDisposing)
        {
            base.Dispose(isDisposing);

            if (isDisposing)
            {
                var view = View;

                if (view == null)
                {
                    return;
                }

                view.PageSelected -= ViewOnPageSelected;
                view.Adapter = null;
                source = null;
            }
        }
    }
}