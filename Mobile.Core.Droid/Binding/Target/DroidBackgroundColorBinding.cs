﻿using System;
using Android.Graphics;
using Android.Views;

namespace Mobile.Core.Droid.Binding.Target
{
    public class DroidBackgroundColorBinding : DroidTargetBinding
    {
        public DroidBackgroundColorBinding(View target) : base(target)
        {
        }

        protected View View => Target as View;

        public override Type TargetType => typeof (bool);

        protected override void SetValueTarget(object value)
        {
            View?.SetBackgroundColor((Color) value);
        }

        protected override object GetValueTarget()
        {
            return null;
        }

        protected override bool ValuesEquals(object newValue, object currentValue)
        {
            return false;
        }
    }
}