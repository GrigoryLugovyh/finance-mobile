﻿using System;
using Android.Widget;
using Mobile.Core.BL.Core.Extensions;

namespace Mobile.Core.Droid.Binding.Target
{
    public class DroidProgressBarBinding : DroidTargetBinding
    {
        public DroidProgressBarBinding(ProgressBar progressBar)
            : base(progressBar)
        {
        }

        protected ProgressBar ProgressBar => Target as ProgressBar;

        public override Type TargetType => typeof (int);

        protected override void SetValueTarget(object value)
        {
            ProgressBar view = ProgressBar;

            if (view == null || value == null)
            {
                return;
            }

            var progress = value.As<int>();

            if (progress > 100)
            {
                progress = 100;
            }

            if (progress < 0)
            {
                progress = 0;
            }

            view.Progress = progress;
        }

        protected override object GetValueTarget()
        {
            ProgressBar view = ProgressBar;

            if (view == null)
            {
                return 0;
            }

            return view.Progress;
        }

        protected override bool ValuesEquals(object newValue, object currentValue)
        {
            return currentValue.As<int>().Equals(newValue.As<int>());
        }
    }
}