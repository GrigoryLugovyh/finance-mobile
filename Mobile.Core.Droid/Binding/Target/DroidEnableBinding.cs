﻿using System;
using Android.Views;

namespace Mobile.Core.Droid.Binding.Target
{
    public class DroidEnableBinding : DroidTargetBinding
    {
        public DroidEnableBinding(object target) : base(target)
        {
        }

        protected View View => Target as View;

        public override Type TargetType => typeof (bool);

        protected override void SetValueTarget(object value)
        {
            if (View == null || !(value is bool))
            {
                return;
            }

            Enable(View, Prepare((bool) value));
        }

        protected override object GetValueTarget()
        {
            if (View == null)
            {
                return false;
            }

            return Prepare(View.Enabled);
        }

        protected override bool ValuesEquals(object newValue, object currentValue)
        {
            return newValue.Equals(currentValue);
        }

        protected virtual bool Prepare(bool value)
        {
            return value;
        }

        private void Enable(View view, bool value)
        {
            if (view == null)
            {
                return;
            }

            if (view is ViewGroup && ((ViewGroup) view).ChildCount > 0)
            {
                var group = (ViewGroup) view;

                for (var i = 0; i < group.ChildCount; i++)
                {
                    var child = group.GetChildAt(i);
                    Enable(child, value);
                }
            }

            view.Enabled = value;
        }
    }
}