﻿using System;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.Views;
using Android.Widget;
using Mobile.Core.Droid.Extensions;

namespace Mobile.Core.Droid.Binding.Target
{
    public class DroidImageDrawableBinding : DroidTargetBinding
    {
        public DroidImageDrawableBinding(ImageView target)
            : base(target)
        {
        }

        private Drawable image;

        protected ImageView ImageView => Target as ImageView;

        public override Type TargetType => typeof (Tuple<object, int?, int?>);

        protected override void SetValueTarget(object value)
        {
            var view = ImageView;

            if (view == null)
            {
                return;
            }

            if (!(value is Tuple<object, int?, int?>))
            {
                view.SetImageBitmap(null);
            }
            else
            {
                var imageParams = (Tuple<object, int?, int?>) value;
                
                if (imageParams.Item1 is byte[])
                {
                    var buffer = imageParams.Item1 as byte[];

                    image =
                        new BitmapDrawable(BitmapFactory.DecodeByteArray(buffer, 0, buffer.Length,
                            new BitmapFactory.Options
                            {
                                InPurgeable = true
                            }));
                }
                else
                {
                    image = (Drawable)imageParams.Item1;
                }
                
                view.Visibility = ViewStates.Visible;

                if (imageParams.Item2.HasValue && imageParams.Item3.HasValue &&
                    ((image.MinimumWidth > imageParams.Item2.Value) ||(image.MinimumHeight > imageParams.Item3.Value)))
                {
                    image = new BitmapDrawable(Bitmap.CreateScaledBitmap(((BitmapDrawable) image).Bitmap, imageParams.Item2.Value,
                        imageParams.Item3.Value,false));
                }

                view.SetImageDrawable(image);
            }
        }

        protected override object GetValueTarget()
        {
            var view = ImageView;
            return view.Drawable;
        }

        protected override bool ValuesEquals(object newValue, object currentValue)
        {
            return false;
        }

        protected override void Dispose(bool isDisposing)
        {
            base.Dispose(isDisposing);

            if (isDisposing)
            {
                image?.Dispose();
                image = null;
            }
        }
    }
}