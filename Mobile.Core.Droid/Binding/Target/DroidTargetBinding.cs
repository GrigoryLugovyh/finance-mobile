﻿using System.Threading;
using Android.App;
using Mobile.Core.BL.Core.Binding.Target;

namespace Mobile.Core.Droid.Binding.Target
{
    public abstract class DroidTargetBinding : MCommonTargetBinding
    {
        protected DroidTargetBinding(object target) : base(target)
        {
        }

        public override void SetValue(object value)
        {
            if (Application.SynchronizationContext == SynchronizationContext.Current)
            {
                base.SetValue(value);
            }
            else
            {
                Application.SynchronizationContext.Post(d => base.SetValue(value), null);
            }
        }
    }
}