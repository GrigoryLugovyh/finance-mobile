﻿using System;
using System.Collections;
using Android.Widget;

namespace Mobile.Core.Droid.Binding.Target
{
    public class DroidSourceViewBinding<T> : DroidListViewBinding<T> where T : AbsListView, IDroidSourceTemplate
    {
        public DroidSourceViewBinding(T listView)
            : base(listView)
        {
        }

        private T View => Target as T;

        public override Type TargetType => typeof (T);

        protected override void SetValueTarget(object value)
        {
            var view = View;

            if (view == null)
            {
                return;
            }

            if (!(value is IList))
            {
                return;
            }

            RefreshSource(value as IList);

            if (view.Adapter == null)
            {
                view.Adapter = GetAdapter();
            }
            else
            {
                ((DroidSourceAdapter) view.Adapter).NotifyDataSetChanged();
            }
        }

        protected override IListAdapter GetAdapter()
        {
            return new DroidSourceAdapter(source, View.ItemTemplateId);
        }
    }
}