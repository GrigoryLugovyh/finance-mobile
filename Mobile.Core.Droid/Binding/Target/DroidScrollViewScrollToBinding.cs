﻿using System;
using Android.Views;
using Android.Widget;

namespace Mobile.Core.Droid.Binding.Target
{
    public class DroidScrollViewScrollToBinding : DroidTargetBinding
    {
        private readonly FocusSearchDirection direction;

        public DroidScrollViewScrollToBinding(object target, FocusSearchDirection direction) : base(target)
        {
            this.direction = direction;
        }

        private ScrollView View => Target as ScrollView;

        public override Type TargetType => typeof (bool);

        protected override void SetValueTarget(object value)
        {
            if (value is bool && (bool) value)
            {
                View?.PageScroll(direction);
            }
        }

        protected override object GetValueTarget()
        {
            return null;
        }

        protected override bool ValuesEquals(object newValue, object currentValue)
        {
            return false;
        }
    }
}