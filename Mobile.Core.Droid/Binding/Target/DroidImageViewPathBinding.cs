﻿using System;
using Android.Widget;
using Java.IO;
using Uri = Android.Net.Uri;

namespace Mobile.Core.Droid.Binding.Target
{
    public class DroidImageViewPathBinding : DroidTargetBinding
    {
        private string filePath;

        public DroidImageViewPathBinding(ImageView target) : base(target)
        {
        }

        protected ImageView ImageView => Target as ImageView;

        public override Type TargetType => typeof(string);

        protected override void SetValueTarget(object value)
        {
            if (ImageView == null || !(value is string))
                return;

            var data = (string) value;

            filePath = data;

            ImageView.SetImageURI(Uri.FromFile(new File(filePath)));
        }

        protected override object GetValueTarget()
        {
            return filePath;
        }

        protected override bool ValuesEquals(object newValue, object currentValue)
        {
            return false;
        }
    }
}