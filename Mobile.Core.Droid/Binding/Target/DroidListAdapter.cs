﻿using System;
using System.Collections;
using Android.Views;
using Android.Widget;
using Mobile.Core.BL.IoCContainer;
using Mobile.Core.Droid.Extensions;
using Mobile.Core.Droid.Views;
using Object = Java.Lang.Object;

namespace Mobile.Core.Droid.Binding.Target
{
    public class DroidListAdapter : BaseAdapter
    {
        protected readonly IDroidBindingContext bindingContext;
        private readonly IList items;
        private readonly IDroidView layout;

        public DroidListAdapter(IList items)
        {
            this.items = items;

            layout = Container.Resolve<IDroidBindingContextStack<IDroidView>>().Current;

            if (layout == null)
            {
                throw new NullReferenceException("There is not layout for BindingContext");
            }

            bindingContext = layout.BindingContext();
        }

        public override int Count => items.Count;

        public override Object GetItem(int position)
        {
            return items[position].ToJava();
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            object source = items[position];

            return GetBindableView(convertView, parent, source);
        }

        protected virtual int TemplateId(object source)
        {
            return layout.ListTemplateLayout(source);
        }

        protected virtual View GetBindableView(View convertView, ViewGroup parent, object source)
        {
            var templateId = TemplateId(source);

            if (convertView != null && convertView.Tag.Equals(templateId))
            {
                return bindingContext.BindingInflate(convertView, source);
            }

            var view = bindingContext.BindingInflate(templateId, null, source);
            view.Tag = templateId;

            return view;
        }
    }
}