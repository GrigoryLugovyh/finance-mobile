﻿using System;
using Mobile.Core.Droid.Controls;

namespace Mobile.Core.Droid.Binding.Target
{
    public class DroidPagerViewIndexBinding : DroidTargetBinding
    {
        public DroidPagerViewIndexBinding(object target) : base(target)
        {
        }

        private DroidPagerView View => Target as DroidPagerView;

        public override Type TargetType => typeof (int);

        protected override void SetValueTarget(object value)
        {
            var view = View;

            if (view == null || !(value is int))
            {
                return;
            }

            if (view.Adapter != null && view.Adapter.Count > 0)
            {
                view.SetCurrentItem((int) value, true);
            }
        }

        protected override object GetValueTarget()
        {
            var view = View;

            if (view == null)
            {
                return -1;
            }

            return view.CurrentItem;
        }

        protected override bool ValuesEquals(object newValue, object currentValue)
        {
            return newValue.Equals(currentValue);
        }
    }
}