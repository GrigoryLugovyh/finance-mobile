﻿using System;
using Android.Graphics;
using Android.Views;
using Android.Widget;
using Mobile.Core.Droid.Extensions;

namespace Mobile.Core.Droid.Binding.Target
{
    public class DroidImageViewBitmapBinding : DroidTargetBinding
    {
        public DroidImageViewBitmapBinding(object target) : base(target)
        {
        }

        private Bitmap image;

        private ImageView View => Target as ImageView;

        public override Type TargetType => typeof (Bitmap);

        protected override void SetValueTarget(object value)
        {
            if (View == null)
            {
                return;
            }

            View.DrawableRecycle();

            image = value as Bitmap;

            if (image == null)
            {
                View.Visibility = ViewStates.Gone;
            }
            else
            {
                View.Visibility = ViewStates.Visible;
                View.SetImageBitmap(image);
            }
        }

        protected override object GetValueTarget()
        {
            return null;
        }

        protected override bool ValuesEquals(object newValue, object currentValue)
        {
            return false;
        }

        protected override void Dispose(bool isDisposing)
        {
            base.Dispose(isDisposing);

            if (isDisposing)
            {
                View?.DrawableRecycle();
            }

            if (isDisposing && image != null)
            {
                image.Recycle();
                image = null;
            }
        }
    }
}