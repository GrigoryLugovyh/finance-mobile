﻿using System;
using System.Reflection;
using Android.Views;

namespace Mobile.Core.Droid.Binding.Target
{
    public class DroidViewLocalBinding : DroidTargetBinding
    {
        public DroidViewLocalBinding(object target) : base(target)
        {
        }

        private View View => Target as View;

        private PropertyInfo LocalTextProperty
        {
            get
            {
                var view = View;

                if (view == null)
                {
                    return null;
                }

                var property = View.GetType().GetProperty("Text");

                return property;
            }
        }

        public override Type TargetType => typeof (string);

        protected override void SetValueTarget(object value)
        {
            if (!(value is string))
            {
                return;
            }

            var prop = LocalTextProperty;

            prop?.SetValue(View, value);
        }

        protected override object GetValueTarget()
        {
            var prop = LocalTextProperty;

            if (prop == null)
            {
                return string.Empty;
            }

            return prop.GetValue(View);
        }

        protected override bool ValuesEquals(object newValue, object currentValue)
        {
            return newValue is string && currentValue is string && Equals(newValue, currentValue);
        }
    }
}