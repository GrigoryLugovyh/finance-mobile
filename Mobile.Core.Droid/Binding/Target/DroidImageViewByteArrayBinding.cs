﻿using System;
using Android.Graphics;
using Android.Views;
using Android.Widget;
using Mobile.Core.Droid.Extensions;

namespace Mobile.Core.Droid.Binding.Target
{
    public class DroidImageViewByteArrayBinding : DroidTargetBinding
    {
        private byte[] imageData;

        public DroidImageViewByteArrayBinding(ImageView target) : base(target)
        {
        }

        private Bitmap image;

        protected ImageView ImageView => Target as ImageView;

        public override Type TargetType => typeof(byte[]);

        protected override void SetValueTarget(object value)
        {
            if (ImageView == null || !(value is byte[]))
                return;

            var data = (byte[]) value;

            imageData = data;

            image = BitmapFactory.DecodeByteArray(data, 0, data.Length, new BitmapFactory.Options
            {
                InPurgeable = true
            });

            if (image == null)
            {
                ImageView.Visibility = ViewStates.Gone;
            }
            else
            {
                ImageView.Visibility = ViewStates.Visible;
                ImageView.SetImageBitmap(image);
            }
        }

        protected override object GetValueTarget()
        {
            return imageData;
        }

        protected override bool ValuesEquals(object newValue, object currentValue)
        {
            return false;
        }

        protected override void Dispose(bool isDisposing)
        {
            base.Dispose(isDisposing);

            if (isDisposing && image != null)
            {
                image.Recycle();
                image = null;
            }
        }
    }
}