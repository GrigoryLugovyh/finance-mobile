﻿namespace Mobile.Core.Droid.Binding
{
    public interface IDroidBindingInflateFactoryHandler
    {
        IDroidBindingInflateFactory Create(object template = null, string scope = "");
    }
}