﻿using System;
using Mobile.Core.BL.Core.Extensions;
using Mobile.Core.BL.IoCContainer;

namespace Mobile.Core.Droid.Binding
{
    public sealed class DroidBindingResourceFactory
    {
        private static readonly object sync = new object();

        private static volatile DroidBindingResourceFactory instance;

        private int bindingBindId;
        private int[] bindingStylableGroupId;
        private Type stylableType;

        public static DroidBindingResourceFactory Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (sync)
                    {
                        if (instance == null)
                        {
                            instance = new DroidBindingResourceFactory();
                            instance.InitResource();
                        }
                    }
                }

                return instance;
            }
        }

        public Type StylableType => stylableType;

        public int[] BindingStylableGroupId => bindingStylableGroupId;

        public int BindingBindId => bindingBindId;

        private bool Initialized { get; set; }

        private void InitResource()
        {
            lock (sync)
            {
                if (!Initialized)
                {
                    try
                    {
                        var resourceHandler = Container.Resolve<IDroidBindingResourceHandler>();

                        resourceHandler.InitResource(out stylableType, out bindingStylableGroupId, out bindingBindId);
                    }
                    catch (Exception ex)
                    {
                        throw ex.MExpand(
                            "Could not get the type of resource, it may be due to the fact that the project file is missing BindingAttributes.xml");
                    }
                    finally
                    {
                        Initialized = true;
                    }
                }
            }
        }
    }
}