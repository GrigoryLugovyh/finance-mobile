﻿using System;
using Mobile.Core.BL.Core;
using Mobile.Core.BL.IoCContainer;
using Mobile.Core.Droid.Platform;

namespace Mobile.Core.Droid.Binding
{
    public class DroidBindingResourceHandler : IDroidBindingResourceHandler
    {
        public void InitResource(out Type styleable, out int[] bindingStylableGroupId, out int bindingBindId)
        {
            var resourceType = Container.Resolve<IDroidBindingResourceTypeDetector>().Detect();

            styleable = resourceType.GetNestedType(DroidConstants.ResourceStyleableNestedTypeName);
            bindingStylableGroupId = (int[]) GetFieldValue(styleable, DroidConstants.ResourceBindingName);
            bindingBindId = (int) GetFieldValue(styleable, DroidConstants.ResourceBindingBindName);
        }

        public object GetFieldValue(Type type, string fieldName,
            int @default = DroidConstants.ResourceBindingDefaultValue)
        {
            var field = @type.GetField(fieldName);
            if (field == null)
            {
                Tracer.Warning("Missing stylable field {0}", fieldName);
                return @default;
            }

            return field.GetValue(null);
        }
    }
}