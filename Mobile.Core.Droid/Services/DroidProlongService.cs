﻿using Android.App;
using Android.Content;
using Mobile.Core.BL.Core.Model;
using Mobile.Core.BL.IoCContainer;
using Mobile.Core.Droid.Platform;

namespace Mobile.Core.Droid.Services
{
    [Service]
    public abstract class DroidProlongService<TBroadcastReceiver> : IntentService
        where TBroadcastReceiver : DroidProlongBroadcastReceiver
    {
        protected virtual long DelayPeriodMillis => 60000;

        protected virtual int PendingRequestCode => -1;

        protected virtual bool AutoProlong => true;

        protected abstract string ServiceName { get; }

        protected abstract void Execute(Context context, Intent intent);

        protected virtual void PostExecute(ActionResult serviceActionResult)
        {
        }

        public override void OnCreate()
        {
            base.OnCreate();

            DroidStarter.Instance.Init(this);
        }

        protected override void OnHandleIntent(Intent intent)
        {
            var dispatcher = Container.Resolve<IMainThreadDispatcher>();

            var serviceActionResult = dispatcher.InvokeAction(() => { Execute(this, intent); });

            if (AutoProlong)
            {
                dispatcher.InvokeAction(
                    () =>
                    {
                        Container.Resolve<IDroidServiceScheduler>()
                            .ProlongService<TBroadcastReceiver>(DelayPeriodMillis, PendingRequestCode);
                    });
            }

            dispatcher.InvokeAction(() => PostExecute(serviceActionResult));

            StopSelf();
        }
    }
}