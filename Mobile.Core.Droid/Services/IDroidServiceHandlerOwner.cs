﻿namespace Mobile.Core.Droid.Services
{
    public interface IDroidServiceHandlerOwner
    {
        void Initialization();
    }
}