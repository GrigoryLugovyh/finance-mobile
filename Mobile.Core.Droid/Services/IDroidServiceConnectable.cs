﻿namespace Mobile.Core.Droid.Services
{
    public interface IDroidServiceConnectable<TService, out TConnector>
        where TConnector : DroidServiceConnector<TService>
        where TService : DroidService
    {
        TConnector Connector { get; }
    }
}