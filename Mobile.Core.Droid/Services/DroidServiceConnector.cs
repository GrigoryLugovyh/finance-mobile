﻿using Android.Content;
using Android.OS;
using Android.Util;
using Java.Lang;
using Mobile.Core.BL.Core.Extensions;
using Exception = System.Exception;

namespace Mobile.Core.Droid.Services
{
    public abstract class DroidServiceConnector<TService> : Object, IServiceConnection, IDroidServiceReceiver
        where TService : DroidService
    {
        protected DroidServiceBinder<TService> binder;

        public DroidServiceBinder<TService> Binder
        {
            get { return binder; }
            set
            {
                if (value == null)
                {
                    return;
                }

                binder = value;
            }
        }

        public virtual BroadcastReceiver Receiver
        {
            get { return null; }
        }

        public virtual int Timeout
        {
            get { return 0; }
        }

        public virtual IntentFilter Filter
        {
            get { return null; }
        }

        public void OnServiceConnected(ComponentName name, IBinder service)
        {
            var serviceBinder = service as DroidServiceBinder<TService>;

            if (serviceBinder == null)
            {
                return;
            }

            serviceBinder.IsBound = InvokeServiceSequence(serviceBinder.Service);

            Binder = serviceBinder;
        }

        public void OnServiceDisconnected(ComponentName name)
        {
            if (binder == null)
            {
                return;
            }

            binder.IsBound = false;
        }

        protected virtual bool InvokeServiceSequence(TService service)
        {
            try
            {
                ServiceStartSequence(service);

                return true;
            }
            catch (Exception ex)
            {
#if DEBUG
                Log.Debug("DroidServiceConnector.InvokeServiceSequence exception", ex.ToLong());
#endif

                return false;
            }
        }

        protected abstract void ServiceStartSequence(TService service);
    }
}