﻿namespace Mobile.Core.Droid.Services
{
    public sealed class DroidServicesWrapper
    {
        private static readonly object sync = new object();

        private static volatile DroidServicesWrapper instance;

        public static DroidServicesWrapper Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (sync)
                    {
                        if (instance == null)
                        {
                            instance = new DroidServicesWrapper
                            {
                                ServiceMonitorOwner = new DroidServiceMonitorOwner()
                            };
                        }
                    }
                }

                return instance;
            }
        }

        private IDroidServicesMonitorOwner ServiceMonitorOwner { get; set; }

        public void AddService<TService, TServiceConnector>()
            where TServiceConnector : DroidServiceConnector<TService>
            where TService : DroidService
        {
            lock (sync)
            {
                ServiceMonitorOwner.AddService<TService, TServiceConnector>();
            }
        }

        public DroidServiceConnector<TService> GetConnector<TService, TServiceConnector>()
            where TServiceConnector : DroidServiceConnector<TService>
            where TService : DroidService
        {
            lock (sync)
            {
                return ServiceMonitorOwner.Connector<TService, TServiceConnector>();
            }
        }
    }
}