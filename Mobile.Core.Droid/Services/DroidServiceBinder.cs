﻿using System.Diagnostics.Contracts;
using Android.OS;

namespace Mobile.Core.Droid.Services
{
    public class DroidServiceBinder<TService> : Binder
        where TService : DroidService
    {
        private readonly DroidService service;

        public DroidServiceBinder(DroidService service)
        {
            Contract.Assert(service != null);

            this.service = service;
        }

        public TService Service
        {
            get { return service as TService; }
        }

        public bool IsBound { get; set; }
    }
}