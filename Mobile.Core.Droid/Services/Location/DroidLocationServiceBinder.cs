﻿namespace Mobile.Core.Droid.Services.Location
{
    public class DroidLocationServiceBinder : DroidServiceBinder<DroidLocationService>
    {
        public DroidLocationServiceBinder(DroidLocationService service) : base(service)
        {
        }
    }
}