﻿namespace Mobile.Core.Droid.Services.Location
{
    public class DroidLocationServiceConnector : DroidServiceConnector<DroidLocationService>
    {
        protected override void ServiceStartSequence(DroidLocationService service)
        {
            service.DetectLocation();
        }
    }
}