﻿using System;
using Android.App;
using Android.Content;
using Android.Locations;
using Android.OS;
using Mobile.Core.BL.Core.Events;
using Mobile.Core.BL.Core.Exceptions;
using Mobile.Core.BL.Core.Extensions;

namespace Mobile.Core.Droid.Services.Location
{
    [Service]
    public class DroidLocationService : DroidService, ILocationListener
    {
        protected LocationManager locationManager = Application.Context.GetSystemService("location") as LocationManager;

        public override string Name
        {
            get { return "Служба опреденения местоположения"; }
        }

        public override string ServiceIntentName
        {
            get { return string.Empty; }
        }

        public Android.Locations.Location LastLocation { get; private set; }

        public void OnLocationChanged(Android.Locations.Location location)
        {
            LastLocation = location;
            LocationChanged.Rise(this, location);
        }

        public void OnProviderDisabled(string provider)
        {
            ProviderDisabled.Rise(this, provider);
        }

        public void OnProviderEnabled(string provider)
        {
            ProviderEnabled.Rise(this, provider);
        }

        public void OnStatusChanged(string provider, Availability status, Bundle extras)
        {
            StatusChanged.Rise(this, new Tuple<string, Availability>(provider, status));
        }

        protected override void Execute()
        {
        }

        [Obsolete("deprecated")]
        public override StartCommandResult OnStartCommand(Intent intent, StartCommandFlags flags, int startId)
        {
            return StartCommandResult.Sticky;
        }

        public override IBinder OnBind(Intent intent)
        {
            return new DroidLocationServiceBinder(this);
        }

        protected override void OnHandleIntent(Intent intent)
        {
        }

        public event EventHandler<MValueEventArgs<Android.Locations.Location>> LocationChanged;
        public event EventHandler<MValueEventArgs<string>> ProviderDisabled;
        public event EventHandler<MValueEventArgs<string>> ProviderEnabled;
        public event EventHandler<MValueEventArgs<Tuple<string, Availability>>> StatusChanged;

        public string LocationProvider
        {
            get
            {
                var locationCriteria = new Criteria
                {
                    Accuracy = Accuracy.Medium,
                    PowerRequirement = Power.Low
                };

                return locationManager.GetBestProvider(locationCriteria, true);
            }
        }

        public void DetectLocation()
        {
            string locationProvider = LocationProvider;

            if (string.IsNullOrWhiteSpace(locationProvider) || !locationManager.IsProviderEnabled(locationProvider))
            {
                throw new MException("Необходимо включить местоположение");
            }

            locationManager.RequestLocationUpdates(locationProvider, 500, 1, this);
        }
    }
}