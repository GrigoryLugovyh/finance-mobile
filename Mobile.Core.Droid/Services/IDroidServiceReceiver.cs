﻿using Android.Content;

namespace Mobile.Core.Droid.Services
{
    public interface IDroidServiceReceiver
    {
        int Timeout { get; }

        IntentFilter Filter { get; }

        BroadcastReceiver Receiver { get; }
    }
}