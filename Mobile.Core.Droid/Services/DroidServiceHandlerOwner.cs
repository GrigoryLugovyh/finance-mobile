﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Mobile.Core.BL.Core.Model;
using Mobile.Core.BL.IoCContainer;

namespace Mobile.Core.Droid.Services
{
    public class DroidServiceHandlerOwner<TService, TConnector> : MainThreadObjectDispatcher, IDroidServiceHandlerOwner,
        IDroidServiceConnectable<TService, TConnector>
        where TConnector : DroidServiceConnector<TService>
        where TService : DroidService
    {
        private readonly CancellationToken token;

        public DroidServiceHandlerOwner(CancellationToken token)
        {
            this.token = token;

            Initialization();
        }

        public TConnector Connector { get; private set; }

        public void Initialization()
        {
            Task.Factory.StartNew(() =>
            {
                Intent intent = CreateServiceIntent();

                if (intent == null)
                {
                    return;
                }

                Application.Context.StartService(intent);

                IServiceConnection connector = CreateServiceConnector();

                var serviceReceiver = connector as IDroidServiceReceiver;

                if (serviceReceiver == null)
                {
                    return;
                }

                IntentFilter filter = serviceReceiver.Filter;

                BroadcastReceiver receiver = serviceReceiver.Receiver;

                if (filter != null && receiver != null)
                {
                    Application.Context.RegisterReceiver(receiver, filter);
                }

                Application.Context.BindService(intent, connector, Bind.AutoCreate);

                if (serviceReceiver.Timeout > 0)
                {
                    Container.Resolve<IDroidServiceScheduler>().RepeatService(intent, serviceReceiver.Timeout);
                }

                Connector = connector as TConnector;

            }, token).Wait(token);
        }

        private Intent CreateServiceIntent()
        {
            return new Intent(Application.Context, typeof (TService));
        }

        private IServiceConnection CreateServiceConnector()
        {
            return Activator.CreateInstance<TConnector>();
        }
    }
}