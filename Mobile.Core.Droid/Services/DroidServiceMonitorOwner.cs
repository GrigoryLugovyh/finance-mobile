﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace Mobile.Core.Droid.Services
{
    public class DroidServiceMonitorOwner : IDroidServicesMonitorOwner
    {
        protected readonly Dictionary<Type, IDroidServiceHandlerOwner> servicesMonitorOwner =
            new Dictionary<Type, IDroidServiceHandlerOwner>();

        protected CancellationTokenSource tokenSource = new CancellationTokenSource();

        public void AddService<TService, TServiceConnector>()
            where TServiceConnector : DroidServiceConnector<TService>
            where TService : DroidService
        {
            if (servicesMonitorOwner.ContainsKey(typeof (TService)))
            {
                return;
            }

            servicesMonitorOwner.Add(typeof (TService),
                new DroidServiceHandlerOwner<TService, TServiceConnector>(tokenSource.Token));
        }

        public DroidServiceConnector<TService> Connector<TService, TServiceConnector>()
            where TServiceConnector : DroidServiceConnector<TService>
            where TService : DroidService
        {
            IDroidServiceHandlerOwner hander;

            if (!servicesMonitorOwner.TryGetValue(typeof (TService), out hander))
            {
                return null;
            }

            var owner = hander as IDroidServiceConnectable<TService, TServiceConnector>;

            if (owner == null)
            {
                return null;
            }

            return owner.Connector;
        }

        public void Abort()
        {
            tokenSource.Cancel();
        }
    }
}