﻿using System;
using Android.App;
using Android.Content;
using Mobile.Core.BL.Core;
using Mobile.Core.Droid.Platform;

namespace Mobile.Core.Droid.Services
{
    [Service]
    public abstract class DroidService : IntentService
    {
        public abstract string Name { get; }
        public abstract string ServiceIntentName { get; }
        protected int ServiceId { get; private set; }
        protected abstract void Execute();

        protected virtual bool CanExecute()
        {
            return true;
        }

        [Obsolete("deprecated")]
        public override StartCommandResult OnStartCommand(Intent intent, StartCommandFlags flags, int startId)
        {
            ServiceId = startId;
            base.OnStartCommand(intent, flags, startId);
            return StartCommandResult.Sticky;
        }

        public override void OnCreate()
        {
            base.OnCreate();

            DroidStarter.Instance.Init(this);
        }

        protected override void OnHandleIntent(Intent intent)
        {
            try
            {
                if (CanExecute())
                {
                    Execute();
                }
            }
            catch (Exception ex)
            {
                Tracer.Error(ex.Message);
            }
            finally
            {
                var serviceIntent = new Intent(ServiceIntentName);

                SendOrderedBroadcast(serviceIntent, null);
            }
        }
    }
}