﻿namespace Mobile.Core.Droid.Services
{
    public interface IDroidServicesMonitorOwner
    {
        void AddService<TService, TServiceConnector>()
            where TService : DroidService
            where TServiceConnector : DroidServiceConnector<TService>;

        DroidServiceConnector<TService> Connector<TService, TServiceConnector>()
            where TServiceConnector : DroidServiceConnector<TService>
            where TService : DroidService;

        void Abort();
    }
}