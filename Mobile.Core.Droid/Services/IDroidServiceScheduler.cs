﻿using System;
using Android.Content;

namespace Mobile.Core.Droid.Services
{
    public interface IDroidServiceScheduler
    {
        void ProlongService<TBroadcastReceiver>(long delayPeriodMillis = 1, int pendingRequestCode = -1)
            where TBroadcastReceiver : DroidProlongBroadcastReceiver;

        void ProlongService(Type broadcastReceiverType, long delayPeriodMillis = 1, int pendingRequestCode = -1);

        void RepeatService(Intent service, int timeout);

        bool AlarmSet(Intent service);
    }
}