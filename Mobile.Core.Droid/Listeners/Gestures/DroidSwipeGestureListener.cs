﻿using System;
using Android.Views;
using Mobile.Core.BL.Core;
using Mobile.Core.BL.Core.Enums;
using Mobile.Core.BL.Core.Views;

namespace Mobile.Core.Droid.Listeners.Gestures
{
    public class DroidSwipeGestureListener : GestureDetector.SimpleOnGestureListener
    {
        private const int SwipeMinDistance = 120;
        private const int SwipeMaxOffPath = 200;
        private const int SwipeThresholdVelocity = 200;

        private readonly IMGestureView view;

        public DroidSwipeGestureListener(IMGestureView view)
        {
            this.view = view;
        }

        public override bool OnFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
        {
            try
            {
                float diffAbs = Math.Abs(e1.GetY() - e2.GetY());
                float diff = e1.GetX() - e2.GetX();

                if (diffAbs <= SwipeMaxOffPath)
                {
                    if (diff > SwipeMinDistance
                        && Math.Abs(velocityX) > SwipeThresholdVelocity)
                    {
                        view.OnGestureResult(MGesture.Left);
                        return true;
                    }
                    if (-diff > SwipeMinDistance
                        && Math.Abs(velocityX) > SwipeThresholdVelocity)
                    {
                        view.OnGestureResult(MGesture.Right);
                        return true;
                    }
                }
            }
            catch (Exception e)
            {
                Tracer.Warning("Error on gestures: {0}", e);
            }

            return base.OnFling(e1, e2, velocityX, velocityY);
        }
    }
}