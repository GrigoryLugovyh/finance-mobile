﻿using System;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;

namespace Mobile.Core.Droid.Listeners.Gestures
{
    public class DroidSwipeGestureDetector : GestureDetector
    {
        protected DroidSwipeGestureDetector(IntPtr javaReference, JniHandleOwnership transfer)
            : base(javaReference, transfer)
        {
        }

        public DroidSwipeGestureDetector(Context context, IOnGestureListener listener) : base(context, listener)
        {
        }

        public DroidSwipeGestureDetector(Context context, IOnGestureListener listener, Handler handler)
            : base(context, listener, handler)
        {
        }

        public DroidSwipeGestureDetector(Context context, IOnGestureListener listener, Handler handler, bool ignoreMultitouch)
            : base(context, listener, handler, ignoreMultitouch)
        {
        }

        public DroidSwipeGestureDetector(IOnGestureListener listener) : base(listener)
        {
        }

        public DroidSwipeGestureDetector(IOnGestureListener listener, Handler handler) : base(listener, handler)
        {
        }
    }
}