﻿using System;
using Android.Graphics;
using Android.Views;
using Mobile.Core.BL.Core.Events;
using Mobile.Core.BL.Core.Extensions;
using Mobile.Core.BL.Core.Platform.Keyboard;
using Object = Java.Lang.Object;

namespace Mobile.Core.Droid.Listeners
{
    public class DroidGlobalLayoutListener : Object, ViewTreeObserver.IOnGlobalLayoutListener, IMKeyboardToggle
    {
        private readonly View layout;

        public DroidGlobalLayoutListener(View layout)
        {
            this.layout = layout;
        }

        public void OnGlobalLayout()
        {
            HandleKeyboard();
        }

        public event EventHandler<MValueEventArgs<bool>> KeyboardToggle;
        
        protected void HandleKeyboard()
        {
            var rect = new Rect();

            layout.GetWindowVisibleDisplayFrame(rect);

            int heightDiff = layout.RootView.Height - (rect.Bottom - rect.Top);

            KeyboardToggle.Rise(this, heightDiff > 100);
        }
    }
}