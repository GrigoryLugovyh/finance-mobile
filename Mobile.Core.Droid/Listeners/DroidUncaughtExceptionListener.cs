﻿using System;
using Java.Lang;
using Mobile.Core.BL.Core.Views;
using Mobile.Core.BL.IoCContainer;
using Mobile.Core.Droid.Extensions;
using Mobile.Core.Droid.Views;
using Object = Java.Lang.Object;

namespace Mobile.Core.Droid.Listeners
{
    public class DroidUncaughtExceptionListener : Object, Thread.IUncaughtExceptionHandler
    {
        private readonly Type startActivityType;

        public DroidUncaughtExceptionListener(Type startActivityType)
        {
            this.startActivityType = startActivityType;
        }

        public void UncaughtException(Thread thread, Throwable ex)
        {
            try
            {
                var throwableView = Container.Resolve<IDroidCurrentActivity>().Activity as IMThrowableView;

                if (throwableView == null)
                {
                    return;
                }

                throwableView.OnUncaughtExceptionThrow(ex);
            }
            finally
            {
                startActivityType.Restart();
            }
        }
    }
}