﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Android.Content;
using Android.Views;
using Android.Widget;
using Mobile.Core.BL.Core;
using Mobile.Core.BL.Core.Assemblies;
using Mobile.Core.BL.Core.Binding.Target;
using Mobile.Core.BL.Core.Exceptions;
using Mobile.Core.BL.Core.Extensions;
using Mobile.Core.BL.Core.Model;
using Mobile.Core.BL.Core.Platform.Apps;
using Mobile.Core.BL.Core.Platform.Bluetooth;
using Mobile.Core.BL.Core.Platform.Calendar;
using Mobile.Core.BL.Core.Platform.Clipboard;
using Mobile.Core.BL.Core.Platform.Data;
using Mobile.Core.BL.Core.Platform.Display;
using Mobile.Core.BL.Core.Platform.FileSystem;
using Mobile.Core.BL.Core.Platform.Keyboard;
using Mobile.Core.BL.Core.Platform.Locale;
using Mobile.Core.BL.Core.Platform.Location;
using Mobile.Core.BL.Core.Platform.Media;
using Mobile.Core.BL.Core.Platform.Network;
using Mobile.Core.BL.Core.Platform.Shared;
using Mobile.Core.BL.Core.Platform.Telephony;
using Mobile.Core.BL.Core.Platform.Trace;
using Mobile.Core.BL.Core.Platform.Usb;
using Mobile.Core.BL.Core.ViewModels;
using Mobile.Core.BL.Core.Views;
using Mobile.Core.BL.IoCContainer;
using Mobile.Core.Droid.Binding;
using Mobile.Core.Droid.Binding.Target;
using Mobile.Core.Droid.Controls;
using Mobile.Core.Droid.Services;
using Mobile.Core.Droid.Views;

namespace Mobile.Core.Droid.Platform
{
    public abstract class DroidModel : MModel, IDroidCurrentAssembly, IDroidCurrentContext
    {
        protected readonly Context appContext;

        protected DroidModel(Context appContext)
        {
            this.appContext = appContext;
        }

        public Assembly CurrentAssembly => GetType().Assembly;

        public string CurrentNamespace => GetType().Namespace;

        public string CurrentFullname => GetType().FullName;

        public Context Context => appContext;

        protected override IMKeyboardHandler CreateKeyboardHandler()
        {
            return new DroidKeyboardHandler();
        }

        protected override IMCalendarHandler CreateCalendarHandler()
        {
            return new DroidCalendarHandler();
        }

        protected override IMTelephonyHandler CreateTelephonyHandler()
        {
            return new DroidTelephonyHandler();
        }

        protected override IMCameraHandler CreateCameraHandler()
        {
            return new DroidCameraHandler();
        }

        protected override IMMemoryHandler CreateMemoryHandler()
        {
            return new DroidMemoryHandler();
        }

        protected override IMLocationHandler CreateLocationHandler()
        {
            return new DroidLocationHandler();
        }

        protected override IMViewDimensions CreateVieweDimensions()
        {
            return new DroidViewDimensions();
        }

        protected override IMLocaleHandler CreateLocaleHandler()
        {
            return new DroidLocaleHandler();
        }

        protected override IMDataBulkHandler CreateDataBulkHandler()
        {
            return new DroidDataBulkHandler();
        }

        protected override IMAppsHandler CreateAppsHandler()
        {
            return new DroidAppsHandler();
        }

        protected override IMNetworkHandler CreateNetworkHandler()
        {
            return new DroidNetworkHandler();
        }

        protected override IMFileSystemHandler CreateFileSystemHandler()
        {
            return new DroidFileSystemHandler();
        }

        protected override IMClipboardHandler CreateClipboardHandler()
        {
            return new DroidClipboardHandler();
        }

        protected override IMScreenHandler CreateScreenHandler()
        {
            return new DroidScreenHandler();
        }

        protected override IMKeyCodeHandler CreateCharHandler()
        {
            return new DroidKeyCodeHandler();
        }

        protected override void InitializePlatform()
        {
            Container.AddRegistration<IDroidBitmapHandler, DroidBitmapHandler>();
            Container.AddRegistration<IBluetoothDeviceManager, BluetoothDeviceManager>();
            Container.AddRegistration<IDroidCommandShellHandler, DroidCommandShellHandler>();
            Container.AddRegistration<IUsbDeviceManager, UsbDeviceManager>();

            base.InitializePlatform();
        }

        protected override void InitializePlatformEnvironments()
        {
            InitializeCurrentContext();
            InitializeCurrentAssembly();
            InitializeActivityMonitor();
            InitializeActivitiesMonitorOwner();
            InitializeDroidTypeCache();
            InitializeActivityListenerFactory();
            InitializeServiceEvironment();
        }

        protected override IMViewViewModelMapping CreateViewViewModelMapping()
        {
            return new DroidViewViewModelMapping("View", "Activity");
        }

        protected override IMTrace CreateTrace()
        {
            return new DroidTrace();
        }

        protected override IMViewsContainer CreateViewsContainer()
        {
            IDroidViewsContainer container = CreateDroidViewsContainer();

            Container.AddRegistration(container);
            Container.AddRegistration<IDroidViewModelLoader>(container);
            Container.AddRegistration<IDroidViewModelRequestDispatcher>(container);

            var viewContainer = container as IMViewsContainer;

            if (viewContainer == null)
            {
                throw new MException("Droid view container must implement IMViewsContainer");
            }

            return viewContainer;
        }

        protected override IMViewDispatcher CreateViewDispatcher()
        {
            IDroidViewPresenter presenter = CreatePresenter();
            return new DroidViewDispatcher(presenter);
        }

        protected override void InitializeBinding()
        {
            base.InitializeBinding();

            InitializeDroidBindingResourceTypeDetector();
            InitializeDroidBindingResourceHandler();
            InitializeDroidBindingViewFactory();
            InitializeDroidBindingInflateFactoryHandler();
            InitializeDroidLayoutCache();
        }

        protected override void FillTargetBindingFactoryDispatcher(IMTargetBindingFactoryDispatcher dispatcher)
        {
            base.FillTargetBindingFactoryDispatcher(dispatcher);

            dispatcher.Add<View>(DroidBindingSyntaxNames.Click, c => new DroidViewClickBinding(c));
            dispatcher.Add<ImageView>(DroidBindingSyntaxNames.ImageClick, ic => new DroidImageViewClickBinding(ic));
            dispatcher.Add<View>(DroidBindingSyntaxNames.LongClick, lc => new DroidViewLongClickBinding(lc));
            dispatcher.Add<ImageView>(DroidBindingSyntaxNames.ImageLongClick,
                ilc => new DroidImageViewLongClickBinding(ilc));
            dispatcher.Add<TextView>(DroidBindingSyntaxNames.Label, t => new DroidTextViewBinding(t));
            dispatcher.Add<EditText>(DroidBindingSyntaxNames.Text, e => new DroidEditTextBinding(e));
            dispatcher.Add<CheckBox>(DroidBindingSyntaxNames.Check, c => new DroidCheckBoxBinding(c));
            dispatcher.Add<ProgressBar>(DroidBindingSyntaxNames.Progress, p => new DroidProgressBarBinding(p));
            dispatcher.Add<ListView>(DroidBindingSyntaxNames.List, lv => new DroidListViewBinding<ListView>(lv));
            dispatcher.Add<GridView>(DroidBindingSyntaxNames.GridView, lv => new DroidListViewBinding<GridView>(lv));
            dispatcher.Add<View>(DroidBindingSyntaxNames.Local, v => new DroidViewLocalBinding(v));
            dispatcher.Add<View>(DroidBindingSyntaxNames.Visible, v => new DroidVisibleBinding(v));
            dispatcher.Add<View>(DroidBindingSyntaxNames.Shown, v => new DroidVisibleBinding(v, gone: false));
            dispatcher.Add<View>(DroidBindingSyntaxNames.Invisible, v => new DroidVisibleBinding(v, true));
            dispatcher.Add<TextView>(DroidBindingSyntaxNames.TextColor, v => new DroidTextColorBinding(v));
            dispatcher.Add<View>(DroidBindingSyntaxNames.BackgroundColor, v => new DroidBackgroundColorBinding(v));
            dispatcher.Add<View>(DroidBindingSyntaxNames.BackgroundDrawable,
                v => new DroidBackgroundDrawableBinding(v));
            dispatcher.Add<ImageView>(DroidBindingSyntaxNames.ImageViewDrawable, v => new DroidImageDrawableBinding(v));
            dispatcher.Add<Button>(DroidBindingSyntaxNames.ButtonText, b => new DroidButtonTextBinding(b));
            dispatcher.Add<ImageView>(DroidBindingSyntaxNames.ImageDataArray,
                view => new DroidImageViewByteArrayBinding(view));
            dispatcher.Add<ImageView>(DroidBindingSyntaxNames.ImagePath,
                view => new DroidImageViewPathBinding(view));
            dispatcher.Add<ImageView>(DroidBindingSyntaxNames.ImageBitmap,
                view => new DroidImageViewBitmapBinding(view));
            dispatcher.Add<DroidListView>(DroidBindingSyntaxNames.DroidListSource,
                lv => new DroidSourceViewBinding<DroidListView>(lv));
            dispatcher.Add<DroidGridView>(DroidBindingSyntaxNames.DroidGrigSource,
                lv => new DroidSourceViewBinding<DroidGridView>(lv));
            dispatcher.Add<DroidPagerView>(DroidBindingSyntaxNames.DroidPager,
                pager => new DroidPagerViewBinding(pager));
            dispatcher.Add<DroidPagerView>(DroidBindingSyntaxNames.DroidPagerIndex,
                pager => new DroidPagerViewIndexBinding(pager));
            dispatcher.Add<View>(DroidBindingSyntaxNames.Enable, view => new DroidEnableBinding(view));
            dispatcher.Add<View>(DroidBindingSyntaxNames.Disable, view => new DroidDisableBinding(view));
            dispatcher.Add<ScrollView>(DroidBindingSyntaxNames.ScrollViewUp,
                view => new DroidScrollViewScrollToBinding(view, FocusSearchDirection.Up));
        }

        protected override IMSharedPreferences CreateSharedPreferences()
        {
            return new DroidSharedPreferences();
        }

        protected virtual IDroidViewPresenter CreatePresenter()
        {
            return new DroidViewPresenter();
        }

        protected virtual void InitializeCurrentContext()
        {
            Container.AddRegistration<IDroidCurrentContext>(this);
        }

        protected virtual void InitializeCurrentAssembly()
        {
            Container.AddRegistration<IDroidCurrentAssembly>(this);
        }

        protected virtual void InitializeActivityMonitor()
        {
            DroidActivityMonitor monitor = CreateActivityMonitor();

            Container.AddRegistration<IDroidActivityListener>(monitor);
            Container.AddRegistration<IDroidCurrentActivity>(monitor);
            Container.AddRegistration<IMRealtimeMonitor>(monitor);
        }

        protected virtual void InitializeActivitiesMonitorOwner()
        {
            IDroidActivitiesMonitorOwner owner = CreateActivitiesMonitorOwner();
            Container.AddRegistration(owner);
        }

        protected virtual void InitializeActivityListenerFactory()
        {
            IDroidSourceListenerFactory listener = CreateActivityListenerFactory();
            Container.AddRegistration(listener);
        }

        protected virtual void InitializeServiceEvironment()
        {
            IDroidServiceScheduler serviceScheduler = CreateDroidServiceScheduler();
            Container.AddRegistration(serviceScheduler);
        }

        protected virtual void InitializeDroidBindingResourceTypeDetector()
        {
            IDroidBindingResourceTypeDetector resourceTypeDetector = CreateDroidBindingResourceTypeDetector();
            Container.AddRegistration(resourceTypeDetector);
        }

        protected virtual void InitializeDroidBindingResourceHandler()
        {
            IDroidBindingResourceHandler handler = CreateDroidBindingResourceHandler();
            Container.AddRegistration(handler);
        }

        protected virtual void InitializeDroidBindingViewFactory()
        {
            IDroidBindingViewFactory viewFactory = CreateDroidBindingViewFactory();
            Container.AddRegistration(viewFactory);
        }

        protected virtual void InitializeDroidBindingInflateFactoryHandler()
        {
            IDroidBindingInflateFactoryHandler handler = CreateDroidBindingInflateFactoryHandler();
            Container.AddRegistration(handler);
        }

        protected virtual void InitializeDroidLayoutCache()
        {
            IDroidBindingContextStack<IDroidView> cash = CreateDroidLayoutStack();
            Container.AddRegistration(cash);
        }

        protected virtual void InitializeDroidTypeCache()
        {
            IDroidCacheTypes namespaceCache = CreateDroidCacheTypes();

            var typeCash = namespaceCache as IMCacheTypes<View>;

            if (typeCash != null)
            {
                foreach (string viewNamespace in CreateDroidViewNamespaces())
                {
                    typeCash.Add(viewNamespace);
                }
            }

            var typeInflater = namespaceCache as IMCacheTypeInflate;

            if (typeInflater != null)
            {
                foreach (var viewType in DroidConstants.ViewTypes)
                {
                    typeInflater.Add(viewType);
                }
            }

            Container.AddRegistration(namespaceCache);
            Container.AddRegistration(typeInflater);
            Container.AddRegistration(typeCash);
        }

        protected virtual List<string> CreateDroidViewNamespaces()
        {
            var namespaces = DroidConstants.ViewNamespaces.ToList();
            namespaces.Add("Mobile.Core.Droid.Controls");
            return namespaces;
        }

        protected virtual DroidActivityMonitor CreateActivityMonitor()
        {
            return new DroidActivityMonitor();
        }

        protected virtual IDroidActivitiesMonitorOwner CreateActivitiesMonitorOwner()
        {
            return new DroidActivitiesMonitorOwner();
        }

        protected virtual IDroidViewsContainer CreateDroidViewsContainer()
        {
            return new DroidViewViewModelContainer(appContext);
        }

        protected virtual IDroidBindingResourceTypeDetector CreateDroidBindingResourceTypeDetector()
        {
            return new DroidBindingResourceTypeDetector();
        }

        protected virtual IDroidBindingResourceHandler CreateDroidBindingResourceHandler()
        {
            return new DroidBindingResourceHandler();
        }

        protected virtual IDroidBindingViewFactory CreateDroidBindingViewFactory()
        {
            return new DroidBindingViewFactory();
        }

        protected virtual IDroidBindingInflateFactoryHandler CreateDroidBindingInflateFactoryHandler()
        {
            return new DroidBindingInflateFactoryHandler();
        }

        public virtual IDroidBindingContextStack<IDroidView> CreateDroidLayoutStack()
        {
            return new DroidBindingContextStack();
        }

        protected virtual IDroidCacheTypes CreateDroidCacheTypes()
        {
            return new DroidCacheTypes();
        }

        protected virtual IDroidSourceListenerFactory CreateActivityListenerFactory()
        {
            return new DroidSourceListenerFactory();
        }

        protected virtual IDroidServiceScheduler CreateDroidServiceScheduler()
        {
            return new DroidServiceSchedule();
        }
    }
}