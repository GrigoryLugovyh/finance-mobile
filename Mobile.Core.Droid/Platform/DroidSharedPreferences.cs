﻿using System.Collections.Generic;
using System.Diagnostics.Contracts;
using Android.App;
using Android.Content;
using Mobile.Core.BL.Core.Model;
using Mobile.Core.BL.Core.Platform.Shared;
using Mobile.Core.BL.IoCContainer;

namespace Mobile.Core.Droid.Platform
{
    public class DroidSharedPreferences : IMSharedPreferences
    {
        private readonly IMApplication application;
        private readonly ISharedPreferences sharedPreferences;

        public DroidSharedPreferences()
        {
            application = Container.Resolve<IMApplication>();
            sharedPreferences = Application.Context.GetSharedPreferences(application.ApplicationName,
                FileCreationMode.Private);
        }

        public void SetValue(KeyValuePair<string, string> pair)
        {
            Contract.Assert(application != null);
            Contract.Assert(sharedPreferences != null);
            Contract.Assert(!string.IsNullOrWhiteSpace(pair.Key));

            ISharedPreferencesEditor editor = sharedPreferences.Edit();
            editor.PutString(pair.Key, pair.Value);
            editor.Apply();
        }

        public string GetValue(string key)
        {
            Contract.Assert(application != null);
            Contract.Assert(sharedPreferences != null);
            Contract.Assert(!string.IsNullOrWhiteSpace(key));

            return sharedPreferences.GetString(key, string.Empty);
        }

	    public void Clear()
	    {
			Contract.Assert(application != null);
			Contract.Assert(sharedPreferences != null);
			ISharedPreferencesEditor editor = sharedPreferences.Edit();
			editor.Clear();
			editor.Apply();
		}
    }
}