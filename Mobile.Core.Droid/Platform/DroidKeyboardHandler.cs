﻿using Android.App;
using Android.Content;
using Android.Views;
using Android.Views.InputMethods;
using Mobile.Core.BL.Core;
using Mobile.Core.BL.Core.Platform.Keyboard;

namespace Mobile.Core.Droid.Platform
{
    public class DroidKeyboardHandler : DroidHandler, IMKeyboardHandler
    {
        private void CurrentFocus(out View view, out InputMethodManager inputManager)
        {
            Context context = Environment.Context;
            Activity activity = Activity;

            view = null;
            inputManager = null;

            if (context == null || activity == null)
            {
                Tracer.Warning("HideKeyboard: current context or current activity is null");
                return;
            }

            inputManager = (InputMethodManager) context.GetSystemService(Context.InputMethodService);

            if (inputManager == null)
            {
                Tracer.Warning("HideKeyboard: failed to resolve inputManager");
                return;
            }

            view = activity.CurrentFocus;

            if (view == null)
            {
                Tracer.Warning("HideKeyboard: no elements has focus at current activity");
            }
        }

        public void HideKeyboard()
        {
            View view;
            InputMethodManager inputManager;

            CurrentFocus(out view, out inputManager);

            if (view == null || inputManager == null)
            {
                return;
            }

            inputManager.HideSoftInputFromWindow(view.WindowToken, HideSoftInputFlags.NotAlways);
        }

        public void ShowKeyboard()
        {
            View view;
            InputMethodManager inputManager;

            CurrentFocus(out view, out inputManager);

            if (view == null || inputManager == null)
            {
                return;
            }

            inputManager.ShowSoftInput(view, ShowFlags.Implicit);
        }
    }
}