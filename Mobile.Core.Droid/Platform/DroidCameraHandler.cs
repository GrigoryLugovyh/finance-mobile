﻿using System.IO;
using System.Threading;
using Android.Content;
using Android.Content.PM;
using Android.Net;
using Android.Provider;
using Mobile.Core.BL.Core;
using Mobile.Core.BL.Core.Enums;
using Mobile.Core.BL.Core.Events;
using Mobile.Core.BL.Core.Model;
using Mobile.Core.BL.Core.Platform.Media;
using Mobile.Core.BL.Core.Platform.Specific;
using Mobile.Core.BL.IoCContainer;
using File = Java.IO.File;

// ReSharper disable RedundantAssignment

namespace Mobile.Core.Droid.Platform
{
    public class DroidCameraHandler : DroidHandler, IMCameraHandler
    {
        private const int CameraRequestCode = 1336;
        private readonly IDroidBitmapHandler bitmapHandler;
        private readonly IMRealtimeMonitor monitor;
        private File imageFile;
        private ManualResetEvent resetEvent;

        public DroidCameraHandler()
        {
            bitmapHandler = Container.Resolve<IDroidBitmapHandler>();
            monitor = Container.Resolve<IMRealtimeMonitor>();
        }

        public virtual bool TakePhoto(string directory, string fileName, bool createPreviewImage = false)
        {
            if (resetEvent != null) return false;

            var result = InvokeFuncOnMainThread(() =>
            {
                if (Activity == null)
                {
                    Tracer.Warning("Activity should be ISourceActivity");
                    return default(ActionResult);
                }

                var camera = new Intent(MediaStore.ActionImageCapture);

                var availableActivities = Activity.PackageManager.QueryIntentActivities(camera,
                    PackageInfoFlags.MatchDefaultOnly);

                if (availableActivities == null || availableActivities.Count <= 0)
                {
                    Tracer.Error("Camera activity doesn't exist");
                    return default(ActionResult);
                }

                if (monitor == null)
                {
                    Tracer.Error("Realtime monitor doesn't exist");
                    return default(ActionResult);
                }

                monitor.RealtimeChanged += MonitorOnRealtimeChanged;

                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }

                imageFile = new File(directory, fileName);

                camera.PutExtra(MediaStore.ExtraOutput, Uri.FromFile(imageFile));

                resetEvent = new ManualResetEvent(false);

                Activity.StartActivityForResult(camera, CameraRequestCode);

                resetEvent.WaitOne();

                var exists = imageFile.Exists();

                if (!exists || !createPreviewImage)
                {
                    return new ActionResult {Success = exists};
                }

                if (!CreatePreviewImage(imageFile.AbsolutePath))
                {
                    Tracer.Error($"Couldn't execute CreatePreviewImage for {imageFile.AbsolutePath}");
                }

                return new ActionResult {Success = true};
            }).Result as ActionResult;

            return result != null && result.Success;
        }

        public byte[] CompressPhoto(byte[] buffer, long maxSize, string filename = null)
        {
            return bitmapHandler.Compress(buffer, maxSize, filename);
        }

        private void MonitorOnRealtimeChanged(object sender, MValueEventArgs<MRealtimeMonitor.MRealtimeObject> e)
        {
            if (resetEvent != null && e.Value.State == MRealtimeState.Result && e.Value.Parameters[0] is ResultEventArgs &&
                ((ResultEventArgs) e.Value.Parameters[0]).RequestCode.Equals(CameraRequestCode))
            {
                monitor.RealtimeChanged -= MonitorOnRealtimeChanged;
                resetEvent?.Set();
                resetEvent = null;
            }
        }

        protected bool CreatePreviewImage(string filePath)
        {
            if (string.IsNullOrEmpty(filePath))
            {
                return false;
            }

            var fileInfo = new FileInfo(filePath);

            if (!fileInfo.Exists)
            {
                return false;
            }
            var rawBitmap = bitmapHandler.GetBitmapFile(fileInfo.FullName);

            if (rawBitmap == null)
            {
                return false;
            }

            var preview = bitmapHandler.Resize(rawBitmap, 200, 200);

            var directory = Container.Resolve<IPlatformSpecific>().ApplicationDataPath;

            var name = Path.GetFileName(filePath);

            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }

            bitmapHandler.Save(preview, Path.Combine(directory, name));

            rawBitmap.Recycle();
            preview.Recycle();

            rawBitmap = null;
            preview = null;

            return true;
        }
    }
}