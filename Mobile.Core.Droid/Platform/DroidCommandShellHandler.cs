﻿using Java.Lang;

namespace Mobile.Core.Droid.Platform
{
    public class DroidCommandShellHandler : DroidHandler, IDroidCommandShellHandler
    {
        public bool Execute(string command)
        {
            Runtime runtime = Runtime.GetRuntime();

            Process process = runtime.Exec(command);
            process.WaitFor();

            return process.ExitValue() == 0;
        }
    }
}