﻿using System.Diagnostics.Contracts;
using System.Threading.Tasks;
using Android.Content;
using Mobile.Core.BL.Core.Enums;
using Mobile.Core.Droid.Services;
using Mobile.Core.Droid.Views;

namespace Mobile.Core.Droid.Platform
{
    public sealed class DroidStarter
    {
        public enum StartDirection
        {
            None,
            Application,
            Service
        }

        private static readonly object sync = new object();
        private static volatile DroidStarter instance;

        public static DroidStarter Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (sync)
                    {
                        if (instance == null)
                        {
                            instance = new DroidStarter
                            {
                                Initializer = new DroidInitializer()
                            };
                        }
                    }
                }

                return instance;
            }
        }

        public StartDirection Direction { get; private set; }

        public IDroidInitializer Initializer { get; set; }

        public bool Started => Initializer != null && Initializer.State == MInitState.Initialized;

        public void Init(Context context, StartDirection direction = StartDirection.Service)
        {
            lock (sync)
            {
                if (Instance.Initializer.State == MInitState.Uninitialized)
                {
                    Instance.Create(context, direction);
                    Instance.Start(new DroidSyncServiceProxyView()).Wait();
                }
            }
        }

        public void Create(Context appContext, StartDirection direction = StartDirection.Application)
        {
            Contract.Assert(Initializer != null);

            Direction = direction;

            lock (sync)
            {
                Initializer.Create(appContext);
            }
        }

        public Task Start(IDroidSplashScreenActivity splash)
        {
            if (Initializer == null)
            {
                return null;
            }

            lock (sync)
            {
                return Initializer.Start(splash);
            }
        }

        public void Remove()
        {
            if (Initializer == null)
            {
                return;
            }

            lock (sync)
            {
                Initializer.Remove();
            }
        }
    }
}