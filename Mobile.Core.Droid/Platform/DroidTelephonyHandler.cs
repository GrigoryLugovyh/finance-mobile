﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Provider;
using Android.Telephony;
using Mobile.Core.BL.Core.Extensions;
using Mobile.Core.BL.Core.Platform.Telephony;

namespace Mobile.Core.Droid.Platform
{
    public class DroidTelephonyHandler : DroidHandler, IMTelephonyHandler
    {
        protected TelephonyManager Telephony
        {
            get { return (TelephonyManager) Environment.Context.GetSystemService(Context.TelephonyService); }
        }

        public string DeviceBrand
        {
            get { return Build.Manufacturer; }
        }

        public string DeviceModel
        {
            get { return Build.Model; }
        }

        public string DeviceId
        {
            get { return Telephony.DeviceId; }
        }

        public string SimSerialNumber
        {
            get { return Telephony.SimSerialNumber; }
        }

        public string PhoneNumber
        {
            get { return Telephony.Line1Number; }
        }

        public string OperatingSystemVersion
        {
            get { return Build.VERSION.Release; }
        }

        public string OperatingSystemId
        {
            get { return Settings.Secure.GetString(Environment.Context.ContentResolver, Settings.Secure.AndroidId); }
        }

        public string OperatingSystemIdNumeric
        {
            get
            {
                return String.IsNullOrEmpty(OperatingSystemId)
                    ? Guid.NewGuid().ToString().HexToDeviceId().ToString(CultureInfo.InvariantCulture)
                    : OperatingSystemId.HexToDeviceId().ToString(CultureInfo.InvariantCulture);
            }
        }

        public bool AirplaneMode
        {
            get
            {
                return Settings.System.GetInt(Environment.Context.ContentResolver, Settings.System.AirplaneModeOn, 0) !=
                       0;
            }
        }

        public bool IsLaungher
        {
            get
            {
                var filter = new IntentFilter(Intent.ActionMain);
                filter.AddCategory(Intent.CategoryHome);

                var filters = new List<IntentFilter> { filter };

                string packageName = Environment.Context.PackageName;
                var activities = new List<ComponentName>();
                PackageManager packageManager = Environment.Context.PackageManager;

                packageManager.GetPreferredActivities(filters, activities, null);

                return activities.Any(activity => packageName.Equals(activity.PackageName));
            }
        }

        public float BatteryLevel
        {
            get
            {
                var batteryIntent = Environment.Context.RegisterReceiver(null,
                    new IntentFilter(Intent.ActionBatteryChanged));

                if (batteryIntent == null)
                {
                    return 0f;
                }

                int level = batteryIntent.GetIntExtra(BatteryManager.ExtraLevel, -1);
                int scale = batteryIntent.GetIntExtra(BatteryManager.ExtraScale, -1);

                if (level == -1 || scale == -1)
                {
                    return 50.0f;
                }

                return ((float) level/scale)*100.0f;
            }
        }

        public bool RunOnEmulator
        {
            get { return Build.Fingerprint.StartsWith("generic"); }
        }
    }
}