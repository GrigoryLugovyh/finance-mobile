﻿using System;

namespace Mobile.Core.Droid.Platform
{
    public interface IDroidCacheTypes
    {
        Type Resolve(string name);
    }
}