﻿namespace Mobile.Core.Droid.Platform
{
    public interface IDroidCommandShellHandler
    {
        bool Execute(string command);
    }
}