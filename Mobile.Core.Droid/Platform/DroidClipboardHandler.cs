﻿using Android.Content;
using Mobile.Core.BL.Core.Platform.Clipboard;

namespace Mobile.Core.Droid.Platform
{
    public class DroidClipboardHandler : DroidHandler, IMClipboardHandler
    {
        public bool CopyTo(string label, string text)
        {
            if (string.IsNullOrEmpty(label) || string.IsNullOrEmpty(text))
            {
                return false;
            }

            var clipboard = (ClipboardManager)Environment.Context.GetSystemService(Context.ClipboardService);
            ClipData clip = ClipData.NewPlainText(label, text);
            clipboard.PrimaryClip = clip;
            return true;
        }
    }
}