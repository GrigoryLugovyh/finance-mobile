﻿#pragma warning disable 618
using System;
using System.Collections.Generic;
using System.Linq;
using Android.Content;
using Android.Database;
using Android.Database.Sqlite;
using Mobile.Core.BL.Core;
using Mobile.Core.BL.Core.Extensions;
using Mobile.Core.BL.Core.Infrastructure;
using Mobile.Core.BL.Core.Platform.Data;
using Mobile.Core.BL.Core.Platform.Specific;
using Mobile.Core.BL.IoCContainer;
using Mobile.Model;
using SQLite;

namespace Mobile.Core.Droid.Platform
{
    public class DroidDataBulkHandler : IMDataBulkHandler
    {
        private const string ClauseSeparator = " and ";
        private readonly Dictionary<string, int> columnIndexCache = new Dictionary<string, int>();
        private SQLiteDatabase database;

        public void Open()
        {
            if ((database == null) || !database.IsOpen)
                database = SQLiteDatabase.OpenDatabase(Container.Resolve<IPlatformSpecific>().SqlitePath, null,
                    DatabaseOpenFlags.OpenReadwrite);
        }

        public void Close()
        {
            if (database == null)
                return;

            if (database.IsOpen)
                database.Close();

            database = null;
        }

        public void Insert<TEntity>(TEntity entity, Action<TEntity> bindAction = null)
            where TEntity : MEntity
        {
            if (!Prepare())
                return;

            var tableName = typeof(TEntity).Name;

            var tableMapping = new TableMapping(typeof(TEntity));

            var keys =
                tableMapping.InsertColumns.Select(
                        column => Tuple.Create(column, $"{typeof(TEntity).FullName}+{column.Name}"))
                    .ToList();

            DatabaseUtils.InsertHelper insertHelper = null;

            try
            {
                insertHelper = new DatabaseUtils.InsertHelper(database, tableName);

                insertHelper.PrepareForInsert();
                bindAction?.Invoke(entity);
                Bind(insertHelper, tableMapping, entity, keys).Execute();
            }
            finally
            {
                insertHelper?.Close();

                Close();
            }
        }

        public void Insert<TEntity>(IEnumerable<TEntity> entities, Action<TEntity> bindAction = null)
            where TEntity : MEntity
        {
            if (!Prepare())
                return;

            var tableName = typeof(TEntity).Name;

            var tableMapping = new TableMapping(typeof(TEntity));

            var keys =
                tableMapping.InsertColumns.Select(
                        column => Tuple.Create(column, $"{typeof(TEntity).FullName}+{column.Name}"))
                    .ToList();

            DatabaseUtils.InsertHelper insertHelper = null;

            try
            {
                database.BeginTransactionNonExclusive();

                insertHelper = new DatabaseUtils.InsertHelper(database, tableName);

                foreach (var entity in entities)
                {
                    insertHelper.PrepareForInsert();
                    bindAction?.Invoke(entity);
                    Bind(insertHelper, tableMapping, entity, keys).Execute();
                }

                database.SetTransactionSuccessful();
            }
            finally
            {
                database.EndTransaction();

                insertHelper?.Close();

                Close();
            }
        }

        public void Update<TEntity>(TEntity entity, Action<TEntity> bindAction = null, bool byId = true,
            Dictionary<string, string> parameters = default(Dictionary<string, string>))
            where TEntity : MEntity
        {
            if (!Prepare())
                return;

            try
            {
                var tableName = typeof(TEntity).Name;

                var tableMapping = new TableMapping(typeof(TEntity));

                var content = new ContentValues();

                bindAction?.Invoke(entity);

                Bind(content, tableMapping, entity);

                var where = MakeWhereClause(byId, byId ? entity.Id.ToString() : entity.ServerId, parameters);

                database.Update(tableName, content, where.Clause, where.Args);
            }
            finally
            {
                Close();
            }
        }

        public void Update<TEntity>(IEnumerable<TEntity> entities, Action<TEntity> bindAction = null, bool byId = true,
            Dictionary<string, string> parameters = default(Dictionary<string, string>))
            where TEntity : MEntity
        {
            if (!Prepare())
                return;

            var tableName = typeof(TEntity).Name;

            var tableMapping = new TableMapping(typeof(TEntity));

            try
            {
                database.BeginTransactionNonExclusive();

                foreach (var entity in entities)
                {
                    var content = new ContentValues();

                    bindAction?.Invoke(entity);

                    Bind(content, tableMapping, entity);

                    var where = MakeWhereClause(byId, byId ? entity.Id.ToString() : entity.ServerId, parameters);

                    database.Update(tableName, content, where.Clause, where.Args);
                }

                database.SetTransactionSuccessful();
            }
            finally
            {
                database.EndTransaction();

                Close();
            }
        }

        public void Delete<TEntity>(bool byId = true, string[] ids = null,
            Dictionary<string, string> parameters = default(Dictionary<string, string>))
            where TEntity : MEntity
        {
            if (!Prepare())
                return;

            if ((ids == null) || (ids.Length == 0))
                return;

            var tableName = typeof(TEntity).Name;

            try
            {
                database.BeginTransactionNonExclusive();

                foreach (var id in ids)
                {
                    var where = MakeWhereClause(byId, id, parameters);

                    database.Delete(tableName, where.Clause, where.Args);
                }

                database.SetTransactionSuccessful();
            }
            finally
            {
                database.EndTransaction();

                Close();
            }
        }

        public void Query(Func<string[], bool> readCursorPortion, string query, params string[] selectionArgs)
        {
            if (readCursorPortion == null)
                return;

            if (string.IsNullOrEmpty(query))
                return;

            if (!Prepare())
                return;

            ICursor cursor = null;

            try
            {
                cursor = database.RawQuery(query, selectionArgs);

                var columns = cursor.GetColumnNames();

                if (columns.Length == 0)
                    return;

                if (cursor.MoveToFirst())
                    while (!cursor.IsAfterLast)
                    {
                        var values = new string[columns.Length];

                        for (var i = 0; i < columns.Length; ++i)
                            values[i] = cursor.GetString(cursor.GetColumnIndex(columns[i]));

                        if (!readCursorPortion(values))
                            return;

                        cursor.MoveToNext();
                    }
            }
            finally
            {
                if (cursor != null)
                {
                    cursor.Close();
                    cursor.Deactivate();
                }

                Close();
            }
        }

        public void Execute(string query)
        {
            if (string.IsNullOrEmpty(query))
                return;

            if (!Prepare())
                return;

            try
            {
                database.ExecSQL(query);
            }
            finally
            {
                Close();
            }
        }

        public T ExecuteScalar<T>(string query)
        {
            return database.RawQuery(query, new string[] {}).GetString(0).As<T>();
        }

        private QueryClause MakeWhereClause(bool byId, string id, Dictionary<string, string> parameters)
        {
            if (byId)
                return new QueryClause($"{nameof(MEntity.Id)}=?", id.Yield().ToArray());

            if (parameters == null)
                parameters = new Dictionary<string, string>();

            return
                new QueryClause(
                    string.Join(ClauseSeparator,
                        nameof(MEntity.ServerId).Yield().Concat(parameters.Keys).Select(s => $"{s}=?")),
                    id.Yield().Concat(parameters.Values).ToArray());
        }

        protected bool Prepare()
        {
            if (database == null)
                Open();

            if (database == null)
            {
                Tracer.Warning("DroidDataBulkHandler connaction is null");
                return false;
            }

            columnIndexCache.Clear();

            return true;
        }

        protected virtual DatabaseUtils.InsertHelper Bind<TEntity>(DatabaseUtils.InsertHelper insertHelper,
            TableMapping mapping, TEntity entity, List<Tuple<TableMapping.Column, string>> keys) where TEntity : MEntity
        {
            foreach (var columnItem in keys)
            {
                var column = columnItem.Item1;
                var columnIndex = GetColumnIndex(insertHelper, column.Name, columnItem.Item2);

                if (columnIndex <= 0)
                    continue;

                var value = GetValue(column.Name, entity);

                if (value == null)
                    insertHelper.BindNull(columnIndex);
                else if (value is string)
                    insertHelper.Bind(columnIndex, (string) value);
                else if (value is int)
                    insertHelper.Bind(columnIndex, (int) value);
                else if (value is double)
                    insertHelper.Bind(columnIndex, (double) value);
                else if (value is float)
                    insertHelper.Bind(columnIndex, (float) value);
                else if (value is DateTime)
                    insertHelper.Bind(columnIndex, ((DateTime) value).ToString("yyyy-MM-dd HH:mm:ss"));
                else if (value is DateTimeOffset)
                    insertHelper.Bind(columnIndex, ((DateTimeOffset) value).Ticks);
                else if (value is bool)
                    insertHelper.Bind(columnIndex, (bool) value);
                else if (value is byte[])
                    insertHelper.Bind(columnIndex, (byte[]) value);
                else if (value is long)
                    insertHelper.Bind(columnIndex, (long) value);
                else if (value.GetType().IsEnum)
                    insertHelper.Bind(columnIndex, Convert.ToInt32(value));
                else
                    insertHelper.Bind(columnIndex, Converter.Serialize(value));
            }

            return insertHelper;
        }

        protected virtual void Bind<TEntity>(ContentValues content,
            TableMapping mapping, TEntity entity) where TEntity : MEntity
        {
            foreach (var column in mapping.InsertColumns)
            {
                var value = GetValue(column.Name, entity);

                if (value == null)
                    content.PutNull(column.Name);
                else if (value is bool)
                    content.Put(column.Name, (bool) value);
                else if (value is byte[])
                    content.Put(column.Name, (byte[]) value);
                else if (value is double)
                    content.Put(column.Name, (double) value);
                else if (value is float)
                    content.Put(column.Name, (float) value);
                else if (value is int)
                    content.Put(column.Name, (int) value);
                else if (value is long)
                    content.Put(column.Name, (long) value);
                else if (value is string)
                    content.Put(column.Name, (string) value);
                else if (value is DateTime)
                    content.Put(column.Name, ((DateTime) value).ToString("yyyy-MM-dd HH:mm:ss"));
                else if (value is DateTimeOffset)
                    content.Put(column.Name, ((DateTimeOffset) value).Ticks);
                else if (value.GetType().IsEnum)
                    content.Put(column.Name, Convert.ToInt32(value));
                else
                    content.Put(column.Name, Converter.Serialize(value));
            }
        }

        protected virtual object GetValue<TEntity>(string columnName, TEntity entity)
        {
            return MPropertyCallAdapterProvider<TEntity>.GetInstance(columnName).InvokeGet(entity);
        }

        private int GetColumnIndex(DatabaseUtils.InsertHelper insertHelper, string columnName, string key)
        {
            if (columnIndexCache.ContainsKey(key))
                return columnIndexCache[key];

            var index = insertHelper.GetColumnIndex(columnName);
            columnIndexCache[key] = index;
            return index;
        }

        internal class QueryClause : Tuple<string, string[]>
        {
            public QueryClause(string whereClause, string[] whereArgs) : base(whereClause, whereArgs)
            {
            }

            public string Clause => Item1;

            public string[] Args => Item2;
        }
    }
}