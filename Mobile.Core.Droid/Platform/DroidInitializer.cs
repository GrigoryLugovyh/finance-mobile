﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Android.Content;
using Mobile.Core.BL.Core.Enums;
using Mobile.Core.BL.Core.Exceptions;
using Mobile.Core.BL.Core.Extensions;
using Mobile.Core.BL.Core.Model;
using Mobile.Core.Droid.Views;

namespace Mobile.Core.Droid.Platform
{
    public class DroidInitializer : IDroidInitializer
    {
        public DroidInitializer()
        {
            State = MInitState.Uninitialized;
        }

        public MInitState State { get; protected set; }

        public DroidModel EntryPoint { get; protected set; }

        public IDroidSplashScreenActivity Splash { get; protected set; }

        public virtual string EntryPointName => MConstants.AppEntryPoint;

        public virtual Type DetectEntryPoint()
        {
            var query =
                AppDomain.CurrentDomain.GetAssemblies()
                    .SelectMany(assembly => assembly.SafeGetTypes(), (assembly, type) => new {assembly, type})
                    .Where(t => string.Equals(t.type.Name, EntryPointName))
                    .Where(t => typeof(DroidModel).IsAssignableFrom(t.type))
                    .Select(t => t.type);

            return query.FirstOrDefault();
        }

        public virtual void Create(Context appContext)
        {
            var entryType = DetectEntryPoint();

            if (entryType == null)
            {
                throw new MException("Unable to find an entry point into the application");
            }

            try
            {
                EntryPoint = (DroidModel) Activator.CreateInstance(entryType, appContext);
            }
            catch (Exception ex)
            {
                throw ex.MExpand("Failed to create an instance of type \"{0}\"", entryType.FullName);
            }
        }

        public virtual Task Start(IDroidSplashScreenActivity splash)
        {
            Splash = splash;

            if (State == MInitState.Initializing || State == MInitState.Initialized)
            {
                if (Splash != null && State == MInitState.Initialized)
                {
                    Splash.InitCompleted();
                }

                return null;
            }

            EntryPoint.BaseInitialize();

            State = MInitState.Initializing;

            return Task.Factory.StartNew(() =>
            {
                EntryPoint.CommonInitialize();

                State = MInitState.Initialized;

                Splash?.InitCompleted();
            });
        }

        public virtual void Remove()
        {
            Splash = null;
        }
    }
}