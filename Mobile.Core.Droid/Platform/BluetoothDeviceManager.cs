﻿using Android.Bluetooth;
using Mobile.Core.BL.Core.Platform.Bluetooth;
using Mobile.Core.Droid.Views;
using System;
using System.Linq;
using BluetoothDevice = Mobile.Core.BL.Core.Platform.Bluetooth.BluetoothDevice;

namespace Mobile.Core.Droid.Platform
{
    //todo подписаться на события Bluetooth через BroadcastReceiver
    public class BluetoothDeviceManager : IBluetoothDeviceManager
    {
        private readonly BluetoothAdapter bluetoothAdapter;

        public BluetoothDeviceManager(IDroidBindingContextStack<IDroidView> droidView)
        {
            bluetoothAdapter = droidView.Current?.BluetoothAdapter;
        }

        public BluetoothDevice GetBondedDeviceByPartName(string namePart)
        {
            var filter =
                new Func<Android.Bluetooth.BluetoothDevice, bool>(
                    bluetoothDevice => bluetoothDevice.Name.ToLower().Contains(namePart.ToLower()));
            return GetBondedDevice(filter);
        }

        public bool IsEnabled => IsAvailable && bluetoothAdapter.IsEnabled;
        public bool IsAvailable => bluetoothAdapter != null;

        public bool Enable()
        {
            return IsEnabled ? IsEnabled : bluetoothAdapter?.Enable() ?? false;
        }

        public BluetoothDevice GetBondedDeviceByName(string name)
        {
            var filter =
                new Func<Android.Bluetooth.BluetoothDevice, bool>(
                    bluetoothDevice => bluetoothDevice.Name.ToLower() == name.ToLower());
            return GetBondedDevice(filter);
        }

        private BluetoothDevice GetBondedDevice(Func<Android.Bluetooth.BluetoothDevice, bool> filter)
        {
            var device =
               bluetoothAdapter.BondedDevices.FirstOrDefault(filter);
            return device == null
                ? null
                : new BluetoothDevice
                {
                    Name = device.Name,
                    State = GetState(device.BondState)
                };
        }

        private static BluetoothDevice.BluetoothDeviceState GetState(Bond bondState)
        {
            return bondState == Bond.Bonded
                ? BluetoothDevice.BluetoothDeviceState.Connected
                : BluetoothDevice.BluetoothDeviceState.Disconnected;
        }
    }
}