﻿using Android.Content;

namespace Mobile.Core.Droid.Platform
{
    public interface IDroidCurrentContext
    {
        Context Context { get; }
    }
}