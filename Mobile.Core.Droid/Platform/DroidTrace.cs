﻿using System;
using System.Diagnostics;
using System.IO;
using Android.Util;
using Java.IO;
using Mobile.Core.BL.Core.Events;
using Mobile.Core.BL.Core.Extensions;
using Mobile.Core.BL.Core.Platform.Trace;
using Environment = Android.OS.Environment;
using File = Java.IO.File;

namespace Mobile.Core.Droid.Platform
{
    public class DroidTrace : IMTrace
    {
        private File dump;

        private File Dump
        {
            get { return dump ?? (dump = new File(DebugPath, "dump.log")); }
        }

        private string DebugPath
        {
            get { return Path.Combine(Environment.ExternalStorageDirectory.AbsolutePath, "Ecodoc"); }
        }

        public event EventHandler<MValueEventArgs<string>> TraceCall;

        public void ClearDump()
        {
#if DEBUG
            if (Dump.Exists())
            {
                Dump.Delete();
            }
#endif
        }

        public void Trace(MTraceLevel level, string tag, Func<string> message)
        {
            Trace(level, tag, message());
        }

        public void Trace(MTraceLevel level, string tag, string message)
        {
            TraceCall.Rise(this, message);
#if DEBUG
            WriteToDump(message);
            Log.Info(tag, message);
            Debug.WriteLine(tag + ":" + level + ":" + message);
#endif
        }

        public void Trace(MTraceLevel level, string tag, string message, params object[] args)
        {
            TraceCall.Rise(this, string.Format(message, args));
#if DEBUG
            try
            {
                WriteToDump(message, args);
                Log.Info(tag, message, args);
                Debug.WriteLine(tag + ":" + level + ":" + message, args);
            }
            catch (Exception ex)
            {
                Trace(MTraceLevel.Error, tag, string.Format("Exception during trace: {0}", ex.ToLong()));
                Trace(level, tag, message);
            }
#endif
        }

        private void WriteToDump(string message, params object[] args)
        {
            string text = string.Format("{0:HH:mm:ss}: {1}{2}", DateTime.Now, string.Format(message, args),
                System.Environment.NewLine);

            if (!Directory.Exists(DebugPath))
            {
                Directory.CreateDirectory(DebugPath);
            }

            if (!Dump.Exists())
            {
                Dump.CreateNewFile();
            }

            var pw = new PrintWriter(new FileWriter(Dump, true));
            pw.Append(text);
            pw.Flush();
            pw.Close();
        }
    }
}