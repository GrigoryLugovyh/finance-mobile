﻿using System;
using System.Threading.Tasks;
using Android.Content;
using Mobile.Core.BL.Core.Enums;
using Mobile.Core.Droid.Views;

namespace Mobile.Core.Droid.Platform
{
    public interface IDroidInitializer
    {
        MInitState State { get; }

        DroidModel EntryPoint { get; }

        IDroidSplashScreenActivity Splash { get; }

        string EntryPointName { get; }

        Type DetectEntryPoint();

        void Create(Context appContext);

        Task Start(IDroidSplashScreenActivity splash);

        void Remove();
    }
}