﻿using System.Collections.Generic;
using Mobile.Core.BL.Core.Platform.Keyboard;

namespace Mobile.Core.Droid.Platform
{
    public class DroidKeyCodeHandler : DroidHandler, IMKeyCodeHandler
    {
        private readonly Dictionary<MKeyCode, int> mKeycodeCharMapping = new Dictionary<MKeyCode, int>
        {
            {MKeyCode.D0, 48},
            {MKeyCode.D1, 49},
            {MKeyCode.D2, 50},
            {MKeyCode.D3, 51},
            {MKeyCode.D4, 52},
            {MKeyCode.D5, 53},
            {MKeyCode.D6, 54},
            {MKeyCode.D7, 55},
            {MKeyCode.D8, 56},
            {MKeyCode.D9, 57},
            {MKeyCode.Enter, 13},
            {MKeyCode.Back, 9},
            {MKeyCode.A, 65},
            {MKeyCode.B, 66},
            {MKeyCode.C, 67},
            {MKeyCode.D, 68},
            {MKeyCode.E, 69},
            {MKeyCode.F, 70},
            {MKeyCode.G, 71},
            {MKeyCode.H, 72},
            {MKeyCode.I, 73},
            {MKeyCode.J, 74},
            {MKeyCode.K, 75},
            {MKeyCode.L, 76},
            {MKeyCode.M, 77},
            {MKeyCode.N, 78},
            {MKeyCode.O, 79},
            {MKeyCode.P, 80},
            {MKeyCode.Q, 81},
            {MKeyCode.R, 82},
            {MKeyCode.S, 83},
            {MKeyCode.T, 84},
            {MKeyCode.U, 85},
            {MKeyCode.V, 86},
            {MKeyCode.W, 87},
            {MKeyCode.X, 88},
            {MKeyCode.Y, 89},
            {MKeyCode.Z, 90},


        };
        public char GetChar(MKeyCode keycode)
        {
            if (mKeycodeCharMapping.ContainsKey(keycode))
                return (char)mKeycodeCharMapping[keycode];
            return '\0';
        }
    }
}