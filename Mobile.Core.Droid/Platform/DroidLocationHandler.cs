﻿using Android.Locations;
using Mobile.Core.BL.Core.Enums;
using Mobile.Core.BL.Core.Platform.Location;
using Mobile.Core.Droid.Services;
using Mobile.Core.Droid.Services.Location;

namespace Mobile.Core.Droid.Platform
{
    public class DroidLocationHandler : MLocationHandler
    {
        protected DroidLocationService Service
        {
            get
            {
                DroidServiceConnector<DroidLocationService> connector =
                    DroidServicesWrapper.Instance.GetConnector<DroidLocationService, DroidLocationServiceConnector>();

                if (connector == null)
                {
                    return null;
                }

                if (connector.Binder == null || !connector.Binder.IsBound)
                {
                    return null;
                }

                return connector.Binder.Service;
            }
        }

        public override MLocationProviderType ProviderType
        {
            get
            {
                DroidLocationService service = Service;

                if (service == null)
                {
                    return MLocationProviderType.Passive;
                }

                var provider = service.LocationProvider;

                if (string.Equals(provider, LocationManager.NetworkProvider))
                {
                    return MLocationProviderType.Network;
                }

                if (string.Equals(provider, LocationManager.GpsProvider))
                {
                    return MLocationProviderType.Gps;
                }

                return MLocationProviderType.Passive;
            }
        }

        public override bool LocationEnable()
        {
            DroidLocationService service = Service;

            if (service == null)
            {
                return false;
            }
            
            var provider = service.LocationProvider;

            return !string.IsNullOrWhiteSpace(provider) && !string.Equals(provider, LocationManager.PassiveProvider);
        }

        public override MСoordinates Location()
        {
            DroidLocationService service = Service;

            if (service == null)
            {
                return MСoordinates.Default;
            }

            if (service.LastLocation == null)
            {
                return MСoordinates.Default;
            }

            return new MСoordinates(service.LastLocation.Longitude, service.LastLocation.Latitude,
                service.LastLocation.Accuracy);
        }
    }
}