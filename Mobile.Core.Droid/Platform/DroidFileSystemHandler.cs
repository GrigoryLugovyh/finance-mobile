﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Android.OS;
using Mobile.Core.BL.Core.Platform.FileSystem;
using File = Java.IO.File;

namespace Mobile.Core.Droid.Platform
{
    public class DroidFileSystemHandler : DroidHandler, IMFileSystemHandler
    {
        public string ApplicationDirectory
        {
            get
            {
                var packageManager = Environment.Context.PackageManager;
                var packageName = Environment.Context.PackageName;

                if (packageManager == null || packageName == null)
                {
                    return string.Empty;
                }

                var packageInfo = packageManager.GetPackageInfo(packageName, 0);

                if (packageInfo == null)
                {
                    return string.Empty;
                }

                return packageInfo.ApplicationInfo.DataDir;
            }
        }

        public int FolderFileCount(string folder, string searchPattern = "*",
            SearchOption searchOption = SearchOption.TopDirectoryOnly, List<string> expectedFileNames = null)
        {
            return GetFiles(folder, searchPattern, searchOption, expectedFileNames).Length;
        }

        public FileInfo GetFileInfo(string filePath)
        {
            return new FileInfo(filePath);
        }

        public string FolderCreateIfNotExist(string directory)
        {
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }

            return directory;
        }

        public long FolderSize(string folder, string searchPattern = "*",
            SearchOption searchOption = SearchOption.TopDirectoryOnly, List<string> expectedFileNames = null)
        {
            return GetFiles(folder, searchPattern, searchOption, expectedFileNames).Sum(file => new File(file).Length());
        }

        public long InternalTotalMemory => DirectoryTotalMemory(Android.OS.Environment.RootDirectory.AbsolutePath);

        public long InternalFreeMemory => DirectoryFreeMemory(Android.OS.Environment.RootDirectory.AbsolutePath);

        public long InternalBusyMemory => DirectoryBusyMemory(Android.OS.Environment.RootDirectory.AbsolutePath);

        public long ExternalTotalMemory
            => DirectoryTotalMemory(Android.OS.Environment.ExternalStorageDirectory.AbsolutePath);

        public long ExternalFreeMemory
            => DirectoryFreeMemory(Android.OS.Environment.ExternalStorageDirectory.AbsolutePath);

        public long ExternalBusyMemory
            => DirectoryBusyMemory(Android.OS.Environment.ExternalStorageDirectory.AbsolutePath);

        public long DataTotalMemory => DirectoryTotalMemory(Android.OS.Environment.DataDirectory.AbsolutePath);

        public long DataFreeMemory => DirectoryFreeMemory(Android.OS.Environment.DataDirectory.AbsolutePath);

        public long DataBusyMemory => DirectoryBusyMemory(Android.OS.Environment.DataDirectory.AbsolutePath);

        private string[] GetFiles(string folder, string searchPattern = "*",
            SearchOption searchOption = SearchOption.TopDirectoryOnly, List<string> expectedFileNames = null)
        {
            var files = Directory.GetFiles(folder, searchPattern, searchOption);
            if (expectedFileNames == null)
                return files;
            var expectedFullFileNames = expectedFileNames.Select(s => Path.Combine(folder, s)).ToList();
            return files.Where(s => expectedFullFileNames.Contains(s)).ToArray();
        }

        protected long DirectoryTotalMemory(string path)
        {
            var statFs = new StatFs(path);
            var total = ((long)statFs.BlockCount * statFs.BlockSize);
            return total;
        }

        protected long DirectoryFreeMemory(string path)
        {
            var statFs = new StatFs(path);
            var free = (statFs.AvailableBlocks * (long)statFs.BlockSize);
            return free;
        }

        protected long DirectoryBusyMemory(string path)
        {
            var statFs = new StatFs(path);
            var total = ((long)statFs.BlockCount * statFs.BlockSize);
            var free = (statFs.AvailableBlocks * (long)statFs.BlockSize);
            var busy = total - free;
            return busy;
        }
    }
}