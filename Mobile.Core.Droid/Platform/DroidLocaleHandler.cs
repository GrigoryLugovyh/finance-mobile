﻿using System.Globalization;
using Android.App;
using Java.Util;
using Mobile.Core.BL.Core.Platform.Locale;

namespace Mobile.Core.Droid.Platform
{
    public class DroidLocaleHandler : IMLocaleHandler
    {
        private CultureInfo culture;

        public CultureInfo Culture
        {
            get
            {
                if (culture == null)
                {
                    Locale locale = Application.Context.Resources.Configuration.Locale;
                    culture = new CultureInfo($"{locale.Language}-{locale.Country}");
                }

                return culture;
            }
        }
    }
}