﻿using System;
using System.Collections.Generic;
using System.IO;
using Android.Graphics;
using Android.Media;

namespace Mobile.Core.Droid.Platform
{
    public class DroidBitmapHandler : DroidHandler, IDroidBitmapHandler
    {
        private readonly Dictionary<int, int> rotateDegreeDict = new Dictionary<int, int>
        {
            {(int) Orientation.Rotate270, 270},
            {(int) Orientation.Rotate180, 180},
            {(int) Orientation.Rotate90, 90}
        };

        public Bitmap GetBitmapFile(string file)
        {
            if (!File.Exists(file))
            {
                return null;
            }

            byte[] buffer = File.ReadAllBytes(file);

            if (buffer.Length == 0)
            {
                return null;
            }

            return RotateImage(BitmapFactory.DecodeByteArray(buffer, 0, buffer.Length), file);
        }

        public Bitmap Resize(Bitmap source, int width, int height)
        {
            return Bitmap.CreateScaledBitmap(source, width, height, true);
        }

        public byte[] Compress(byte[] buffer, long limit, string fileName = null)
        {
            if (limit < 1)
            {
                return buffer;
            }

            var bitmapOptions = new BitmapFactory.Options {InJustDecodeBounds = true};
            BitmapFactory.DecodeByteArray(buffer, 0, buffer.Length, bitmapOptions);

            int scale = 1;
            while ((bitmapOptions.OutWidth*bitmapOptions.OutHeight)*(1/Math.Pow(scale, 2)) > limit)
            {
                scale++;
            }

            if (scale > 0)
            {
                scale--;

                var newBitmapOptions = new BitmapFactory.Options {InSampleSize = scale};
                var newBitmap = BitmapFactory.DecodeByteArray(buffer, 0, buffer.Length, newBitmapOptions);
                if (!string.IsNullOrEmpty(fileName))
                    newBitmap = RotateImage(newBitmap, fileName);

                double y = Math.Sqrt(limit/(((double) newBitmap.Width)/newBitmap.Height));
                double x = (y/newBitmap.Height)*newBitmap.Width;

                newBitmap = Resize(newBitmap, (int) x, (int) y);

                using (var stream = new MemoryStream())
                {
                    newBitmap.Compress(Bitmap.CompressFormat.Jpeg, 90, stream);
                    buffer = stream.GetBuffer();
                }

                newBitmap.Recycle();
                // ReSharper disable once RedundantAssignment
                newBitmap = null;
            }

            return buffer;
        }

        public byte[] BitmapBytes(Bitmap bitmap, int quality = 100)
        {
            byte[] buffer;

            using (var stream = new MemoryStream())
            {
                bitmap.Compress(Bitmap.CompressFormat.Jpeg, quality, stream);
                buffer = stream.GetBuffer();
            }

            return buffer;
        }

        public void Save(Bitmap bmp, string fileName)
        {
            using (var stream = new FileStream(fileName, FileMode.CreateNew))
            {
                bmp.Compress(Bitmap.CompressFormat.Jpeg, 80, stream);
                stream.Flush();
                stream.Close();
            }
        }

        private Bitmap RotateImage(Bitmap image, string file)
        {
            Dispatcher.InvokeAction(() =>
            {
                var exif = new ExifInterface(file);
                var orientation = exif.GetAttributeInt(ExifInterface.TagOrientation, (int) Orientation.Normal);
                if (rotateDegreeDict.ContainsKey(orientation))
                    image = RotateImage(image, rotateDegreeDict[orientation]);
            });
            return image;
        }

        private Bitmap RotateImage(Bitmap img, int degree)
        {
            var matrix = new Matrix();
            matrix.PostRotate(degree);
            var rotatedImg = Bitmap.CreateBitmap(img, 0, 0, img.Width, img.Height, matrix, true);
            img.Recycle();
            // ReSharper disable once RedundantAssignment
            img = null;
            return rotatedImg;
        }
    }
}