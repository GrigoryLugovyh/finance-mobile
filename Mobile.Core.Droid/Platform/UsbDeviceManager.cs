﻿using System.Linq;
using Android.Content;
using Android.Hardware.Usb;
using Mobile.Core.BL.Core.Platform.Usb;
using Mobile.Core.BL.IoCContainer;
using Mobile.Core.Droid.Views;

namespace Mobile.Core.Droid.Platform
{
    public class UsbDeviceManager : IUsbDeviceManager
    {
        public bool DeviceIsConnected(int vid, int pid)
        {
            var usbManager =
                Container.Resolve<IDroidCurrentActivity>().Activity.GetSystemService(Context.UsbService) as UsbManager;
            return (usbManager != null) &&
                   usbManager.DeviceList.Any(pair => pair.Value.ProductId == pid && pair.Value.VendorId == vid);
        }
    }
}