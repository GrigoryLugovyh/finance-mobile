﻿using System.Reflection;

namespace Mobile.Core.Droid.Platform
{
    public interface IDroidCurrentAssembly
    {
        Assembly CurrentAssembly { get; }

        string CurrentNamespace { get; }

        string CurrentFullname { get; }
    }
}