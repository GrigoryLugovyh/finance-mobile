﻿using System;

namespace Mobile.Core.Droid.Platform
{
    public static class DroidConstants
    {
        public const string ResourceName = "Resource";
        public const string ResourceStyleableNestedTypeName = "Styleable";
        public const string ResourceBindingName = "Binding";
        public const string ResourceBindingBindName = "Binding_Bind";
        public const int ResourceBindingDefaultValue = 0;
        public const string TypeSeparator = ".";
        public const string AndroidId = "AdroidId";

        public static readonly string[] ViewNamespaces =
        {
            "Android.Views",
            "Android.Widget",
            "Android.Webkit"
        };

        public static readonly Type[] ViewTypes =
        {
            typeof(Android.Widget.RelativeLayout),
            typeof(Android.Widget.LinearLayout),
            typeof(Android.Widget.TableLayout),
            typeof(Android.Widget.FrameLayout),
            typeof(Android.Widget.GridLayout),
            typeof(Android.Widget.ImageButton),
            typeof(Android.Widget.TableRow),
            typeof(Android.Widget.Space),
            typeof(Android.Views.SurfaceView)
        };
    }
}