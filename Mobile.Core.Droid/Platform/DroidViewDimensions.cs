﻿using Mobile.Core.BL.Core.Platform.Display;
using Mobile.Core.BL.IoCContainer;

namespace Mobile.Core.Droid.Platform
{
    public class DroidViewDimensions : IMViewDimensions
    {
        private float scale;

        public float Scale
        {
            get
            {
                return scale > 0
                    ? scale
                    : (scale = Container.Resolve<IDroidCurrentContext>().Context.Resources.DisplayMetrics.Density);
            }
        }

        public int InDpUnits(int pixels)
        {
            return (int) (pixels*Scale + 0.5f);
        }
    }
}