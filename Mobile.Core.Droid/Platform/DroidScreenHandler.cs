﻿using Android.Graphics;
using Android.Views;
using Mobile.Core.BL.Core.Exceptions;
using Mobile.Core.BL.Core.Platform.Media;
using Mobile.Core.BL.IoCContainer;
// ReSharper disable RedundantAssignment

namespace Mobile.Core.Droid.Platform
{
    public class DroidScreenHandler : DroidHandler, IMScreenHandler
    {
        private IDroidBitmapHandler bitmapHandler;

        private IDroidBitmapHandler BitmapHandler
            => bitmapHandler ?? (bitmapHandler = Container.Resolve<IDroidBitmapHandler>());

        public byte[] TakeScreenshot(int quality = 100)
        {
            View view = null;
            Bitmap screenshot = null;

            try
            {
                view = Activity?.Window?.DecorView?.RootView;

                if (view == null)
                {
                    throw new MException("Current root view is null");
                }

                view.DrawingCacheEnabled = true;

                screenshot = Bitmap.CreateBitmap(view.DrawingCache);

                if (quality < 0 || quality > 100)
                {
                    quality = 100;
                }

                return BitmapHandler.BitmapBytes(screenshot, quality);
            }
            finally
            {
                if (view != null)
                {
                    view.DrawingCacheEnabled = false;
                }

                if (screenshot != null)
                {
                    screenshot.Recycle();
                    screenshot = null;
                }
            }
        }
    }
}