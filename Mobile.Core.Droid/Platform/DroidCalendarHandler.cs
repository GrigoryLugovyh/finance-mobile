﻿using System;
using Android.App;
using Android.Content;
using Java.Lang;
using Java.Util;
using Mobile.Core.BL.Core;
using Mobile.Core.BL.Core.Platform.Calendar;

namespace Mobile.Core.Droid.Platform
{
    public class DroidCalendarHandler : DroidHandler, IMCalendarHandler
    {
        protected AlarmManager Alarm => (AlarmManager) Environment.Context.GetSystemService(Context.AlarmService);

        public void Set(DateTimeOffset dateTime)
        {
            Calendar calendar = Calendar.Instance;

            calendar.Set(
                dateTime.Year,
                dateTime.Month,
                dateTime.Day,
                dateTime.Hour,
                dateTime.Minute,
                dateTime.Second);

            try
            {
                Alarm.SetTime(calendar.TimeInMillis);
            }
            catch (RuntimeException ex)
            {
                Tracer.Error("DroidCalendarHandler.Set: {0}", ex.Message);
            }
        }
    }
}