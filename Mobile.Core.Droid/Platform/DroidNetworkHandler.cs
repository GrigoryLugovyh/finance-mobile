﻿using Android.Content;
using Android.Net;
using Android.Net.Wifi;
using Android.Text.Format;
using Mobile.Core.BL.Core.Exceptions;
using Mobile.Core.BL.Core.Model;
using Mobile.Core.BL.Core.Platform.Network;
using Mobile.Core.BL.IoCContainer;
using Org.Apache.Http.Client.Methods;
using Org.Apache.Http.Impl.Client;
using Org.Apache.Http.Util;
using Org.Json;

namespace Mobile.Core.Droid.Platform
{
    public class DroidNetworkHandler : DroidHandler, IMNetworkHandler
    {
        private IDroidCommandShellHandler commandShell;

        protected IDroidCommandShellHandler CommandShell
        {
            get { return commandShell ?? (commandShell = Container.Resolve<IDroidCommandShellHandler>()); }
        }

        public bool CheckConnection(string url)
        {
            return InvokeOnMainThread(() =>
            {
                if (!CommandShell.Execute(string.Format("ping -c 1 -w 5 {0}", url)))
                {
                    throw new MException("The server {0} is not reachable", url);
                }

            }).Success;
        }

        public bool WifiEnabled
        {
            get
            {
                var wifiService = Environment.Context.GetSystemService(Context.WifiService);

                if (wifiService == null)
                {
                    return false;
                }

                if ((wifiService as WifiManager) == null)
                {
                    return false;
                }

                return ((WifiManager) wifiService).IsWifiEnabled;
            }
        }

        public bool WifiConnected
        {
            get
            {
                var networkInfo = GetNetworkInfo(ConnectivityType.Wifi);

                if (networkInfo == null)
                {
                    return false;
                }

                return networkInfo.IsConnected;
            }
        }

        public string WifiAddress
        {
            get
            {
                var wifiService = Environment.Context.GetSystemService(Context.WifiService);

                if (wifiService == null)
                {
                    return string.Empty;
                }

                if ((wifiService as WifiManager) == null)
                {
                    return string.Empty;
                }

#pragma warning disable 618
                return Formatter.FormatIpAddress(((WifiManager) wifiService).ConnectionInfo.IpAddress);
#pragma warning restore 618
            }
        }

        public string[] ExternalAddress(string url, params string[] args)
        {
            var result = InvokeFuncOnMainThread(() =>
            {
                var results = new string[args.Length];

                var httpClient = new DefaultHttpClient();
                var httpGet = new HttpGet(url);
                var response = httpClient.Execute(httpGet);
                var entity = response.Entity;
                var str = EntityUtils.ToString(entity);
                var jsonData = new JSONObject(str);

                for (var i = 0; i < args.Length; ++i)
                {
                    results[i] = jsonData.GetString(args[i]);
                }

                return new ActionResult { Success = true, Result = results };

            }).Result as string[] ?? new string[args.Length];

            return result;
        }

        public bool MobileConnected
        {
            get
            {
                var networkInfo = GetNetworkInfo(ConnectivityType.Mobile);

                if (networkInfo == null)
                {
                    return false;
                }

                return networkInfo.Type == ConnectivityType.Mobile && networkInfo.IsConnected;
            }
        }

        public string NetworkClass
        {
            get
            {
                var defaultName = string.Empty;

                var connectivityManager =
                    Environment.Context.GetSystemService(Context.ConnectivityService) as ConnectivityManager;

                if (connectivityManager == null)
                {
                    return defaultName;
                }

                var networkInfo = connectivityManager.ActiveNetworkInfo;

                if (networkInfo == null || !networkInfo.IsConnected)
                {
                    return defaultName;
                }

                if (networkInfo.Type == ConnectivityType.Wifi)
                {
                    return "WIFI";
                }

                if (networkInfo.Type == ConnectivityType.Mobile)
                {
                    return networkInfo.SubtypeName;
                }

                return defaultName;
            }
        }

        protected NetworkInfo GetNetworkInfo(ConnectivityType сonnectivityType)
        {
            var connManager = Environment.Context.GetSystemService(Context.ConnectivityService);

            if (connManager == null)
            {
                return null;
            }

            if ((connManager as ConnectivityManager) == null)
            {
                return null;
            }

            return ((ConnectivityManager) connManager).GetNetworkInfo(сonnectivityType);
        }
    }
}