﻿using Android.App;
using Mobile.Core.BL.Core.Platform;
using Mobile.Core.BL.Core.ViewModels;
using Mobile.Core.BL.IoCContainer;
using Mobile.Core.Droid.Views;

namespace Mobile.Core.Droid.Platform
{
    public class DroidHandler : MHandler
    {
        protected Activity Activity
        {
            get
            {
                var activity = Container.Resolve<IDroidCurrentActivity>().Activity ??
                               Container.Resolve<IDroidActivitiesMonitorOwner>()
                                   .GetActivity(
                                       Container.Resolve<IDroidViewModelRequestDispatcher>()
                                           .GetViewType(
                                               new MViewModelRequest(
                                                   Container.Resolve<IMViewModelStack>().Current.GetType()))) as
                                   Activity;

                return activity;
            }
        }

        protected IDroidCurrentContext Environment => Container.Resolve<IDroidCurrentContext>();
    }
}