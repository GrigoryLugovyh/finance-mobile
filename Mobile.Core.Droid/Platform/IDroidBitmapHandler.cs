﻿using Android.Graphics;

namespace Mobile.Core.Droid.Platform
{
    public interface IDroidBitmapHandler
    {
        Bitmap GetBitmapFile(string file);
        
        Bitmap Resize(Bitmap source, int width, int height);

        byte[] Compress(byte[] buffer, long limit,string file=null);

        byte[] BitmapBytes(Bitmap bitmap, int quality = 100);

        void Save(Bitmap bmp, string fileName);
    }
}