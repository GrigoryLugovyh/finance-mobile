﻿using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using Android.Content;
using Android.Net;
using Android.Content.PM;
using Android.Provider;
using Java.IO;
using Mobile.Core.BL.Core.Exceptions;
using Mobile.Core.BL.Core.Platform.Apps;

namespace Mobile.Core.Droid.Platform
{
    public class DroidAppsHandler : DroidHandler, IMAppsHandler
    {
        private readonly Dictionary<MApp, string> applicationDictionary = new Dictionary<MApp, string>
        {
            {MApp.Location, Settings.ActionLocationSourceSettings},
            {MApp.WifiManager, Settings.ActionWifiSettings},
            {MApp.Settings, Settings.ActionSettings},
            {MApp.Mail, "com.android.email"},
            {MApp.DateSettings, Settings.ActionDateSettings}
        };

        public void Run(string packageName)
        {
            Contract.Assert(!string.IsNullOrWhiteSpace(packageName));
            var intent = Environment.Context.PackageManager.GetLaunchIntentForPackage(packageName);
            StartIntent(intent);
        }

        public void Run(MApp app)
        {
            var appName = GetAppName(app);
            Contract.Assert(!string.IsNullOrWhiteSpace(appName));
            var appIntent = new Intent(appName);
            StartIntent(appIntent);
        }

        public string GetAppName(MApp app)
        {
            string appDetails;

            if (!applicationDictionary.TryGetValue(app, out appDetails))
            {
                throw new MException("Невозможно запустить приложение");
            }
            return appDetails;
        }

        public MAppInfo GetAppInfo(MApp app)
        {
            var appName = GetAppName(app);
            Contract.Assert(!string.IsNullOrWhiteSpace(appName));
            var appIntent = new Intent(appName);
            return new MAppInfo
            {
                PackageName = null,
                Icon = Environment.Context.PackageManager.GetActivityIcon(appIntent)
            };
        }

        public List<MAppInfo> GetAppsInfo(List<string> packageNames)
        {
            return
                Environment.Context.PackageManager.GetInstalledApplications(PackageInfoFlags.MetaData)
                    .Where(info => packageNames.Contains(info.PackageName.ToLowerInvariant()))
                    .Select(info => new MAppInfo
                    {
                        PackageName = info.PackageName,
                        Dir = info.PublicSourceDir,
                        Label = Environment.Context.PackageManager.GetApplicationLabel(info),
                        Icon = Environment.Context.PackageManager.GetApplicationIcon(info)
                    }).ToList();
        }

        public void OpenFile(string filePath, string contentType)
        {
            var file = new File(filePath);
            var path = Uri.FromFile(file);
            var openIntent = new Intent(Intent.ActionView);
            openIntent.SetFlags(ActivityFlags.NewTask);
            openIntent.SetDataAndType(path, contentType);
            ViewDispatcher.RequestAction(() => Environment.Context.StartActivity(openIntent));
        }

        private void StartIntent(Intent intent)
        {
            intent.AddFlags(ActivityFlags.NewTask);
            intent.AddFlags(ActivityFlags.BroughtToFront);
            Environment.Context.StartActivity(intent);
        }

        public void Install(string fullPath)
        {
            Intent intent = new Intent(Intent.ActionView);
            intent.SetDataAndType(Uri.FromFile(new File(fullPath)), "application/vnd.android.package-archive");
            intent.AddFlags(ActivityFlags.NewTask);
            intent.AddFlags(ActivityFlags.BroughtToFront);

            Environment.Context.StartActivity(intent);
        }
    }
}