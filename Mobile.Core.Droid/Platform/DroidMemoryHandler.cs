﻿using System.Linq;
using Android.App;
using Android.Content;
using Android.OS;
using Mobile.Core.BL.Core.Extensions;
using Mobile.Core.BL.Core.Platform.Media;

namespace Mobile.Core.Droid.Platform
{
    public class DroidMemoryHandler : DroidHandler, IMMemoryHandler
    {
        private ActivityManager activityManager;

        protected ActivityManager ActivityManager => activityManager ??
                                                     (activityManager =
                                                         (ActivityManager)
                                                             Environment.Context.GetSystemService(
                                                                 Context.ActivityService));

        protected Debug.MemoryInfo CurrentMemoryInfo => GetProcessMemory(new[] {(int) ProcessId}).FirstOrDefault();

        public long ProcessId => Process.MyPid();

        public long TotalMemoryUsage
        {
            get
            {
                var memory = CurrentMemoryInfo;

                if (memory == null)
                {
                    return 0L;
                }

                return memory.TotalPss*1024;
            }
        }

        public long NativeMemoryUsage
        {
            get
            {
                var memory = CurrentMemoryInfo;

                if (memory == null)
                {
                    return 0L;
                }

                return memory.NativePss*1024;
            }
        }

        public long VirtualMachineMemoryUsage
        {
            get
            {
                var memory = CurrentMemoryInfo;

                if (memory == null)
                {
                    return 0L;
                }

                return memory.DalvikPss*1024;
            }
        }

        public long OtherMemoryUsage
        {
            get
            {
                var memory = CurrentMemoryInfo;

                if (memory == null)
                {
                    return 0L;
                }

                return memory.OtherPss*1024;
            }
        }

        public string Description => $"TotalMemoryUsage: {TotalMemoryUsage.ToReadableSize()} // NativeMemoryUsage: {NativeMemoryUsage.ToReadableSize()} // VirtualMachineMemoryUsage: {VirtualMachineMemoryUsage.ToReadableSize()} // OtherMemoryUsage: {OtherMemoryUsage.ToReadableSize()}";

        protected Debug.MemoryInfo[] GetProcessMemory(int[] pids)
        {
            return ActivityManager.GetProcessMemoryInfo(pids);
        }
    }
}