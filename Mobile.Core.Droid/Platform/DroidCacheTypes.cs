﻿using System;
using System.Collections.Generic;
using System.Linq;
using Android.Views;
using Mobile.Core.BL.Core.Assemblies;
using Mobile.Core.BL.Core.Exceptions;

namespace Mobile.Core.Droid.Platform
{
    public class DroidCacheTypes : MCacheTypes<View>, IDroidCacheTypes
    {
        private readonly Dictionary<string, Type> operativeDroidTypesCache = new Dictionary<string, Type>();

        protected readonly string typeSeparator = DroidConstants.TypeSeparator;

        public Type Resolve(string name)
        {
            if (string.IsNullOrWhiteSpace(name) || name.Contains(typeSeparator))
            {
                return null;
            }

            if (operativeDroidTypesCache.ContainsKey(name))
            {
                return operativeDroidTypesCache[name];
            }

            if (InvariantFullNameTypeCache.Count > 0)
            {
                foreach (
                    var fullTypeName in namespaces.Select(ns => $"{ns}{typeSeparator}{name}"))
                {
                    Type type;
                    if (InvariantFullNameTypeCache.TryGetValue(fullTypeName.ToLowerInvariant(), out type))
                    {
                        if (!operativeDroidTypesCache.ContainsKey(fullTypeName))
                        {
                            operativeDroidTypesCache[name] = type;
                        }

                        return type;
                    }
                }
            }

            throw new MException("!!!!!Failed to get the data type for {0}. You need to add control to DroidConstants.ViewTypes", name);
        }
    }
}