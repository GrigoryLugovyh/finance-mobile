﻿using System;
using Android.Content;
using Android.Graphics.Drawables;
using Android.Util;
using Android.Views;
using Android.Widget;
using Mobile.Core.BL.Core;

namespace Mobile.Core.Droid.Extensions
{
    public static class DroidWidgetExtension
    {
        public static View ToDroidView(this object element)
        {
            return element as View;
        }

        public static int InitItemTemplateId(this IAttributeSet attrs, Context context)
        {
            if (attrs == null || context == null)
            {
                return Android.Resource.Layout.SimpleListItem1;
            }

            try
            {
                var typedArray = context.ObtainStyledAttributes(attrs, Resource.Styleable.ListTemplate);
                var layoutName = typedArray.GetString(Resource.Styleable.ListTemplate_itemTemplate);

                if (layoutName == null)
                {
                    return Android.Resource.Layout.SimpleListItem1;
                }

                return context.Resources.GetIdentifier(layoutName.ToLower(), "layout", context.PackageName);
            }
            catch (Exception ex)
            {
                Tracer.Error(ex.Message + ex.StackTrace);
                return Android.Resource.Layout.SimpleListItem1;
            }
        }

        public static void DrawableRecycle(this ImageView imageView)
        {
            ((BitmapDrawable) imageView?.Drawable)?.Bitmap.Recycle();
        }
    }
}