﻿using Mobile.Core.BL.Core.Model;
using Mobile.Core.BL.IoCContainer;
using Mobile.Core.Droid.Services;

namespace Mobile.Core.Droid.Extensions
{
    public static class DroidIntentServiceExtension
    {
        public static ActionResult Schedule<TBroadcastReceiver>(long delayPeriodMillis = 1, int pendingRequestCode = -1)
            where TBroadcastReceiver : DroidProlongBroadcastReceiver
        {
            return Container.Resolve<IMainThreadDispatcher>().InvokeAction(() =>
            {
                Container.Resolve<IDroidServiceScheduler>()
                    .ProlongService<TBroadcastReceiver>(delayPeriodMillis, pendingRequestCode);
            });
        }
    }
}