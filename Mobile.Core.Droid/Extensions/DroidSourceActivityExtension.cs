﻿using Android.App;
using Mobile.Core.BL.Core.ViewModels;
using Mobile.Core.BL.Core.Views;
using Mobile.Core.Droid.Views;

namespace Mobile.Core.Droid.Extensions
{
    public static class DroidSourceActivityExtension
    {
        public static Activity ToActivity(this IMSource source)
        {
            return source as Activity;
        }

        public static Activity ToActivity(this ISourceActivity source)
        {
            return source as Activity;
        }

        public static IMViewModel ToViewModel(this ISourceActivity activity)
        {
            if (activity.EmptyViewModel)
            {
                return null;
            }

            var view = activity as IDroidView;

            return view?.ViewModel;
        }
    }
}