﻿using System;
using Android.App;
using Android.Content;
using Java.Lang;
using Mobile.Core.BL.Core.Exceptions;
using Mobile.Core.BL.IoCContainer;
using Mobile.Core.Droid.Binding;
using Mobile.Core.Droid.Views;
using Exception = System.Exception;

namespace Mobile.Core.Droid.Extensions
{
    public static class DroidActivityExtension
    {
        private static IDroidActivitiesMonitorOwner activitiesMonitorOwner;

        public static IDroidActivitiesMonitorOwner DroidActivitiesMonitorOwner
            => activitiesMonitorOwner ?? (activitiesMonitorOwner = Container.Resolve<IDroidActivitiesMonitorOwner>());

        private static void SafeExecuteDroidActivitiesMonitorOwnerMethods(Action<IDroidActivitiesMonitorOwner> action,
            ISourceActivity activity)
        {
            try
            {
                action(DroidActivitiesMonitorOwner);
            } 
            catch (Exception)
            {
                Restart(activity.GetType());
            }
        }

        public static void AddActivityListener(this ISourceActivity activity)
        {
            SafeExecuteDroidActivitiesMonitorOwnerMethods(owner => owner.AddListener(activity), activity);
        }

        public static void RemoveActivityListener(this ISourceActivity activity)
        {
            SafeExecuteDroidActivitiesMonitorOwnerMethods(owner => owner.RemoveListener(activity), activity);
        }

        public static string UniqueId(this ISourceActivity activity)
        {
            if (activity == null)
            {
                throw new ArgumentNullException(nameof(activity));
            }

            return $"{activity}_{activity.GetHashCode()}";
        }

        public static IDroidBindingContext BindingContext(this IDroidView view)
        {
            if (!(view?.BindingContext is IDroidBindingContext))
            {
                throw new MException("DroidListAdapter: currentInflator");
            }

            return (IDroidBindingContext) view.BindingContext;
        }

        public static void Restart(this Type startActivityType)
        {
            if (startActivityType == null) return;

            var intent = PendingIntent.GetActivity(Application.Context, 0,
                new Intent(Application.Context, startActivityType), PendingIntentFlags.OneShot);

            if (intent == null) return;

            AlarmManager alarmManager = (AlarmManager) Application.Context.GetSystemService(Context.AlarmService);
            alarmManager.Set(AlarmType.Rtc, JavaSystem.CurrentTimeMillis() + 500, intent);
            JavaSystem.Exit(2);
        }
    }
}