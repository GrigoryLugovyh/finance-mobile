﻿using Android.Views;
using Mobile.Core.BL.Core.Binding;
using Mobile.Core.BL.Core.Exceptions;
using Mobile.Core.Droid.Binding;

namespace Mobile.Core.Droid.Extensions
{
    public static class DroidBindingExtension
    {
        public static View BindingInflate(this IMBindingContextOwner owner, int layoutResId)
        {
            var context = owner.BindingContext as IDroidBindingContext;

            if (context == null)
                throw new MException("Сontext must implement IDroidBindingContext");

            return context.BindingInflate(layoutResId);
        }
    }
}