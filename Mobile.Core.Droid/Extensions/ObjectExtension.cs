﻿namespace Mobile.Core.Droid.Extensions
{
    internal class JavaWrapper : Java.Lang.Object
    {
        public object Instance { get; set; }

        public JavaWrapper(object instance)
        {
            Instance = instance;
        }
    }

    public static class ObjectExtension
    {
        public static Java.Lang.Object ToJava<T>(this T value)
        {
            return new JavaWrapper(value);
        }

        public static T FromJava<T>(this Java.Lang.Object obj) where T : class
        {
            var propertyInfo = obj.GetType().GetProperty("Instance");
            return propertyInfo == null ? null : propertyInfo.GetValue(obj, null) as T;
        }
    }
}