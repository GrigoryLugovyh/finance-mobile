﻿using Android.Content;
using Mobile.Core.BL.Core;

namespace Mobile.Core.Droid.Extensions
{
    public static class DroidContextExtension
    {
        public static Context ToDroidContext(this object data)
        {
            var context = data as Context;

            if (context == null)
            {
                Tracer.Warning("DroidContextExtension: Input not containst Android context");
            }

            return context;
        }
    }
}