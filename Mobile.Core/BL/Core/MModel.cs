﻿using System;
using System.Reflection;
using Mobile.Core.BL.Core.Binding;
using Mobile.Core.BL.Core.Binding.Converter;
using Mobile.Core.BL.Core.Binding.Decorator;
using Mobile.Core.BL.Core.Binding.Selector;
using Mobile.Core.BL.Core.Binding.Selector.Extended.Tree;
using Mobile.Core.BL.Core.Binding.Source;
using Mobile.Core.BL.Core.Binding.Target;
using Mobile.Core.BL.Core.Data;
using Mobile.Core.BL.Core.Enums;
using Mobile.Core.BL.Core.Events;
using Mobile.Core.BL.Core.Extensions;
using Mobile.Core.BL.Core.Infrastructure;
using Mobile.Core.BL.Core.Injections;
using Mobile.Core.BL.Core.Model;
using Mobile.Core.BL.Core.Platform.Apps;
using Mobile.Core.BL.Core.Platform.Calendar;
using Mobile.Core.BL.Core.Platform.Clipboard;
using Mobile.Core.BL.Core.Platform.Data;
using Mobile.Core.BL.Core.Platform.Display;
using Mobile.Core.BL.Core.Platform.FileSystem;
using Mobile.Core.BL.Core.Platform.Keyboard;
using Mobile.Core.BL.Core.Platform.Locale;
using Mobile.Core.BL.Core.Platform.Location;
using Mobile.Core.BL.Core.Platform.Media;
using Mobile.Core.BL.Core.Platform.Network;
using Mobile.Core.BL.Core.Platform.Shared;
using Mobile.Core.BL.Core.Platform.Specific;
using Mobile.Core.BL.Core.Platform.Telephony;
using Mobile.Core.BL.Core.Platform.Trace;
using Mobile.Core.BL.Core.Platform.Watch;
using Mobile.Core.BL.Core.Services;
using Mobile.Core.BL.Core.Settings;
using Mobile.Core.BL.Core.ViewModels;
using Mobile.Core.BL.Core.Views;
using Mobile.Core.BL.IoCContainer;
using Mobile.Core.DAL;
namespace Mobile.Core.BL.Core
{
    public abstract class MModel
    {
        private MInitState state;

        public MInitState State
        {
            get => state;
            protected set
            {
                state = value;
                StateChanged.Rise(this, value);
            }
        }

        public event EventHandler<MValueEventArgs<MInitState>> StateChanged;
        protected abstract IMTrace CreateTrace();
        protected abstract IMApplication CreateApplication();
        protected abstract IMViewsContainer CreateViewsContainer();
        protected abstract IMViewDispatcher CreateViewDispatcher();
        protected abstract IPlatformSpecific CreatePlatformSpecific();
        protected abstract IMKeyboardHandler CreateKeyboardHandler();
        protected abstract IMCalendarHandler CreateCalendarHandler();
        protected abstract IMTelephonyHandler CreateTelephonyHandler();
        protected abstract IMCameraHandler CreateCameraHandler();
        protected abstract IMMemoryHandler CreateMemoryHandler();
        protected abstract IMLocationHandler CreateLocationHandler();
        protected abstract IMViewDimensions CreateVieweDimensions();
        protected abstract IMLocaleHandler CreateLocaleHandler();
        protected abstract IMDataBulkHandler CreateDataBulkHandler();
        protected abstract IMAppsHandler CreateAppsHandler();
        protected abstract IMNetworkHandler CreateNetworkHandler();
        protected abstract IMFileSystemHandler CreateFileSystemHandler();
        protected abstract IMClipboardHandler CreateClipboardHandler();
        protected abstract IMScreenHandler CreateScreenHandler();
        protected abstract IMKeyCodeHandler CreateCharHandler();
        protected abstract IMViewViewModelMapping CreateViewViewModelMapping();

        protected virtual IMSettings CreateSettings()
        {
            return new MSettings();
        }

        protected virtual IMDateTimeService CreateDateTimeService()
        {
            return new MDateTimeService();
        }

        protected virtual IMStorageService CreateStorageService()
        {
            return new MStorageService();
        }

        protected virtual IMJsonService CreateJsonService()
        {
            return new MJsonService();
        }

        protected virtual IMErrorMessagesService CreateErrorMessagesService()
        {
            return new MErrorMessagesService();
        }

        protected virtual IMainThreadDispatcher CreateMainThreadDispatcher()
        {
            return new MainThreadDispatcher();
        }

        protected virtual IMSynchronizationContext CreateSynchronizationContex()
        {
            return new MSynchronizationContext();
        }

        protected virtual IMViewModelsCache CreateViewModelsCache()
        {
            return new MViewModelsCache();
        }

        protected virtual IMViewModelStack CreateViewModelStack()
        {
            return new MViewModelStack();
        }

        protected virtual IMViewModelParametersStackOwner CreateViewModelParametersStackOwner()
        {
            return new MViewModelParametersStackOwner();
        }

        protected virtual IMBinder CreateBinder()
        {
            return new MBinder();
        }

        protected virtual IMTargetBindingFactoryDispatcher CreateTargetBindingFactoryDispatcher()
        {
            return new MTargetBindingFactoryDispatcher();
        }

        protected virtual IMSourceBindingFactoryDispatcher CreateSourceBindingFactoryDispatcher()
        {
            return new MSourceBindingFactoryDispatcher();
        }

        protected virtual IMMethodSourceBindingStack CreateMethodSourceBindingStack()
        {
            return new MMethodSourceBindingStack();
        }

        protected virtual IMBindingSyntaxConstants CreateBindingSyntaxConstants()
        {
            return new MBindingSyntaxConstants();
        }

        protected virtual IMTypeMemberDetector CreateTypeMemberDetector()
        {
            return new MTypeMemberDetector();
        }

        protected virtual IMBindingContainer CreateBindingContainer()
        {
            return new MBindingContainer();
        }

        protected virtual IMNotifyPropertyChangedSubscriptionFactory CreateNotifyPropertyChangedSubscriptionFactory()
        {
            return new MNotifyPropertyChangedSubscriptionFactory();
        }

        protected virtual IMDecoratorsOwner CreateDecoratorProjections()
        {
            return new MDecoratorsOwner();
        }

        protected virtual IMConverterFactoryDispatcher CreateConverterFactoryDispatcher()
        {
            return new MConverterFactoryDispatcher();
        }

        protected virtual IMBindingSelector CreateBindingSelector()
        {
            return new MBindingExtendedSelector();
        }

        protected virtual IMExtendedSelectorsOwner CreateExtendedSelectorsOwner()
        {
            return new MExtendedSelectorsOwner();
        }

        protected virtual IMSelectorTreeBuilder CreateSelectorTreeBuilder()
        {
            return new MSelectorTreeBuilder();
        }

        protected virtual IMSharedPreferences CreateSharedPreferences()
        {
            return new MSharedPreferences();
        }

        protected virtual IMInjectionDispatcher CreateInjectionDispatcher()
        {
            return new MInjectionDispatcher();
        }

        protected virtual IMDataMapperDispatcher CreateDataMapperDispatcher()
        {
            return new MDataMapperDispatcher();
        }

        protected virtual IStorageSessionDispatcher CreateStorageSessionDispatcher()
        {
            return new StorageSessionDispatcher();
        }

        protected virtual void InitializePlatformEnvironments()
        {
            // init for each platform
        }

        protected virtual void InitializePreModelStartInstance()
        {
            // init before model start
        }

        protected virtual void InitializePostModelStartInstance()
        {
            // init after model started
        }

        public virtual void Initialize()
        {
            BaseInitialize();
            CommonInitialize();
        }

        public virtual void BaseInitialize()
        {
            if (State != MInitState.Uninitialized)
                return;

            State = MInitState.Initializing;

            Container.Init();

            InitializeTrace();

            InitializeWatcher();

            Tracer.Diagnostic("Base started at {0} ms", Watcher.Spot(() =>
            {
                Tracer.Diagnostic("Base starting ...");

                Tracer.Diagnostic("Settings start");

                InitializeSettings();

                Tracer.Diagnostic("DAL start");

                InitializeDataAccessLayer();

                Tracer.Diagnostic("Platform start");

                InitializePlatformEnvironments();

                State = MInitState.BaseInitialized;
            }).TotalMilliseconds);
        }

        public virtual void CommonInitialize()
        {
            if (State != MInitState.BaseInitialized || State == MInitState.Initialized)
                return;

            Tracer.Diagnostic("Common started at {0} ms", Watcher.Spot(() =>
            {
                Tracer.Diagnostic("Common starting ...");

                Tracer.Diagnostic("Pre start");

                InitializePreModelStartInstance();

                Tracer.Diagnostic("MainThreadDispatcher start");

                InitializeMainThreadDispatcher();

                Tracer.Diagnostic("SynchronizationContext start");

                InitializeSynchronizationContext();

                Tracer.Diagnostic("PlatformSpecific start");

                InitializePlatformSpecific();

                Tracer.Diagnostic("Platform start");

                InitializePlatform();

                Tracer.Diagnostic("Services start");

                InitializeServices();

                Tracer.Diagnostic("Container start");

                InitializeViewsContainer();

                Tracer.Diagnostic("ViewDispatcher start");

                InitializeViewDispatcher();

                Tracer.Diagnostic("Application init");

                InitializeApplication();

                Tracer.Diagnostic("Preferences start");

                InitializeSharedPreferences();

                Tracer.Diagnostic("ViewModels start");

                InitializeViewModels();

                Tracer.Diagnostic("Views start");

                InitializeViews();

                Tracer.Diagnostic("Binding start");

                InitializeBinding();

                Tracer.Diagnostic("Injection start");

                InitializeInjectionDispatcher();

                Tracer.Diagnostic("DataMapper start");

                InitializeDataMapperDispatcher();

                Tracer.Diagnostic("Application start");

                StartApplication();

                Tracer.Diagnostic("Services start");

                StartServices();

                Tracer.Diagnostic("Post start");

                InitializePostModelStartInstance();

                State = MInitState.Initialized;
            }).TotalMilliseconds);
        }

        protected virtual void InitializeTrace()
        {
            var trace = CreateTrace();
            Container.AddRegistration(trace);
            trace.ClearDump();
        }

        protected virtual void InitializeWatcher()
        {
            Container.AddRegistration<IMWatcher, MWatcher>();
        }

        protected virtual void InitializeMainThreadDispatcher()
        {
            var threadDispatcher = CreateMainThreadDispatcher();
            Container.AddRegistration(threadDispatcher);
        }

        protected virtual void InitializeSynchronizationContext()
        {
            var synchronizationContex = CreateSynchronizationContex();
            Container.AddRegistration(synchronizationContex);
        }

        protected virtual void InitializeInjectionDispatcher()
        {
            var injectionDispatcher = CreateInjectionDispatcher();
            Container.AddRegistration(injectionDispatcher);
        }

        protected virtual void InitializeDataMapperDispatcher()
        {
            var dataMapperDispatcher = CreateDataMapperDispatcher();
            Container.AddRegistration(dataMapperDispatcher);
        }

        protected virtual void InitializeApplication()
        {
            var application = CreateApplication();
            Container.AddRegistration(application);
        }

        protected virtual void InitializeStorage()
        {
            Container.Resolve<IMStorageService>().Reinitialize();
        }

        protected virtual void StartApplication()
        {
            var app = Container.Resolve<IMApplication>();
            app.Initialization();

            InitializeStorage();

            var dataMapperDispatcher = Container.Resolve<IMDataMapperDispatcher>();
            app.InitDataMapper(dataMapperDispatcher);

            var injectionDispatcher = Container.Resolve<IMInjectionDispatcher>();
            app.InitInjections(injectionDispatcher);
            injectionDispatcher.Inject();

            var decoratorOwner = Container.Resolve<IMDecoratorsOwner>();
            app.InitDecorator(decoratorOwner);
        }

        protected virtual void StartServices()
        {
        }

        protected virtual void InitializeViewsContainer()
        {
            var container = CreateViewsContainer();
            Container.AddRegistration(container);
        }

        protected virtual void InitializeViewDispatcher()
        {
            var dispatcher = CreateViewDispatcher();
            Container.AddRegistration<IMainThreadDispatcher>(dispatcher);
        }

        protected virtual void InitializeSettings()
        {
            var settings = CreateSettings();
            Container.AddRegistration(settings);
        }

        protected virtual void InitializeDataAccessLayer()
        {
            InitializeUnitOfWork();
            InitializeStorageSession();
            InitializeStorageSessionDispatcher();
        }

        protected virtual void InitializeUnitOfWork()
        {
            Container.AddRegistration<IUnitOfWork, UnitOfWork>().AsMultiInstance();
        }

        protected virtual void InitializeStorageSession()
        {
            Container.AddRegistration<IStorageSession, StorageSession>().AsMultiInstance();
        }

        protected virtual void InitializeStorageSessionDispatcher()
        {
            var sessionDispatcher = CreateStorageSessionDispatcher();
            Container.AddRegistration(sessionDispatcher);
        }

        protected virtual void InitializeServices()
        {
            var storageService = CreateStorageService();
            Container.AddRegistration(storageService);

            var dateTimeService = CreateDateTimeService();
            Container.AddRegistration(dateTimeService);

            var jsonService = CreateJsonService();
            Container.AddRegistration(jsonService);

            var errrorMessagesService = CreateErrorMessagesService();
            Container.AddRegistration(errrorMessagesService);
        }

        protected virtual void InitializePlatform()
        {
            var keyboard = CreateKeyboardHandler();
            Container.AddRegistration(keyboard);

            var calendar = CreateCalendarHandler();
            Container.AddRegistration(calendar);

            var telephony = CreateTelephonyHandler();
            Container.AddRegistration(telephony);

            var camera = CreateCameraHandler();
            Container.AddRegistration(camera);

            var memory = CreateMemoryHandler();
            Container.AddRegistration(memory);

            var location = CreateLocationHandler();
            Container.AddRegistration(location);

            var dimensions = CreateVieweDimensions();
            Container.AddRegistration(dimensions);

            var localeHandler = CreateLocaleHandler();
            Container.AddRegistration(localeHandler);

            var bulkHandler = CreateDataBulkHandler();
            Container.AddRegistration(bulkHandler);

            var appsHandler = CreateAppsHandler();
            Container.AddRegistration(appsHandler);

            var networkHandler = CreateNetworkHandler();
            Container.AddRegistration(networkHandler);

            var fileSystemHandler = CreateFileSystemHandler();
            Container.AddRegistration(fileSystemHandler);

            var clipboardHandler = CreateClipboardHandler();
            Container.AddRegistration(clipboardHandler);

            var screenHandler = CreateScreenHandler();
            Container.AddRegistration(screenHandler);

            var charHandler = CreateCharHandler();
            Container.AddRegistration(charHandler);
        }

        protected virtual void InitializePlatformSpecific()
        {
            var platformSpecific = CreatePlatformSpecific();
            Container.AddRegistration(platformSpecific);
        }

        protected virtual void InitializeViewModels()
        {
            var viewModelsBuilder = new MViewModelsBuilder();

            var viewModelsAssemblies = GetViewModelAssemblies();

            foreach (var assembly in viewModelsAssemblies)
                viewModelsBuilder.Set(assembly);

            Container.AddRegistration<IMViewModelsSetter>(viewModelsBuilder);
            Container.AddRegistration<IMViewModelsGetter>(viewModelsBuilder);

            var viewModelsMapping = CreateViewViewModelMapping();
            var detector = new MViewModelTypeDetector(viewModelsBuilder, viewModelsMapping);

            Container.AddRegistration<IMViewModelTypeDetector>(detector);
            Container.AddRegistration<IMTypeDetector>(detector);

            var viewModelsCache = CreateViewModelsCache();
            Container.AddRegistration(viewModelsCache);

            var viewModelStack = CreateViewModelStack();
            Container.AddRegistration(viewModelStack);

            var viewModelParametersStackOwner = CreateViewModelParametersStackOwner();
            Container.AddRegistration(viewModelParametersStackOwner);

            var charHandler = CreateCharHandler();
            Container.AddRegistration(charHandler);
        }

        protected virtual void InitializeViews()
        {
            var assemblies = GetViewAssemblies();
            var builder = new MViewsBuilder();
            var viewModelViews = builder.Build(assemblies);

            if (viewModelViews == null)
                return;

            var container = Container.Resolve<IMViewsContainer>();
            container.Add(viewModelViews);
        }

        protected virtual void InitializeTypeMemberDetector()
        {
            var detector = CreateTypeMemberDetector();
            Container.AddRegistration(detector);
        }

        protected virtual void InitializeBindingContainer()
        {
            var container = CreateBindingContainer();
            Container.AddRegistration(container);
        }

        protected virtual void InitializeNotifyPropertyChangedSubscriptionFactory()
        {
            var factory = CreateNotifyPropertyChangedSubscriptionFactory();
            Container.AddRegistration(factory);
        }

        protected virtual void InitializeDecorator()
        {
            var decoratorProjections = CreateDecoratorProjections();
            Container.AddRegistration(decoratorProjections);
        }

        protected virtual void InitializeConverter()
        {
            var converterFactoryDispatcher = CreateConverterFactoryDispatcher();
            Container.AddRegistration(converterFactoryDispatcher);

            FillConverterFactoryDispatcher(converterFactoryDispatcher);
        }

        protected virtual void FillConverterFactoryDispatcher(IMConverterFactoryDispatcher dispatcher)
        {
        }

        protected virtual void InitializeBinding()
        {
            InitializeTypeMemberDetector();
            InitializeBindingContainer();
            InitializeBindingTextParser();
            InitializeBinder();
            InitializeTargetBindingFactoryDispatcher();
            InitializeSourceBindingFactoryDispatcher();
            InitializeNotifyPropertyChangedSubscriptionFactory();
            InitializeDecorator();
            InitializeConverter();
        }

        protected virtual void InitializeBindingTextParser()
        {
            var selector = CreateBindingSelector();
            Container.AddRegistration(selector);

            var owner = CreateExtendedSelectorsOwner();
            Container.AddRegistration(owner);

            var builder = CreateSelectorTreeBuilder();
            Container.AddRegistration(builder);
        }

        protected virtual void InitializeBinder()
        {
            var binder = CreateBinder();
            Container.AddRegistration(binder);
        }

        protected virtual void InitializeTargetBindingFactoryDispatcher()
        {
            var dispatcher = CreateTargetBindingFactoryDispatcher();
            Container.AddRegistration(dispatcher);

            FillTargetBindingFactoryDispatcher(dispatcher);
        }

        protected virtual void FillTargetBindingFactoryDispatcher(IMTargetBindingFactoryDispatcher dispatcher)
        {
        }

        protected virtual void InitializeSourceBindingFactoryDispatcher()
        {
            var dispatcher = CreateSourceBindingFactoryDispatcher();
            Container.AddRegistration(dispatcher);

            var constants = CreateBindingSyntaxConstants();
            Container.AddRegistration(constants);

            var methodSourceStack = CreateMethodSourceBindingStack();
            Container.AddRegistration(methodSourceStack);

            FillSourceBindingFactoryDispatcher(dispatcher, constants);
        }

        protected virtual void FillSourceBindingFactoryDispatcher(IMSourceBindingFactoryDispatcher dispatcher,
            IMBindingSyntaxConstants constants)
        {
            dispatcher.Add(constants.Property, (s, p) => new MPropertySourceBinding(s, p as PropertyInfo));
            dispatcher.Add(constants.Method, (s, m) => new MMethodSourceBinding(s, m as MethodInfo));
            dispatcher.Add(constants.Field, (s, f) => new MFieldSourceBinding(s, f as FieldInfo));
        }

        protected virtual void InitializeSharedPreferences()
        {
            var preferences = CreateSharedPreferences();
            Container.AddRegistration(preferences);
        }

        protected virtual Assembly[] GetViewAssemblies()
        {
            var assembly = GetType().GetTypeInfo().Assembly;

            return new[]
            {
                assembly
            };
        }

        protected virtual Assembly[] GetViewModelAssemblies()
        {
            var app = Container.Resolve<IMApplication>();
            var assembly = app.GetType().GetTypeInfo().Assembly;

            return new[]
            {
                assembly
            };
        }
    }
}