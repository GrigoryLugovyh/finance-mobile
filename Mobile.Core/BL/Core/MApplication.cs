﻿using Mobile.Core.BL.Core.Binding.Decorator;
using Mobile.Core.BL.Core.Data;
using Mobile.Core.BL.Core.Injections;
using Mobile.Core.BL.Core.Model;
using Mobile.Core.BL.Core.ViewModels;
using Mobile.Core.BL.IoCContainer;

namespace Mobile.Core.BL.Core
{
    public class MApplication : IMApplication
    {
        public virtual string ApplicationName => "LgaApplications";

        public virtual void Initialization()
        {
        }

        public virtual void InitInjections(IMInjectionDispatcher dispatcher)
        {
        }

        public virtual void InitDataMapper(IMDataMapperDispatcher dispatcher)
        {
        }

        public virtual void InitDecorator(IMDecoratorsOwner decoratorsOwner)
        {
        }

        protected void RegisterViewModel<TViewModel>() where TViewModel : IMViewModel
        {
            Container.AddRegistration<IMViewStart>(new MViewStart<TViewModel>());
        }
    }
}