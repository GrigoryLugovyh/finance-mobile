﻿using System;
using Mobile.Core.BL.Core.Enums;

namespace Mobile.Core.BL.Core.Exceptions
{
    public class MExceptionRequest
    {
        public MExceptionRequest(Exception exception, MExceptionHandlingAppearance appearance)
        {
            Exception = exception;
            Appearance = appearance;
        }

        public Exception Exception { get; private set; }

        public MExceptionHandlingAppearance Appearance { get; private set; }
    }
}