﻿using System;

namespace Mobile.Core.BL.Core.Exceptions
{
    public class MException : Exception
    {
        public MException()
        {
        }

        public MException(string message)
            : base(message)
        {
        }

        public MException(string messageFormat, params object[] messageFormatArguments)
            : base(string.Format(messageFormat, messageFormatArguments))
        {
        }

        public MException(Exception innerException, string messageFormat, params object[] formatArguments)
            : base(string.Format(messageFormat, formatArguments), innerException)
        {
        }

        public int Code { get; set; }

        public int StatusCode { get; set; }

        public int LoggingLevel { get; set; } = 0xF;
    }
}