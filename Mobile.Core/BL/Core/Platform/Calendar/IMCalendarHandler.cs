﻿using System;

namespace Mobile.Core.BL.Core.Platform.Calendar
{
    public interface IMCalendarHandler
    {
        void Set(DateTimeOffset dateTime);
    }
}