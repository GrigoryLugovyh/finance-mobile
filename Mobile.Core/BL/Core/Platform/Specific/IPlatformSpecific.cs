﻿namespace Mobile.Core.BL.Core.Platform.Specific
{
    public interface IPlatformSpecific
    {
        string ApplicationDataPath { get; }
        string SqlitePath { get; }
        string Version { get; }
        int VersionCode { get; }
    }
}