﻿using System.Collections.Generic;

namespace Mobile.Core.BL.Core.Platform.Apps
{
    public interface IMAppsHandler
    {
        void Run(string packageName);
        void Run(MApp app);
        void Install(string fullPath);
        string GetAppName(MApp app);
        MAppInfo GetAppInfo(MApp app);
        List<MAppInfo> GetAppsInfo(List<string> packageNames);
        void OpenFile(string filePath, string contentType);
    }
}