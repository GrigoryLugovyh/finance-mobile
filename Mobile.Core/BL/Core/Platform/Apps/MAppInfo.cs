﻿namespace Mobile.Core.BL.Core.Platform.Apps
{
    public class MAppInfo
    {
        public string Label { get; set; }
        public string PackageName { get; set; }
        public string Dir { get; set; }
        public object Icon { get; set; }

    }
}