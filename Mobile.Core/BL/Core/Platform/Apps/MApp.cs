﻿namespace Mobile.Core.BL.Core.Platform.Apps
{
    public enum MApp
    {
        Uncknown = 0,
        Location = 1,
        WifiManager = 2,
        Settings = 3,
        Mail = 4,
        DateSettings = 5
    }
}