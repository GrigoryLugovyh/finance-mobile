﻿using System.Collections.Generic;
using System.IO;

namespace Mobile.Core.BL.Core.Platform.FileSystem
{
    public interface IMFileSystemHandler
    {
        string ApplicationDirectory { get; }

        long InternalTotalMemory { get; }

        long InternalFreeMemory { get; }

        long InternalBusyMemory { get; }

        long ExternalTotalMemory { get; }

        long ExternalFreeMemory { get; }

        long ExternalBusyMemory { get; }

        long DataTotalMemory { get; }

        long DataFreeMemory { get; }

        long DataBusyMemory { get; }

        FileInfo GetFileInfo(string filePath);

        string FolderCreateIfNotExist(string directory);

        long FolderSize(string folder, string searchPattern = "*",
            SearchOption searchOption = SearchOption.TopDirectoryOnly, List<string> expectedFileNames = null);

        int FolderFileCount(string folder, string searchPattern = "*",
            SearchOption searchOption = SearchOption.TopDirectoryOnly, List<string> expectedFileNames = null);
    }
}