﻿namespace Mobile.Core.BL.Core.Platform.Clipboard
{
    public interface IMClipboardHandler
    {
        bool CopyTo(string label, string text);
    }
}