﻿using Mobile.Core.BL.Core.Model;
using Mobile.Core.BL.Core.Views;

namespace Mobile.Core.BL.Core.Platform
{
    public class MHandler : MainThreadObjectDispatcher
    {
        protected IMViewDispatcher ViewDispatcher
        {
            get { return Dispatcher as IMViewDispatcher; }
        }
    }
}