﻿using System;
using Mobile.Core.BL.Core.Events;

namespace Mobile.Core.BL.Core.Platform.Location
{
    public interface IMLocationListener
    {
        MLocation LastLocation { get; }

        event EventHandler<MValueEventArgs<MLocation>> LocationChanged;
    }
}