﻿namespace Mobile.Core.BL.Core.Platform.Location
{
    public class MLocation
    {
        public MLocation(float accuracy, double latitude, double longitude)
        {
            Accuracy = accuracy;
            Latitude = latitude;
            Longitude = longitude;
        }

        public float Accuracy { get; private set; }

        public double Latitude { get; private set; }

        public double Longitude { get; private set; }
    }
}