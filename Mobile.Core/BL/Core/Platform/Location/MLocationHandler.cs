﻿using System;
using System.Diagnostics.Contracts;
using Mobile.Core.BL.Core.Enums;

namespace Mobile.Core.BL.Core.Platform.Location
{
    public abstract class MLocationHandler : MHandler, IMLocationHandler
    {
        public abstract MLocationProviderType ProviderType { get; }
        public abstract bool LocationEnable();
        public abstract MСoordinates Location();

        public double DistanceTo(MСoordinates end, int round = 1)
        {
            MСoordinates currentСoordinates = Location();

            if (currentСoordinates.Equals(MСoordinates.Default))
            {
                return 0d;
            }

            return Distance(currentСoordinates, end, round);
        }

        public double Distance(MСoordinates start, MСoordinates end, int round = 1)
        {
            Contract.Assert(start != null);
            Contract.Assert(end != null);

            double theta = start.Longitude - end.Longitude;

            double dist = Math.Sin(DegreeToRadian(start.Latitude))*Math.Sin(DegreeToRadian(end.Latitude)) +
                          Math.Cos(DegreeToRadian(start.Latitude))*Math.Cos(DegreeToRadian(end.Latitude))*
                          Math.Cos(DegreeToRadian(theta));

            dist = Math.Acos(dist);
            dist = RadianToDegree(dist);
            dist = dist*60*1.1515*1.609344;

            return Math.Round(dist, round);
        }

        private double DegreeToRadian(double degree)
        {
            return (degree*Math.PI/180.0);
        }

        private double RadianToDegree(double radian)
        {
            return (radian*180.0/Math.PI);
        }
    }
}