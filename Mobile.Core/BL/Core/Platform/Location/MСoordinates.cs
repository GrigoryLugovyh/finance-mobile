﻿using System;
using System.Globalization;

namespace Mobile.Core.BL.Core.Platform.Location
{
    public class MСoordinates
    {
        public MСoordinates(double longitude, double latitude) : this(longitude, latitude, 0)
        {
        }

        public MСoordinates(double longitude, double latitude, float accuracy)
        {
            Longitude = longitude;
            Latitude = latitude;
            Accuracy = accuracy;
        }

        public double Longitude { get; private set; }

        public double Latitude { get; private set; }

        public float Accuracy { get; private set; }

        public static MСoordinates Default => new MСoordinates(0d, 0d, 0);

        public override int GetHashCode()
        {
            return (Longitude + Latitude).GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return (obj is MСoordinates) && ((MСoordinates)obj).Latitude.Equals(Latitude) &&
                   ((MСoordinates)obj).Longitude.Equals(Longitude);
        }

        public bool IsDefault()
        {
            return Math.Abs(Latitude) <= 0 && Math.Abs(Longitude) <= 0;
        }

        public override string ToString()
        {
            Func<string, string> fixSeparator = s => s.Replace(',', '.');

            return
                $"{fixSeparator(Longitude.ToString(CultureInfo.InvariantCulture))}, {fixSeparator(Latitude.ToString(CultureInfo.InvariantCulture))} ({fixSeparator(((int)Accuracy).ToString(CultureInfo.InvariantCulture))} м)";
        }
    }
}