﻿using Mobile.Core.BL.Core.Enums;

namespace Mobile.Core.BL.Core.Platform.Location
{
    public interface IMLocationHandler
    {
        MLocationProviderType ProviderType { get; } 

        bool LocationEnable();

        MСoordinates Location();

        double DistanceTo(MСoordinates end, int round = 1);

        double Distance(MСoordinates start, MСoordinates end, int round = 1);
    }
}