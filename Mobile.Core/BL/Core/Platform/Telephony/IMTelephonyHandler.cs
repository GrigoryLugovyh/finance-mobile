﻿namespace Mobile.Core.BL.Core.Platform.Telephony
{
    public interface IMTelephonyHandler
    {
        string DeviceBrand { get; }

        string DeviceModel { get; }

        string DeviceId { get; }

        string SimSerialNumber { get; }

        string PhoneNumber { get; }

        string OperatingSystemVersion { get; }

        string OperatingSystemId { get; }

        string OperatingSystemIdNumeric { get; }

        bool AirplaneMode { get; }

        bool IsLaungher { get; }

        float BatteryLevel { get; }

        bool RunOnEmulator { get; }
    }
}