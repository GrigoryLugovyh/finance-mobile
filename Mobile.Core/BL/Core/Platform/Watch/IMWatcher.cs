﻿using System;

namespace Mobile.Core.BL.Core.Platform.Watch
{
    public interface IMWatcher
    {
        TimeSpan CurrentTime { get; }

        void Start();

        void Stop();

        TimeSpan Spot(Action action);
    }
}