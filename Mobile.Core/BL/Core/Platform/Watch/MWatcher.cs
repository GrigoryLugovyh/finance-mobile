﻿using System;
using System.Diagnostics;

namespace Mobile.Core.BL.Core.Platform.Watch
{
    public class MWatcher : IMWatcher
    {
        protected readonly Stopwatch stopwatch;

        public MWatcher(Stopwatch stopwatch)
        {
            this.stopwatch = stopwatch;
        }

        public TimeSpan CurrentTime => TimeSpan.FromMilliseconds(stopwatch.ElapsedMilliseconds);

        public void Start()
        {
            stopwatch.Restart();
        }

        public void Stop()
        {
            stopwatch.Stop();
        }

        public virtual TimeSpan Spot(Action action)
        {
            if (action == null)
            {
                return TimeSpan.MinValue;
            }

            try
            {
                Start();
                action.Invoke();
            }
            catch (Exception ex)
            {
                Tracer.Error($"{ex.Message}{Environment.NewLine}{ex.StackTrace}");
                throw;
            }
            finally
            {
                Stop();
            }

            return CurrentTime;
        }
    }
}