﻿namespace Mobile.Core.BL.Core.Platform.Keyboard
{
    public interface IMKeyCodeHandler
    {
        char GetChar(MKeyCode key);
    }
}