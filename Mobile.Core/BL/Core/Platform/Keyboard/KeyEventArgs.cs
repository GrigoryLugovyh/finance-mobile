﻿using System.Collections.Generic;

namespace Mobile.Core.BL.Core.Platform.Keyboard
{
    public class KeyEventArgs
    {
        public KeyEventArgs(List<MKeyCode> keys)
        {
            Keys = keys;
        }

        public KeyEventArgs(MKeyCode key)
        {
            Keys = new List<MKeyCode> {key};
        }

        public bool Handle { get; set; }

        public MKeyCode Keycode => Keys.Count > 0 ? Keys[0] : MKeyCode.None;
        public List<MKeyCode> Keys { get; set; }
    }
}