﻿namespace Mobile.Core.BL.Core.Platform.Keyboard
{
    public interface IMKeyboardHandler
    {
        void HideKeyboard();
        void ShowKeyboard();
    }
}