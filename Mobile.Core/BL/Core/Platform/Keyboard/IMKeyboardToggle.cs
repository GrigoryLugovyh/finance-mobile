﻿using System;
using Mobile.Core.BL.Core.Events;

namespace Mobile.Core.BL.Core.Platform.Keyboard
{
    public interface IMKeyboardToggle
    {
        event EventHandler<MValueEventArgs<bool>> KeyboardToggle; 
    }
}