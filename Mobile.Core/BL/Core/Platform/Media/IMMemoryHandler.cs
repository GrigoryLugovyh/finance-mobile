﻿namespace Mobile.Core.BL.Core.Platform.Media
{
    public interface IMMemoryHandler
    {
        long ProcessId { get; }

        long TotalMemoryUsage { get; }

        long NativeMemoryUsage { get; }

        long VirtualMachineMemoryUsage { get; }

        long OtherMemoryUsage { get; }

        string Description { get; }
    }
}