﻿using Mobile.Core.BL.Core.Enums;

namespace Mobile.Core.BL.Core.Platform.Media
{
    public class GestureEventArgs
    {
        public GestureEventArgs(MGesture gesture)
        {
            Gesture = gesture;
        }

        public MGesture Gesture { get; private set; }
    }
}