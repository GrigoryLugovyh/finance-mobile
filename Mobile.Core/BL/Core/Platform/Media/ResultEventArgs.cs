﻿using Mobile.Core.BL.Core.Enums;

namespace Mobile.Core.BL.Core.Platform.Media
{
    public class ResultEventArgs
    {
        public ResultEventArgs(int request, MResult result, params object[] args)
        {
            RequestCode = request;
            ResultCode = result;
            Arguments = args;
        }

        public int RequestCode { get; private set; }

        public MResult ResultCode { get; private set; }

        public object[] Arguments { get; private set; }
    }
}