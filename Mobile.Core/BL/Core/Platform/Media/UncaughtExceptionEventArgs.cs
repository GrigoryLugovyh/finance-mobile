﻿using System;

namespace Mobile.Core.BL.Core.Platform.Media
{
    public class UncaughtExceptionEventArgs
    {
        public UncaughtExceptionEventArgs(string message, string stack, Exception exception = null)
        {
            Message = message;
            StackTrace = stack;
            Exception = exception;
        }

        public string Message { get; private set; }

        public string StackTrace { get; private set; }

        public Exception Exception { get; private set; }
    }
}