﻿namespace Mobile.Core.BL.Core.Platform.Media
{
    public interface IMScreenHandler
    {
        byte[] TakeScreenshot(int quality = 100);
    }
}