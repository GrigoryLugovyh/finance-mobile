﻿namespace Mobile.Core.BL.Core.Platform.Media
{
    public interface IMCameraHandler
    {
        bool TakePhoto(string directory, string fileName, bool createPreviewImage = false);

        byte[] CompressPhoto(byte[] buffer, long maxSize, string filename=null);

    }
}