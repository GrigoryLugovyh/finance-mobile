﻿using System.Globalization;

namespace Mobile.Core.BL.Core.Platform.Locale
{
    public interface IMLocaleHandler
    {
        CultureInfo Culture { get; }
    }
}