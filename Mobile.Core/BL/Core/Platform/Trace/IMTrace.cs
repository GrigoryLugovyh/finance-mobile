﻿using System;
using Mobile.Core.BL.Core.Events;

namespace Mobile.Core.BL.Core.Platform.Trace
{
    public interface IMTrace
    {
        event EventHandler<MValueEventArgs<string>> TraceCall;

        void ClearDump();

        void Trace(MTraceLevel level, string tag, Func<string> message);

        void Trace(MTraceLevel level, string tag, string message);

        void Trace(MTraceLevel level, string tag, string message, params object[] args);
    }
}