﻿namespace Mobile.Core.BL.Core.Platform.Trace
{
    public enum MTraceLevel
    {
        Diagnostic,

        Warning,

        Error
    }
}