﻿namespace Mobile.Core.BL.Core.Platform.Bluetooth
{
    public class BluetoothDevice
    {
        public string Name { get; set; }

        public BluetoothDeviceState State { get; set; }

        public enum BluetoothDeviceState
        {
            Disconnected,
            Connected
        }
    }
}