﻿namespace Mobile.Core.BL.Core.Platform.Bluetooth
{
    public interface IBluetoothDeviceManager
    {
        bool IsEnabled { get; }
        bool IsAvailable { get; }
        bool Enable();
        BluetoothDevice GetBondedDeviceByName(string name);
        BluetoothDevice GetBondedDeviceByPartName(string namePart);
    }
}