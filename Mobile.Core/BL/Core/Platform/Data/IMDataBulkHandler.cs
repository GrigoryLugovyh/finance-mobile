﻿using System;
using System.Collections.Generic;
using Mobile.Model;

namespace Mobile.Core.BL.Core.Platform.Data
{
    public interface IMDataBulkHandler
    {
        void Open();

        void Close();

        void Insert<TEntity>(TEntity entity, Action<TEntity> bindAction = null)
            where TEntity : MEntity;

        void Insert<TEntity>(IEnumerable<TEntity> entities, Action<TEntity> bindAction = null) where TEntity : MEntity;

        void Update<TEntity>(TEntity entity, Action<TEntity> bindAction = null, bool byId = true,
            Dictionary<string, string> parameters = null)
            where TEntity : MEntity;

        void Update<TEntity>(IEnumerable<TEntity> entities, Action<TEntity> bindAction = null, bool byId = true,
            Dictionary<string, string> parameters = null)
            where TEntity : MEntity;

        void Delete<TEntity>(bool byId = true, string[] ids = null, Dictionary<string, string> parameters = null)
            where TEntity : MEntity;

        void Query(Func<string[], bool> readCursorPortion, string query, params string[] selectionArgs);

        void Execute(string query);

        T ExecuteScalar<T>(string query);
    }
}