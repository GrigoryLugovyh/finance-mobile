﻿namespace Mobile.Core.BL.Core.Platform.Usb
{
    public interface IUsbDeviceManager
    {
        bool DeviceIsConnected(int vid, int pid);
    }
}