﻿namespace Mobile.Core.BL.Core.Platform.Display
{
    public interface IMViewDimensions
    {
        float Scale { get; }

        int InDpUnits(int pixels);
    }
}