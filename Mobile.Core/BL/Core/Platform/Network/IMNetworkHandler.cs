﻿namespace Mobile.Core.BL.Core.Platform.Network
{
    public interface IMNetworkHandler
    {
        bool CheckConnection(string url);

        bool WifiEnabled { get; }

        bool WifiConnected { get; }

        string WifiAddress { get; }

        bool MobileConnected { get; }

        string NetworkClass { get; }

        string[] ExternalAddress(string url, params string[] args);
    }
}