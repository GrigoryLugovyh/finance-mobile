﻿using System.Collections.Generic;

namespace Mobile.Core.BL.Core.Platform.Shared
{
    public class MSharedPreferences : IMSharedPreferences
    {
        protected static Dictionary<string, string> preferences = new Dictionary<string, string>();

        public void SetValue(KeyValuePair<string, string> pair)
        {
            preferences[pair.Key] = pair.Value;
        }

        public string GetValue(string key)
        {
            if (!preferences.ContainsKey(key))
            {
                SetValue(new KeyValuePair<string, string>(key, string.Empty));
            }

            return preferences[key];
        }

	    public void Clear()
	    {
		    preferences.Clear();
	    }
    }
}