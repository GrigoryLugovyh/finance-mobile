﻿using System.Collections.Generic;

namespace Mobile.Core.BL.Core.Platform.Shared
{
    public interface IMSharedPreferences
    {
        void SetValue(KeyValuePair<string, string> pair);

        string GetValue(string key);
	    void Clear();
    }
}