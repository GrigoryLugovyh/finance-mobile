﻿using System;

namespace Mobile.Core.BL.Core.Attributes
{
    [AttributeUsage(AttributeTargets.Assembly)]
    public class MEntryPointAttribute : MAttribute
    {

    }
}