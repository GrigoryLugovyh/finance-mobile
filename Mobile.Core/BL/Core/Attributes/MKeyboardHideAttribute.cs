﻿using System;

namespace Mobile.Core.BL.Core.Attributes
{
    [AttributeUsage(AttributeTargets.Method)]
    public class MKeyboardHideAttribute : MAttribute
    {
    }
}