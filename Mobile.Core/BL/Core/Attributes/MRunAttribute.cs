﻿using System;
using Mobile.Core.BL.Core.Enums;

namespace Mobile.Core.BL.Core.Attributes
{
    [AttributeUsage(AttributeTargets.Method)]
    public class MRunAttribute : MAttribute
    {
        public MRunAttribute() : this(MMethodRunType.Sync, string.Empty, string.Empty, MExceptionHandlingAppearance.None)
        {
        }

        public MRunAttribute(string title, string message)
            : this(MMethodRunType.Async, title, message, MExceptionHandlingAppearance.None)
        {
        }

        public MRunAttribute(MExceptionHandlingAppearance appearance)
            : this(MMethodRunType.Sync, string.Empty, string.Empty, appearance)
        {
        }

        public MRunAttribute(MMethodRunType runType, string title, string message,
            MExceptionHandlingAppearance appearance)
        {
            RunType = runType;
            Title = title;
            Message = message;
            Appearance = appearance;
        }

        public bool RunInWaitShell => RunType == MMethodRunType.Async;

        public string Title { get; protected set; }

        public string Message { get; protected set; }

        public MMethodRunType RunType { get; protected set; }

        public MExceptionHandlingAppearance Appearance { get; protected set; }
    }
}