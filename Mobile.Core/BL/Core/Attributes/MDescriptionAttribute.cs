﻿using System;

namespace Mobile.Core.BL.Core.Attributes
{
    [AttributeUsage(AttributeTargets.Method)]
    public class MDescriptionAttribute : MAttribute
    {
        public MDescriptionAttribute(string name)
        {
            Name = name;
        }

        public string Name { get; private set; }
    }
}