﻿using System;
using Mobile.Core.BL.Core.Enums;

namespace Mobile.Core.BL.Core.Attributes
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    public class MAttribute : Attribute
    {
        public MAttribute()
        {
            Priority = 1.0;
            Order = MExecuteAttributeOrderEnum.AfterMethod;
        }

        public double Priority { get; set; }

        public MExecuteAttributeOrderEnum Order { get; set; }

        public override string ToString()
        {
            return GetType().Name;
        }
    }
}