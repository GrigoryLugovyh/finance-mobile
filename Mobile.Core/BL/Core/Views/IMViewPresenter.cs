﻿using Mobile.Core.BL.Core.ViewModels;

namespace Mobile.Core.BL.Core.Views
{
    public interface IMViewPresenter
    {
        void Show(MViewModelRequest viewModelRequest);

        void Finish(MViewModelRequest viewModelRequest);

        void Finish();
    }
}