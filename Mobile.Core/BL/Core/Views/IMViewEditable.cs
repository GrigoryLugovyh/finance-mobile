﻿namespace Mobile.Core.BL.Core.Views
{
    public interface IMViewEditable
    {
        string ActionBarTitle { get; set; }
    }
}