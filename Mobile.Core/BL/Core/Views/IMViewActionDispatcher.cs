﻿using System;
using System.Threading.Tasks;
using Mobile.Core.BL.Core.Enums;
using Mobile.Core.BL.Core.Messages;
using Mobile.Core.BL.Core.Model;

namespace Mobile.Core.BL.Core.Views
{
    public interface IMViewActionDispatcher
    {
        ActionResult RequestAction(Action action, MExceptionHandlingAppearance appearance = MExceptionHandlingAppearance.None);

        Task RequestActionAsync(Action action, MExceptionHandlingAppearance appearance = MExceptionHandlingAppearance.None);

// ReSharper disable once InconsistentNaming
        void RequestActionOnUI(Action<object> action);

// ReSharper disable once InconsistentNaming
        Task RequestActionOnUIAsync(Action<object> action);

        void ShowMessageQuick(string message, params object[] args);

        Task ShowMessageQuickAsync(string message, params object[] args);

        void ShowMessage(MDialogRequest request);

        Task ShowMessageAsync(MDialogRequest request);

        Task<ActionResult> RequestActionInWaitShell(Action action, MWaitShellRequest request,
            MExceptionHandlingAppearance appearance = MExceptionHandlingAppearance.None);
    }
}