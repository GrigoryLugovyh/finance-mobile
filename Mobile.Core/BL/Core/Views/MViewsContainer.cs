﻿using System;
using System.Collections.Generic;
using Mobile.Core.BL.Core.Exceptions;
using Mobile.Core.BL.Core.ViewModels;

namespace Mobile.Core.BL.Core.Views
{
    public abstract class MViewViewModelContainer : IMViewsContainer
    {
        private readonly Dictionary<Type, Type> bindingViewModelTypes = new Dictionary<Type, Type>();
        private readonly Dictionary<Type, Type> bindingViewTypes = new Dictionary<Type, Type>();

        public Type GetViewModelType(Type viewType)
        {
            Type binding;
            if (bindingViewModelTypes.TryGetValue(viewType, out binding))
            {
                return binding;
            }

            throw new MException("Not found viewModel for {0}", viewType.Name);
        }

        public Type GetViewType(Type viewModelType)
        {
            Type binding;

            if (bindingViewTypes.TryGetValue(viewModelType, out binding))
            {
                return binding;
            }

            throw new MException("Not found view for {0}", viewModelType.Name);
        }

        public void Add<TViewModel, TView>()
            where TViewModel : IMViewModel
            where TView : IMView
        {
            Add(typeof (TViewModel), typeof (TView));
        }

        public void Add(IDictionary<Type, Type> viewModelViews)
        {
            foreach (var pair in viewModelViews)
            {
                Add(pair.Key, pair.Value);
            }
        }

        public void Add(Type viewModelType, Type viewType)
        {
            bindingViewTypes[viewModelType] = viewType;
            bindingViewModelTypes[viewType] = viewModelType;
        }
    }
}