﻿using Mobile.Core.BL.Core.Model;
using Mobile.Core.BL.Core.ViewModels;

namespace Mobile.Core.BL.Core.Views
{
    public interface IMViewDispatcher : IMainThreadDispatcher, IMViewActionDispatcher, IMViewEnvironment
    {
        ActionResult ShowViewModel(MViewModelRequest viewModelRequest);

        ActionResult FinishViewModel(MViewModelRequest viewModelRequest);

        ActionResult FinishViewModel();
    }
}