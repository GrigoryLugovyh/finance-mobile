﻿using Mobile.Core.BL.Core.Data;
using Mobile.Core.BL.Core.ViewModels;

namespace Mobile.Core.BL.Core.Views
{
    public interface IMView : IMDataConsumer
    {
        IMViewModel ViewModel { get; set; }
    }
}