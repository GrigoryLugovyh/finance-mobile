﻿using System;

namespace Mobile.Core.BL.Core.Views
{
    public interface IMSource
    {
        event EventHandler DisposeCalled;
    }
}