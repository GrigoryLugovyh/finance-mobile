﻿using System;

namespace Mobile.Core.BL.Core.Views
{
    public interface IMThrowableView
    {
        void OnUncaughtExceptionThrow(Exception exception);
    }
}