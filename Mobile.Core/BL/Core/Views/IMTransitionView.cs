﻿using Mobile.Core.BL.Core.Enums;

namespace Mobile.Core.BL.Core.Views
{
    public interface IMTransitionView
    {
        void OnTransition(MTransition transition);
    }
}