﻿using Mobile.Core.BL.Core.Enums;

namespace Mobile.Core.BL.Core.Views
{
    public interface IMGestureView
    {
        void OnGestureResult(MGesture gesture); 
    }
}