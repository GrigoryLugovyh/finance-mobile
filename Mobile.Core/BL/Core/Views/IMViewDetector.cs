﻿using System;

namespace Mobile.Core.BL.Core.Views
{
    public interface IMViewDetector
    {
        Type GetViewType(Type viewModelType);
    }
}