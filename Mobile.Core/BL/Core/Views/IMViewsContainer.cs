﻿using System;
using System.Collections.Generic;
using Mobile.Core.BL.Core.ViewModels;

namespace Mobile.Core.BL.Core.Views
{
    public interface IMViewsContainer : IMViewDetector, IMViewModelsDetector
    {
        void Add<TViewModel, TView>()
            where TViewModel : IMViewModel
            where TView : IMView;

        void Add(IDictionary<Type, Type> viewModelViews);

        void Add(Type viewModelType, Type viewType);
    }
}