﻿using System;
using System.Collections.Generic;

namespace Mobile.Core.BL.Core.Assemblies
{
    public abstract class MCacheTypes<T> : IMCacheTypes<T>, IMCacheTypeInflate
    {
        protected readonly HashSet<string> namespaces = new HashSet<string>();

        protected MCacheTypes()
        {
            InvariantFullNameTypeCache = new Dictionary<string, Type>();
        }

        public void Add(string @namespace)
        {
            if (namespaces.Contains(@namespace))
                return;

            namespaces.Add(@namespace);
        }

        public void Add(Type type)
        {
            var typeFullname = type.FullName.ToLowerInvariant();

            if (!InvariantFullNameTypeCache.ContainsKey(typeFullname))
                InvariantFullNameTypeCache[typeFullname] = type;
        }

        public Type CurrentType => typeof(T);

        public Dictionary<string, Type> InvariantFullNameTypeCache { get; protected set; }
    }
}