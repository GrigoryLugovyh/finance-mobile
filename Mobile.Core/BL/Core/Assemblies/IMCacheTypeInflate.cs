﻿using System;

namespace Mobile.Core.BL.Core.Assemblies
{
    public interface IMCacheTypeInflate
    {
        void Add(Type type);
    }
}