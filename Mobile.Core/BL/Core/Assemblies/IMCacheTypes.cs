﻿using System;
using System.Collections.Generic;

namespace Mobile.Core.BL.Core.Assemblies
{
// ReSharper disable once UnusedTypeParameter
    public interface IMCacheTypes<out T>
    {
        Type CurrentType { get; }

        void Add(string @namespace);

        Dictionary<string, Type> InvariantFullNameTypeCache { get; }
    }
}