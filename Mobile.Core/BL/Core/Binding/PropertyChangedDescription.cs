﻿namespace Mobile.Core.BL.Core.Binding
{
    public class PropertyChangedDescription
    {
        public PropertyChangedDescription(string sourceName, string propertyName)
        {
            SourceName = sourceName;
            PropertyName = propertyName;
        }

        public string SourceName { get; }
        public string PropertyName { get; }

        protected bool Equals(PropertyChangedDescription other)
        {
            return string.Equals(SourceName, other.SourceName) && string.Equals(PropertyName, other.PropertyName);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((PropertyChangedDescription) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((SourceName != null ? SourceName.GetHashCode() : 0)*397) ^
                       (PropertyName != null ? PropertyName.GetHashCode() : 0);
            }
        }
    }
}