﻿using System;
using System.ComponentModel;

namespace Mobile.Core.BL.Core.Binding
{
    public class MNotifyPropertyChangedSubscriptionFactory : IMNotifyPropertyChangedSubscriptionFactory
    {
        public IMNotifyPropertyChangedSubscription Create(INotifyPropertyChanged source,
            EventHandler<PropertyChangedEventArgs> eventHandler)
        {
            return new MNotifyPropertyChangedSubscription(source, eventHandler);
        }
    }
}