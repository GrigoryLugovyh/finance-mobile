﻿namespace Mobile.Core.BL.Core.Binding
{
    public interface IMBindingContextOwner
    {
        IMBindingContext BindingContext { get; set; } 
    }
}