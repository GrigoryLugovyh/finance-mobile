﻿namespace Mobile.Core.BL.Core.Binding
{
    public interface IMBindingSyntaxConstants
    {
        string Property { get; }

        string Method { get; }

        string Field { get; }
    }
}