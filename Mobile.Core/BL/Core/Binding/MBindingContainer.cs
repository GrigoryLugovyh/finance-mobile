﻿using System;
using System.Collections.Generic;
using System.Linq;
using MoreLinq;

namespace Mobile.Core.BL.Core.Binding
{
    public class MBindingContainer : IMBindingContainer
    {
        protected Dictionary<string, List<Tuple<object, string, List<IMUpdatableBinding>>>> container =
            new Dictionary<string, List<Tuple<object, string, List<IMUpdatableBinding>>>>();

        private string Scope { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="viewKey"></param>
        /// <param name="target"></param>
        /// <param name="bindings"></param>
        /// <param name="scope">obsolete</param>
        public void RegisterBindings(string viewKey, object target, IEnumerable<IMUpdatableBinding> bindings, string scope = "")
        {
            lock (container)
            {
                if (string.IsNullOrEmpty(viewKey) || target == null)
                {
                    return;
                }

                var bindingsCollected = bindings.ToList();

                if (!bindingsCollected.Any())
                {
                    return;
                }

                if (!string.IsNullOrEmpty(scope))
                {
                    Scope = scope;
                }
                
                if (!container.ContainsKey(viewKey))
                {
                    container.Add(viewKey,
                        new List<Tuple<object, string, List<IMUpdatableBinding>>>
                        {
                            Tuple.Create(target, Scope, bindingsCollected)
                        });
                    return;
                }

                container[viewKey].Add(Tuple.Create(target, Scope, bindingsCollected));
            }
        }

        public void RefreshBindings(string viewKey, object target, object source)
        {
            lock (container)
            {
                if (string.IsNullOrEmpty(viewKey) || target == null || source == null)
                {
                    return;
                }

                if (!container.ContainsKey(viewKey))
                {
                    return;
                }

                container[viewKey].ToList().Where(tuple => ReferenceEquals(tuple.Item1, target))
                    .ForEach(tuple => { tuple.Item3.ForEach(binding => binding.RefreshBinding(source)); });
            }
        }

        public void UnregisterBindings(string viewKey)
        {
            lock (container)
            {
                if (string.IsNullOrEmpty(viewKey))
                {
                    return;
                }

                if (!container.ContainsKey(viewKey))
                {
                    return;
                }

                container[viewKey].ToList().ForEach(tuple =>
                {
                    tuple.Item3.ForEach(binding => binding.Dispose());
                    tuple.Item3.Clear();
                });

                container[viewKey].Clear();

                container.Remove(viewKey);

                Scope = string.Empty;
            }
        }

        public void UnregisterBindingsBySource(string viewKey, object source)
        {
            lock (container)
            {
                if (string.IsNullOrEmpty(viewKey) || !container.ContainsKey(viewKey) || source == null)
                {
                    return;
                }

                Func<IMUpdatableBinding, bool> bindingContainsPredicate =
                    binding => ReferenceEquals(binding.BindingRequest.Source, source) ||
                               ReferenceEquals(binding.BindingRequest.Template, source);

                var removeCandidates = container[viewKey].Where(tuple => tuple.Item3.Any(bindingContainsPredicate)).ToList();

                if (!removeCandidates.Any())
                {
                    return;
                }

                removeCandidates.ForEach(tuple =>
                {
                    List<IMUpdatableBinding> removedBindings = new List<IMUpdatableBinding>();

                    tuple.Item3.Where(bindingContainsPredicate).ForEach(binding =>
                    {
                        binding.Dispose();
                        removedBindings.Add(binding);
                    });

                    removedBindings.ForEach(binding =>
                    {
                        tuple.Item3.Remove(binding);
                    });
                });

                removeCandidates.ForEach(tuple =>
                {
                    if (!tuple.Item3.Any())
                    {
                        container[viewKey].Remove(tuple);
                    }
                });

                removeCandidates.Clear();
            }
        }

        [Obsolete("Use UnregisterBindingsBySource for partial unregister")]
        public void UnregisterBindingsByScope(string viewKey, string scope)
        {
            lock (container)
            {
                if (string.IsNullOrEmpty(viewKey) || string.IsNullOrEmpty(scope))
                {
                    return;
                }

                if (!container.ContainsKey(viewKey))
                {
                    return;
                }

                var removeCandidates = container[viewKey].Where(tuple => tuple.Item2 == scope).ToList();

                if (removeCandidates.Any())
                {
                    removeCandidates.ForEach(tuple =>
                    {
                        tuple.Item3.ForEach(binding => binding.Dispose());
                        tuple.Item3.Clear();
                    });

                    removeCandidates.ForEach(tuple =>
                    {
                        container[viewKey].Remove(tuple);
                    });

                    removeCandidates.Clear();
                }
            }
        }
    }
}