﻿using System;
using System.ComponentModel;

namespace Mobile.Core.BL.Core.Binding
{
    public interface IMNotifyPropertyChangedSubscriptionFactory
    {
        IMNotifyPropertyChangedSubscription Create(INotifyPropertyChanged source,
            EventHandler<PropertyChangedEventArgs> eventHandler);
    }
}