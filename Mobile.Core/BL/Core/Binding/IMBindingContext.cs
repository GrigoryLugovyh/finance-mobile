﻿using System;
using Mobile.Core.BL.Core.Data;
using Mobile.Core.BL.Core.ViewModels;

namespace Mobile.Core.BL.Core.Binding
{
    public interface IMBindingContext : IMDataConsumer
    {
        IMViewModel ViewModel { get; set; }
    }
}