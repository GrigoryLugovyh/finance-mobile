﻿using System;
using Mobile.Core.BL.Core.Model;

namespace Mobile.Core.BL.Core.Binding
{
    public class MBinding : MainThreadObjectDispatcher, IMBinding
    {
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool isDisposing)
        {
        }
    }
}