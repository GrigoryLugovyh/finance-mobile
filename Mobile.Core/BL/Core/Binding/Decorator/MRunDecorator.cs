﻿using Mobile.Core.BL.Core.Attributes;
using Mobile.Core.BL.Core.Messages;
using Mobile.Core.BL.Core.Model;

namespace Mobile.Core.BL.Core.Binding.Decorator
{
    public class MRunDecorator : MDecorator
    {
        public override ActionResult Operation(params object[] parameters)
        {
            if (MethodAction == null)
            {
                Tracer.Warning("MRunDecorator: MethodAction is null");
                return default(ActionResult);
            }

            if (!(Attribute is MRunAttribute))
            {
                Tracer.Warning("MRunDecorator: Attribute is not RunAttribute");
                return default(ActionResult);
            }

            var asyncAttribute = Attribute as MRunAttribute;

            if (asyncAttribute.RunInWaitShell)
            {
                ViewDispatcher.RequestActionInWaitShell(MethodAction,
                    new MWaitShellRequest(asyncAttribute.Title, asyncAttribute.Message), asyncAttribute.Appearance);
                return new ActionResult {Success = true};
            }

            return InvokeOnMainThread(MethodAction, asyncAttribute.Appearance);
        }
    }
}