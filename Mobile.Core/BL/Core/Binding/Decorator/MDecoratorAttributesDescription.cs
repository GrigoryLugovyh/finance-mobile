﻿using System;
using System.Collections.Generic;
using System.Linq;
using Mobile.Core.BL.Core.Attributes;

namespace Mobile.Core.BL.Core.Binding.Decorator
{
    public class MDecoratorAttributesDescription : Tuple<IEnumerable<MAttribute>, IEnumerable<MAttribute>>
    {
        public MDecoratorAttributesDescription(IEnumerable<MAttribute> item1, IEnumerable<MAttribute> item2)
            : base(item1, item2)
        {
        }

        public bool Descriptionable
        {
            get { return Item1.Any(i => i is MDescriptionAttribute) || Item2.Any(i => i is MDescriptionAttribute); }
        }

        public bool Runnable
        {
            get { return Item1.Any(i => i is MRunAttribute) || Item2.Any(i => i is MRunAttribute); }
        }

        public IEnumerable<MAttribute> AfterExecuteMethod
        {
            get { return Item1.OrderBy(a => a.Priority); }
        }

        public IEnumerable<MAttribute> BeforeExecuteMethod
        {
            get { return Item2.OrderBy(a => a.Priority); }
        }
    }
}