﻿using System;
using Mobile.Core.BL.Core.Attributes;
using Mobile.Core.BL.Core.Model;

namespace Mobile.Core.BL.Core.Binding.Decorator
{
    public interface IMDecorator
    {
        void SetComponents(MAttribute attribute, Action methodAction);

        ActionResult Operation(params object[] parameters);
    }
}