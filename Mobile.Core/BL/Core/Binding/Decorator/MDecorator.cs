﻿using System;
using Mobile.Core.BL.Core.Attributes;
using Mobile.Core.BL.Core.Model;
using Mobile.Core.BL.Core.Views;

namespace Mobile.Core.BL.Core.Binding.Decorator
{
    public abstract class MDecorator : MainThreadObjectDispatcher, IMDecorator
    {
        protected Action MethodAction { get; private set; }

        protected MAttribute Attribute { get; private set; }

        protected IMViewDispatcher ViewDispatcher
        {
            get { return Dispatcher as IMViewDispatcher; }
        }

        public virtual void SetComponents(MAttribute attribute, Action methoAction)
        {
            Attribute = attribute;
            MethodAction = methoAction;
        }

        public abstract ActionResult Operation(params object[] parameters);
    }
}