﻿using Mobile.Core.BL.Core.Attributes;
using Mobile.Core.BL.Core.Binding.Source;
using Mobile.Core.BL.Core.Model;
using Mobile.Core.BL.IoCContainer;

namespace Mobile.Core.BL.Core.Binding.Decorator
{
    public class MDescriptionDecorator : MDecorator
    {
        private IMMethodSourceBindingStack sourceBindingStack;

        protected IMMethodSourceBindingStack SourceBindingStack
        {
            get { return sourceBindingStack ?? (sourceBindingStack = Container.Resolve<IMMethodSourceBindingStack>()); }
        }

        public override ActionResult Operation(params object[] parameters)
        {
            var description = Attribute as MDescriptionAttribute;

            if (description == null)
            {
                return default(ActionResult);
            }

            SourceBindingStack.Push(description.Name);

            return new ActionResult {Success = true};
        }
    }
}