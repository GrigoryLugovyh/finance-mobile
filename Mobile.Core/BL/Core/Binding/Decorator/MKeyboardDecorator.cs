﻿using Mobile.Core.BL.Core.Model;

namespace Mobile.Core.BL.Core.Binding.Decorator
{
    public class MKeyboardDecorator : MDecorator
    {
        public override ActionResult Operation(params object[] parameters)
        {
            ViewDispatcher.RequestActionAsync(() => ViewDispatcher.HideKeyboard());

            return new ActionResult {Success = true};
        }
    }
}