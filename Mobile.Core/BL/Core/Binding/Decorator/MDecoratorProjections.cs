﻿using System;
using System.Collections.Generic;
using Mobile.Core.BL.Core.Attributes;

namespace Mobile.Core.BL.Core.Binding.Decorator
{
    public class MDecoratorsOwner : IMDecoratorsOwner
    {
        private static readonly MDecorator runDecorator = new MRunDecorator();
        private static readonly MDecorator keyboardDecorator = new MKeyboardDecorator();
        private static readonly MDecorator descriptionDecorator = new MDescriptionDecorator();

        protected Dictionary<Type, IMDecorator> decorators = new Dictionary<Type, IMDecorator>
        {
            {
                typeof (MRunAttribute), runDecorator
            },
            {
                typeof (MKeyboardHideAttribute), keyboardDecorator
            },
            {
                typeof (MDescriptionAttribute), descriptionDecorator
            }
        };

        public void Add(Type attribute, IMDecorator decorator)
        {
            decorators[attribute] = decorator;
        }

        public IMDecorator Create(MAttribute attribute, Action methodAction)
        {
            IMDecorator decorator;

            if (!decorators.TryGetValue(attribute.GetType(), out decorator))
            {
                Tracer.Warning("MDecoratorProjections: no decorator for {0}", attribute);
            }

            decorator?.SetComponents(attribute, methodAction);

            return decorator;
        }
    }
}