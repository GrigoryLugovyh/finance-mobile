﻿using System;
using Mobile.Core.BL.Core.Attributes;

namespace Mobile.Core.BL.Core.Binding.Decorator
{
    public interface IMDecoratorsOwner
    {
        void Add(Type attribute, IMDecorator decorator);

        IMDecorator Create(MAttribute attribute, Action methodAction);
    }
}