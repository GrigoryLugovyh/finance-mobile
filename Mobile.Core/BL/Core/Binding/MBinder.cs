﻿using System.Collections.Generic;
using System.Linq;
using Mobile.Core.BL.Core.Binding.Selector;
using Mobile.Core.BL.Core.ViewModels;
using Mobile.Core.BL.IoCContainer;

namespace Mobile.Core.BL.Core.Binding
{
    public class MBinder : IMBinder
    {
        public IEnumerable<IMUpdatableBinding> Bind(IMViewModel source, object target, string bindingText, object template)
        {
            var parser = Container.Resolve<IMBindingSelector>();

            IList<MBindingDescription> descriptions;

            if (!parser.Parse(bindingText, out descriptions))
            {
                return new IMUpdatableBinding[0];
            }

            return Bind(source, target, template, descriptions);
        }

        public IEnumerable<IMUpdatableBinding> Bind(IMViewModel source, object target, object template,
            IEnumerable<MBindingDescription> descriptions)
        {
            return descriptions.Select(d => Bind(new MBindingRequest(source, target, d, template)));
        }

        public IMUpdatableBinding Bind(MBindingRequest request)
        {
            return new MCommonBinding(request);
        }
    }
}