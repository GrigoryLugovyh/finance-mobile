﻿namespace Mobile.Core.BL.Core.Binding
{
    public interface IMBindingDescription
    {
        MBindingDescription BindingDescription { get; }
    }
}