﻿namespace Mobile.Core.BL.Core.Binding.Target
{
    public interface IMTargetBindingFactory : IMBindingFactory
    {
        IMTargetBinding CreateBinding(object target, string name);
    }
}