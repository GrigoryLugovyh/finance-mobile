﻿namespace Mobile.Core.BL.Core.Binding.Target.Values
{
    public interface IPagerValue
    {
        object Value { get; set; }
        int CurrentItem { get; set; }
    }
}