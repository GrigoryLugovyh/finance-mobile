﻿namespace Mobile.Core.BL.Core.Binding.Target
{
    public class MTargetBindingItemArgs
    {
        public MTargetBindingItemArgs(object item, int position, params object[] arguments)
        {
            Item = item;
            Position = position;
            Arguments = arguments;
        }

        public bool Result { get; set; }

        public object Item { get; private set; }

        public int Position { get; private set; }

        public object[] Arguments { get; private set; }
    }
}