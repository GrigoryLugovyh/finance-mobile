﻿using System;
using Mobile.Core.BL.Core.Events;
using Mobile.Core.BL.Core.Extensions;

namespace Mobile.Core.BL.Core.Binding.Target
{
    public abstract class MTargetBinding : MBinding, IMTargetBinding
    {
        private readonly WeakReference target;

        protected MTargetBinding(object target)
        {
            this.target = new WeakReference(target);
        }

        protected object Target => target.Target;

        public abstract Type TargetType { get; }

        public virtual void Subscribe()
        {
        }

        public event EventHandler<MValueEventArgs<object>> TargetValueChanged;

        public event EventHandler<MValueEventArgs<MTargetBindingItemArgs>> TargetItemValueChanged;

        public abstract void SetValue(object value);

        protected virtual void RiseValueChanged(object value)
        {
            TargetValueChanged.Rise(this, value);
        }

        protected virtual bool RiseItemValueChanged(object item, int position, params object[] args)
        {
            var request = new MTargetBindingItemArgs(item, position, args);
            TargetItemValueChanged.Rise(this, request);
            return request.Result;
        }
    }
}