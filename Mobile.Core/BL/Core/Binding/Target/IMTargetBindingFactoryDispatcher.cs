﻿namespace Mobile.Core.BL.Core.Binding.Target
{
    public interface IMTargetBindingFactoryDispatcher
    {
        IMTargetBinding CreateBinding(object target, string name);

        void AddFactory(IMTargetBindingFactory factory);
    }
}