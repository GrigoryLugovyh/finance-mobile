﻿using System;

namespace Mobile.Core.BL.Core.Binding.Target
{
    public abstract class MCommonTargetBinding : MTargetBinding
    {
        private bool updatingSource;
        private bool updatingTarget;

        protected MCommonTargetBinding(object target) : base(target)
        {
        }

        protected abstract void SetValueTarget(object value);

        protected abstract object GetValueTarget();

        protected abstract bool ValuesEquals(object newValue, object currentValue);

        public override void SetValue(object value)
        {
            if (updatingTarget)
            {
                return;
            }

            var target = Target;

            if (target == null)
            {
                return;
            }

            try
            {
                updatingTarget = true;

                var currentValue = GetValueTarget();

                if (!ValuesEquals(value, currentValue))
                {
                    SetValueTarget(value);
                }
            }
            finally
            {
                updatingTarget = false;
            }
        }

        protected override void RiseValueChanged(object value)
        {
            SourceUpdating(() => base.RiseValueChanged(value));
        }

        protected override bool RiseItemValueChanged(object item, int position, params object[] args)
        {
            bool r = false;

            SourceUpdating(() =>
            {
                r = base.RiseItemValueChanged(item, position, args);
            });

            return r;
        }

        protected virtual void SourceUpdating(Action action)
        {
            if (updatingTarget || updatingSource)
            {
                return;
            }

            try
            {
                updatingSource = true;

                action();
            }
            finally
            {
                updatingSource = false;
            }
        }
    }
}