﻿using System;
using Mobile.Core.BL.Core.Events;

namespace Mobile.Core.BL.Core.Binding.Target
{
    public interface IMTargetBinding : IMBinding
    {
        Type TargetType { get; }

        void Subscribe();

        void SetValue(object value);

        event EventHandler<MValueEventArgs<object>> TargetValueChanged;

        event EventHandler<MValueEventArgs<MTargetBindingItemArgs>> TargetItemValueChanged;
    }
}