﻿using System.Collections.Generic;

namespace Mobile.Core.BL.Core.Binding.Target
{
    public class MTargetBindingFactoryDispatcher : IMTargetBindingFactoryDispatcher
    {
        protected readonly IDictionary<string, IMTargetBindingFactory> factories;

        public MTargetBindingFactoryDispatcher()
        {
            factories = new Dictionary<string, IMTargetBindingFactory>();
        }

        public virtual IMTargetBinding CreateBinding(object target, string name)
        {
            var key = GetKey(name);

            IMTargetBindingFactory factory;

            if (!factories.TryGetValue(key, out factory))
            {
                return null;
            }

            return factory.CreateBinding(target, name);
        }

        public virtual void AddFactory(IMTargetBindingFactory factory)
        {
            var key = GetKey(factory.BindingType.Name);

            if (!factories.ContainsKey(key))
            {
                factories.Add(key, factory);
            }
        }

        private string GetKey(string name)
        {
            return name.ToLowerInvariant();
        }
    }
}