﻿namespace Mobile.Core.BL.Core.Binding
{
    public interface IBindingFactory
    {
        object Template { get; set; }
    }
}