﻿using System;
using System.Collections.Generic;
using Mobile.Core.BL.Core.Binding.Selector.Extended;

namespace Mobile.Core.BL.Core.Binding
{
    public class MBindingDescription : IDisposable
    {
        public MBindingDescription()
        {
        }

        public MBindingDescription(string target, string value, Tuple<string, string> arguments)
        {
            Target = target;
            Value = value;
            Arguments = arguments;
        }

        /// <summary>
        ///     Команда из BindingSyntaxNames (value, text, click, longclick, list ...)
        /// </summary>
        public string Target { get; set; }

        /// <summary>
        ///     Объект в ViewModel к которому осуществляется биндинг
        /// </summary>
        public string Value { get; set; }

        public ValueElementParser.ValueElementType ValueElementType { get; set; }

        public List<object> Params { get; set; }

        public Tuple<string, string> Arguments { get; set; }

        public object[] ParamsArray => Params?.ToArray();

        public override string ToString()
        {
            return $"command \"{Target ?? "-"}\" for target \"{Value ?? "-"}\"";
        }

        public void Dispose()
        {
            Params?.Clear();
            Params = null;
            Target = null;
            Value = null;
            Arguments = null;
        }
    }
}