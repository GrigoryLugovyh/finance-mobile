﻿using System;
using Mobile.Core.BL.Core.ViewModels;

namespace Mobile.Core.BL.Core.Binding
{
    public class MBindingRequest : IDisposable
    {
        public MBindingRequest(IMViewModel source, object target, MBindingDescription description, object template)
        {
            Source = source;
            Target = target;
            Description = description;
            Template = template;
        }

        public IMViewModel Source { get; set; }

        public object Template { get; set; }

        public object Target { get; set; }

        public MBindingDescription Description { get; set; }
        public void Dispose()
        {
            Description.Dispose();
            Template = null;
            Target = null;
        }
    }
}