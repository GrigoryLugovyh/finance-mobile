﻿using System;
using System.Reflection;

namespace Mobile.Core.BL.Core.Binding
{
    public interface IMTypeMemberDetector
    {
        MemberInfo GetMemberInfo(Type source, string name);
    }
}