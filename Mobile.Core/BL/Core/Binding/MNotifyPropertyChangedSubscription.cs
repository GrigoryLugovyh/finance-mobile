﻿using System;
using System.ComponentModel;

namespace Mobile.Core.BL.Core.Binding
{
    public class MNotifyPropertyChangedSubscription : IMNotifyPropertyChangedSubscription
    {
        protected readonly INotifyPropertyChanged propertyChanged;
        protected EventHandler<PropertyChangedEventArgs> eventHandler;

        public MNotifyPropertyChangedSubscription(INotifyPropertyChanged property,
            EventHandler<PropertyChangedEventArgs> @event)
        {
            propertyChanged = property;
            eventHandler = @event;

            propertyChanged.PropertyChanged += NotifyPropertyChanged;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void NotifyPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            eventHandler.Invoke(sender, e);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (propertyChanged != null)
                {
                    propertyChanged.PropertyChanged -= NotifyPropertyChanged;
                }

                eventHandler = null;
            }
        }
    }
}