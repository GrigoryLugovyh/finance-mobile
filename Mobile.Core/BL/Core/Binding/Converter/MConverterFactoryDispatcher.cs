﻿using System.Collections.Generic;

namespace Mobile.Core.BL.Core.Binding.Converter
{
    public class MConverterFactoryDispatcher : IMConverterFactoryDispatcher
    {
        protected readonly IDictionary<string, IMConverter> converters;

        public MConverterFactoryDispatcher()
        {
            converters = new Dictionary<string, IMConverter>();
        }

        public IMConverter this[string key]
        {
            get
            {
                IMConverter converter;

                if (!converters.TryGetValue(key, out converter))
                {
                    Tracer.Error(
                        "MConverterFactoryDispatcher: converters douesn't contain any converter with name \"{0}\"", key);
                    return null;
                }

                return converter;
            }
        }

        public void Add(IMConverter converter, string converterName = null)
        {
            if (converter == null)
            {
                return;
            }

            var key = string.IsNullOrEmpty(converterName) ? converter.GetType().Name : converterName;

            if (converters.ContainsKey(key))
            {
                Tracer.Warning("MConverterFactoryDispatcher: Converter already exists with name \"{0}\"", key);
                return;
            }

            converters.Add(new KeyValuePair<string, IMConverter>(key, converter));
        }
    }
}