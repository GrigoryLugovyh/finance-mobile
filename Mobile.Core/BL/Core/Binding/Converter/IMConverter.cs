﻿namespace Mobile.Core.BL.Core.Binding.Converter
{
    public interface IMConverter
    {
        object Convert(object value);
        object ConvertBack(object value);
    }
}