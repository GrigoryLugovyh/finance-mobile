﻿using Mobile.Core.BL.Core.Extensions;
using Mobile.Core.BL.Core.Model;

namespace Mobile.Core.BL.Core.Binding.Converter
{
    public abstract class MConverter : MainThreadObjectDispatcher, IMConverter
    {
        public object Convert(object value)
        {
            return InvokeFuncOnMainThread(() => ConvertValue(value)).ToActualValue();
        }

        public object ConvertBack(object value)
        {
            return InvokeFuncOnMainThread(() => ConvertBackValue(value)).ToActualValue();
        }

        protected abstract object ConvertValue(object value);

        protected abstract object ConvertBackValue(object value);
    }
}