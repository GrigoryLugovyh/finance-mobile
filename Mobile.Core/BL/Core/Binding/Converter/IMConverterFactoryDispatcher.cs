﻿namespace Mobile.Core.BL.Core.Binding.Converter
{
    public interface IMConverterFactoryDispatcher
    {
        IMConverter this[string key] { get; }

        void Add(IMConverter converter, string converterName = null);
    }
}