﻿using System.Collections.Generic;
using Mobile.Core.BL.Core.ViewModels;

namespace Mobile.Core.BL.Core.Binding
{
    public interface IMBinder
    {
        IEnumerable<IMUpdatableBinding> Bind(IMViewModel source, object target, string bindingText, object template);

        IEnumerable<IMUpdatableBinding> Bind(IMViewModel source, object target, object template, IEnumerable<MBindingDescription> descriptions);

        IMUpdatableBinding Bind(MBindingRequest request);
    }
}