﻿using System;
using System.Reflection;
using Mobile.Core.BL.Core.Binding.Source;

namespace Mobile.Core.BL.Core.Binding
{
    public class MCommonSourceBindingFactory : IMSourceBindingFactory
    {
        protected readonly Func<object, MemberInfo, IMSourceBinding> sourceBinding;

        public MCommonSourceBindingFactory(Func<object, MemberInfo, IMSourceBinding> binding)
        {
            sourceBinding = binding;
        }

        public IMSourceBinding CreateBinding(object source, MemberInfo info)
        {
            return sourceBinding(source, info);
        }
    }
}