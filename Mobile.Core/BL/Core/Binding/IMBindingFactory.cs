﻿namespace Mobile.Core.BL.Core.Binding
{
    public interface IMBindingFactory
    {
        MBindingType BindingType { get; } 
    }
}