﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace Mobile.Core.BL.Core.Binding
{
    public class MTypeMemberDetector : IMTypeMemberDetector
    {
        private readonly Dictionary<Type, Dictionary<string, MemberInfo>> memberInfoCache =
            new Dictionary<Type, Dictionary<string, MemberInfo>>();

        public virtual MemberInfo GetMemberInfo(Type source, string name)
        {
            if (source == null || string.IsNullOrWhiteSpace(name))
            {
                return null;
            }

            if (memberInfoCache.ContainsKey(source) && memberInfoCache[source].ContainsKey(name))
            {
                return memberInfoCache[source][name];
            }

            var property = source.GetProperty(name);

            if (property != null)
            {
                return AddMemberInfoToCache(source, name, property);
            }

            var method = source.GetMethod(name);

            if (method != null)
            {
                return AddMemberInfoToCache(source, name, method);
            }

            var field = source.GetField(name);

            if (field != null)
            {
                return AddMemberInfoToCache(source, name, field);
            }

            Tracer.Warning("Could not find member info for {0} in {1}", name, source);

            return null;
        }

        private MemberInfo AddMemberInfoToCache(Type source, string name, MemberInfo memberInfo)
        {
            if (!memberInfoCache.ContainsKey(source))
                memberInfoCache.Add(source, new Dictionary<string, MemberInfo>());

            if (!memberInfoCache[source].ContainsKey(name))
            {
                memberInfoCache[source].Add(name, memberInfo);
            }
            else
            {
                memberInfoCache[source][name] = memberInfo;
            }

            return memberInfo;
        }
    }
}