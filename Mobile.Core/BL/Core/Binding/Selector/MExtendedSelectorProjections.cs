﻿using System;
using System.Collections.Generic;
using Mobile.Core.BL.Core.Binding.Selector.Extended;
using Mobile.Core.BL.Core.Binding.Selector.Extended.Tree;
using Mobile.Core.BL.Core.Enums;

namespace Mobile.Core.BL.Core.Binding.Selector
{
    public class MExtendedSelectorsOwner : IMExtendedSelectorsOwner
    {
        protected readonly Dictionary<MSelectorType, Action<MBindingDescription, ElementTree>> extenders = new Dictionary
            <MSelectorType, Action<MBindingDescription, ElementTree>>
        {
            {
                MSelectorType.Value, (description, tree) =>
                {
                    description.Value = tree.BindingElement;

                    if (tree.BindingElementType != ValueElementParser.ValueElementType.Common)
                    {
                        if (description.Params == null)
                        {
                            description.Params = new List<object>();
                        }

                        description.Params.Add(tree.BindingParameter);
                    }
                }
            },
            {
                MSelectorType.Target, (description, tree) => { description.Target = tree.BindingElement.ToLowerInvariant(); }
            },
            {
                MSelectorType.Attribute, (description, tree) => { description.Arguments = tree.BindingAttributes; }
            },
            {
                MSelectorType.Command, (description, tree) =>
                {
                    // empty
                }
            },
            {
                MSelectorType.Parameter, (description, tree) =>
                {
                    // empty
                }
            }
        };

        protected readonly Dictionary<MSelectorType, ElementParser> parsers = new Dictionary
            <MSelectorType, ElementParser>
        {
            {
                MSelectorType.Command, new CommandElementParser()
            },
            {
                MSelectorType.Parameter, new ParameterElementParser()
            },
            {
                MSelectorType.Target, new TargetElementParser()
            },
            {
                MSelectorType.Value, new ValueElementParser()
            },
            {
                MSelectorType.Attribute, new AttributeElementParser()
            }
        };

        public ElementParser Get(MSelectorType @type)
        {
            return parsers[type];
        }

        public Dictionary<MSelectorType, ElementParser> Parsers
        {
            get { return parsers; }
        }

        public void FillDescription(MBindingDescription bindingDescription, ElementTree element)
        {
            bindingDescription.ValueElementType = element.BindingElementType;

            extenders[element.SelectorType].Invoke(bindingDescription, element);
        }
    }
}