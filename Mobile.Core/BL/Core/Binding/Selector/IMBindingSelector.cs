﻿using System.Collections.Generic;

namespace Mobile.Core.BL.Core.Binding.Selector
{
    public interface IMBindingSelector
    {
        bool Parse(string inputBindingText, out IList<MBindingDescription> description);
    }
}