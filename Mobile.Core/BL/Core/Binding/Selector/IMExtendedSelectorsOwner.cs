﻿using System.Collections.Generic;
using Mobile.Core.BL.Core.Binding.Selector.Extended;
using Mobile.Core.BL.Core.Binding.Selector.Extended.Tree;
using Mobile.Core.BL.Core.Enums;

namespace Mobile.Core.BL.Core.Binding.Selector
{
    public interface IMExtendedSelectorsOwner
    {
        ElementParser Get(MSelectorType @type);

        Dictionary<MSelectorType, ElementParser> Parsers { get; }

        void FillDescription(MBindingDescription bindingDescription, ElementTree element);
    }
}