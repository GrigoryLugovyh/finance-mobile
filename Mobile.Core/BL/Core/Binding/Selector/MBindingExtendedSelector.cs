﻿using System.Collections.Generic;
using Mobile.Core.BL.Core.Binding.Selector.Extended.Tree;
using Mobile.Core.BL.IoCContainer;

namespace Mobile.Core.BL.Core.Binding.Selector
{
    public class MBindingExtendedSelector : IMBindingSelector
    {
        private IMSelectorTreeBuilder builder;

        private IMSelectorTreeBuilder Builder => builder ?? (builder = Container.Resolve<IMSelectorTreeBuilder>());

        public bool Parse(string inputBindingText, out IList<MBindingDescription> description)
        {
            description = Builder.Build(inputBindingText);
            return description.Count > 0;
        }
    }
}