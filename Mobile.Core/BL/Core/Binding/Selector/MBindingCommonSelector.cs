﻿using System.Collections.Generic;

namespace Mobile.Core.BL.Core.Binding.Selector
{
    public class MBindingCommonSelector : IMBindingSelector
    {
        protected IList<MBindingDescription> bindingDescriptions;
        protected string bindingText;

        protected virtual char Separator
        {
            get { return ':'; }
        }

        protected virtual int BindingElementsCount
        {
            get { return 2; }
        }

        public virtual bool Parse(string inputBindingText, out IList<MBindingDescription> descriptions)
        {
            descriptions = null;

            if (!Prepare(inputBindingText))
            {
                Tracer.Warning("BindingText has bad format - {0}", bindingText);
                return false;
            }

            Process();

            if (!IsBindingDescriptionValid())
            {
                return false;
            }

            descriptions = bindingDescriptions;

            return true;
        }

        protected virtual bool Prepare(string inputBindingText)
        {
            bindingDescriptions = new List<MBindingDescription>();

            bindingText = inputBindingText.Trim();

            return IsBindingTextValid();
        }

        protected virtual bool IsBindingTextValid()
        {
            return !string.IsNullOrWhiteSpace(bindingText) && bindingText.IndexOf(Separator) > -1;
        }

        protected virtual bool IsBindingDescriptionValid()
        {
            return bindingDescriptions != null && bindingDescriptions.Count > 0;
        }

        protected virtual void Process()
        {
            var elements = bindingText.Split(Separator);

            if (elements.Length >= BindingElementsCount)
            {
                bindingDescriptions.Add(new MBindingDescription(elements[0].ToLowerInvariant().Trim(), elements[1].Trim(), null));
            }
        }
    }
}