﻿using System.Collections.Generic;

namespace Mobile.Core.BL.Core.Binding.Selector.Extended
{
    public class TargetElementParser : ElementParser
    {
        protected override char Separator
        {
            get { return ':'; }
        }

        public override List<string> Parse(string rawBinding)
        {
            var index = rawBinding.IndexOf(Separator);

            if (index == -1)
            {
                return null;
            }

            Element = rawBinding.Substring(0, index).Trim();

            return ClearElements(new[] {rawBinding.Substring(index + 1)});
        }
    }
}