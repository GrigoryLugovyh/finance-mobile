﻿using System;
using System.Collections.Generic;

namespace Mobile.Core.BL.Core.Binding.Selector.Extended
{
    public class AttributeElementParser : ElementParser
    {
        protected override char Separator
        {
            get { return ':'; }
        }

        public override List<string> Parse(string rawBinding)
        {
            if (string.IsNullOrWhiteSpace(rawBinding))
            {
                return base.Parse(rawBinding);
            }

            int startIndex = rawBinding.IndexOf('{') + 1;
            string raw = rawBinding.Trim().Substring(startIndex, rawBinding.LastIndexOf('}') - startIndex).Trim();

            if (string.IsNullOrWhiteSpace(raw))
            {
                return base.Parse(rawBinding);
            }

            string[] args = raw.Split(Separator);

            if (args.Length < 1)
            {
                return base.Parse(rawBinding);
            }

            Arguments = new Tuple<string, string>(args[0].Trim().ToLowerInvariant(), args[1].Trim());

            return new List<string>
            {
                raw
            };
        }
    }
}