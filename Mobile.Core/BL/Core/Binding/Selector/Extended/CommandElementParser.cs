﻿namespace Mobile.Core.BL.Core.Binding.Selector.Extended
{
    public class CommandElementParser : ElementParser
    {
        protected override char Separator
        {
            get { return '\0'; }
        }
    }
}