﻿using System;
using System.Collections.Generic;
using Mobile.Core.BL.Core.Enums;

namespace Mobile.Core.BL.Core.Binding.Selector.Extended.Tree
{
    public class ElementTree : IDisposable
    {
        public ElementTree()
        {
            ChildElements = new List<ElementTree>();
        }

        public string BindingText { get; set; }

        public string BindingElement { get; set; }

        public object BindingParameter { get; set; }

        public ValueElementParser.ValueElementType BindingElementType { get; set; }

        public Tuple<string, string> BindingAttributes { get; set; }

        public MSelectorType SelectorType { get; set; }

        public List<ElementTree> ChildElements { get; set; }

        public override string ToString()
        {
            return $"command: {BindingText}, selector: {SelectorType}, elementType: {BindingElementType}";
        }

        public void Dispose()
        {
            BindingText = null;
            BindingElement = null;
            BindingParameter = null;
            BindingAttributes = null;
            ChildElements?.ForEach(tree => tree.Dispose());
        }
    }
}