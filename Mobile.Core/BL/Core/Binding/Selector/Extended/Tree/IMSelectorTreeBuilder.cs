﻿using System.Collections.Generic;

namespace Mobile.Core.BL.Core.Binding.Selector.Extended.Tree
{
    public interface IMSelectorTreeBuilder
    {
        ElementTree Tree { get; }

        IList<MBindingDescription> Build(string inputBindingText);
    }
}