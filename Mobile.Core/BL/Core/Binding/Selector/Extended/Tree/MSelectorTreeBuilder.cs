﻿using System.Collections.Generic;
using System.Linq;
using Mobile.Core.BL.Core.Enums;
using Mobile.Core.BL.Core.Extensions;
using Mobile.Core.BL.IoCContainer;

namespace Mobile.Core.BL.Core.Binding.Selector.Extended.Tree
{
    public class MSelectorTreeBuilder : IMSelectorTreeBuilder
    {
        private IMExtendedSelectorsOwner selectorsOwner;

        protected IMExtendedSelectorsOwner SelectorsOwner
            => selectorsOwner ?? (selectorsOwner = Container.Resolve<IMExtendedSelectorsOwner>());

        public ElementTree Tree { get; protected set; }

        public IList<MBindingDescription> Build(string inputBindingText)
        {
            Tree = new ElementTree
            {
                SelectorType = MSelectorType.Command,
                BindingText = inputBindingText,
                ChildElements = GetChildElements(inputBindingText, MSelectorType.Command.Next())
            };

            IList<MBindingDescription> descriptions = new List<MBindingDescription>();

            Tree.ChildElements.ForEach(childElement =>
            {
                var description = new MBindingDescription();
                Collect(description, childElement);
                descriptions.Add(description);
            });

            Tree.Dispose();
            Tree = null;

            return descriptions;
        }

        protected virtual void Collect(MBindingDescription bindingDescription, ElementTree elementTree)
        {
            elementTree.ChildElements?.ForEach(childElement =>
            {
                SelectorsOwner.FillDescription(bindingDescription, childElement);
                Collect(bindingDescription, childElement);
            });
        }

        protected virtual List<ElementTree> GetChildElements(string inputBindingText, MSelectorType selectorType)
        {
            ElementParser parser = SelectorsOwner.Get(selectorType);

            if (parser == null)
            {
                Tracer.Warning("MSelectorTreeBuilder: no parser for selector {0}", selectorType);
                return null;
            }

            var treeElements = new List<ElementTree>();

            List<string> elements = parser.Parse(inputBindingText);

            if (elements != null && elements.Count > 0)
            {
                treeElements.AddRange(elements.Select(element => new ElementTree
                {
                    SelectorType = selectorType,
                    BindingText = element,
                    BindingElement = parser.Element,
                    BindingParameter = parser.Parameter,
                    BindingElementType = parser.ElementType,
                    BindingAttributes = parser.Arguments,
                    ChildElements = selectorType.IsLast() ? null : GetChildElements(element, selectorType.Next())
                }));
            }

            return treeElements;
        }
    }
}