﻿using System;
using System.Collections.Generic;

namespace Mobile.Core.BL.Core.Binding.Selector.Extended
{
    public class ValueElementParser : ElementParser
    {
        private readonly IDictionary<ValueElementType, Func<string, Tuple<string, string>>> valueElementManager = new Dictionary
            <ValueElementType, Func<string, Tuple<string, string>>>
        {
            {
                ValueElementType.Common, rawElement => Tuple.Create<string, string>(rawElement.Trim(), null)
            },
            {
                ValueElementType.Index, rawElement =>
                {
                    var element = string.Empty;
                    var parameter = rawElement.Trim().Trim('[', ']');

                    if (!string.IsNullOrWhiteSpace(parameter))
                    {
                        element = "get_Item";
                    }
                    return Tuple.Create(element, parameter);
                }
            },
            {
                ValueElementType.Converter, rawElement =>
                {
                    rawElement = rawElement.Trim();

                    var elementCandidate = rawElement.Substring(rawElement.IndexOf('(') + 1,
                        rawElement.IndexOf(')') - rawElement.IndexOf('(') - 1);

                    var parameterCandidate = rawElement.Substring(0, rawElement.IndexOf('('));

                    if (string.IsNullOrWhiteSpace(elementCandidate) ||
                        elementCandidate.IndexOfAny(new[] {'(', ')'}) > -1 ||
                        string.IsNullOrWhiteSpace(parameterCandidate) ||
                        parameterCandidate.IndexOfAny(new[] {'(', ')'}) > -1)
                    {
                        return Tuple.Create<string, string>(rawElement, null);
                    }

                    return Tuple.Create(elementCandidate, parameterCandidate);
                }
            }
        };

        protected override char Separator
        {
            get { return ','; }
        }

        public override List<string> Parse(string rawBinding)
        {
            var index = rawBinding.IndexOf(Separator);

            var rawElement = rawBinding.Substring(0, index > -1 ? index : rawBinding.Length).Trim();

            string element;
            string parameter;
            GetActualElement(rawElement, out element, out parameter);

            Element = element;
            Parameter = parameter;

            return ClearElements(new[] {index > -1 ? rawBinding.Substring(index + 1) : string.Empty});
        }

        private void GetActualElement(string rawElement, out string element, out string parameter)
        {
            var elementType = SupplyValueElementType(rawElement);

            var result = valueElementManager[elementType].Invoke(rawElement);

            element = result.Item1;
            parameter = result.Item2;

            ElementType = elementType;
        }

        private ValueElementType SupplyValueElementType(string rawElement)
        {
            var element = rawElement;

            if (element.StartsWith("[") && element.EndsWith("]"))
            {
                return ValueElementType.Index;
            }

            if (element.IndexOf('(') < element.IndexOf(')') &&
                element.IndexOf("converter", StringComparison.OrdinalIgnoreCase) > -1)
            {
                return ValueElementType.Converter;
            }

            return ValueElementType.Common;
        }

        public enum ValueElementType
        {
            Common,
            Index,
            Converter
        }
    }
}