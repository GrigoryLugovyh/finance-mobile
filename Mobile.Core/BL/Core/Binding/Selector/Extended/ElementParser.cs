﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Mobile.Core.BL.Core.Binding.Selector.Extended
{
    public abstract class ElementParser
    {
        public string Element { get; protected set; }

        public object Parameter { get; protected set; }

        public ValueElementParser.ValueElementType ElementType { get; protected set; }

        public Tuple<string, string> Arguments { get; protected set; }

        protected abstract char Separator { get; }

        protected virtual StringSplitOptions SplitOption
        {
            get { return StringSplitOptions.RemoveEmptyEntries; }
        }

        public virtual List<string> Parse(string rawBinding)
        {
            string[] elements = Split(rawBinding);

            return ClearElements(elements);
        }

        protected virtual string[] Split(string rawBinding)
        {
            if (!IsBindingValid(rawBinding))
            {
                return new string[0];
            }

            return rawBinding.Split(new[] {Separator}, SplitOption);
        }

        protected virtual List<string> ClearElements(string[] elements)
        {
            return elements.Select(element => element.Trim()).ToList();
        }

        protected virtual bool IsBindingValid(string binding)
        {
            return !string.IsNullOrEmpty(binding);
        }
    }
}