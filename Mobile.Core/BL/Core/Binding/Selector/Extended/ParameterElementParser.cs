﻿namespace Mobile.Core.BL.Core.Binding.Selector.Extended
{
    public class ParameterElementParser : ElementParser
    {
        protected override char Separator
        {
            get { return ';'; }
        }
    }
}