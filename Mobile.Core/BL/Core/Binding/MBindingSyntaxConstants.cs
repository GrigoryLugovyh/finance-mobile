﻿namespace Mobile.Core.BL.Core.Binding
{
    public class MBindingSyntaxConstants : IMBindingSyntaxConstants
    {
        private const string ConstProperty = "MonoProperty";

        private const string ConstMethod = "MonoMethod";

        private const string ConstField = "MonoField";

        public string Property => ConstProperty;

        public string Method => ConstMethod;

        public string Field => ConstField;
    }
}