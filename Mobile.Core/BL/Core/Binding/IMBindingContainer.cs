﻿using System.Collections.Generic;
using Mobile.Core.BL.Core.ViewModels;

namespace Mobile.Core.BL.Core.Binding
{
    public interface IMBindingContainer
    {
        void RegisterBindings(string viewKey, object target, IEnumerable<IMUpdatableBinding> bindings, string scope = "");
        
        void RefreshBindings(string viewKey, object target, object source);

        void UnregisterBindings(string viewKey);

        void UnregisterBindingsBySource(string viewKey, object source);

        void UnregisterBindingsByScope(string viewKey, string scope);
    }
}