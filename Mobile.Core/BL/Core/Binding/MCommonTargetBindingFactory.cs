﻿using System;
using Mobile.Core.BL.Core.Binding.Target;

namespace Mobile.Core.BL.Core.Binding
{
    public class MCommonTargetBindingFactory<TTarget> : IMTargetBindingFactory
        where TTarget : class
    {
        protected readonly string nameBinding;
        protected readonly Func<TTarget, IMTargetBinding> targetBinding;

        public MCommonTargetBindingFactory(string name, Func<TTarget, IMTargetBinding> binding)
        {
            nameBinding = name;
            targetBinding = binding;
        }

        public MBindingType BindingType => new MBindingType(typeof (TTarget), nameBinding);

        public virtual IMTargetBinding CreateBinding(object target, string name)
        {
            var originTarget = target as TTarget;

            if (originTarget == null)
            {
                return null;
            }

            return targetBinding(originTarget);
        }
    }
}