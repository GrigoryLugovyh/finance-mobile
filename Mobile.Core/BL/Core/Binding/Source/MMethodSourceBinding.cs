﻿#pragma warning disable 0067

using System;
using System.Collections.Generic;
using System.Reflection;
using System.Windows.Input;
using Mobile.Core.BL.Core.Attributes;
using Mobile.Core.BL.Core.Binding.Converter;
using Mobile.Core.BL.Core.Binding.Decorator;
using Mobile.Core.BL.Core.Extensions;
using Mobile.Core.BL.Core.Model;
using Mobile.Core.BL.IoCContainer;
using MoreLinq;

namespace Mobile.Core.BL.Core.Binding.Source
{
    public class MMethodSourceBinding : MSourceBinding, ICommand
    {
        protected readonly MethodInfo methodInfo;
        private IMDecoratorsOwner projections;

        public MMethodSourceBinding(object source, MethodInfo info) : base(source)
        {
            if (info == null)
            {
                throw new ArgumentException("info");
            }

            methodInfo = info;

            ActionResults = new Dictionary<MAttribute, ActionResult>();
        }

        protected IMDecoratorsOwner Projections => projections ?? (projections = Container.Resolve<IMDecoratorsOwner>());

        protected MethodInfo MethodInfo => methodInfo;

        public bool Successfully { get; private set; }

        public IDictionary<MAttribute, ActionResult> ActionResults { get; }

        public override Type SourceType => typeof (ICommand);

        public bool CanExecute(object parameter)
        {
            return Source.HasMethod(MethodInfo);
        }

        public void Execute(object parameter)
        {
            Successfully = true;

            var action =
                new Action(
                    () =>
                        MethodInfo.Invoke(Source,
                            parameter == null ? null : parameter is object[] ? (object[]) parameter : new[] {parameter}));

            var description = MethodInfo.GetDecoratorAttributes();

            Action<MAttribute, ActionResult> fillResults = (attribute, result) =>
            {
                if (!ActionResults.ContainsKey(attribute))
                {
                    ActionResults.Add(new KeyValuePair<MAttribute, ActionResult>(attribute, null));
                }

                ActionResults[attribute] = result;
            };

            Func<IEnumerable<MAttribute>, bool> executor = attributes =>
            {
                bool done = true;

                attributes.ForEach(attribute =>
                {
                    var result = Projections.Create(attribute, action).Operation();
                    fillResults(attribute, result);
                    done &= result.Success;
                });

                return done;
            };

            Successfully &= executor(description.BeforeExecuteMethod);

            if (Successfully)
            {
                if (!description.Runnable)
                {
                    var result = InvokeOnMainThread(action);
                    fillResults(new MRunAttribute(), result);
                    Successfully &= result.Success;
                }

                Successfully &= executor(description.AfterExecuteMethod);
            }
        }

        public event EventHandler CanExecuteChanged;

        public override void SetValue(object value, IMConverter converter = null)
        {
            Execute(value);
        }

        public override object GetValue(object[] values, IMConverter converter = null)
        {
            if (values != null && values.Length > 0 && CanExecute(null))
            {
                return InvokeFuncOnMainThread(() => MethodInfo.Invoke(Source, values));
            }

            return this;
        }
    }
}