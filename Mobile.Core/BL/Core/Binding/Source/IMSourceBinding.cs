﻿using System;
using System.ComponentModel;
using Mobile.Core.BL.Core.Binding.Converter;
using Mobile.Core.BL.Core.Events;

namespace Mobile.Core.BL.Core.Binding.Source
{
    public interface IMSourceBinding : IMBinding
    {
        object Source { get; }

        Type SourceType { get; }

        void BindSource(object value);

        void SetValue(object value, IMConverter converter = null);

        object GetValue(object[] values, IMConverter converter = null);

        event EventHandler<MValueEventArgs<PropertyChangedEventArgs>> SourceValueChanged;
    }
}