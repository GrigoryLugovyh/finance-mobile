﻿namespace Mobile.Core.BL.Core.Binding.Source
{
    public interface IMSourceBindingFactoryDispatcher
    {
        IMSourceBinding CreateBinding(MBindingRequest request, string name);

        void AddFactory(string elementReflectionName, IMSourceBindingFactory factory);
    }
}