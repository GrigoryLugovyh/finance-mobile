﻿using System;
using System.ComponentModel;
using Mobile.Core.BL.Core.Binding.Converter;
using Mobile.Core.BL.Core.Events;
using Mobile.Core.BL.Core.Extensions;

namespace Mobile.Core.BL.Core.Binding.Source
{
    public abstract class MSourceBinding : MBinding, IMSourceBinding
    {
        protected MSourceBinding(object source)
        {
            Source = source;
        }

        public object Source { get; private set; }

        public abstract Type SourceType { get; }

        public void BindSource(object value)
        {
            Source = value;
        }

        public abstract void SetValue(object value, IMConverter converter = null);

        public abstract object GetValue(object[] values, IMConverter converter = null);

        public event EventHandler<MValueEventArgs<PropertyChangedEventArgs>> SourceValueChanged;

        protected void RiseSourceValueChanged(PropertyChangedEventArgs e)
        {
            SourceValueChanged.Rise(this, e);
        }
    }
}