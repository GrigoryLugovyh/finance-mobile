﻿using System;
using System.ComponentModel;
using System.Reflection;
using Mobile.Core.BL.Core.Binding.Converter;
using Mobile.Core.BL.Core.Extensions;

namespace Mobile.Core.BL.Core.Binding.Source
{
    public class MPropertySourceBinding : MSourceBinding
    {
        protected IDisposable propertySubscriber;
        protected readonly PropertyInfo propertyInfo;

        public MPropertySourceBinding(object source, PropertyInfo info) : base(source)
        {
            if (info == null)
            {
                throw new ArgumentException("info");
            }

            propertyInfo = info;

            var sourceNotify = source as INotifyPropertyChanged;

            if (sourceNotify != null)
            {
                propertySubscriber = sourceNotify.Subscribe(SourcePropertyValueChanged);
            }
        }

        protected PropertyInfo PropertyInfo => propertyInfo;

        public override Type SourceType => PropertyInfo?.PropertyType ?? typeof (object);

        private void SourcePropertyValueChanged(object sender, PropertyChangedEventArgs e)
        {
            RiseSourceValueChanged(e);
        }

        public override void SetValue(object value, IMConverter converter = null)
        {
            PropertyInfo?.SetValue(Source, converter == null ? value : converter.Convert(value));
        }

        public override object GetValue(object[] values, IMConverter converter = null)
        {
            if (PropertyInfo == null)
            {
                return null;
            }

            return converter == null
                ? PropertyInfo.GetValue(Source)
                : converter.ConvertBack(PropertyInfo.GetValue(Source));
        }

        protected override void Dispose(bool isDisposing)
        {
            if (isDisposing)
            {
                if (propertySubscriber != null)
                {
                    propertySubscriber.Dispose();
                    propertySubscriber = null;
                }
            }
        }
    }
}