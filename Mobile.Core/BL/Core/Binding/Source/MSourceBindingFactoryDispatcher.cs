﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Mobile.Core.BL.IoCContainer;

namespace Mobile.Core.BL.Core.Binding.Source
{
    public class MSourceBindingFactoryDispatcher : IMSourceBindingFactoryDispatcher
    {
        protected readonly IDictionary<string, IMSourceBindingFactory> factories;
        private readonly IMTypeMemberDetector typeMemberDetector;

        public MSourceBindingFactoryDispatcher()
        {
            factories = new Dictionary<string, IMSourceBindingFactory>();
            typeMemberDetector = Container.Resolve<IMTypeMemberDetector>();
        }

        public virtual IMSourceBinding CreateBinding(MBindingRequest request, string name)
        {
            MemberInfo info;

            if (request.Template != null)
            {
                info = GetMemberInfo(request.Template.GetType(), name);

                if (info != null)
                    return GetBinding(request.Template, info, name);
            }

            info = GetMemberInfo(request.Source.GetType(), name);

            if (info != null)
            {
                return GetBinding(request.Source, info, name);
            }

            return null;
        }

        public virtual void AddFactory(string element, IMSourceBindingFactory factory)
        {
            if (!factories.ContainsKey(element))
            {
                factories.Add(element, factory);
            }
        }

        protected IMSourceBinding GetBinding(object source, MemberInfo info, string name)
        {
            string elementName = info.GetType().Name;

            IMSourceBindingFactory factory;

            if (!factories.TryGetValue(elementName, out factory))
            {
                Tracer.Warning("Could not find factory for {0} by {1}", name, info);
                return null;
            }

            return factory.CreateBinding(source, info);
        }

        protected MemberInfo GetMemberInfo(Type source, string name)
        {
            MemberInfo info = typeMemberDetector.GetMemberInfo(source, name);
            return info;
        }
    }
}