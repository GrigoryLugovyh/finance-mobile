﻿using System;
using System.Reflection;
using Mobile.Core.BL.Core.Binding.Converter;

namespace Mobile.Core.BL.Core.Binding.Source
{
    public class MFieldSourceBinding : MSourceBinding
    {
        protected readonly FieldInfo fieldInfo;

        public MFieldSourceBinding(object source, FieldInfo info) : base(source)
        {
            if (info == null)
            {
                throw new ArgumentException("info");
            }

            fieldInfo = info;
        }

        protected FieldInfo FieldInfo => fieldInfo;

        public override Type SourceType => FieldInfo != null ? FieldInfo.FieldType : typeof(object);

        public override void SetValue(object value, IMConverter converter = null)
        {
            if (FieldInfo == null)
            {
                return;
            }

            FieldInfo.SetValue(Source, value);
        }

        public override object GetValue(object[] values, IMConverter converter = null)
        {
            if (FieldInfo == null)
            {
                return null;
            }

            return FieldInfo.GetValue(Source);
        }
    }
}