﻿using System.Reflection;

namespace Mobile.Core.BL.Core.Binding.Source
{
    public interface IMSourceBindingFactory
    {
        IMSourceBinding CreateBinding(object source, MemberInfo info);
    }
}