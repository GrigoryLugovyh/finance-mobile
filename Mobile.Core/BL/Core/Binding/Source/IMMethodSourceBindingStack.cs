﻿using Mobile.Core.BL.Core.Infrastructure;

namespace Mobile.Core.BL.Core.Binding.Source
{
    public interface IMMethodSourceBindingStack : IMContextStack<string>
    {
    }
}