﻿using System;

namespace Mobile.Core.BL.Core.Binding
{
    public class MBindingType
    {
        public MBindingType()
        {
        }

        public MBindingType(Type type, string name)
        {
            Type = type;
            Name = name;
        }

        public Type Type { get; set; }

        public string Name { get; set; }
    }
}