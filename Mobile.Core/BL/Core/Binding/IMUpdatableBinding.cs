﻿namespace Mobile.Core.BL.Core.Binding
{
    public interface IMUpdatableBinding : IMBinding
    {
        MBindingRequest BindingRequest { get; }

        void RefreshBinding(object source);

        bool BindingSourceCreated { get; }

        bool BindingTargetCreated { get; }

        bool BindingTargetUpdated { get; }
    }
}