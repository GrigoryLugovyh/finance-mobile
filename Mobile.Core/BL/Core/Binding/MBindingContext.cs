﻿using Mobile.Core.BL.Core.ViewModels;

namespace Mobile.Core.BL.Core.Binding
{
    public class MBindingContext : IMBindingContext
    {
        public object DataContext { get; set; }

        public IMViewModel ViewModel { get; set; }
    }
}