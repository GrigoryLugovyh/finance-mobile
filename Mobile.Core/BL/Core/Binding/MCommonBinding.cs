﻿using System;
using System.ComponentModel;
using Mobile.Core.BL.Core.Binding.Converter;
using Mobile.Core.BL.Core.Binding.Selector.Extended;
using Mobile.Core.BL.Core.Binding.Source;
using Mobile.Core.BL.Core.Binding.Target;
using Mobile.Core.BL.Core.Events;
using Mobile.Core.BL.Core.Extensions;
using Mobile.Core.BL.Core.ViewModels;
using Container = Mobile.Core.BL.IoCContainer.Container;

namespace Mobile.Core.BL.Core.Binding
{
    public class MCommonBinding : MBinding, IMUpdatableBinding
    {
        protected IMSourceBinding bindingItemSource;
        protected IMSourceBinding bindingSource;
        protected IMTargetBinding bindingTarget;
        private IMConverterFactoryDispatcher converterFactoryDispatcher;
        private IMConverter currentConverter;
        private bool currentConverterDetected;

        private MBindingRequest itemBindingRequest;
        private IMSourceBindingFactoryDispatcher sourceBindingFactoryDispatcher;
        private IMTargetBindingFactoryDispatcher targetBindingFactoryDispatcher;

        public MCommonBinding(MBindingRequest request)
        {
            BindingRequest = request;
            CreateBindings();
        }

        protected IMTargetBindingFactoryDispatcher
            TargetBindingFactoryDispatcher =>
                targetBindingFactoryDispatcher ??
                (targetBindingFactoryDispatcher = Container.Resolve<IMTargetBindingFactoryDispatcher>());

        protected IMSourceBindingFactoryDispatcher
            SourceBindingFactoryDispatcher =>
                sourceBindingFactoryDispatcher ??
                (sourceBindingFactoryDispatcher = Container.Resolve<IMSourceBindingFactoryDispatcher>());

        protected IMConverterFactoryDispatcher
            ConverterFactoryDispatcher =>
                converterFactoryDispatcher ??
                (converterFactoryDispatcher = Container.Resolve<IMConverterFactoryDispatcher>());

        private IMConverter CurrentConverter
        {
            get
            {
                if (currentConverter != null && currentConverterDetected)
                {
                    return currentConverter;
                }

                if (BindingRequest.Description.ValueElementType != ValueElementParser.ValueElementType.Converter)
                {
                    currentConverterDetected = true;
                    return null;
                }

                var converterName = string.Empty;

                if (BindingRequest.Description.ParamsArray != null && BindingRequest.Description.ParamsArray.Length > 0)
                {
                    converterName = BindingRequest.Description.ParamsArray[0].ToString();
                }

                if (string.IsNullOrWhiteSpace(converterName))
                {
                    currentConverterDetected = true;
                    return null;
                }

                currentConverter = ConverterFactoryDispatcher[converterName];
                currentConverterDetected = true;

                return currentConverter;
            }
        }

        public MBindingRequest BindingRequest { get; protected set; }

        public void RefreshBinding(object source)
        {
            if (source == null || bindingSource == null)
            {
                return;
            }

            bindingSource.BindSource(source);

            UpdateTargetBinding();
        }

        public bool BindingSourceCreated { get; private set; }
        public bool BindingTargetCreated { get; private set; }
        public bool BindingTargetUpdated { get; private set; }

        private void CreateBindings()
        {
            CreateTargetBinding();
            CreateSourceBinding();
            UpdateTargetBinding();
        }

        protected virtual void UpdateTargetBinding()
        {
            if (bindingSource != null)
            {
                var value = bindingSource.GetValue(BindingRequest.Description.ParamsArray, CurrentConverter);
                SetSourceValueToTarget(value);
                BindingTargetUpdated = true;
            }
        }

        protected virtual void CreateTargetBinding()
        {
            if (BindingRequest == null)
            {
                return;
            }

            bindingTarget = TargetBindingFactoryDispatcher.CreateBinding(BindingRequest.Target,
                BindingRequest.Description.Target);

            if (bindingTarget != null)
            {
                bindingTarget.Subscribe();

                bindingTarget.TargetValueChanged += BindingTargetOnTargetValueChanged;

                bindingTarget.TargetItemValueChanged += BindingTargetOnTargetItemValueChanged;

                BindingTargetCreated = true;
            }
        }

        private void BindingTargetOnTargetValueChanged(object sender, MValueEventArgs<object> e)
        {
            SetTargetValueToSource(e.Value);
        }

        private void BindingTargetOnTargetItemValueChanged(object sender, MValueEventArgs<MTargetBindingItemArgs> e)
        {
            SetTargetItemToSource(e.Value);
        }

        protected virtual void SetTargetValueToSource(object value)
        {
            InvokeOnMainThread(() =>
            {
                if (bindingSource == null)
                {
                    return;
                }

                bindingSource.SetValue(value, CurrentConverter);
            });
        }

        protected virtual void SetTargetItemToSource(MTargetBindingItemArgs bindingItemArgs)
        {
            bindingItemSource?.SetValue(bindingItemArgs, CurrentConverter);
        }

        protected virtual void CreateSourceBinding()
        {
            if (BindingRequest == null)
            {
                return;
            }

            bindingSource = SourceBindingFactoryDispatcher.CreateBinding(BindingRequest,
                BindingRequest.Description.Value);

            if (bindingSource != null)
            {
                bindingSource.SourceValueChanged += BindingSourceOnSourceValueChanged;

                if (BindingRequest.Description.Arguments != null)
                {
                    var args = BindingRequest.Description.Arguments;

                    itemBindingRequest = new MBindingRequest(BindingRequest.Source, null,
                        new MBindingDescription(args.Item1, args.Item2, null), null);

                    bindingItemSource = SourceBindingFactoryDispatcher.CreateBinding(itemBindingRequest,
                        itemBindingRequest.Description.Value);
                }

                BindingSourceCreated = true;
            }
        }

        protected virtual void BindingSourceOnSourceValueChanged(object sender,
            MValueEventArgs<PropertyChangedEventArgs> e)
        {
            var description = e.Value.Description();

            if (description == null)
            {
                return;
            }

            var source = bindingSource.Source as IMViewModel;

            if (!string.Equals(description.PropertyName, BindingRequest.Description.Value))
            {
                if (source != null && !source.RefreshAllProperties)
                {
                    return;
                }
            }

            SetSourceValueToTargetByFunc(
                () => bindingSource.GetValue(BindingRequest.Description.ParamsArray, CurrentConverter));
        }

        protected virtual void SetSourceValueToTargetByFunc(Func<object> value)
        {
            SetSourceValueToTarget(value);
        }

        protected virtual void SetSourceValueToTarget(object value)
        {
            InvokeOnMainThread(() =>
            {
                if (bindingTarget == null)
                {
                    return;
                }

                bindingTarget.SetValue(value.ToActualValue());
            });
        }

        protected virtual void ClearTargetBinding()
        {
            if (bindingTarget != null)
            {
                bindingTarget.TargetItemValueChanged -= BindingTargetOnTargetItemValueChanged;
                bindingTarget.TargetValueChanged -= BindingTargetOnTargetValueChanged;
                bindingTarget.Dispose();
                bindingTarget = null;
            }
        }

        protected virtual void ClearSourceBinding()
        {
            if (bindingSource != null)
            {
                bindingSource.SourceValueChanged -= BindingSourceOnSourceValueChanged;
                bindingSource.Dispose();
                bindingSource = null;
            }

            if (bindingItemSource != null)
            {
                bindingItemSource.Dispose();
                bindingItemSource = null;
            }
        }

        protected virtual void ClearRequest()
        {
            if (BindingRequest != null)
            {
                BindingRequest.Dispose();
                BindingRequest = null;
            }

            if (itemBindingRequest != null)
            {
                itemBindingRequest.Dispose();
                itemBindingRequest = null;
            }
        }

        private void ClearBindings()
        {
            ClearTargetBinding();
            ClearSourceBinding();
            ClearRequest();
        }

        protected override void Dispose(bool isDisposing)
        {
            if (isDisposing)
            {
                ClearBindings();
            }
        }
    }
}