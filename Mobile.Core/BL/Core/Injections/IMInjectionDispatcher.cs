﻿using System;

namespace Mobile.Core.BL.Core.Injections
{
    public interface IMInjectionDispatcher
    {
        void Add(Type type, Func<object, object> injectionHandler);

        void Inject();
    }
}