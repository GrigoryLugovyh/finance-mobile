﻿using System;
using System.Collections.Generic;
using System.Linq;
using Mobile.Core.BL.Core.Extensions;
using Mobile.Core.BL.Core.Model;
using Mobile.Core.BL.IoCContainer;
using MoreLinq;

namespace Mobile.Core.BL.Core.Injections
{
    public class MInjectionDispatcher : IMInjectionDispatcher
    {
        private const string InjectionRequiredPathName = "Injection";

        protected Dictionary<Type, Func<object, object>> injectionsHandler = new Dictionary<Type, Func<object, object>>();

        public void Add(Type type, Func<object, object> injectionHandler)
        {
            injectionsHandler[type] = injectionHandler;
        }

        public void Inject()
        {
            var threadDispatcher = Container.Resolve<IMainThreadDispatcher>();

            threadDispatcher.InvokeAction(() => injectionsHandler.Keys.ForEach(k => k.Assembly.
                SafeGetTypes().
                Where(t => t.Name.LastIndexOf(InjectionRequiredPathName, StringComparison.OrdinalIgnoreCase) > -1).ForEach(
                    t =>
                    {
                        object target = Activator.CreateInstance(t);

                        Func<object, object> injector = injectionsHandler[k];

                        if (injector != null && !target.SafeInvoke("AddHandler", injector))
                        {
                            Tracer.Error("Injection {0} wasn't does");
                        }
                    })));
        }
    }
}