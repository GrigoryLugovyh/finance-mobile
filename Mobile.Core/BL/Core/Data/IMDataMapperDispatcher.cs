﻿namespace Mobile.Core.BL.Core.Data
{
    public interface IMDataMapperDispatcher
    {
        void Add<TIn, TOut>(IMDataMapper<TIn, TOut> mapper);

        TOut Convert<TIn, TOut>(TIn instance);
    }
}