﻿namespace Mobile.Core.BL.Core.Data
{
    public interface IMDataConsumer
    {
        object DataContext { get; set; }
    }
}