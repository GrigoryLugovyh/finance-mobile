﻿using System;

namespace Mobile.Core.BL.Core.Data
{
    public class MDataMapper<TIn, TOut> : IMDataMapper<TIn, TOut>
    {
        public MDataMapper(Func<TIn, TOut> convert)
        {
            OutType = typeof (TOut);
            InType = typeof (TIn);
            Convert = convert;
        }

        public Type OutType { get; protected set; }
        public Type InType { get; }

        public Func<TIn, TOut> Convert { get; protected set; }
    }
}