﻿using System;
using System.Collections.Generic;

namespace Mobile.Core.BL.Core.Data
{
    public class MDataMapperDispatcher : IMDataMapperDispatcher
    {
        protected Dictionary<DataMapperKey, object> converters = new Dictionary<DataMapperKey, object>();

        public void Add<TIn, TOut>(IMDataMapper<TIn, TOut> mapper)
        {
            if (mapper.Convert == null)
            {
                return;
            }

            converters[new DataMapperKey(mapper.InType, mapper.OutType)] = mapper;
        }

        public TOut Convert<TIn, TOut>(TIn instance)
        {
            object converter;
            var key = new DataMapperKey(typeof (TIn), typeof (TOut));
            if (!converters.TryGetValue(key, out converter))
            {
                Tracer.Warning("No converter for output type {0}", typeof (TOut).Name);
                return default(TOut);
            }

            if (!(converter is IMDataMapper<TIn, TOut>))
            {
                Tracer.Warning("Converter isn't convertable to IMDataMapper");
                return default(TOut);
            }

            return ((IMDataMapper<TIn, TOut>) converter).Convert(instance);
        }

        public class DataMapperKey : IEquatable<DataMapperKey>
        {
            public DataMapperKey(Type typeIn, Type typeOut)
            {
                TypeIn = typeIn;
                TypeOut = typeOut;
            }

            public Type TypeIn { get; set; }
            public Type TypeOut { get; set; }

            public bool Equals(DataMapperKey other)
            {
                return TypeIn == other.TypeIn && TypeOut == other.TypeOut;
            }

            public override bool Equals(object obj)
            {
                return (obj is DataMapperKey) && Equals((DataMapperKey) obj);
            }

            public override int GetHashCode()
            {
                return TypeIn.GetHashCode()*TypeOut.GetHashCode();
            }
        }
    }
}