﻿using System;

namespace Mobile.Core.BL.Core.Data
{
    public interface IMDataMapper<in TIn, out TOut>
    {
        Type OutType { get; }
        Type InType { get; }

        Func<TIn, TOut> Convert { get; }
    }
}