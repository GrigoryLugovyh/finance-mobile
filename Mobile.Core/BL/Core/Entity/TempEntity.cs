﻿using System;
using System.Collections.Generic;
using Mobile.Model;
using Mobile.Model.Attributes;

namespace Mobile.Core.BL.Core.Entity
{
    public class TempEntity : MEntity
    {
        public string StringProperty { get; set; }

        public double DoubleProperty { get; set; }

        public int IntProperty { get; set; }

        public DateTime DateTimeProperty { get; set; }

        public DateTimeOffset DateTimeOffsetProperty { get; set; }
    }

    public class TempRefEntity : MEntity
    {
        public int Number { get; set; }

        [Ref(typeof(List<TempEntity>))]
        public List<TempEntity> Names { get; set; }

        [Ref(typeof (Dictionary<string, List<int>>))]
        public Dictionary<string, List<int>> Address { get; set; }

        [Ref(typeof (TempEntity))]
        public TempEntity TempEntity { get; set; }
    }
}