﻿using System;

namespace Mobile.Core.BL.Core.Infrastructure
{
    public class MPropertyCallAdapter<TThis, TResult> : IMPropertyCallAdapter<TThis>
    {
        private readonly Func<TThis, TResult> getterInvocation;

        public MPropertyCallAdapter(Func<TThis, TResult> getterInvocation)
        {
            this.getterInvocation = getterInvocation;
        }

        public object InvokeGet(TThis @this)
        {
            return getterInvocation.Invoke(@this);
        }
    }
}