﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Mobile.Core.BL.Core.Events;
using Mobile.Core.BL.Core.Model;

namespace Mobile.Core.BL.Core.Infrastructure
{
    public interface IMSynchronizationContext
    {
        event EventHandler<MValueEventArgs<string>> ActionStarted;
        event EventHandler<MValueEventArgs<string>> ActionAborted;
        event EventHandler<MValueEventArgs<ActionResult>> ActionCompleted;
        bool Post(string key, Action<CancellationToken> action, bool abort = false);
        bool Abort(string key);
        Task KeyAction(string key);
        int Count { get; }
        HashSet<string> ActiveKeys { get; }
    }
}