﻿namespace Mobile.Core.BL.Core.Infrastructure
{
    public interface IMPropertyCallAdapter<in TThis>
    {
        object InvokeGet(TThis @this);
    }
}