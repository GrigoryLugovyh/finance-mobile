﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Mobile.Core.BL.Core.Events;
using Mobile.Core.BL.Core.Extensions;
using Mobile.Core.BL.Core.Model;
using Mobile.Core.BL.IoCContainer;
using MoreLinq;

namespace Mobile.Core.BL.Core.Infrastructure
{
    public class MSynchronizationContext : IMSynchronizationContext
    {
        private readonly IDictionary<string, SynchronizationContextItem> keysStorage =
            new Dictionary<string, SynchronizationContextItem>();

        private readonly object sync = new object();

        private IMainThreadDispatcher dispatcher;

        protected IMainThreadDispatcher Dispatcher
            => dispatcher ?? (dispatcher = Container.Resolve<IMainThreadDispatcher>());

        public event EventHandler<MValueEventArgs<string>> ActionStarted;
        public event EventHandler<MValueEventArgs<string>> ActionAborted;
        public event EventHandler<MValueEventArgs<ActionResult>> ActionCompleted;

        public bool Post(string key, Action<CancellationToken> action, bool abort = false)
        {
            lock (sync)
            {
                if (string.IsNullOrWhiteSpace(key))
                {
                    throw new ArgumentException("key");
                }

                if (action == null)
                {
                    throw new ArgumentException("action");
                }

                if (keysStorage.ContainsKey(key) && !abort)
                {
                    return false;
                }

                if (keysStorage.ContainsKey(key) && abort && !Abort(key))
                {
                    return false;
                }

                CancellationTokenSource tokenSource = new CancellationTokenSource();

                var actionTask = new Task(() =>
                {
                    try
                    {
                        using (tokenSource.Token.Register(Thread.CurrentThread.Abort))
                        {

                            var result = Dispatcher.InvokeAction(action, tokenSource.Token);
                            result.Name = key;
                            ActionCompleted.Rise(this, result);
                        }
                    }
                    finally
                    {
                        lock (sync)
                        {
                            if (keysStorage.ContainsKey(key))
                            {
                                keysStorage.Remove(key);
                            }
                        }
                    }
                }, tokenSource.Token);

                keysStorage.Add(key, new SynchronizationContextItem(tokenSource, actionTask));

                actionTask.Start();

                ActionStarted.Rise(this, key);

                return true;
            }
        }

        public bool Abort(string key)
        {
            lock (sync)
            {
                if (!keysStorage.ContainsKey(key))
                {
                    return true;
                }

                keysStorage[key].TokenSource.Cancel();

                keysStorage.Remove(key);

                ActionAborted.Rise(this, key);

                return true;
            }
        }

        public Task KeyAction(string key)
        {
            lock (sync)
            {
                if (!keysStorage.ContainsKey(key))
                {
                    return null;
                }

                return keysStorage[key].ActionTask;
            }
        }

        public int Count
        {
            get
            {
                lock (sync)
                {
                    return keysStorage.Count;
                }
            }
        }

        public HashSet<string> ActiveKeys
        {
            get
            {
                lock (sync)
                {
                    return keysStorage.Keys.ToHashSet();
                }
            }
        }

        internal class SynchronizationContextItem
        {
            public SynchronizationContextItem(CancellationTokenSource tokenSource, Task actionTask)
            {
                TokenSource = tokenSource;
                ActionTask = actionTask;
            }

            public CancellationTokenSource TokenSource { get; }

            public Task ActionTask { get; }
        }
    }
}