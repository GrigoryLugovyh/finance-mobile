﻿using System.Collections.Generic;
using System.Linq;

namespace Mobile.Core.BL.Core.Infrastructure
{
    public class MultiLanguageText
    {
        public MultiLanguageText()
        {
            Texts = new Dictionary<string, string>();
        }

        public MultiLanguageText(Dictionary<string, string> texts)
        {
            Texts = texts;
        }

        public string AvailibleCultures
        {
            get { return string.Join("|", Texts.Keys); }
        }

        public string SearchString
        {
            get { return string.Join("|", Texts.Values); }
        }

        public Dictionary<string, string> Texts { get; set; }

        public string Text(string culture = null)
        {
            if (culture == null)
                return Texts.Values.FirstOrDefault() ?? string.Empty;
            return Texts.ContainsKey(culture) ? Texts[culture] : string.Empty;
        }
    }
}

