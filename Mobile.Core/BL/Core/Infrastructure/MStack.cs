﻿using System.Collections.Generic;

namespace Mobile.Core.BL.Core.Infrastructure
{
    public class MStack<TContext> : Stack<TContext>, IMContextStack<TContext>
    {
        protected readonly object sync = new object();

        protected TContext previous;

        protected TContext inception;

        public virtual TContext Current
        {
            get
            {
                lock (sync)
                {
                    if (Count == 0)
                    {
                        return default(TContext);
                    }

                    return Peek();
                }
            }
        }

        public virtual new void Push(TContext context)
        {
            lock (sync)
            {
                if (Equals(context, default(TContext)))
                {
                    return;
                }

                if (context.Equals(inception) && Count > 1)
                {
                    inception = default(TContext);

                    Clear();
                }

                if (Equals(inception, default(TContext)))
                {
                    inception = context;
                }

                previous = Current;

                base.Push(context);
            }
        }
    }
}