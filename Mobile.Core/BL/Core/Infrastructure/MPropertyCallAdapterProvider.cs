﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Mobile.Core.BL.Core.Exceptions;

namespace Mobile.Core.BL.Core.Infrastructure
{
    public class MPropertyCallAdapterProvider<TInstance>
    {
        private static readonly Dictionary<string, IMPropertyCallAdapter<TInstance>> instances =
           new Dictionary<string, IMPropertyCallAdapter<TInstance>>();

        public static IMPropertyCallAdapter<TInstance> GetInstance(string propertyName)
        {
            IMPropertyCallAdapter<TInstance> instance;
            if (!instances.TryGetValue(propertyName, out instance))
            {
                var property = typeof(TInstance).GetProperty(
                    propertyName,
                    BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);

                MethodInfo getMethod;
                Delegate getterInvocation;

                if (property != null && (getMethod = property.GetGetMethod(true)) != null)
                {
                    var openGetterType = typeof(Func<,>);
                    var concreteGetterType = openGetterType
                        .MakeGenericType(typeof(TInstance), property.PropertyType);

                    getterInvocation =
                        Delegate.CreateDelegate(concreteGetterType, null, getMethod);
                }
                else
                {
                    throw new MException("MPropertyCallAdapterProvider: There is not property with name {0}", propertyName);
                }

                var openAdapterType = typeof(MPropertyCallAdapter<,>);
                var concreteAdapterType = openAdapterType
                    .MakeGenericType(typeof(TInstance), property.PropertyType);
                instance = Activator
                    .CreateInstance(concreteAdapterType, getterInvocation)
                        as IMPropertyCallAdapter<TInstance>;

                instances.Add(propertyName, instance);
            }

            return instance;
        }
    }
}