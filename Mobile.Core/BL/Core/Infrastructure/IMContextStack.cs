﻿namespace Mobile.Core.BL.Core.Infrastructure
{
    public interface IMContextStack<TContext>
    {
        TContext Current { get; }

        void Push(TContext context);
    }
}