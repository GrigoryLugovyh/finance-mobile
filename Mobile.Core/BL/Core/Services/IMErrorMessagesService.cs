﻿using System;
using Mobile.Core.BL.Core.Exceptions;

namespace Mobile.Core.BL.Core.Services
{
    public interface IMErrorMessagesService
    {
        string ProvideForExceptionMessage(Exception exception);
        object ProvideForException(Exception exception);

        MException ActualException(Exception ex);
    }
}