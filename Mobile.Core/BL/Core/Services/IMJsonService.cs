﻿namespace Mobile.Core.BL.Core.Services
{
    public interface IMJsonService
    {
        string Serialize<TData>(TData input);

        string Serialize<TData>(TData input, bool idented);

        TData Deserialize<TData>(string output);
    }
}