﻿using System;
using System.Collections.Generic;
using System.Linq;
using Mobile.Core.BL.Core.Exceptions;

#if DEBUG
using Mobile.Core.BL.Core.Extensions;
#endif

namespace Mobile.Core.BL.Core.Services
{
    public class MErrorMessagesService : IMErrorMessagesService
    {
        private Dictionary<string, object> exceptionDescriptionCandidates;

        protected virtual Dictionary<string, object> ExceptionDescriptionCandidates =>
            exceptionDescriptionCandidates ??
            (exceptionDescriptionCandidates =
                new Dictionary
                    <string, object>
                {
                    {
                        "timed out",
                        "Сервер не отвечает"
                    },
                    {
                        "invalid_grant",
                        "Неверный логин или пароль"
                    }
                });

        public virtual string ProvideForExceptionMessage(Exception exception)
        {
#if DEBUG
            return exception.ToLong();
#else
            var message = "При выполнении запроса произошла ошибка. Обратитесь в техническую поддержку.";

            var mexception = ActualException(exception);

            string codemessage;
            if (mexception != null &&
                (ProvideForCode(mexception.Code, out codemessage) ||
                 (mexception.StatusCode > 0 && ProvideForStatusCode(mexception.StatusCode, out codemessage))))
            {
                message = codemessage;
                return message;
            }

            var messageCandidate = ProvideForException(exception)?.ToString();

            if (!string.IsNullOrEmpty(messageCandidate))
            {
                message = messageCandidate;
            }

            return message;
#endif
        }

        public MException ActualException(Exception ex)
        {
            if (ex == null)
            {
                return null;
            }

            if (ex is MException)
            {
                return (MException) ex;
            }

            return ActualException(ex.InnerException);
        }

        public virtual object ProvideForException(Exception exception)
        {
            if (exception == null)
            {
                return string.Empty;
            }

            var desc = ExceptionDescriptionCandidates.Where(
                exceptionCandidate => CheckExceptionMessage(exception, exceptionCandidate.Key));

            var keyValuePairs = desc as IList<KeyValuePair<string, object>> ?? desc.ToList();

            return keyValuePairs.Any() ? keyValuePairs.First().Value : null;
        }

        protected virtual bool CheckExceptionMessage(Exception exception, string key)
        {
            if (exception == null)
            {
                return false;
            }

            return exception.Message.Contains(key) || CheckExceptionMessage(exception.InnerException, key);
        }

        protected virtual bool ProvideForCode(int code, out string message)
        {
            message = null;
            return false;
        }

        protected virtual bool ProvideForStatusCode(int statusCode, out string message)
        {
            message = null;
            return false;
        }
    }
}