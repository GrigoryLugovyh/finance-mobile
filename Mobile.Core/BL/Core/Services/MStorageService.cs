﻿using System.Collections.Generic;
using System.Linq;
using Mobile.Core.BL.IoCContainer;
using Mobile.Core.DAL;
using MoreLinq;

namespace Mobile.Core.BL.Core.Services
{
    public class MStorageService : IMStorageService
    {
        private IEnumerable<ICommonRepository> repositories;

        private IEnumerable<ICommonRepository> Repositories
        {
            get
            {
                if (repositories == null || !repositories.Any())
                {
                    repositories = Container.ResolveAll<ICommonRepository>().ToList();
                }

                return repositories;
            }
        }

        public void Reinitialize()
        {
            Tracer.Diagnostic("Storage was reinitialized in {0} ms",
                Watcher.Spot(() => { Repositories.ForEach(repository => repository.Init()); }).TotalMilliseconds);
        }

        public void Clear()
        {
            Tracer.Diagnostic("Storage was cleared in {0} ms",
                Watcher.Spot(() => { Repositories.ForEach(repository => repository.Truncate()); }).TotalMilliseconds);
        }

	    public void DeleteAllData()
	    {
			Tracer.Diagnostic("Storage data have been deleted in {0} ms",
				Watcher.Spot(() => { Repositories.ForEach(repository => repository.DeleteAll()); }).TotalMilliseconds);
		}
    }
}