﻿using Mobile.Core.BL.Core.Model;
using Mobile.Core.BL.Core.Views;

namespace Mobile.Core.BL.Core.Services
{
    public abstract class MService : MainThreadObjectDispatcher
    {
        protected IMViewDispatcher ViewDispatcher
        {
            get { return Dispatcher as IMViewDispatcher; }
        }
    }
}