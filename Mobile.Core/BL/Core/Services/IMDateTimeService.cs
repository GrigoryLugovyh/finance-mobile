﻿using System;

namespace Mobile.Core.BL.Core.Services
{
    public interface IMDateTimeService
    {
        DateTime UtcNow { get; }

        DateTime Now { get; }

        DateTimeOffset OffsetUtcNow { get; }

        DateTimeOffset OffsetNow { get; }
    }
}