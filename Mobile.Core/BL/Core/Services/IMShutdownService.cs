﻿namespace Mobile.Core.BL.Core.Services
{
	public interface IMShutdownService
	{
		void Shutdown();
	}
}