﻿namespace Mobile.Core.BL.Core.Services
{
    public interface IMStorageService
    {
        void Reinitialize();

        void Clear();

		void DeleteAllData();
	}
}