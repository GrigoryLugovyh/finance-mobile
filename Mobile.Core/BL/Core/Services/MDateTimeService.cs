﻿using System;

namespace Mobile.Core.BL.Core.Services
{
    public class MDateTimeService : MService, IMDateTimeService
    {
        public DateTime UtcNow => DateTime.UtcNow;

        public DateTime Now => DateTime.Now;

        public DateTimeOffset OffsetUtcNow => DateTimeOffset.UtcNow;

        public DateTimeOffset OffsetNow => DateTimeOffset.Now;
    }
}