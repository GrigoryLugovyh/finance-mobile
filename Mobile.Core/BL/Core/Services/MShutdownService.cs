﻿using System;
using Mobile.Core.DAL;

namespace Mobile.Core.BL.Core.Services
{
    public class MShutdownService : IMShutdownService
    {
        public void Shutdown()
        {
            lock (StorageSessionSync.Context)
            {
                Environment.Exit(0);
            }
        }
    }
}