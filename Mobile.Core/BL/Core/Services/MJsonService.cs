﻿using Newtonsoft.Json;

namespace Mobile.Core.BL.Core.Services
{
    public class MJsonService : MService, IMJsonService
    {
        public string Serialize<TData>(TData input)
        {
            return Serialize(input, false);
        }

        public string Serialize<TData>(TData input, bool idented)
        {
            return JsonConvert.SerializeObject(input, idented ? Formatting.Indented : Formatting.None);
        }

        public TData Deserialize<TData>(string output)
        {
            return JsonConvert.DeserializeObject<TData>(output);
        }
    }
}