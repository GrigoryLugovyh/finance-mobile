﻿using System;

namespace Mobile.Core.BL.Core.ViewModels
{
    public interface IMViewStart
    {
        Type ViewModelType { get; }

        void Start();
    }
}