﻿using System;
using System.Collections.Generic;

namespace Mobile.Core.BL.Core.ViewModels
{
    public class MViewModelParametersStackOwner : IMViewModelParametersStackOwner
    {
        private readonly Dictionary<Type, MViewModelParameterStack> parametersStackOwner =
            new Dictionary<Type, MViewModelParameterStack>();

        private readonly object sync = new object();

        public void SetValue<TViewModel>(params object[] parameters) where TViewModel : IMViewModel
        {
            SetValue(typeof (TViewModel), parameters);
        }

        public object[] GetValue<TViewModel>(Type viewModelType) where TViewModel : IMViewModel
        {
            return GetValue(typeof (TViewModel));
        }

        public void SetValue(Type viewModelType, params object[] parameters)
        {
            lock (sync)
            {
                MViewModelParameterStack viewModelStack;

                if (!parametersStackOwner.TryGetValue(viewModelType, out viewModelStack))
                {
                    parametersStackOwner[viewModelType] = new MViewModelParameterStack();
                }

                parametersStackOwner[viewModelType].Push(parameters);
            }
        }

        public object[] GetValue(Type viewModelType)
        {
            lock (sync)
            {
                MViewModelParameterStack viewModelStack;

                if (!parametersStackOwner.TryGetValue(viewModelType, out viewModelStack))
                {
                    parametersStackOwner[viewModelType] = new MViewModelParameterStack();
                }

                return parametersStackOwner[viewModelType].Current;
            }
        }
    }
}