﻿using Mobile.Core.BL.Core.Infrastructure;

namespace Mobile.Core.BL.Core.ViewModels
{
    public class MViewModelParameterStack : MStack<object[]>
    {
        public override object[] Current
        {
            get
            {
                lock (sync)
                {
                    if (Count == 0)
                    {
                        return null;
                    }

                    return Pop();
                }
            }
        }
    }
}