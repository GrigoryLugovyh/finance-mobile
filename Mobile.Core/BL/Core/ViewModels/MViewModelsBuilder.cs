﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Mobile.Core.BL.Core.Extensions;

namespace Mobile.Core.BL.Core.ViewModels
{
    public class MViewModelsBuilder : IMViewModelsSetter, IMViewModelsGetter
    {
        protected Dictionary<string, Type> viewModelsFullName = new Dictionary<string, Type>();
        protected Dictionary<string, Type> viewModelsName = new Dictionary<string, Type>();

        public bool TryGetByName(string name, out Type viewModelType)
        {
            return viewModelsName.TryGetValue(name, out viewModelType);
        }

        public bool TryGetByFullName(string name, out Type viewModelType)
        {
            return viewModelsFullName.TryGetValue(name, out viewModelType);
        }

        public void Set(Assembly assembly)
        {
            var viewModelTypes =
                assembly.SafeGetTypes()
                    .Where(type => !type.GetTypeInfo().IsAbstract)
                    .Where(type => !type.GetTypeInfo().IsInterface)
                    .Where(type => typeof (IMViewModel).IsAssignableFrom(type));

            foreach (var viewModelType in viewModelTypes)
            {
                Set(viewModelType);
            }
        }

        public void Set<TViewModel>() where TViewModel : IMViewModel
        {
            Set(typeof (TViewModel));
        }

        public void Set(Type viewModelType)
        {
            viewModelsName[viewModelType.Name] = viewModelType;
            viewModelsFullName[viewModelType.FullName] = viewModelType;
        }
    }
}