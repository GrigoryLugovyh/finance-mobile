﻿using System;

namespace Mobile.Core.BL.Core.ViewModels
{
    public interface IMViewModelsGetter
    {
        bool TryGetByName(string name, out Type viewModelType);

        bool TryGetByFullName(string name, out Type viewModelType);
    }
}