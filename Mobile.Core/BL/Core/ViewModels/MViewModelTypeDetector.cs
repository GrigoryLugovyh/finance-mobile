﻿using System;
using System.Linq;
using System.Reflection;
using Mobile.Core.BL.Core.Model;
using Mobile.Core.BL.Core.Views;

namespace Mobile.Core.BL.Core.ViewModels
{
    public class MViewModelTypeDetector : IMViewModelTypeDetector
    {
        protected readonly IMViewModelsGetter viewModelBuilder;
        protected readonly IMViewViewModelMapping viewModelMapping;

        public MViewModelTypeDetector(IMViewModelsGetter viewModelBuilder, IMViewViewModelMapping viewModelMapping)
        {
            this.viewModelBuilder = viewModelBuilder;
            this.viewModelMapping = viewModelMapping;
        }

        public Type DetectType(Type @type)
        {
            if (!CheckCandidateTypeIsAView(type))
                return null;

           var concrete = LookupAssociatedConcreteViewModelType(type);
            if (concrete != null)
                return concrete;

            var typeByName = LookupNamedViewModelType(type);
            if (typeByName != null)
                return typeByName;

            Tracer.Warning("No ViewModel for view type {0}", @type.Name);

            return null;
        }

        protected virtual Type LookupAssociatedConcreteViewModelType(Type @type)
        {
            var viewModelPropertyInfo = type
                .GetProperties()
                .FirstOrDefault(x => String.Equals(x.Name, MConstants.ViewModelName)
                                     && !x.PropertyType.GetTypeInfo().IsInterface
                                     && !x.PropertyType.GetTypeInfo().IsAbstract);

            if (viewModelPropertyInfo == null)
                return null;

            return viewModelPropertyInfo.PropertyType;
        }

        protected virtual Type LookupNamedViewModelType(Type @type)
        {
            var viewName = type.Name;
            var viewModelName = viewModelMapping.Map(viewName);

            Type t;
            viewModelBuilder.TryGetByName(viewModelName, out t);
            return t;
        }

        protected virtual bool CheckCandidateTypeIsAView(Type @type)
        {
            if (type == null)
                return false;

            if (type.GetTypeInfo().IsAbstract)
                return false;

            if (!typeof (IMView).IsAssignableFrom(type))
                return false;

            return true;
        }
    }
}