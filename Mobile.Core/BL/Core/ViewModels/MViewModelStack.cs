﻿using Mobile.Core.BL.Core.Infrastructure;

namespace Mobile.Core.BL.Core.ViewModels
{
    public class MViewModelStack : MStack<IMViewModel>, IMViewModelStack
    {
        public IMViewModel Previous => previous ?? inception;
    }
}