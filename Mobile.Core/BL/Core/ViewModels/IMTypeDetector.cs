﻿using System;

namespace Mobile.Core.BL.Core.ViewModels
{
    public interface IMTypeDetector
    {
        Type DetectType(Type @type);
    }
}