﻿namespace Mobile.Core.BL.Core.ViewModels
{
    public interface IMViewModelStart
    {
        void Start();
    }
}