﻿namespace Mobile.Core.BL.Core.ViewModels
{
    public interface IMViewViewModelMapping
    {
        string Map(string name);
    }
}