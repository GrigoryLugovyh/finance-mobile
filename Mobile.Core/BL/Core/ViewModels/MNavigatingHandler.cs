﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using Mobile.Core.BL.Core.Binding.Source;
using Mobile.Core.BL.Core.Enums;
using Mobile.Core.BL.Core.Extensions;
using Mobile.Core.BL.Core.Messages;
using Mobile.Core.BL.Core.Model;
using Mobile.Core.BL.Core.Views;
using Mobile.Core.BL.IoCContainer;

namespace Mobile.Core.BL.Core.ViewModels
{
    public class MNavigatingHandler : MNotifyPropertyChanged
    {
        protected readonly object navigatingSync = new object();

        private IMViewDispatcher dispatcher;
        private IMViewModelParametersStackOwner viewModelParametersStackOwner;
        private IMViewModelsCache viewModelsCache;
        private IMViewModelStack viewModelStack;
        private IMViewStart viewStart;

        private IMViewStart ViewStart => viewStart ?? (viewStart = Container.Resolve<IMViewStart>());

        protected IMViewModelStack ViewModelStack =>
            viewModelStack ?? (viewModelStack = Container.Resolve<IMViewModelStack>());

        protected IMViewModelsCache ViewModelsCache
            => viewModelsCache ?? (viewModelsCache = Container.Resolve<IMViewModelsCache>());

        protected IMViewDispatcher ViewDispatcher => dispatcher ?? (dispatcher = Dispatcher as IMViewDispatcher);

        protected IMViewModelParametersStackOwner ViewModelParametersStackOwner =>
            viewModelParametersStackOwner ??
            (viewModelParametersStackOwner = Container.Resolve<IMViewModelParametersStackOwner>());

        public virtual void RiseAction(Expression<Action> action)
        {
            var method = GetType()
                .GetMethod(action.GetMethodInfo().Name,
                    BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Default);

            if (method == null)
            {
                return;
            }

            var source = this as IMViewModel;

            if (source == null)
            {
                return;
            }

            var argument = action.GetMethodParameter();

            using (var binding = new MMethodSourceBinding(source, method))
            {
                binding.Execute(argument);
            }
        }

        protected ActionResult ShowViewModel<TViewModel>(params object[] parameters)
            where TViewModel : IMViewModel
        {
            return ShowViewModel(typeof(TViewModel), MTransition.None, parameters);
        }

        protected ActionResult ShowViewModel<TViewModel>(MTransition transition = MTransition.None,
            params object[] parameters)
            where TViewModel : IMViewModel
        {
            return ShowViewModel(typeof(TViewModel), transition, parameters);
        }

        protected ActionResultMulti ShowViewModelAndFinishAll<TViewModel>(params object[] parameters)
            where TViewModel : IMViewModel
        {
            lock (navigatingSync)
            {
                var actionResultMulti = new ActionResultMulti
                {
                    ShowViewModel(typeof(TViewModel), MTransition.Fade, parameters)
                };

                if (actionResultMulti.Success)
                {
                    if (!(ViewModelStack is Stack<IMViewModel> viewModels))
                    {
                        return default(ActionResultMulti);
                    }

                    IMViewModel viewModelCandidate;

                    while (viewModels.Any() && (viewModelCandidate = viewModels.Pop()) != null)
                    {
                        Tracer.Diagnostic($"Closing {viewModelCandidate.GetType().Name}");

                        var finishResult =
                            ViewDispatcher.FinishViewModel(new MViewModelRequest(viewModelCandidate.GetType()));

                        actionResultMulti.Add(finishResult);
                    }
                }

                return actionResultMulti;
            }
        }

        protected ActionResult ShowViewModelAndFinishThis<TViewModel>(params object[] parameters)
            where TViewModel : IMViewModel
        {
            lock (navigatingSync)
            {
                var actionResult = ShowViewModel(typeof(TViewModel), MTransition.Fade, parameters);

                if (actionResult.Success)
                {
                    return FinishViewModel();
                }

                return actionResult;
            }
        }

        protected ActionResult ShowViewModel(Type viewModelType, MTransition transition = MTransition.None,
            params object[] parameters)
        {
            lock (navigatingSync)
            {
                if (ViewDispatcher != null)
                {
                    var viewModel = ViewModelsCache.GetViewModel(viewModelType);

                    if (viewModel == null)
                    {
                        Tracer.Error($"There is not any ViewModel with type {viewModelType}");
                        return default(ActionResult);
                    }

                    if (viewModel.OneActiveInstance && ViewModelStack.Current?.GetType() == viewModel.GetType() &&
                        (ViewStart == null || ViewStart.ViewModelType != viewModel.GetType()))
                    {
                        Tracer.Warning($"Already exists ViewModel instance with type {viewModelType}");
                        return default(ActionResult);
                    }

                    if (parameters != null && parameters.Length > 0)
                    {
                        ViewModelParametersStackOwner.SetValue(viewModelType, parameters);
                    }

                    //if (ViewStart != null && ViewStart.ViewModelType == viewModel.GetType() &&
                    //    !FinishViewModel().Success)
                    //{
                    //    Tracer.Warning($"Failed to finish ViewModel type {ViewStart.ViewModelType}");
                    //    return default(ActionResult);
                    //}

                    Tracer.Diagnostic($"Showing ViewModel {viewModelType.Name}");
                    return ViewDispatcher.ShowViewModel(new MViewModelRequest(viewModelType, transition));
                }

                return default(ActionResult);
            }
        }

        protected ActionResult FinishViewModel<TViewModel>()
            where TViewModel : IMViewModel
        {
            lock (navigatingSync)
            {
                if (ViewDispatcher != null)
                {
                    Tracer.Diagnostic($"Closing {typeof(TViewModel).Name}");
                    return ViewDispatcher.FinishViewModel(new MViewModelRequest(typeof(TViewModel)));
                }

                return default(ActionResult);
            }
        }

        protected ActionResult FinishViewModel()
        {
            lock (navigatingSync)
            {
                if (ViewDispatcher != null)
                {
                    Tracer.Diagnostic("Closing current ViewModel");
                    return ViewDispatcher.FinishViewModel();
                }

                return default(ActionResult);
            }
        }

        protected ActionResult RequestAction(Action action,
            MExceptionHandlingAppearance appearance = MExceptionHandlingAppearance.None)
        {
            return ViewDispatcher?.RequestAction(action, appearance);
        }

        protected Task RequestActionAsync(Action action,
            MExceptionHandlingAppearance appearance = MExceptionHandlingAppearance.None)
        {
            return ViewDispatcher?.RequestActionAsync(action, appearance);
        }

// ReSharper disable once InconsistentNaming
        protected void RequestActionOnUI(Action<object> action)
        {
            ViewDispatcher?.RequestActionOnUI(action);
        }

// ReSharper disable once InconsistentNaming
        protected Task RequestActionOnUIAsync(Action<object> action)
        {
            return ViewDispatcher?.RequestActionOnUIAsync(action);
        }

        protected void ShowMessageQuick(string message, params object[] args)
        {
            ViewDispatcher?.ShowMessageQuick(message, args);
        }

        protected Task ShowMessageQuickAsync(string message, params object[] args)
        {
            ViewDispatcher?.ShowMessageQuickAsync(message, args);

            return null;
        }

        protected void ShowMessage(MDialogRequest request)
        {
            ViewDispatcher?.ShowMessage(request);
        }

        protected Task ShowMessageAsync(MDialogRequest request)
        {
            return ViewDispatcher?.ShowMessageAsync(request);
        }

        public Task<ActionResult> RequestActionInWaitShell(Action action, MWaitShellRequest request,
            MExceptionHandlingAppearance appearance = MExceptionHandlingAppearance.None)
        {
            return ViewDispatcher?.RequestActionInWaitShell(action, request, appearance);
        }
    }
}