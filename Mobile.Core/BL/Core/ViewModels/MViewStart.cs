﻿using System;

namespace Mobile.Core.BL.Core.ViewModels
{
    public class MViewStart<TViewModel> : MNavigatingHandler, IMViewStart
        where TViewModel : IMViewModel
    {
        public Type ViewModelType => typeof(TViewModel);

        public void Start()
        {
            ShowViewModel<TViewModel>();
        }
    }
}