﻿using System;

namespace Mobile.Core.BL.Core.ViewModels
{
    public interface IMViewModelsCache
    {
        IMViewModel GetViewModel(Type type);
    }
}