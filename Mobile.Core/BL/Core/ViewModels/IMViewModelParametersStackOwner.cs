﻿using System;

namespace Mobile.Core.BL.Core.ViewModels
{
    public interface IMViewModelParametersStackOwner
    {
        void SetValue<TViewModel>(params object[] parameters) where TViewModel : IMViewModel;
        object[] GetValue<TViewModel>(Type viewModelType) where TViewModel : IMViewModel;
        void SetValue(Type viewModelType, params object[] parameters);
        object[] GetValue(Type viewModelType);
    }
}