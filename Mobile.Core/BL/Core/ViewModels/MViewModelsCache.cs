﻿using System;
using System.Collections.Generic;
using Mobile.Core.BL.IoCContainer;

namespace Mobile.Core.BL.Core.ViewModels
{

    public class MViewModelsCache : IMViewModelsCache
    {
        private readonly object sync = new object();

        protected readonly IDictionary<Type, IMViewModel> viewModelsTypes;

        public MViewModelsCache()
        {
            viewModelsTypes = new Dictionary<Type, IMViewModel>();
        }

        public IMViewModel GetViewModel(Type type)
        {
            lock (sync)
            {
                IMViewModel viewModel;

                if (viewModelsTypes.TryGetValue(type, out viewModel))
                {
                    return viewModel;
                }

                viewModel = Container.Resolve<IMViewModel>(type);

                viewModelsTypes.Add(type, viewModel);

                return viewModel;
            }
        }
    }
}