﻿using System;

namespace Mobile.Core.BL.Core.ViewModels
{
    public interface IMViewModelsDetector
    {
        Type GetViewModelType(Type viewType);
    }
}