﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Mobile.Core.BL.Core.Extensions;
using Mobile.Core.BL.IoCContainer;

namespace Mobile.Core.BL.Core.ViewModels
{
    public class MViewsBuilder : IMViewModelViewBuilder
    {
        public IDictionary<Type, Type> Build(Assembly[] assemblies)
        {
            var detector = Container.Resolve<IMTypeDetector>();

            IEnumerable<KeyValuePair<Type, Type>> views =
                assemblies.SelectMany(assembly => assembly.SafeGetTypes(), (assembly, type) => new {assembly, type})
                    .Select(@t => new {@t, viewModelType = detector.DetectType(@t.type)})
                    .Where(@t => @t.viewModelType != null)
                    .Select(@t => new KeyValuePair<Type, Type>(@t.viewModelType, @t.@t.type));

            try
            {
                return views.ToDictionary(v => v.Key, v => v.Value);
            }
            catch (Exception ex)
            {
                Tracer.Error("MViewsBuilder exception: {0}", ex.ToLong());

                throw;
            }
        }
    }
}