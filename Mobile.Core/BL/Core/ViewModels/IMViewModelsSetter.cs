﻿using System;
using System.Reflection;

namespace Mobile.Core.BL.Core.ViewModels
{
    public interface IMViewModelsSetter
    {
        void Set(Assembly assembly);

        void Set(Type viewModelType);

        void Set<TViewModel>() where TViewModel : IMViewModel;
    }
}