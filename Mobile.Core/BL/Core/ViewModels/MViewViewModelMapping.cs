﻿using Mobile.Core.BL.Core.Model;

namespace Mobile.Core.BL.Core.ViewModels
{
    public class MViewViewModelMapping : IMViewViewModelMapping
    {
        public virtual string Postfix
        {
            get { return MConstants.ViewModelName; }
        }

        public virtual string Map(string name)
        {
            return string.Format("{0}{1}", name, Postfix);
        }
    }
}