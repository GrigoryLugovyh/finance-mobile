﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace Mobile.Core.BL.Core.ViewModels
{
    public interface IMViewModelViewBuilder
    {
        IDictionary<Type, Type> Build(Assembly[] assemblies);
    }
}