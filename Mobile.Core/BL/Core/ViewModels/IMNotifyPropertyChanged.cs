﻿using System;
using System.ComponentModel;
using System.Linq.Expressions;

namespace Mobile.Core.BL.Core.ViewModels
{
    public interface IMNotifyPropertyChanged : INotifyPropertyChanged
    {
        void RaisePropertyChanged<T>(Expression<Func<T>> property);

        void RaisePropertyChanged(string property);

        void RaisePropertyChanged(PropertyChangedEventArgs args);
    }
}