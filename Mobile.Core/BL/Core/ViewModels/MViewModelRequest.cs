﻿using System;
using Mobile.Core.BL.Core.Enums;

namespace Mobile.Core.BL.Core.ViewModels
{
    public class MViewModelRequest
    {
        public MViewModelRequest(Type viewModelType) : this(viewModelType, MTransition.None)
        {
        }

        public MViewModelRequest(Type viewModelType, MTransition transition)
        {
            ViewModelType = viewModelType;
            Transition = transition;
        }

        public Type ViewModelType { get; protected set; }

        public MTransition Transition { get; protected set; }
    }
}