﻿using System;
using System.ComponentModel;
using System.Linq.Expressions;
using Mobile.Core.BL.Core.Extensions;
using Mobile.Core.BL.Core.Model;
using Mobile.Core.BL.Core.Settings;
using Container = Mobile.Core.BL.IoCContainer.Container;

namespace Mobile.Core.BL.Core.ViewModels
{
    public class MNotifyPropertyChanged : MainThreadObjectDispatcher, IMNotifyPropertyChanged
    {
        public void RaisePropertyChanged<T>(Expression<Func<T>> property)
        {
            RaisePropertyChanged(this.GetPropertyNameFromExpression(property));
        }

        public void RaisePropertyChanged(string property)
        {
            RaisePropertyChanged(new PropertyChangedEventArgs($"{GetType().Name}.{property}"));
        }

        public void RaisePropertyChanged(PropertyChangedEventArgs e)
        {
            var action = new Action(() => OnPropertyChanged(e));

            if (Container.Resolve<IMSettings>().RisePropertyChangedOnUserInterfaceThread)
            {
                InvokeOnMainThread(action);
            }
            else
            {
                action();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            var handler = PropertyChanged;
            handler?.Invoke(this, e);
        }
    }
}