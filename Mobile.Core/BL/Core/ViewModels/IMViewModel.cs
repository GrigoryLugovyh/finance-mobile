﻿using Mobile.Core.BL.Core.Platform.Keyboard;
using Mobile.Core.BL.Core.Platform.Media;
using Mobile.Core.BL.Core.Views;

namespace Mobile.Core.BL.Core.ViewModels
{
    public interface IMViewModel
    {
        string Name { get; }
        string this[string index] { get; }
        bool RefreshAllProperties { get; set; }
        bool OneActiveInstance { get; set; }
        IMViewEditable View { get; set; }
        void InitViewModel();
        void CheckViewModel();
        void ResumeViewModel();
        void PauseViewModel();
        void DestroyViewModel();
        void BackViewModel(KeyEventArgs args);
        void ResultViewModel(ResultEventArgs args);
        void GestureViewModel(GestureEventArgs gesture);
        void UncaughtExceptionViewModel(UncaughtExceptionEventArgs exception);
        void MenuViewModel(int id);
        void OnKeyDown(KeyEventArgs args);
    }
}