﻿using Mobile.Core.BL.Core.Infrastructure;

namespace Mobile.Core.BL.Core.ViewModels
{
    public interface IMViewModelStack : IMContextStack<IMViewModel>
    {
        IMViewModel Previous { get; }
    }
}