﻿using System.Collections.Generic;
using Mobile.Core.BL.Core.Model;
using Mobile.Core.BL.Core.Platform.Keyboard;
using Mobile.Core.BL.Core.Platform.Media;
using Mobile.Core.BL.Core.Platform.Shared;
using Mobile.Core.BL.Core.Services;
using Mobile.Core.BL.Core.Views;
using Mobile.Core.BL.IoCContainer;

namespace Mobile.Core.BL.Core.ViewModels
{
    public class MViewModel : MNavigatingHandler, IMViewModel
    {
        private IMDateTimeService dateTimeService;
        private IMViewModelParametersStackOwner parametersStackOwner;
        private IMSharedPreferences sharedPreferences;

        protected string Title { get; set; }

        protected object[] Parameters { get; private set; }

        protected IMViewModelParametersStackOwner ParametersStackOwner => parametersStackOwner ??
                                                                          (parametersStackOwner =
                                                                              Container
                                                                                  .Resolve
                                                                                  <IMViewModelParametersStackOwner>());

        protected IMSharedPreferences SharedPreferences
            => sharedPreferences ?? (sharedPreferences = Container.Resolve<IMSharedPreferences>());

        protected IMDateTimeService DateTimeService
            => dateTimeService ?? (dateTimeService = Container.Resolve<IMDateTimeService>());

        protected bool HasParameters => Parameters != null && Parameters.Length > 0;

        protected bool HasTitle => View != null && !string.IsNullOrEmpty(Title);

        public string Name => GetType().Name;

        public virtual string this[string index] => index;

        public bool RefreshAllProperties { get; set; } = true;

        public bool OneActiveInstance { get; set; } = false;

        public IMViewEditable View { get; set; }

        public void InitViewModel()
        {
            RiseAction(() => Init());
        }

        public void CheckViewModel()
        {
            RiseAction(() => Check());
        }

        public void ResumeViewModel()
        {
            RiseAction(() => Resume());
        }

        public void PauseViewModel()
        {
            RiseAction(() => Pause());
        }

        public void DestroyViewModel()
        {
            RiseAction(() => Destroy());
        }

        public void BackViewModel(KeyEventArgs args)
        {
            RiseAction(() => Back(args));
        }

        public void ResultViewModel(ResultEventArgs args)
        {
            RiseAction(() => Result(args));
        }

        public void GestureViewModel(GestureEventArgs gesture)
        {
            RiseAction(() => Gesture(gesture));
        }

        public void UncaughtExceptionViewModel(UncaughtExceptionEventArgs exception)
        {
            RiseAction(() => UncaughtException(exception));
        }

        public void MenuViewModel(int id)
        {
            RiseAction(() => Menu(id));
        }

        public void OnKeyDown(KeyEventArgs args)
        {
            RiseAction(() => KeyDown(args));
        }

        public void OnFocusChanged(KeyEventArgs args)
        {
            FocusChanged(args);
        }

        protected virtual void Init()
        {
            Parameters = ParametersStackOwner.GetValue(GetType());

            if (HasParameters)
            {
                ApplyParameters();
            }

            if (HasTitle)
            {
                ApplyTitle();
            }
        }

        protected virtual void Check()
        {
            if (ViewModelStack.Current == null || ViewModelStack.Current.GetType() != GetType())
            {
                ViewModelStack.Push(this);
            }

            SharedPreferences.SetValue(new KeyValuePair<string, string>(MConstants.ViewModelDateLastActivity,
                DateTimeService.UtcNow.ToString(MConstants.DateTimeBaseFormat)));
        }

        protected virtual void ApplyTitle()
        {
            View.ActionBarTitle = Title;
        }

        protected virtual void ApplyParameters()
        {
        }

        protected virtual void Resume()
        {
        }

        protected virtual void Pause()
        {
        }

        protected virtual void Destroy()
        {
            View = null;
        }

        protected virtual void Back(KeyEventArgs args)
        {
        }

        protected virtual void KeyDown(KeyEventArgs args)
        {
        }

        protected virtual void FocusChanged(KeyEventArgs args)
        {
        }

        protected virtual void Result(ResultEventArgs args)
        {
        }

        protected virtual void Gesture(GestureEventArgs gesture)
        {
        }

        protected virtual void UncaughtException(UncaughtExceptionEventArgs exception)
        {
        }

        protected virtual void Menu(int id)
        {
        }

        public override string ToString()
        {
            return GetType().Name;
        }
    }
}