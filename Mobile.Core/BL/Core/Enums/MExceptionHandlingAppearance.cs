﻿namespace Mobile.Core.BL.Core.Enums
{
    public enum MExceptionHandlingAppearance
    {
        None = 0,

        Toast,

        Dialog,

        Throw
    }
}