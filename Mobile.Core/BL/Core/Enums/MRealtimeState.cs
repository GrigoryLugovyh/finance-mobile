﻿namespace Mobile.Core.BL.Core.Enums
{
    public enum MRealtimeState
    {
        Activate,
        Start,
        Restart,
        Resume,
        Pause,
        Deactivate,
        Destroy,
        Result
    }
}