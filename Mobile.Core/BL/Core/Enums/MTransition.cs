﻿namespace Mobile.Core.BL.Core.Enums
{
    public enum MTransition
    {
        None = 0,
        Fade,
        ToCenter,
        FromCenter,
        Scale,
        ToLeft,
        OutLeft,
        ToRight,
        OutRight
    }
}