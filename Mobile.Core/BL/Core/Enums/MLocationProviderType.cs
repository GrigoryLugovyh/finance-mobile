﻿namespace Mobile.Core.BL.Core.Enums
{
    public enum MLocationProviderType
    {
        Passive,

        Network,

        Gps
    }
}