﻿namespace Mobile.Core.BL.Core.Enums
{
    public enum MInitState
    {
        Uninitialized,

        Initializing,

        BaseInitialized,

        Initialized
    }
}