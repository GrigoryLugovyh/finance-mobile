﻿using System;

namespace Mobile.Core.BL.Core.Enums
{
    [Flags]
    public enum MSelectorType
    {
        Command = 0,
        Parameter,
        Target,
        Value,
        Attribute
    }
}