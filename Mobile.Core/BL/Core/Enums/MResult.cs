﻿namespace Mobile.Core.BL.Core.Enums
{
    public enum MResult
    {
        Ok = -1,
        Canceled = 0,
        FirstUser = 1
    }
}