﻿namespace Mobile.Core.BL.Core.Messages
{
    public class MWaitShellRequest
    {
        public MWaitShellRequest(string title, string message, params object[] args)
        {
            Title = title;
            Message = string.Format(message, args);
        }

        public string Title { get; private set; }

        public string Message { get; private set; }
    }
}