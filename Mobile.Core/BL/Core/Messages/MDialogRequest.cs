﻿using System;
using System.Linq;

namespace Mobile.Core.BL.Core.Messages
{
    public enum DialogType
    {
        DtNoIcon,
        DtWarning,
        DtError,
        DtConfirmation,
        DtInformation,
        DtWait
    }

    public class MDialogRequest
    {
        public MDialogRequest()
        {
        }

        public MDialogRequest(Action positiveAction, string positiveButtonText, string title, string message,
            params object[] args)
            : this(
                positiveAction, null, null, positiveButtonText, null, null, null, title, message,
                DialogType.DtInformation, args)
        {
        }

        public MDialogRequest(Action positiveAction, Action negativeAction, string positiveButtonText,
            string negativeButtonText, string title, string message, params object[] args)
            : this(
                positiveAction, negativeAction, null, positiveButtonText, negativeButtonText, null, null, title, message,
                DialogType.DtInformation,
                args)
        {
        }

        public MDialogRequest(string message, DialogType dialogType, params object[] args)
            : this(string.Empty, message, "Ok", dialogType, args)
        {
        }

        public MDialogRequest(string title, string message, string buttontext, DialogType dialogType,
            params object[] args)
        {
            Title = title;
            Message = string.Format(message, args);
            NeutralButton = new DialogRequestButton {Action = () => { }, ButtonText = buttontext};
            DialogType = dialogType;
        }

        public MDialogRequest(Action positiveAction, Action negativeAction, Action neutralAction,
            string positiveButtonText, string negativeButtonText, string neutralButtonText, int? icon,
            string title, string message, DialogType dialogType, params object[] args)
        {
            Title = title;
            Message = string.Format(message, args);
            Icon = icon;
            PositiveButton = new DialogRequestButton {Action = positiveAction, ButtonText = positiveButtonText};
            NegativeButton = new DialogRequestButton {Action = negativeAction, ButtonText = negativeButtonText};
            NeutralButton = new DialogRequestButton {Action = neutralAction, ButtonText = neutralButtonText};
            DialogType = dialogType;
            bool modal;

            if (args != null && args.Any() && bool.TryParse(args[0].ToString(), out modal))
                Modal = modal;
        }

        public DialogType DialogType { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public int? Icon { get; protected set; }
        public Action CloseLinkAction { get; set; }
        public bool HasCloseLink { get; set; }
        public bool Modal { get; set; }
        public DialogRequestButton NeutralButton { get; set; }
        public DialogRequestButton PositiveButton { get; set; }
        public DialogRequestButton NegativeButton { get; set; }

        public override string ToString()
        {
            return Message;
        }

        public class DialogRequestButton
        {
            public Action Action { get; set; }
            public string ButtonText { get; set; }
            public string ButtonTextComment { get; set; }
        }
    }
}