﻿using System;

namespace Mobile.Core.BL.Core.Messages
{
    public class MWaitDialogRequest
    {
        public MWaitDialogRequest()
        {
        }

        public MWaitDialogRequest(string title, Action action, bool canCancelAction, string message,
            params object[] args)
        {
            Title = title;
            Message = string.Format(message, args);
            Action = action;
            CanCancelAction = canCancelAction;
        }

        public Action Action { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public bool CanCancelAction { get; set; }
    }
}