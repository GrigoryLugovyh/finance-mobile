﻿using System;
using Mobile.Core.BL.Core.Platform.Watch;
using Mobile.Core.BL.IoCContainer;

namespace Mobile.Core.BL.Core
{
    public sealed class Watcher
    {
        private static readonly object sync = new object();

        private static volatile IMWatcher instance;

        private static IMWatcher Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (sync)
                    {
                        if (instance == null)
                        {
                            instance = Container.Resolve<IMWatcher>();
                        }
                    }
                }

                return instance;
            }
        }

        public static TimeSpan Spot(Action action)
        {
            return Instance.Spot(action);
        }
    }
}