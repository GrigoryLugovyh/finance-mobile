﻿using Mobile.Core.BL.Core.Platform.Trace;
using Mobile.Core.BL.IoCContainer;

namespace Mobile.Core.BL.Core
{
    public sealed class Tracer
    {
        private static readonly object sync = new object();

        private static volatile IMTrace instance;

        public static IMTrace Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (sync)
                    {
                        if (instance == null)
                        {
                            instance = Container.Resolve<IMTrace>();
                        }
                    }
                }

                return instance;
            }
        }

        private static void TaggedTrace(MTraceLevel level, string tag, string message, params object[] args)
        {
            Instance?.Trace(level, tag, message, args);
        }

        public static void Diagnostic(string message, params object[] args)
        {
            TaggedTrace(MTraceLevel.Diagnostic, string.Empty, message, args);
        }

        public static void Warning(string message, params object[] args)
        {
            TaggedTrace(MTraceLevel.Warning, string.Empty, message, args);
        }

        public static void Error(string message, params object[] args)
        {
            TaggedTrace(MTraceLevel.Error, string.Empty, message, args);
        }
    }
}