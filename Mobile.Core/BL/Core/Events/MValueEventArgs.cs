﻿using System;

namespace Mobile.Core.BL.Core.Events
{
    public class MValueEventArgs<T> : EventArgs
    {
        public MValueEventArgs(T value)
        {
            Value = value;
        }

        public T Value { get; protected set; }
    }
}