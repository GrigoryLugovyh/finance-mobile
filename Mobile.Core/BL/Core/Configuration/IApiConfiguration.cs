﻿using System.Collections.Generic;

namespace Mobile.Core.BL.Core.Configuration
{
    public interface IApiConfiguration
    {
        int? Version { get; }

        List<string> ApiHosts { get; set; }

        bool Localize { get; }
    }
}