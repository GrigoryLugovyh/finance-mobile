﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading;
using Mobile.Core.BL.Core.Enums;
using Mobile.Core.BL.Core.Events;
using Mobile.Core.BL.Core.Exceptions;
using Mobile.Core.BL.Core.Extensions;

namespace Mobile.Core.BL.Core.Model
{
    public class ActionResult
    {
        public string Name { get; set; }

        public bool Success { get; set; }

        public object Result { get; set; }

        public Exception Exception { get; set; }
    }

    public class ActionResultMulti : List<ActionResult>
    {
        public bool Success => this.Any() && this.All(result => result.Success);

        public IEnumerable<object> Results
            => this.Where(result => result.Result != null).Select(result => result.Result);

        public IEnumerable<Exception> Exceptions
            => this.Where(result => result.Exception != null).Select(result => result.Exception);
    }

    public class MainThreadDispatcher : IMainThreadDispatcher
    {
        public event EventHandler<MValueEventArgs<MExceptionRequest>> ExceptionCall;

        public ActionResult InvokeAction(Action action,
            MExceptionHandlingAppearance appearance = MExceptionHandlingAppearance.None)
        {
            ActionResult result = new ActionResult();

            try
            {
                action?.Invoke();

                result.Success = true;
            }
            catch (Exception ex)
            {
                if (appearance == MExceptionHandlingAppearance.Throw)
                {
                    throw;
                }

                ProcessActionException(result, ex, appearance);
            }

            return result;
        }

        public ActionResult InvokeAction<TValue>(Action<TValue> action, TValue value,
            MExceptionHandlingAppearance appearance = MExceptionHandlingAppearance.None)
        {
            ActionResult result = new ActionResult();

            try
            {
                action?.Invoke(value);

                result.Success = true;
            }
            catch (Exception ex)
            {
                if (appearance == MExceptionHandlingAppearance.Throw)
                {
                    throw;
                }

                ProcessActionException(result, ex, appearance);
            }

            return result;
        }

        public ActionResult InvokeFunc(Func<object> function,
            MExceptionHandlingAppearance appearance = MExceptionHandlingAppearance.None)
        {
            ActionResult result = new ActionResult();

            try
            {
                if (function != null)
                {
                    result.Result = function();
                }

                result.Success = true;
            }
            catch (Exception ex)
            {
                if (appearance == MExceptionHandlingAppearance.Throw)
                {
                    throw;
                }

                ProcessActionException(result, ex, appearance);
            }

            return result;
        }

        public ActionResultMulti InvokeActions(Action main, Action<object> exception = null, Action<object> final = null,
            MExceptionHandlingAppearance appearance = MExceptionHandlingAppearance.None)
        {
            ActionResultMulti results = new ActionResultMulti();

            Action<Action<object>, object> internalActionProcessor = (action, value) =>
            {
                if (action != null)
                {
                    var actionResult = InvokeAction(action, value, appearance);
                    actionResult.Name = nameof(action);
                    results.Add(actionResult);
                }
            };

            try
            {
                main?.Invoke();

                results.Add(new ActionResult {Name = nameof(main), Success = true});
            }
            catch (Exception ex)
            {
                if (appearance == MExceptionHandlingAppearance.Throw)
                {
                    throw;
                }

                results.Add(new ActionResult {Name = nameof(main), Success = false});

                ProcessActionException(results.First(), ex, appearance);

                internalActionProcessor(exception, ex);
            }
            finally
            {
                internalActionProcessor(final, null);
            }

            return results;
        }

        protected void ProcessActionException(ActionResult result, Exception ex, MExceptionHandlingAppearance appearance)
        {
            Exception candidateException;

            //жесткое прерывание потока через Thread.Abort всегда приводит к исключению. считаем что это ок и супрессим его здесь
            if (ex is ThreadAbortException) return;

            if (ex is TargetInvocationException)
            {
                candidateException = ex.InnerException;
            }
            else
            {
                candidateException = ex;
            }

            result.Exception = candidateException;

            Tracer.Error("Exception while invoking method: {0}", candidateException.ToLong());

            ExceptionCall.Rise(this, new MExceptionRequest(candidateException, appearance));
        }
    }
}