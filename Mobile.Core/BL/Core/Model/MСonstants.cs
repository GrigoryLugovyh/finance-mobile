﻿namespace Mobile.Core.BL.Core.Model
{
    public static class MConstants
    {
        public const int NoContext = 0;

        public const string AppEntryPoint = "EntryPoint";

        public const string ViewModelName = "ViewModel";

        public const string ViewModelDateLastActivity = "LastActivity";

        public const string DateTimeBaseFormat = "dd.MM.yyyy HH:mm:ss";

        public class Platform
        {
            public const string Android = "android";

            public const string Windows = "windows";
        }
    }
}