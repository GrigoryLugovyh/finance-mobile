﻿using System;
using Mobile.Core.BL.Core.Enums;
using Mobile.Core.BL.IoCContainer;

namespace Mobile.Core.BL.Core.Model
{
    public class MainThreadObjectDispatcher
    {
        private IMainThreadDispatcher dispatcher;

        protected IMainThreadDispatcher Dispatcher => dispatcher ?? (dispatcher = Container.Resolve<IMainThreadDispatcher>());

        protected ActionResult InvokeOnMainThread(Action action,
            MExceptionHandlingAppearance appearance = MExceptionHandlingAppearance.None)
        {
            if (Dispatcher != null)
            {
                return Dispatcher.InvokeAction(action, appearance);
            }

            Tracer.Warning("MainThreadDispatcher not resolved");

            return default(ActionResult);
        }

        protected ActionResult InvokeFuncOnMainThread(Func<object> function,
            MExceptionHandlingAppearance appearance = MExceptionHandlingAppearance.None)
        {
            if (Dispatcher != null)
            {
                return Dispatcher.InvokeFunc(function, appearance);
            }

            Tracer.Warning("MainThreadDispatcher not resolved");

            return default(ActionResult);
        }
    }
}