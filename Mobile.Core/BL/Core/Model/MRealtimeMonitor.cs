﻿using System;
using Mobile.Core.BL.Core.Enums;
using Mobile.Core.BL.Core.Events;
using Mobile.Core.BL.Core.Extensions;

namespace Mobile.Core.BL.Core.Model
{
    public class MRealtimeMonitor : IMRealtimeMonitor
    {
        public event EventHandler<MValueEventArgs<MRealtimeObject>> RealtimeChanged;

        protected virtual void OnRealtimeChanged(MRealtimeState s, params object[] args)
        {
            RealtimeChanged.Rise(this, new MRealtimeObject(s, args));
        }

        public class MRealtimeObject : Tuple<MRealtimeState, object[]>
        {
            public MRealtimeObject(MRealtimeState state, object[] @params) : base(state, @params)
            {
            }

            public MRealtimeState State => Item1;

            public object[] Parameters => Item2;
        }
    }
}