﻿using Mobile.Core.BL.Core.Binding.Decorator;
using Mobile.Core.BL.Core.Data;
using Mobile.Core.BL.Core.Injections;

namespace Mobile.Core.BL.Core.Model
{
    public interface IMApplication
    {
        string ApplicationName { get; }

        void Initialization();

        void InitInjections(IMInjectionDispatcher dispatcher);

        void InitDataMapper(IMDataMapperDispatcher dispatcher);

        void InitDecorator(IMDecoratorsOwner decoratorsOwner);
    }
}