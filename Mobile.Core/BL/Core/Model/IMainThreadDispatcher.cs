﻿using System;
using Mobile.Core.BL.Core.Enums;
using Mobile.Core.BL.Core.Events;
using Mobile.Core.BL.Core.Exceptions;

namespace Mobile.Core.BL.Core.Model
{
    public interface IMainThreadDispatcher
    {
        event EventHandler<MValueEventArgs<MExceptionRequest>> ExceptionCall;

        ActionResult InvokeAction(Action action, MExceptionHandlingAppearance appearance = MExceptionHandlingAppearance.None);

        ActionResult InvokeAction<TValue>(Action<TValue> action, TValue value,
            MExceptionHandlingAppearance appearance = MExceptionHandlingAppearance.None);

        ActionResult InvokeFunc(Func<object> function, MExceptionHandlingAppearance appearance = MExceptionHandlingAppearance.None);

        ActionResultMulti InvokeActions(Action main, Action<object> exception = null, Action<object> final = null,
            MExceptionHandlingAppearance appearance = MExceptionHandlingAppearance.None);
    }
}