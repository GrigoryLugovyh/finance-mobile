﻿using System;
using Mobile.Core.BL.Core.Events;

namespace Mobile.Core.BL.Core.Model
{
    public interface IMRealtimeMonitor
    {
        event EventHandler<MValueEventArgs<MRealtimeMonitor.MRealtimeObject>> RealtimeChanged;
    }
}