﻿using System;
using System.ComponentModel;
using Mobile.Core.BL.Core.Binding;
using Container = Mobile.Core.BL.IoCContainer.Container;

namespace Mobile.Core.BL.Core.Extensions
{
    public static class NotifyPropertyExtension
    {
        public static IMNotifyPropertyChangedSubscription Subscribe(this INotifyPropertyChanged source,
            EventHandler<PropertyChangedEventArgs> eventHandler)
        {
            var factory = Container.Resolve<IMNotifyPropertyChangedSubscriptionFactory>();

            if (factory == null)
            {
                Tracer.Warning("Subscribe: notifyPropertyChangedSubscriptionFactory not resolved");
                return null;
            }

            return factory.Create(source, eventHandler);
        }

        public static PropertyChangedDescription Description(this PropertyChangedEventArgs propertyChanged)
        {
            var descriptionCandidate = propertyChanged.PropertyName.Split('.');

            if (descriptionCandidate.Length != 2)
            {
                return null;
            }

            return new PropertyChangedDescription(descriptionCandidate[0], descriptionCandidate[1]);
        }
    }
}