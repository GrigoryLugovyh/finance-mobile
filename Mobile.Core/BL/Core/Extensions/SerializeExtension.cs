﻿using Mobile.Core.BL.Core.Services;
using Mobile.Core.BL.IoCContainer;

namespace Mobile.Core.BL.Core.Extensions
{
    public static class SerializeExtension
    {
        public static string Serialize(this object instance)
        {
            return Container.Resolve<IMJsonService>().Serialize(instance);
        }

        public static TInstance Deserialize<TInstance>(this string value)
        {
            return Container.Resolve<IMJsonService>().Deserialize<TInstance>(value);
        }
    }
}