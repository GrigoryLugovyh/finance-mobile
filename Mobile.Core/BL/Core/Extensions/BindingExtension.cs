﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Mobile.Core.BL.Core.Assemblies;
using Mobile.Core.BL.Core.Binding;
using Mobile.Core.BL.Core.Binding.Source;
using Mobile.Core.BL.Core.Binding.Target;
using Mobile.Core.BL.IoCContainer;

namespace Mobile.Core.BL.Core.Extensions
{
    public static class BindingExtension
    {
        private static IMBindingContainer bindingContainer;
        private static IMCacheTypeInflate cacheTypeInflate;

        public static IMBindingContainer BindingContainer
            => bindingContainer ?? (bindingContainer = Container.Resolve<IMBindingContainer>());

        public static IMCacheTypeInflate CacheTypeInflate => cacheTypeInflate ??
                                                             (cacheTypeInflate = Container
                                                                 .Resolve<IMCacheTypeInflate>());

        public static void Add<TWidget>(this IMTargetBindingFactoryDispatcher dispatcher, string name,
            Func<TWidget, IMTargetBinding> binding)
            where TWidget : class
        {
            dispatcher.AddFactory(new MCommonTargetBindingFactory<TWidget>(name, binding));
            CacheTypeInflate.Add(typeof(TWidget));
        }

        public static void Add(this IMSourceBindingFactoryDispatcher dispatcher,
            string elementName, Func<object, MemberInfo, IMSourceBinding> binding)
        {
            dispatcher.AddFactory(elementName, new MCommonSourceBindingFactory(binding));
        }

        public static void RegisterBindings(this IEnumerable<IMUpdatableBinding> bindings, object view, string key,
            string scope = "")
        {
            BindingContainer.RegisterBindings(key, view, bindings, scope);
        }

        public static void UnregisterBindings(string key)
        {
            BindingContainer.UnregisterBindings(key);
        }

        public static void UnregisterBindingsBySource(string key, object source)
        {
            BindingContainer.UnregisterBindingsBySource(key, source);
        }

        [Obsolete("Use UnregisterBindingsBySource for partial unregister")]
        public static void UnregisterBindingsByScope(string key, string scope)
        {
            BindingContainer.UnregisterBindingsByScope(key, scope);
        }
    }
}