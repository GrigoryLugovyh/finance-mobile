﻿using System;
using Mobile.Core.BL.Core.Events;

namespace Mobile.Core.BL.Core.Extensions
{
    public static class DelegateExtension
    {
        public static void Rise(this EventHandler handler, object sender = null)
        {
            var handlerCopy = handler;
            handlerCopy?.Invoke(sender, EventArgs.Empty);
        }

        public static T Rise<T>(this EventHandler<MValueEventArgs<T>> handler, object sender, T value)
        {
            var handlerCopy = handler;
            handlerCopy?.Invoke(sender, new MValueEventArgs<T>(value));
            return value;
        }
    }
}