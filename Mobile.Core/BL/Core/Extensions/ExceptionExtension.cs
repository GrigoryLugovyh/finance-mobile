﻿using System;
using Mobile.Core.BL.Core.Exceptions;

namespace Mobile.Core.BL.Core.Extensions
{
    public static class ExceptionExtension
    {
        public static string ToLong(this Exception exception)
        {
            if (exception == null)
            {
                return "null exception";
            }

            if (exception.InnerException != null)
            {
                var innerExceptionText = exception.InnerException.ToLong();
                return string.Format("{0}: {1}\n\t{2}\nInnerException was {3}",
                    exception.GetType().Name,
                    exception.Message,
                    exception.StackTrace,
                    innerExceptionText);
            }

            return string.Format("{0}: {1}\n\t{2}",
                exception.GetType().Name,
                exception.Message,
                exception.StackTrace);
        }

        public static Exception MExpand(this Exception exception)
        {
            if (exception is MException)
            {
                return exception;
            }

            return MExpand(exception, exception.Message);
        }

        public static Exception MExpand(this Exception exception, string message)
        {
            return new MException(exception, message);
        }

        public static Exception MExpand(this Exception exception, string messageFormat, params object[] formatArguments)
        {
            return new MException(exception, messageFormat, formatArguments);
        }
    }
}