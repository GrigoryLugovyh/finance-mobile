﻿using System;
using System.Linq.Expressions;

namespace Mobile.Core.BL.Core.Extensions
{
    public static class ExpressionExtension
    {
        public static Expression<Func<T, bool>> And<T>(this Expression<Func<T, bool>> left,
            Expression<Func<T, bool>> right)
        {
            var body = Expression.And(left.Body, right.Body);

            return Expression.Lambda<Func<T, bool>>(body, left.Parameters[0]);
        }

        public static Expression<Func<T, bool>> Or<T>(this Expression<Func<T, bool>> left,
            Expression<Func<T, bool>> right)
        {
            var body = Expression.Or(left.Body, right.Body);

            return Expression.Lambda<Func<T, bool>>(body, left.Parameters[0]);
        }
    }
}