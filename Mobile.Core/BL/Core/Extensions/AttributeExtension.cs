﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Mobile.Core.BL.Core.Extensions
{
    public static class AttributeExtension
    {
        public static TAttribute GetAttribute<TAttribute>(this MemberInfo member)
            where TAttribute : Attribute
        {
            var attribute = member.GetAttributes<TAttribute>().FirstOrDefault();

            if (attribute == null)
            {
                return default(TAttribute);
            }

            return attribute;
        }

        public static IEnumerable<TAttribute> GetAttributes<TAttribute>(this MemberInfo member, bool inherit = true)
            where TAttribute : Attribute
        {
            return member.GetCustomAttributes(typeof(TAttribute), inherit).Cast<TAttribute>();
        }
    }
}