﻿using System;
using System.Globalization;

namespace Mobile.Core.BL.Core.Extensions
{
    public static class LongExtension
    {
        public static string ToReadableSize(this long size, int digit = 1)
        {
            Func<double, string> floatForm = l => Math.Round(l, digit).ToString(CultureInfo.InvariantCulture);

            const long kb = 1 * 1024;
            const long mb = kb * 1024;
            const long gb = mb * 1024;
            const long tb = gb * 1024;
            const long pb = tb * 1024;
            const long eb = pb * 1024;

            if (size < kb) return floatForm(size) + " байт";
            if (size >= kb && size < mb) return floatForm((double)size / kb) + " Кбайт";
            if (size >= mb && size < gb) return floatForm((double)size / mb) + " Мбайт";
            if (size >= gb && size < tb) return floatForm((double)size / gb) + " Гбайт";
            if (size >= tb && size < pb) return floatForm((double)size / tb) + " Тбайт";
            if (size >= pb && size < eb) return floatForm((double)size / pb) + " Пбайт";
            if (size >= eb) return floatForm((double)size / eb) + " Эбайт";

            return "???";
        }
    }
}