﻿using System;
using Mobile.Core.BL.Core.Model;

namespace Mobile.Core.BL.Core.Extensions
{
    public static class ActionExtension
    {
        public static object ToActualValue(this object value)
        {
            if (value is Func<object>)
            {
                return (value as Func<object>).Invoke();
            }

            if (value is ActionResult)
            {
                return (value as ActionResult).Result;
            }

            return value;
        }
    }
}