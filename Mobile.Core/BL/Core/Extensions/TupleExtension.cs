﻿using System;

namespace Mobile.Core.BL.Core.Extensions
{
    public static class TupleExtension
    {
        public static Action<Tuple<TFirst, TSecond>> CombineWith<TFirst, TSecond>(
            this Action<TFirst> actionFirst,
            Action<TSecond> actionSecond)
        {
            return t =>
            {
                if (actionFirst != null)
                {
                    actionFirst(t.Item1);
                }

                if (actionSecond != null)
                {
                    actionSecond(t.Item2);
                }
            };
        }

        public static Action<Tuple<TFirst, TSecond, TThird>> CombineWith<TFirst, TSecond, TThird>(
            this Action<Tuple<TFirst, TSecond>> tuple,
            Action<TThird> action)
        {
            return t =>
            {
                tuple.Execute(t.Item1, t.Item2);

                tuple(Tuple.Create(t.Item1, t.Item2));

                if (action != null)
                {
                    action(t.Item3);
                }
            };
        }

        public static Action<Tuple<TFirst, TSecond, TThird, TFourth>> CombineWith<TFirst, TSecond, TThird, TFourth>(
            this Action<Tuple<TFirst, TSecond, TThird>> tuple,
            Action<TFourth> action)
        {
            return t =>
            {
                tuple.Execute(t.Item1, t.Item2, t.Item3);

                if (action != null)
                {
                    action(t.Item4);
                }
            };
        }

        public static Action<Tuple<TFirst, TSecond, TThird, TFourth, TFifth>> CombineWith
            <TFirst, TSecond, TThird, TFourth, TFifth>(
            this Action<Tuple<TFirst, TSecond, TThird, TFourth>> tuple,
            Action<TFifth> action)
        {
            return t =>
            {
                tuple.Execute(t.Item1, t.Item2, t.Item3, t.Item4);

                if (action != null)
                {
                    action(t.Item5);
                }
            };
        }

        public static Action<Tuple<TFirst, TSecond, TThird, TFourth, TFifth, TSixth>> CombineWith
            <TFirst, TSecond, TThird, TFourth, TFifth, TSixth>(
            this Action<Tuple<TFirst, TSecond, TThird, TFourth, TFifth>> tuple,
            Action<TSixth> action)
        {
            return t =>
            {
                tuple.Execute(t.Item1, t.Item2, t.Item3, t.Item4, t.Item5);

                if (action != null)
                {
                    action(t.Item6);
                }
            };
        }

        public static Action<Tuple<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TSeventh>> CombineWith
            <TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TSeventh>(
            this Action<Tuple<TFirst, TSecond, TThird, TFourth, TFifth, TSixth>> tuple,
            Action<TSeventh> action)
        {
            return t =>
            {
                tuple.Execute(t.Item1, t.Item2, t.Item3, t.Item4, t.Item5, t.Item6);

                if (action != null)
                {
                    action(t.Item7);
                }
            };
        }

        public static void Execute<TFirst, TSecond>(
            this Action<Tuple<TFirst, TSecond>> tuple,
            TFirst itemFirst,
            TSecond itemSecond)
        {
            tuple(Tuple.Create(itemFirst, itemSecond));
        }

        public static void Execute<TFirst, TSecond, TThird>(
            this Action<Tuple<TFirst, TSecond, TThird>> tuple,
            TFirst itemFirst,
            TSecond itemSecond,
            TThird itemThird)
        {
            tuple(Tuple.Create(itemFirst, itemSecond, itemThird));
        }

        public static void Execute<TFirst, TSecond, TThird, TFourth>(
            this Action<Tuple<TFirst, TSecond, TThird, TFourth>> tuple,
            TFirst itemFirst,
            TSecond itemSecond,
            TThird itemThird,
            TFourth itemFourth)
        {
            tuple(Tuple.Create(itemFirst, itemSecond, itemThird, itemFourth));
        }

        public static void Execute<TFirst, TSecond, TThird, TFourth, TFifth>(
            this Action<Tuple<TFirst, TSecond, TThird, TFourth, TFifth>> tuple,
            TFirst itemFirst,
            TSecond itemSecond,
            TThird itemThird,
            TFourth itemFourth,
            TFifth itemFifth)
        {
            tuple(Tuple.Create(itemFirst, itemSecond, itemThird, itemFourth, itemFifth));
        }

        public static void Execute<TFirst, TSecond, TThird, TFourth, TFifth, TSixth>(
            this Action<Tuple<TFirst, TSecond, TThird, TFourth, TFifth, TSixth>> tuple,
            TFirst itemFirst,
            TSecond itemSecond,
            TThird itemThird,
            TFourth itemFourth,
            TFifth itemFifth,
            TSixth itemSixth)
        {
            tuple(Tuple.Create(itemFirst, itemSecond, itemThird, itemFourth, itemFifth, itemSixth));
        }
    }
}