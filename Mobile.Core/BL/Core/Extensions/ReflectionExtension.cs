﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Mobile.Core.BL.Core.Extensions
{
    public static class ReflectionExtension
    {
        public static bool SafeInvoke(this object instance, string methodName, params object[] arguments)
        {
            try
            {
                MethodInfo method =
                    instance.GetType().GetMethodInfo().FirstOrDefault(m => string.Equals(m.Name, methodName));

                if (method == null)
                {
                    return false;
                }

                method.Invoke(instance, arguments);

                return true;
            }
            catch (Exception ex)
            {
                Tracer.Warning("SafeInvoke {0} in {1} => {2}", methodName, instance.GetType().Name, ex.Message);
                return false;
            }
        }

        public static IEnumerable<Type> GetAssignableTypes(this Assembly assembly, Type @type)
        {
            return assembly.SafeGetTypes().Where(t => t.IsAssignableFrom(@type)).Select(t => t);
        }

        public static IEnumerable<Type> GetDefinedTypes(this Assembly assembly)
        {
            return assembly.DefinedTypes.Select(a => a.AsType());
        }

        public static EventInfo GetEventInfo(this Type type, string name)
        {
            return type.GetRuntimeEvent(name);
        }

        public static IEnumerable<Type> GetInterfaceInfo(this Type type)
        {
            return type.GetTypeInfo().ImplementedInterfaces;
        }

        public static bool IsAssignableFrom(this Type type, Type other)
        {
            return type.GetTypeInfo().IsAssignableFrom(other.GetTypeInfo());
        }

        public static object[] GetCustomAttributes(this Type type, Type aType, bool isInherit)
        {
            return type.GetTypeInfo().GetCustomAttributes(aType, isInherit);
        }

        public static IEnumerable<ConstructorInfo> GetConstructorInfo(this Type type)
        {
            return type.GetTypeInfo().DeclaredConstructors.Where(c => c.IsPublic);
        }

        public static MethodInfo GetAddMethod(this EventInfo @event, bool nonPublic = false)
        {
            if (@event.RemoveMethod == null || (!nonPublic && !@event.RemoveMethod.IsPublic))
            {
                return null;
            }

            return @event.RemoveMethod;
        }

        public static MethodInfo GetGetMethodInfo(this PropertyInfo property, bool nonPublic = false)
        {
            if (property.GetMethod == null || (!nonPublic && !property.GetMethod.IsPublic))
            {
                return null;
            }

            return property.GetMethod;
        }

        public static MethodInfo GetSetMethodInfo(this PropertyInfo property, bool nonPublic = false)
        {
            if (property.SetMethod == null || (!nonPublic && !property.SetMethod.IsPublic))
            {
                return null;
            }

            return property.SetMethod;
        }

        public static IEnumerable<PropertyInfo> GetPropertyInfo(this Type type)
        {
            return GetPropertyInfo(type, BindingFlags.FlattenHierarchy | BindingFlags.Public);
        }

        private static IEnumerable<PropertyInfo> GetPropertyInfo(this Type type, BindingFlags flags)
        {
            IEnumerable<PropertyInfo> p = type.GetTypeInfo().DeclaredProperties;

            if ((flags & BindingFlags.FlattenHierarchy) == BindingFlags.FlattenHierarchy)
            {
                p = type.GetRuntimeProperties();
            }

            return
                p.Select(property => new {property, getMethod = property.GetMethod})
                    .Where(@t => @t.getMethod != null)
                    .Where(@t => (flags & BindingFlags.Public) != BindingFlags.Public || @t.getMethod.IsPublic)
                    .Where(@t => (flags & BindingFlags.Instance) != BindingFlags.Instance || !@t.getMethod.IsStatic)
                    .Where(@t => (flags & BindingFlags.Static) != BindingFlags.Static || @t.getMethod.IsStatic)
                    .Select(@t => @t.property);
        }

        public static PropertyInfo GetPropertyInfo(this Type type, string name, BindingFlags flags,
            StringComparison comparison = StringComparison.CurrentCulture)
        {
            return GetPropertyInfo(type, flags).FirstOrDefault(p => String.Equals(p.Name, name, comparison));
        }

        public static PropertyInfo GetPropertyInfo(this Type type, string name,
            StringComparison comparison = StringComparison.CurrentCulture)
        {
            return
                GetPropertyInfo(type, BindingFlags.Public | BindingFlags.FlattenHierarchy)
                    .FirstOrDefault(p => String.Equals(p.Name, name, comparison));
        }

        public static IEnumerable<MethodInfo> GetMethodInfo(this Type type, BindingFlags flags)
        {
            IEnumerable<MethodInfo> ps = type.GetTypeInfo().DeclaredMethods;

            if ((flags & BindingFlags.FlattenHierarchy) == BindingFlags.FlattenHierarchy)
            {
                ps = type.GetRuntimeMethods();
            }

            return ps
                .Where(p => (flags & BindingFlags.Public) != BindingFlags.Public || p.IsPublic)
                .Where(p => (flags & BindingFlags.Instance) != BindingFlags.Instance || !p.IsStatic)
                .Where(p => (flags & BindingFlags.Static) != BindingFlags.Static || p.IsStatic);
        }

        public static IEnumerable<MethodInfo> GetMethodInfo(this Type type)
        {
            return GetMethodInfo(type, BindingFlags.FlattenHierarchy | BindingFlags.Public);
        }

        public static MethodInfo GetMethodInfo(this Type type, string name, BindingFlags flags,
            StringComparison comparison = StringComparison.CurrentCulture)
        {
            return GetMethodInfo(type, flags).FirstOrDefault(m => String.Equals(m.Name, name, comparison));
        }

        public static MethodInfo GetMethodInfo(this Type type, string name,
            StringComparison comparison = StringComparison.CurrentCulture)
        {
            return
                GetMethodInfo(type, BindingFlags.Public | BindingFlags.FlattenHierarchy)
                    .FirstOrDefault(m => String.Equals(m.Name, name, comparison));
        }

        public static IEnumerable<ConstructorInfo> GetConstructorInfo(this Type type, BindingFlags flags)
        {
            return type.GetConstructors()
                .Where(c => (flags & BindingFlags.Public) != BindingFlags.Public || c.IsPublic)
                .Where(c => (flags & BindingFlags.Instance) != BindingFlags.Instance || !c.IsStatic)
                .Where(c => (flags & BindingFlags.Static) != BindingFlags.Static || c.IsStatic);
        }

        public static IEnumerable<FieldInfo> GetFieldInfo(this Type type)
        {
            return GetFieldInfo(type, BindingFlags.Public | BindingFlags.FlattenHierarchy);
        }

        public static IEnumerable<FieldInfo> GetFieldInfo(this Type type, BindingFlags flags)
        {
            IEnumerable<FieldInfo> fields = type.GetTypeInfo().DeclaredFields;

            if ((flags & BindingFlags.FlattenHierarchy) == BindingFlags.FlattenHierarchy)
            {
                fields = type.GetRuntimeFields();
            }

            return fields
                .Where(f => (flags & BindingFlags.Public) != BindingFlags.Public || f.IsPublic)
                .Where(f => (flags & BindingFlags.Instance) != BindingFlags.Instance || !f.IsStatic)
                .Where(f => (flags & BindingFlags.Static) != BindingFlags.Static || f.IsStatic);
        }

        public static FieldInfo GetFieldInfo(this Type type, string name, BindingFlags flags,
            StringComparison comparison = StringComparison.CurrentCulture)
        {
            return GetFieldInfo(type, flags).FirstOrDefault(p => String.Equals(p.Name, name, comparison));
        }

        public static FieldInfo GetFieldInfo(this Type type, string name,
            StringComparison comparison = StringComparison.CurrentCulture)
        {
            return
                GetFieldInfo(type, BindingFlags.Public | BindingFlags.FlattenHierarchy)
                    .FirstOrDefault(p => String.Equals(p.Name, name, comparison));
        }

        public static Type[] GetGenericArguments(this Type type)
        {
            return type.GenericTypeArguments;
        }

        public static bool WithAttribute(this MethodInfo method, Type attribute)
        {
            return method.GetCustomAttributes(attribute, true).Any();
        }

        public static string PropertiesDescription(this object instance, string propertyNameValueFormat = "{0}: {1}, ", BindingFlags flags = BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly)
        {
            var properties = instance.GetType().GetProperties(flags);

            var builder = new StringBuilder();

            foreach (var property in properties)
            {
                var value = property.GetValue(instance, null) ?? "(null)";
                builder.AppendLine(string.Format(propertyNameValueFormat, property.Name, value));
            }

            return builder.ToString();
        }

        public static bool HasMethod(this object source, MethodInfo method)
        {
            if (source == null || method == null)
            {
                return false;
            }

            return source.GetType().GetMethod(method.Name) != null;
        }

        public static bool HasProperty(this object source, string propertyName)
        {
            if (source == null || string.IsNullOrEmpty(propertyName))
            {
                return false;
            }

            return source.GetType().GetProperty(propertyName) != null;
        }
    }
}