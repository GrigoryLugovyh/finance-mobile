﻿using System;
using System.Linq;

namespace Mobile.Core.BL.Core.Extensions
{
    public static class EnumExtension
    {
        public static TEnum Next<TEnum>(this TEnum @enum) where TEnum : struct, IConvertible
        {
            if (!(typeof (TEnum).IsEnum))
            {
                throw new ArgumentException("TEnum");
            }

            return Enum.GetValues(typeof (TEnum)).Cast<TEnum>().FirstOrDefault(e => e.As<int>() > @enum.As<int>());
        }

        public static bool IsLast<TEnum>(this TEnum @enum) where TEnum : struct, IConvertible
        {
            if (!(typeof(TEnum).IsEnum))
            {
                throw new ArgumentException("TEnum");
            }

            return @enum.Equals(Enum.GetValues(typeof (TEnum)).Cast<TEnum>().Last());
        }
    }
}