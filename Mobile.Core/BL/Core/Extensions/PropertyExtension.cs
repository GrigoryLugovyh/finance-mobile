﻿using System;
using System.Linq.Expressions;
using System.Reflection;

namespace Mobile.Core.BL.Core.Extensions
{
    public static class PropertyExtension
    {
        public static TInstance NewValue<TInstance>(this TInstance instance, Expression<Func<TInstance, object>> expression,
            object value)
        {
            var propertyInfo = FindMemberExpression(expression).Member as PropertyInfo;

            if (propertyInfo != null)
            {
                propertyInfo.SetValue(instance, value, null);
            }

            return instance;
        }

        public static string GetPropertyNameFromExpression<T>(this object target, Expression<Func<T>> expression)
        {
            if (expression == null)
            {
                throw new ArgumentException("expression");
            }

            var memberExpression = FindMemberExpression(expression);

            if (memberExpression == null)
            {
                throw new ArgumentException("expression");
            }

            var member = memberExpression.Member as PropertyInfo;

            if (member == null)
            {
                throw new ArgumentException("expression");
            }

            if (member.DeclaringType == null)
            {
                throw new ArgumentException("expression");
            }

            if (target != null)
            {
                if (!member.DeclaringType.IsInstanceOfType(target))
                {
                    throw new ArgumentException("expression");
                }
            }

            if (member.GetGetMethod(true).IsStatic)
            {
                throw new ArgumentException("expression");
            }

            return member.Name;
        }

        public static MethodInfo GetMethodInfo(this Expression<Action> expression)
        {
            if (expression == null)
            {
                throw new ArgumentException("expression");
            }

            var memberExpression = expression.Body as MethodCallExpression;

            if (memberExpression == null)
            {
                throw new ArgumentException("memberExpression");
            }

            return memberExpression.Method;
        }

        public static object GetMethodParameter(this Expression<Action> expression)
        {
            if (expression == null)
            {
                return null;
            }

            var memberExpression = expression.Body as MethodCallExpression;

            if (memberExpression == null)
            {
                return null;
            }

            if (memberExpression.Arguments.Count == 0)
            {
                return null;
            }

            return Expression.Lambda(memberExpression.Arguments[0], expression.Parameters).Compile().DynamicInvoke();
        }

        private static MemberExpression FindMemberExpression(LambdaExpression expression)
        {
            var body = expression.Body as UnaryExpression;

            if (body != null)
            {
                var unary = body;
                var member = unary.Operand as MemberExpression;

                if (member == null)
                {
                    throw new ArgumentException("expression");
                }

                return member;
            }

            return expression.Body as MemberExpression;
        }        
    }
}