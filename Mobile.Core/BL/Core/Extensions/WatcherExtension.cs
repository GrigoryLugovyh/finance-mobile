﻿using System.Diagnostics;
using System.Diagnostics.Contracts;

namespace Mobile.Core.BL.Core.Extensions
{
    public static class WatcherExtension
    {
        public static int TimeSpan(this Stopwatch watcher)
        {
            Contract.Assert(watcher != null);

            watcher.Stop();
            var span = watcher.Elapsed;
            watcher.Restart();

            return span.Milliseconds;
        }
    }
}