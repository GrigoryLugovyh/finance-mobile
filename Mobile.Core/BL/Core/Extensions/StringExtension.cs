﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using MoreLinq;

namespace Mobile.Core.BL.Core.Extensions
{
    public static class StringExtension
    {
        public static string AsString(this object value)
        {
            return value?.ToString() ?? string.Empty;
        }

        public static string NoSnakesPls(this string item)
        {
            return item.Replace("_", "");
        }

        public static string ReplaceSeparator(this string item)
        {
            return item.Replace(".", CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator)
                .Replace(",", CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator);
        }

        public static bool TryParseToDecimal(this string value, out decimal result)
        {
            return decimal.TryParse((value ?? string.Empty).ReplaceSeparator(), NumberStyles.Any, CultureInfo.CurrentCulture, out result);
        }

        public static long HexToDeviceId(this string @string)
        {
	        var item = Regex.Replace(@string, "[^A-Fa-f0-9]", string.Empty, RegexOptions.None);
	        long result = 0;
	        const int length = 8;

	        for (var i = 0; i < item.Length; i = i + length)
            {
                unchecked
                {
                    result += long.Parse(item.Substring(i, i + length > item.Length ? item.Length - i : length),
                        NumberStyles.HexNumber);
                }
            }

            return
                long.Parse(result.ToString().Length > 10
                    ? result.ToString().Remove(10)
                    : result.ToString().PadRight(10, '0'));
        }

        public static string NoLastSlash(this string uri)
        {
            if (string.IsNullOrEmpty(uri))
            {
                return string.Empty;
            }

            return uri.TrimEnd('/');
        }

        public static string ToLowerSafe(this string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return string.Empty;
            }

            return value.ToLower();
        }

        public static string ClearChars(this string value, char[] badChars = null, char[] goodChars = null)
        {
            var chars = new List<char> { '[', ']', '{', '}', '\\', '\"', '\'' };

            Action<char[], bool> actualizeChars = (cs, direction) =>
            {
                if (cs != null && cs.Any())
                {
                    cs.ForEach(c =>
                    {
                        if (!chars.Contains(c) && direction)
                        {
                            chars.Add(c);
                        }

                        if (chars.Contains(c) && !direction)
                        {
                            chars.Remove(c);
                        }
                    });
                }
            };

            actualizeChars(goodChars, true);
            actualizeChars(badChars, false);

            var resultBuilder = new StringBuilder();

            value.ForEach(c =>
            {
                if (chars.Contains(c))
                {
                    return;
                }

                resultBuilder.Append(c);
            });


            return resultBuilder.ToString();
        }
    }
}