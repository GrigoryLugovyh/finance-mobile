﻿using System;
using System.Linq;

namespace Mobile.Core.BL.Core.Extensions
{
    public static class CharExtension
    {
        public static string ToNumeric(this char[] array)
        {
            return array.Aggregate(string.Empty,
                (current, @char) => current + Math.Abs((int) Char.GetNumericValue(@char)));
        }

    }
}