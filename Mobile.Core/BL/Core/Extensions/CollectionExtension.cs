﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Mobile.Core.BL.Core.Extensions
{
    public static class CollectionExtension
    {
        public static IEnumerable<T> Yield<T>(this T value)
        {
            yield return value;
        }

        public static bool HasValue(this ICollection collection)
        {
            return collection != null && collection.Count > 0;
        }

        public static bool ContainsArray<T>(this IEnumerable<T> collection, params T[] items)
        {
            return items.Aggregate(true, (current, item) => current & collection.Contains(item));
        }

        public static bool ScrambledEquals<T>(this IEnumerable<T> firstEnumerable, IEnumerable<T> secondEnumerable)
        {
            var cnt = new Dictionary<T, int>();

            foreach (T s in firstEnumerable)
            {
                if (cnt.ContainsKey(s))
                {
                    cnt[s]++;
                }
                else
                {
                    cnt.Add(s, 1);
                }
            }

            foreach (T s in secondEnumerable)
            {
                if (cnt.ContainsKey(s))
                {
                    cnt[s]--;
                }
                else
                {
                    return false;
                }
            }

            return cnt.Values.All(c => c == 0);
        }
    }
}