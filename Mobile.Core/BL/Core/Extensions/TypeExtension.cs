﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using Mobile.Core.BL.Core.Data;
using Container = Mobile.Core.BL.IoCContainer.Container;

namespace Mobile.Core.BL.Core.Extensions
{
    public static class TypeExtension
    {
        public static T Create<T>(this Type type)
        {
            return (T) Activator.CreateInstance(type);
        }

        public static IEnumerable<Type> SafeGetTypes(this Assembly assembly)
        {
            try
            {
                return assembly.GetTypes();
            }
            catch (ReflectionTypeLoadException e)
            {
                Tracer.Warning("ReflectionTypeLoadException \"{0}\" in SafeGetTypes for {1}", e.ToLong(),
                    assembly.FullName);

                return new Type[0];
            }
        }

        public static IEnumerable<Type> CreatableTypes(this Assembly assembly)
        {
            return assembly
                .SafeGetTypes()
                .Select(t => t.GetTypeInfo())
                .Where(t => !t.IsAbstract)
                .Where(t => t.DeclaredConstructors.Any(c => !c.IsStatic && c.IsPublic))
                .Select(t => t.AsType());
        }

        public static IEnumerable<Type> EndingWith(this IEnumerable<Type> types, string endingWith)
        {
            return types.Where(x => x.Name.EndsWith(endingWith));
        }

        public static IEnumerable<Type> StartingWith(this IEnumerable<Type> types, string endingWith)
        {
            return types.Where(x => x.Name.StartsWith(endingWith));
        }

        public static IEnumerable<Type> Containing(this IEnumerable<Type> types, string containing)
        {
            return types.Where(x => x.Name.Contains(containing));
        }

        public static IEnumerable<Type> InNamespace(this IEnumerable<Type> types, string namespaceBase)
        {
            return types.Where(x => x.Namespace != null && x.Namespace.StartsWith(namespaceBase));
        }

        public static IEnumerable<Type> WithAttribute(this IEnumerable<Type> types, Type attributeType)
        {
            return types.Where(x => x.GetCustomAttributes(attributeType, true).Any());
        }

        public static IEnumerable<Type> WithAttribute<TAttribute>(this IEnumerable<Type> types)
            where TAttribute : Attribute
        {
            return types.WithAttribute(typeof(TAttribute));
        }

        public static bool Is(this object instance, Type type)
        {
            return instance.GetType() == type;
        }

        public static T As<T>(this object instance)
        {
            return (T) TypeDescriptor.GetConverter(typeof(T)).ConvertTo(instance, typeof(T));
        }

        public static TChild Draw<TChild>(this object parent)
        {
            var child = typeof(TChild).Create<TChild>();

            if (parent != null)
            {
                foreach (
                    var property in
                    parent.GetType()
                        .GetProperties()
                        .Where(property => property.CanWrite)
                        .Where(property => child.HasProperty(property.Name)))
                {
                    property.SetValue(child, property.GetValue(parent, null), null);
                }
            }

            return child;
        }

        public static TOut Convert<TIn, TOut>(this TIn instance)
        {
            return Container.Resolve<IMDataMapperDispatcher>().Convert<TIn, TOut>(instance);
        }

        public static object MakeGenericTypeSafe(this Type type, params Type[] typeArguments)
        {
            try
            {
                return Container.Resolve(type.MakeGenericType(typeArguments));
            }
            catch (Exception ex)
            {
                Tracer.Warning("Exception during MakeGenericTypeSafe for type \"{0}\" and argumants \"{1}\": {2}",
                    type.Name, string.Join(",", typeArguments.Select(t => t.Name)), ex.ToLong());

                return null;
            }
        }

        public static bool IsSubclassOfGeneric(this Type targetType, Type genericType)
        {
            while (targetType != null && targetType != typeof(object))
            {
                var currentType = targetType.IsGenericType ? targetType.GetGenericTypeDefinition() : targetType;

                if (genericType == currentType)
                {
                    return true;
                }

                targetType = targetType.BaseType;
            }

            return false;
        }
    }
}