﻿using System;

namespace Mobile.Core.BL.Core.Extensions
{
    public static class DateTimeExtension
    {
        public static DateTime DropMs(this DateTime value)
        {
            return value.Subtract(TimeSpan.FromMilliseconds(value.Millisecond));
        }

        public static string ToUrlSegment(this DateTime value)
        {
            return DateTime.SpecifyKind(value, DateTimeKind.Utc).ToString("yyyyMMddTHHmmss.fffZ");
        }
    }
}