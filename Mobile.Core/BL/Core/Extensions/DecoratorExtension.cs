﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Mobile.Core.BL.Core.Attributes;
using Mobile.Core.BL.Core.Binding.Decorator;
using Mobile.Core.BL.Core.Enums;

namespace Mobile.Core.BL.Core.Extensions
{
    public static class DecoratorExtension
    {
        public static MDecoratorAttributesDescription GetDecoratorAttributes(this MethodInfo method)
        {
            var attributes = method.GetAttributes<MAttribute>();
            var mAttributes = attributes as IList<MAttribute> ?? attributes.ToList();
            return
                new MDecoratorAttributesDescription(
                    mAttributes.Where(a => a.Order == MExecuteAttributeOrderEnum.AfterMethod),
                    mAttributes.Where(a => a.Order == MExecuteAttributeOrderEnum.BeforeMethod));
        }
    }
}