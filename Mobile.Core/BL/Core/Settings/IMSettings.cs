﻿namespace Mobile.Core.BL.Core.Settings
{
    public interface IMSettings
    {
        bool RisePropertyChangedOnUserInterfaceThread { get; set; }

        bool GarbageCollectAfterFinishView { get; set; }
    }
}