﻿namespace Mobile.Core.BL.Core.Settings
{
    public class MSettings : IMSettings
    {
        public MSettings()
        {
            RisePropertyChangedOnUserInterfaceThread = true;
            GarbageCollectAfterFinishView = true;
        }

        public bool RisePropertyChangedOnUserInterfaceThread { get; set; }

        public bool GarbageCollectAfterFinishView { get; set; }
    }
}