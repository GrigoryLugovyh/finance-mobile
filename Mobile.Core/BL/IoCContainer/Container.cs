﻿using System;
using System.Collections.Generic;
using TinyIoC;

namespace Mobile.Core.BL.IoCContainer
{
    public static class Container
    {
        public static void Init()
        {
        }

        public static T Resolve<T>() where T : class
        {
            return TinyIoCContainer.Current.Resolve<T>();
        }

        public static object Resolve(Type type)
        {
            return TinyIoCContainer.Current.Resolve(type);
        }

        public static IEnumerable<T> ResolveAll<T>() where T : class
        {
            return TinyIoCContainer.Current.ResolveAll<T>(true);
        }

        public static T Resolve<T>(Type type) where T : class
        {
            return (T) TinyIoCContainer.Current.Resolve(type);
        }

        public static TinyIoCContainer.RegisterOptions AddRegistration<TInterface, TImplementation>()
            where TImplementation : class, TInterface
            where TInterface : class
        {
            return TinyIoCContainer.Current.Register<TInterface, TImplementation>();
        }

        public static void AddRegistration<TInterfaceFirst, TInterfaceSecond, TImplementation>()
            where TImplementation : class, TInterfaceFirst, TInterfaceSecond
            where TInterfaceFirst : class
            where TInterfaceSecond : class
        {
            TinyIoCContainer.Current.Register<TInterfaceFirst, TImplementation>();
            TinyIoCContainer.Current.Register<TInterfaceSecond, TImplementation>();
        }

        public static void AddRegistration<TInterfaceFirst, TInterfaceSecond, TInterfaceThird, TImplementation>()
            where TImplementation : class, TInterfaceFirst, TInterfaceSecond, TInterfaceThird
            where TInterfaceFirst : class
            where TInterfaceSecond : class
            where TInterfaceThird : class
        {
            TinyIoCContainer.Current.Register<TInterfaceFirst, TImplementation>();
            TinyIoCContainer.Current.Register<TInterfaceSecond, TImplementation>();
            TinyIoCContainer.Current.Register<TInterfaceThird, TImplementation>();
        }

        public static TinyIoCContainer.RegisterOptions AddRegistration<TInterface>(TInterface instance)
            where TInterface : class
        {
            return TinyIoCContainer.Current.Register(instance);
        }

        public static TinyIoCContainer.RegisterOptions AddRegistration(Type registerType,
            Type registerImplementationType)
        {
            return TinyIoCContainer.Current.Register(registerType, registerImplementationType);
        }

        public static TinyIoCContainer.MultiRegisterOptions RegisterMultiple<TInterface>(IEnumerable<Type> implementationTypes)
        {
            return TinyIoCContainer.Current.RegisterMultiple<TInterface>(implementationTypes);
        }
    }
}