﻿using Mobile.Core.BL.Core.Entity;

namespace Mobile.Core.DAL
{
    public interface ITempEntityRepository : IBaseRepository<TempEntity>
    {
    }

    public interface ITempRefEntityRepository : IBaseRepository<TempRefEntity>
    {
    }
}