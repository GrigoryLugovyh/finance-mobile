﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using Mobile.Core.BL.Core.Exceptions;
using Mobile.Core.BL.Core.Extensions;
using Mobile.Core.BL.Core.Platform.Data;
using Mobile.Core.BL.Core.Services;
using Mobile.Core.BL.IoCContainer;
using Mobile.Model;
using SQLite;

namespace Mobile.Core.DAL
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public abstract class BaseRepository<T> : IBaseRepository<T> where T : MEntity, new()
    {
        private IMDataBulkHandler bulk;
        private IMDateTimeService dateTime;
        private IStorageSessionDispatcher storageDispatcher;

        protected IMDataBulkHandler dataBulk => bulk ?? (bulk = Container.Resolve<IMDataBulkHandler>());

        protected IMDateTimeService dateTimeService => dateTime ?? (dateTime = Container.Resolve<IMDateTimeService>());

        protected IStorageSessionDispatcher StorageSessionDispatcher
            => storageDispatcher ?? (storageDispatcher = Container.Resolve<IStorageSessionDispatcher>());

        protected virtual Expression<Func<T, bool>> ContextPredicate
        {
            get { return arg => true; }
        }

        protected virtual Dictionary<string, string> BatchParameters => new Dictionary<string, string>();

        public virtual int Save(T instance, bool checkExistById = false)
        {
            InContext(instance);

            return InDbContext(database => Exists(instance, checkExistById)
                ? database.Update(instance)
                : database.Insert(instance));
        }

        public void Save(IEnumerable<T> instances, bool checkExistById = false)
        {
            foreach (var instance in instances)
                Save(instance, checkExistById);
        }

        public virtual void BatchInsert(T entity)
        {
            StorageSessionDispatcher.Execute<T>(connection => { dataBulk.Insert(entity, InContext); });
        }

        public virtual void BatchInsert(IEnumerable<T> entities, int portion = 100)
        {
            StorageSessionDispatcher.Execute<T>(
                connection =>
                {
                    BatchPortionAction(enumerable => dataBulk.Insert(enumerable, InContext), entities, portion);
                });
        }

        public virtual void BatchUpdate(T entity, bool byId = true)
        {
            StorageSessionDispatcher.Execute<T>(
                connection => { dataBulk.Update(entity, InContext, byId, BatchParameters); });
        }

        public virtual void BatchUpdate(IEnumerable<T> entities, bool byId = true, int portion = 100)
        {
            StorageSessionDispatcher.Execute<T>(connection =>
            {
                BatchPortionAction(enumerable => dataBulk.Update(entities, InContext, byId, BatchParameters), entities,
                    portion);
            });
        }

        public virtual void BatchDelete(bool byId = true, params string[] ids)
        {
            StorageSessionDispatcher.Execute<T>(connection => { dataBulk.Delete<T>(byId, ids, BatchParameters); });
        }

        public virtual T Single(Expression<Func<T, bool>> predicate, bool getUndeleted = true)
        {
            return
                InSafeNoElementContext(
                    () =>
                        InDbContext(
                            database =>
                                Query(getUndeleted ? predicate.And(arg => arg.IsDeleted == false) : predicate,
                                    database).Take(1).FirstOrDefault()));
        }

        public T First(bool getUndeleted = true)
        {
            return Single(arg => true, getUndeleted);
        }

        public virtual T Read(int id)
        {
            return InSafeNoElementContext(() => InDbContext(database => database.Get<T>(id)));
        }

        public virtual void Delete(int id)
        {
//            if (!Exists(id))
//            {
//#if(DEBUG)
//                throw new MException("Entity with id = {0} doesn't exist in corrent context", id);
//#endif
//            }

            InDbContext(database => database.Delete<T>(id));
        }

        public virtual bool Exists(int id)
        {
            return Count(x => x.Id == id) > 0;
        }

        public virtual bool Exists(T instance, bool checkById = false)
        {
            if (!checkById && !string.IsNullOrWhiteSpace(instance.ServerId))
            {
                T existing = null;

                InDbContext(
                    database =>
                        existing =
                            database.Table<T>()
                                .Where(ContextPredicate)
                                .Where(x => x.ServerId == instance.ServerId)
                                .Take(1)
                                .FirstOrDefault());

                if (existing != null)
                {
                    instance.Id = existing.Id;
                    return true;
                }

                return false;
            }

            return Exists(instance.Id);
        }

        public virtual long Count()
        {
            return Count(e => true);
        }

        public virtual long Count(Expression<Func<T, bool>> predicate)
        {
            return InDbContext(database => Query(predicate, database).Count());
        }

        public virtual long CountTotal()
        {
            return
                InDbContext(
                    database =>
                            database.ExecuteScalar<long>("select count(*) from " + typeof(T).Name));
        }

        public virtual List<T> Get(Expression<Func<T, bool>> predicate, bool getUndeleted = true)
        {
            return
                InDbContext(
                    database =>
                        Query(getUndeleted ? predicate.And(arg => arg.IsDeleted == false) : predicate, database)
                            .ToList());
        }

        public virtual List<T> All(bool getUndeleted = true)
        {
            return Get(arg => true, getUndeleted);
        }

        public List<T> Table(Expression<Func<T, bool>> predicate)
        {
            return InDbContext(database => database.Table<T>().Where(predicate).ToList());
        }

        public virtual int AsUnloaded(T instance)
        {
            if (!instance.Unloaded)
            {
                instance.Unloaded = true;
                return Save(instance);
            }

            return 0;
        }

        public int Execute(string query, params object[] args)
        {
            return InDbContext(database => database.Execute(query, args));
        }

        public void Execute(Func<string[], bool> readPortion, string query, params string[] args)
        {
            InDbContext(database => database.ExecuteCustom(readPortion, query, args));
        }

        public virtual bool Delete(Expression<Func<T, bool>> predicate)
        {
            return InDbContext(database =>
            {
                foreach (var item in Get(predicate))
                    Delete(item.Id);

                return true;
            });
        }

        public long GetHashSum()
        {
            return
                (long) InDbContext(
                    database =>
                        database.ExecuteScalar<double>("select ifnull(AVG(HashCode), 0) from " + typeof(T).Name)*
                        1000);
        }

        private void BatchPortionAction(Action<IEnumerable<T>> action, IEnumerable<T> entities, int portionBatch = 100)
        {
            var entitiesList = entities.ToList();

            var currentPortion = entitiesList.Take(portionBatch).ToList();
            var currentCount = portionBatch;

            while (currentPortion.Any())
            {
                action(currentPortion);

                currentPortion = entitiesList.Skip(currentCount).Take(portionBatch).ToList();
                currentCount += portionBatch;
            }
        }

        protected virtual TableQuery<T> Query(Expression<Func<T, bool>> predicate, SQLiteConnection database)
        {
            return database.Table<T>().Where(ContextPredicate).Where(predicate);
        }

        protected virtual void InContext(T instance)
        {
            instance.Updated = dateTimeService.UtcNow;
        }

        protected void InitTable(SQLiteConnection connection)
        {
            connection.CreateTable<T>();
        }

        protected void DropTable(SQLiteConnection connection)
        {
            connection.DropTable<T>();
        }

        protected void InDbContext(Action<SQLiteConnection> action)
        {
            StorageSessionDispatcher.Execute<T>(action);
        }

        protected TOut InDbContext<TOut>(Func<SQLiteConnection, TOut> func)
        {
            return StorageSessionDispatcher.Execute<T, TOut>(func);
        }

        private TOut InSafeNoElementContext<TOut>(Func<TOut> func)
        {
            var originalCulture = Thread.CurrentThread.CurrentUICulture;
            try
            {
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");
                return func();
            }
            catch (InvalidOperationException ex)
            {
                if (ex.Message.ToLower().Equals("sequence contains no elements", StringComparison.OrdinalIgnoreCase))
                    return default(TOut);

                throw;
            }
            finally
            {
                Thread.CurrentThread.CurrentUICulture = originalCulture;
            }
        }
    }
}