﻿using System;
using System.Threading;
using Mobile.Core.BL.IoCContainer;
using Mobile.Model;
using SQLite;

namespace Mobile.Core.DAL
{
    public sealed class StorageSessionSync
    {
        public static readonly object Context = new object();
    }

    public class StorageSessionDispatcher : IStorageSessionDispatcher
    {
        private IStorageSession currentSession;
        private int invokeCount;

        public TOut Execute<TEntity, TOut>(Func<SQLiteConnection, TOut> executingFunc) where TEntity : MEntity
        {
            return Execute(executingFunc, typeof(TEntity));
        }

        public TOut Execute<TOut>(Func<SQLiteConnection, TOut> executingFunc, Type entityType = null)
        {
            if (executingFunc == null)
                throw new ArgumentNullException(nameof(executingFunc));

            lock (StorageSessionSync.Context)
            {
                return Invoke(entityType, executingFunc);
            }
        }

        public void Execute<TEntity>(Action<SQLiteConnection> executingAction) where TEntity : MEntity
        {
            Execute(executingAction, typeof(TEntity));
        }

        public void Execute(Action<SQLiteConnection> executingAction, Type entityType = null)
        {
            if (executingAction == null)
                throw new ArgumentNullException(nameof(executingAction));

            lock (StorageSessionSync.Context)
            {
                Invoke(entityType, connection =>
                {
                    executingAction(connection);
                    return 0;
                });
            }
        }

        protected virtual TOut Invoke<TOut>(Type entityType, Func<SQLiteConnection, TOut> func)
        {
            try
            {
                if (currentSession == null)
                    currentSession = Container.Resolve<IStorageSession>();

                invokeCount++;

                while (true)
                    try
                    {
                        return func(currentSession.Connection);
                    }
                    catch (SQLiteException sqLiteException)
                    {
                        var message = sqLiteException.Message.ToLower();

                        if (message.Contains("locked") || message.Contains("busy"))
                        {
                            Console.WriteLine(sqLiteException);
                            Thread.Sleep(100);
                            continue;
                        }

                        if (message.Contains("no such table") && (entityType?.IsSubclassOf(typeof(MEntity)) ?? false))
                        {
                            currentSession.Connection.CreateTable(entityType);
                            continue;
                        }

                        throw;
                    }
            }
            finally
            {
                if ((currentSession != null) && (--invokeCount <= 0))
                {
                    currentSession.Dispose();
                    currentSession = null;
                }
            }
        }
    }
}