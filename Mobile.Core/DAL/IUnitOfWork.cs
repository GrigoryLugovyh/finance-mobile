﻿using System;

namespace Mobile.Core.DAL
{
    public interface IUnitOfWork
    {
        void Invoke(Action action);
    }
}