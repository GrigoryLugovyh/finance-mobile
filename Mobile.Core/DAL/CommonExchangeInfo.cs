﻿using System;
using System.Linq;

namespace Mobile.Core.DAL
{
    public class CommonExchangeInfo : Tuple<int, Tuple<string, string>[]>
    {
        public CommonExchangeInfo(int hash, params Tuple<string, string>[] arguments) : base(hash, arguments)
        {
        }

        public int Hash => Item1;

        public Tuple<string, string>[] Arguments => Item2;

        public string this[string key]
        {
            get
            {
                var value = Item2.FirstOrDefault(tuple => tuple.Item1 == key);

                if (value != null)
                {
                    return value.Item2;
                }

                return string.Empty;
            }
        }
    }
}