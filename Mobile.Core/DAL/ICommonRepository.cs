﻿using System.Collections.Generic;

namespace Mobile.Core.DAL
{
    public interface ICommonRepository
    {
        void Init();
        void Truncate();
        void DeleteAll();
        List<string> Obsolete();
        List<CommonExchangeInfo> ExchangeList(params string[] args);
    }
}