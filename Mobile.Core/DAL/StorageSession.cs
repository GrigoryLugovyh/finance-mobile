﻿using System.IO;
using Mobile.Core.BL.Core.Platform.Specific;
using Mobile.Core.BL.IoCContainer;
using SQLite;

namespace Mobile.Core.DAL
{
    public class StorageSession : IStorageSession
    {
        private SQLiteConnection connection;
        private IPlatformSpecific platformSpecific;

        public StorageSession()
        {
            if (!Directory.Exists(PlatformSpecific.ApplicationDataPath))
                Directory.CreateDirectory(PlatformSpecific.ApplicationDataPath);
        }

        private IPlatformSpecific PlatformSpecific
            => platformSpecific ?? (platformSpecific = Container.Resolve<IPlatformSpecific>());

        public SQLiteConnection Connection
            => connection ?? (connection = new SQLiteConnection(PlatformSpecific.SqlitePath,
                   SQLiteOpenFlags.Create | SQLiteOpenFlags.ReadWrite | SQLiteOpenFlags.FullMutex |
                   SQLiteOpenFlags.SharedCache));

        public void Dispose()
        {
            connection?.Dispose();
            connection = null;
        }
    }
}