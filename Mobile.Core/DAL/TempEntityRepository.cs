﻿using Mobile.Core.BL.Core.Entity;

namespace Mobile.Core.DAL
{
    public class TempEntityRepository : BaseCommonRepository<TempEntity>, ITempEntityRepository
    {
    }

    public class TempRefEntityRepository : BaseCommonRepository<TempRefEntity>, ITempRefEntityRepository
    {
    }
}