﻿using Mobile.Core.BL.IoCContainer;

namespace Mobile.Core.DAL
{
    public class UnitOfWorkFactory
    {
        public static IUnitOfWork Create => Container.Resolve<IUnitOfWork>();
    }
}