﻿using System;
using SQLite;

namespace Mobile.Core.DAL
{
    public interface IStorageSession : IDisposable
    {
        SQLiteConnection Connection { get; }
    }
}