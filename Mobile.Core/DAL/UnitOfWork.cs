﻿using System;
using Mobile.Core.BL.IoCContainer;

namespace Mobile.Core.DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        public void Invoke(Action transactionAction)
        {
            Container.Resolve<IStorageSessionDispatcher>()
                .Execute(connection => { connection.RunInTransaction(transactionAction); });
        }
    }
}