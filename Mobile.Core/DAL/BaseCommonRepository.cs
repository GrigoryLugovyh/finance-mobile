﻿using System.Collections.Generic;
using System.Linq;
using Mobile.Model;

namespace Mobile.Core.DAL
{
    public abstract class BaseCommonRepository<T> : BaseRepository<T>, ICommonRepository where T : MEntity, new()
    {
        public void Init()
        {
            InDbContext(InitTable);
        }

        public virtual void Truncate()
        {
            InDbContext(connection =>
            {
                DropTable(connection);
                InitTable(connection);
            });
        }

        public void DeleteAll()
	    {
            InDbContext(database => database.DeleteAll<T>());
        }

	    public virtual List<string> Obsolete()
        {
            return Enumerable.Empty<string>().ToList();
        }

        public virtual List<CommonExchangeInfo> ExchangeList(params string[] args)
        {
            return Enumerable.Empty<CommonExchangeInfo>().ToList();
        }
    }
}