﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Mobile.Model;

namespace Mobile.Core.DAL
{
    public interface IBaseRepository<T> where T : MEntity
    {
        int Save(T instance, bool checkExistById = false);
        void Save(IEnumerable<T> instances, bool checkExistById = false);
        void BatchInsert(T entity);
        void BatchInsert(IEnumerable<T> entities, int portion = 100);
        void BatchUpdate(T entity, bool byId = true);
        void BatchUpdate(IEnumerable<T> entities, bool byId = true, int portion = 100);
        void BatchDelete(bool byId = true, params string[] ids);
        T Read(int id);
        T Single(Expression<Func<T, bool>> predicate, bool getUndeleted = true);
        T First(bool getUndeleted = true);
        void Delete(int id);
        bool Exists(int id);
        bool Exists(T instance, bool checkById = false);
        long Count();
        List<T> All(bool getUndeleted = true);
        List<T> Get(Expression<Func<T, bool>> predicate, bool getUndeleted = true);
        List<T> Table(Expression<Func<T, bool>> predicate);
        int AsUnloaded(T instance);
        int Execute(string query, params object[] args);
        void Execute(Func<string[], bool> readPortion, string query, params string[] args);
        bool Delete(Expression<Func<T, bool>> predicate);
        long Count(Expression<Func<T, bool>> predicate);
        long CountTotal();
        long GetHashSum();
    }
}