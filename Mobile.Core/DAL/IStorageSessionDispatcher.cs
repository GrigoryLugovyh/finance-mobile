﻿using System;
using Mobile.Model;
using SQLite;

namespace Mobile.Core.DAL
{
    public interface IStorageSessionDispatcher
    {
        TOut Execute<TEntity, TOut>(Func<SQLiteConnection, TOut> executingFunc) where TEntity : MEntity;

        TOut Execute<TOut>(Func<SQLiteConnection, TOut> executingFunc, Type entityType = null);

        void Execute<TEntity>(Action<SQLiteConnection> executingAction) where TEntity : MEntity;

        void Execute(Action<SQLiteConnection> executingAction, Type entityType = null);
    }
}