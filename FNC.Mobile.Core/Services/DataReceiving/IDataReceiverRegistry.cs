﻿using System.Threading.Tasks;

namespace FNC.Mobile.Core.Services.DataReceiving
{
    public interface IDataReceiverRegistry
    {
        void AddReceiver<TData>(IDataReceiver<TData> receiver);

        Task<TData> Receive<TData>(params object[] parameters);
    }
}