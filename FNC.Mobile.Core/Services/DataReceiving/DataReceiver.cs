﻿using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace FNC.Mobile.Core.Services.DataReceiving
{
    public abstract class DataReceiver<TData> : IDataReceiver<TData>
    {
        protected abstract string Url { get; }

        public virtual async Task<TData> Get(params object[] parameters) =>
            JsonConvert.DeserializeObject<TData>(await new HttpClient().GetStringAsync(string.Format(Url, parameters)));
    }
}