﻿namespace FNC.Mobile.Core.Services.DataReceiving.Dto
{
    public class StockDto
    {
        public string T { get; set; }

        public string N { get; set; }

        public string E { get; set; }

        public string Id { get; set; }
    }
}