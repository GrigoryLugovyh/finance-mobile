﻿namespace FNC.Mobile.Core.Services.DataReceiving.Dto
{
    public class MatchStockDto
    {
        public StockDto[] Matches { get; set; }
    }
}