﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FNC.Mobile.Core.Services.DataReceiving
{
    public class DataReceiverRegistry : IDataReceiverRegistry
    {
        private readonly Dictionary<Type, object> dataReceiverOwner = new Dictionary<Type, object>();

        public void AddReceiver<TData>(IDataReceiver<TData> receiver)
        {
            if (dataReceiverOwner.ContainsKey(typeof(TData))) return;

            dataReceiverOwner.Add(typeof(TData), receiver);
        }

        public async Task<TData> Receive<TData>(params object[] parameters)
        {
            return await (dataReceiverOwner[typeof(TData)] as IDataReceiver<TData>)?.Get(parameters);
        }
    }
}