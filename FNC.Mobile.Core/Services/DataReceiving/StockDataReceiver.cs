﻿using FNC.Mobile.Core.Services.DataReceiving.Dto;

namespace FNC.Mobile.Core.Services.DataReceiving
{
    public class StockDataReceiver : DataReceiver<MatchStockDto>
    {
        protected override string Url => "https://finance.google.com/finance/match?matchtype=matchall&q={0}";
    }
}