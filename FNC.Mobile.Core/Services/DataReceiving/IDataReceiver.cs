﻿using System.Threading.Tasks;

namespace FNC.Mobile.Core.Services.DataReceiving
{
    public interface IDataReceiver<TData>
    {
        Task<TData> Get(params object[] parameters);
    }
}