﻿using FNC.Mobile.Core.Model;

namespace FNC.Mobile.Core.Repository
{
    public class StockRepository : FinanceRepository<Stock>, IStockRepository
    {
    }
}