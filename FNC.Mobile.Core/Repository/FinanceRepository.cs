﻿using FNC.Mobile.Core.Model;
using Mobile.Core.DAL;

namespace FNC.Mobile.Core.Repository
{
    public class FinanceRepository<TModel> : BaseCommonRepository<TModel> where TModel : FinanceModel, new()
    {
        public FinanceRepository() => Init();
    }
}