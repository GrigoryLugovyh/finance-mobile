﻿using FNC.Mobile.Core.Model;
using Mobile.Core.DAL;

namespace FNC.Mobile.Core.Repository
{
    public interface IPortfolioRepository : IBaseRepository<Portfolio>
    {
    }
}