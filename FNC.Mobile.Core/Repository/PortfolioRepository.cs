﻿using FNC.Mobile.Core.Model;

namespace FNC.Mobile.Core.Repository
{
    public class PortfolioRepository : FinanceRepository<Portfolio>, IPortfolioRepository
    {
    }
}