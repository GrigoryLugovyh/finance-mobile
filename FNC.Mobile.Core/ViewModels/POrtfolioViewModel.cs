﻿using FNC.Mobile.Core.Model;
using Mobile.Core.BL.Core.ViewModels;

namespace FNC.Mobile.Core.ViewModels
{
    public class PortfolioViewModel : MViewModel
    {
        private Portfolio portfolio;

        public string PortfolioName => portfolio.Name;

        protected override void ApplyParameters()
        {
            base.ApplyParameters();

            portfolio = Parameters[0] as Portfolio;
        }

        public void AddStock()
        {
            ShowViewModel<AddStockViewModel>();
        }
    }
}