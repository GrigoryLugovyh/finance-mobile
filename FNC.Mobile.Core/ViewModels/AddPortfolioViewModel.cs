﻿using FNC.Mobile.Core.Model;
using FNC.Mobile.Core.Repository;
using Mobile.Core.BL.Core.ViewModels;

namespace FNC.Mobile.Core.ViewModels
{
    public class AddPortfolioViewModel : MViewModel
    {
        private readonly IPortfolioRepository portfolioRepository;
        private string portfolioName;

        public AddPortfolioViewModel(IPortfolioRepository portfolioRepository)
        {
            this.portfolioRepository = portfolioRepository;
        }

        public string PortfolioName
        {
            get => portfolioName;
            set
            {
                portfolioName = value;
                RaisePropertyChanged(() => PortfolioName);
            }
        }

        public void Add()
        {
            portfolioRepository.Save(new Portfolio
            {
                Name = PortfolioName
            });

            FinishViewModel();
        }

        public void Cancel()
        {
            FinishViewModel();
        }
    }
}