﻿using System.Collections.Generic;
using System.Linq;
using FNC.Mobile.Core.Services.DataReceiving;
using FNC.Mobile.Core.Services.DataReceiving.Dto;
using Mobile.Core.BL.Core.ViewModels;
using Mobile.Core.BL.IoCContainer;

namespace FNC.Mobile.Core.ViewModels
{
    public class AddStockViewModel : MViewModel
    {
        private readonly IDataReceiverRegistry dataReceiverRegistry = Container.Resolve<IDataReceiverRegistry>();

        private bool loading;
        private string searchString;
        private List<StockDto> stockCandidates;

        public string SearchString
        {
            get => searchString;
            set
            {
                searchString = value;
                RaisePropertyChanged(() => SearchString);

                if (searchString.Length <= 1 || loading) return;

                lock (searchString)
                {
                    loading = true;

                    RequestActionAsync(() =>
                    {
                        try
                        {
                            StockCandidates = dataReceiverRegistry.Receive<MatchStockDto>(SearchString).Result.Matches
                                .ToList();
                        }
                        finally
                        {
                            loading = false;
                        }
                    });
                }
            }
        }

        public List<StockDto> StockCandidates
        {
            get => stockCandidates;
            set
            {
                stockCandidates = value;
                RaisePropertyChanged(() => StockCandidates);
            }
        }
    }
}