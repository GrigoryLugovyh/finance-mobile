﻿using System.Collections.Generic;
using FNC.Mobile.Core.Model;
using FNC.Mobile.Core.Repository;
using Mobile.Core.BL.Core.Binding.Target;
using Mobile.Core.BL.Core.ViewModels;

namespace FNC.Mobile.Core.ViewModels
{
    public class PortfoliosViewModel : MViewModel
    {
        private readonly IPortfolioRepository portfolioRepository;

        public PortfoliosViewModel(IPortfolioRepository portfolioRepository)
        {
            this.portfolioRepository = portfolioRepository;
        }

        private List<Portfolio> portfolio = new List<Portfolio>();

        public List<Portfolio> Portfolios
        {
            get => portfolio;
            set
            {
                portfolio = value;
                RaisePropertyChanged(() => Portfolios);
            }
        }

        protected override void Resume()
        {
            base.Resume();

            Portfolios = portfolioRepository.All();
        }

        public void AddPortfolio()
        {
            ShowViewModel<AddPortfolioViewModel>();
        }

        public void SelectPortfolio(MTargetBindingItemArgs portfolioItem)
        {
            ShowViewModel<PortfolioViewModel>(portfolioItem.Item);
        }
    }
}