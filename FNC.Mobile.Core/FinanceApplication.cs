﻿using FNC.Mobile.Core.Repository;
using FNC.Mobile.Core.Services.DataReceiving;
using FNC.Mobile.Core.ViewModels;
using Mobile.Core.BL.Core;
using Mobile.Core.BL.IoCContainer;

namespace FNC.Mobile.Core
{
    public class FinanceApplication : MApplication
    {
        public override string ApplicationName => "Finance chart application";

        public override void Initialization()
        {
            RegisterRepositories();
            RegisterServices();

            RegisterViewModel<PortfoliosViewModel>();
        }

        private static void RegisterRepositories()
        {
            Container.AddRegistration<IPortfolioRepository, PortfolioRepository>();
            Container.AddRegistration<IStockRepository, StockRepository>();
        }

        private static void RegisterServices()
        {
            IDataReceiverRegistry dataReceiverRegistry = new DataReceiverRegistry();
            dataReceiverRegistry.AddReceiver(new StockDataReceiver());

            Container.AddRegistration(dataReceiverRegistry);
        }
    }
}