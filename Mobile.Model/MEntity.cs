﻿using System;
using Mobile.Model.Attributes;

namespace Mobile.Model
{
    public abstract class MEntity
    {
        [Indexed]
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        [Indexed]
        public string ServerId { get; set; }

        [Indexed(Name = MModelConstants.CommonIndexName, Order = -1)]
        public bool IsDeleted { get; set; }

        [Indexed(Name = MModelConstants.CommonUnloadedIndexName, Order = -2)]
        public bool Unloaded { get; set; }
        [Indexed]
        public DateTimeOffset Updated { get; set; }
    }
}