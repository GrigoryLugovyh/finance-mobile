﻿using System;

namespace Mobile.Model.Attributes
{
    public class RefAttribute : Attribute
    {
        public RefAttribute(Type type)
        {
            RefType = type;
        }

        public Type RefType { get; private set; }
    }
}