﻿using System;

namespace Mobile.Model.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class NotNullAttribute : Attribute
    {
    }
}