﻿using Android.Content;
using FNC.Mobile.Core;
using FNC.Mobile.Droid.Specific;
using Mobile.Core.BL.Core.Model;
using Mobile.Core.BL.Core.Platform.Specific;
using Mobile.Core.Droid.Platform;

namespace FNC.Mobile.Droid
{
    public class EntryPoint : DroidModel
    {
        public EntryPoint(Context appContext) : base(appContext)
        {
        }

        protected override void InitializeStorage()
        {
        }

        protected override IMApplication CreateApplication() => new FinanceApplication();

        protected override IPlatformSpecific CreatePlatformSpecific() => new FinancePlatformSpecific();
    }
}