﻿using System;
using System.IO;
using Mobile.Core.BL.Core.Platform.Specific;

namespace FNC.Mobile.Droid.Specific
{
    internal class FinancePlatformSpecific : IPlatformSpecific
    {
        public string ApplicationDataPath =>
            Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);

        public string SqlitePath => Path.Combine(ApplicationDataPath, "finance.db");

        public string Version { get; } = "1.0.0";

        public int VersionCode { get; } = 1;
    }
}