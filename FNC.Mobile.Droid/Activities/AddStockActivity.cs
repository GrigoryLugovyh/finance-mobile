﻿using Android.App;
using Android.OS;
using Mobile.Core.Droid.Views;

namespace FNC.Mobile.Droid.Activities
{
    [Activity(Label = nameof(AddStockActivity))]
    public class AddStockActivity : DroidActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.AddStock);
        }
    }
}