﻿using Android.OS;
using Android.Views;
using Mobile.Core.Droid.Views;

namespace FNC.Mobile.Droid.Activities
{
    public class DialogDroidActivity : DroidActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            RequestWindowFeature(WindowFeatures.NoTitle);
        }
    }
}