﻿using Android.App;
using Android.OS;
using Mobile.Core.Droid.Views;

namespace FNC.Mobile.Droid.Activities
{
    [Activity(Label = nameof(StockActivity))]
    public class StockActivity : DroidActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Stock);
        }
    }
}