﻿using Android.App;
using Android.OS;

namespace FNC.Mobile.Droid.Activities
{
    [Activity(Label = nameof(AddPortfolioActivity))]
    public class AddPortfolioActivity : DialogDroidActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout._AddPortfolio);
        }
    }
}