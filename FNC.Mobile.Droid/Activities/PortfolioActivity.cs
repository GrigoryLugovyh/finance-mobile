﻿using Android.App;
using Android.OS;
using Mobile.Core.Droid.Views;

namespace FNC.Mobile.Droid.Activities
{
    [Activity(Label = nameof(PortfolioActivity))]
    public class PortfolioActivity : DroidActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Portfolio);
        }
    }
}