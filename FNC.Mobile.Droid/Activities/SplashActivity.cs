﻿using Android.App;
using Mobile.Core.Droid.Views;

namespace FNC.Mobile.Droid.Activities
{
    [Activity(Label = "FinChart", MainLauncher = true)]
    public class SplashActivity : DroidSplashScreenActivity
    {
        public SplashActivity() : base(Resource.Layout.Splash)
        {
        }
    }
}